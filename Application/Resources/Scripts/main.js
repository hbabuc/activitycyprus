$(document).ready(function(){
		$("#datepicker").datetimepicker({
			format:'d-m-Y H:i',
			minDate: 0,
			minTime: 0,
			lang: 'tr'
		});
		
		$("#datepicker2").datetimepicker({
				format:'d-m-Y H:i',
				onShow: function(ct){
					this.setOptions({
						formatDate:'d-m-Y',
						formatTime:'H:i',
						minDate: $("#datepicker").val().split(' ')[0],
						minTime: $("#datepicker").val().split(' ')[1].trim()
						});
				},
				lang: 'tr'
			});
		
	});
	

	
	
  $(document).ready(function () {
    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
  
  // Can also be used with $(document).ready()
		$(window).load(function() {
		  $('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails"
		  });
		});