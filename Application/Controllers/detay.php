<?php
	
	class detay extends Controller{

		public function index($seolink){

				if(Uri::segment(1) && Uri::segment(2))
				{
					$a = $this->detay_model->verileri_cek(Uri::segment(2),Uri::segment(1),((!empty(trim(User::id()))) ? User::id() : 0));
					
					if($a->totalRows() < 1)
						redirect(baseurl("404"));
					
					$row = $a->row();
					
					$c = array("aktiviteid" => $row->id,"userid"=>((!empty(trim(User::id()))) ? User::id() : 0));
					
					$data = array(
						"veriler" => $row,
						"resimler"=>$this->detay_model->resimcek($row->id)->result(),
						"yorumlar"=> $this->detay_model->yorumcek($row->id)->result(),
						"oylama" => $this->detay_model->oncedenPuanVermismi($c)
					);
				
					$veriler = array(
					
						"sayfa"=>Import::page("detayview",$data,true)
						
					);
					
					Import::Masterpage($veriler);
				
				}
				else
				{
					redirect(baseurl("404"));
				}
		
				
				
				
				//echo Uri::segment(1);
			
		}
		
		function like(){
			if($_POST){
				if(User::check())
				{
					
					$postlar = Method::post();
					if($this->db->where('userid=',User::id(),"and")->where('yorumid=',$postlar['id'])->get('yorum_begeni')->totalRows() < 1){
							$this->detay_model->like_ekle($postlar['id'],User::id());
							$sayi = $this->detay_model->likelar($postlar['id'])->totalRows();
							$arr['durum'] = 1;
							$arr['sayi'] = $sayi;
					}
					else
					{
						$arr['oncedenOylamismi'] = 1;
					}
				}
				else
				{
					$arr['oncedenOylamismi'] = 0;
					$arr['durum'] = 0;
				}
			}
			else
			{
				redirect(baseurl("home"));
			}
				
				
			echo json_encode($arr);
		}
		
		
		function dislike(){
			if($_POST){
				
				if(User::check())
				{	
					$postlar = Method::post();
					
					if($this->db->where('userid=',User::id(),"and")->where('yorumid=',$postlar['id'])->get('yorum_begeni')->totalRows() < 1){
						$this->detay_model->dislike_ekle($postlar['id'],User::id());
						$sayi = $this->detay_model->dislikelar($postlar['id'])->totalRows();
						$arr['durum'] = 1;
						$arr['sayi'] = $sayi;
					}
					else
					{
						$arr['oncedenOylamismi'] = 1;
					}
				}
				else
				{
					$arr['oncedenOylamismi'] = 0;
					$arr['durum'] = 0;
				}
			}
			else
			{
				redirect(baseurl("home"));
			}	
		
			echo json_encode($arr);
		}
		
		
		function yorumgonder(){
			if($_POST){
				if(User::check()){ //Kullanici kontrol�
				
						Validation::rules('yorum',array("html"));
						
						$postlar = Method::post();

						$postlar['onay'] = 1;
						$postlar['userid'] = User::id();
						
						//KULLANICI �NCEDEN PUANLAMA YAPMISSA
						//BUNDAN SONRAK� PUANLAMALARI GECERS�Z YAPMA
						if($this->detay_model->oncedenPuanVermismi($postlar) > 0)
							$postlar['derece'] = 0;
						
						
						if($this->detay_model->yorumKaydet($postlar)){
							$arr["durum"] = 1;
						}
						else
						{
							$arr["durum"] = 2;
						}
						
				}
				else
				{
					$arr["durum"] = 0;
				}
			}
			else
			{
				redirect(baseurl("home"));
			}
			
			echo json_encode($arr);
		}
		
		function katil(){
			if($_POST){
				if(User::check()){
					
					$postlar = Method::post();
					$postlar['userid'] = User::id();
					$postlar['tip'] = 2;
					$postlar['durum'] = 1;
					
					
					if($this->detay_model->cinsiyetControl($postlar)){	
						if($this->detay_model->limitControl($postlar) > 0){
							if($this->detay_model->distanceControl($postlar) > 0){
						
								unset($postlar['lat']);
								unset($postlar['lng']);
								if($this->detay_model->katil($postlar)){
									$arr['durum'] = 1;
								} 
								else
								{
									$arr['durum'] = 0;
								}	
						
							}
							else
							{
								$arr['durum'] = 2;
								$distance = $this->detay_model->distanceCek($postlar);
								$arr['distance'] = $distance->distance;
							}
					
						}
						else
						{
							$arr['durum'] = 3;
						}
					}
					else
					{
						$arr['durum'] = 5;
					}
					
				}
				else
				{
					$arr['durum'] = 4;
				}
			}
			else
			{
				redirect(baseurl("home"));
			}
			
			echo json_encode($arr);
		}
		
		

		
		function rezervasyon(){
			if($_POST){
				if(User::check())
				{
					$postlar = Method::post();
					$postlar['userid'] = User::id();
					$postlar['durum'] = 1;
					$postlar['tip'] = 1;
					
					if($this->detay_model->cinsiyetControl($postlar)){
						if($this->detay_model->limitControl($postlar) > 0){
							if($this->detay_model->rezervasyon($postlar)){
								Bildirim::davetiyeSil($postlar["userid"],$postlar["aktiviteid"]);
								$arr['durum'] = 1;
							} 
							else
							{
								$arr['durum'] = 0;
							}
						}
						else
						{
							$arr['durum'] = 3;
						}
					}
					else
					{
						$arr['durum'] = 5;
					}
				}
				else
				{
					$arr['durum'] = 4;
				}
			}
			else
			{
				redirect(baseurl("home"));
			}
				
			echo json_encode($arr);
		}
		
		function rezervasyonIptal(){
			if($_POST){
				if(User::check())
				{
					$postlar = Method::post();
					$postlar['userid'] = User::id();
					if($this->detay_model->rezervasyonIptal($postlar['userid'],$postlar['aktiviteid'])){
						$arr['durum'] = 1; 
					} 
					else
					{
						$arr['durum'] = 0;
					}
				}		
				else
				{
					$arr['durum'] = 2;
				}
			}
			else
			{
				redirect(baseurl("home"));
			}
			echo json_encode($arr);
			
		}
		
		
		
		function sikayetgonder(){
			if($_POST){
				
				if(User::check()){
					
					Validation::rules('aciklama',array("html"));
					$postlar = Method::post();
					$postlar['userid'] = User::id();
					
					if($this->detay_model->sikayetekle($postlar)){
						$arr['durum'] = 1;
					}
					else
					{
						$arr['durum'] = 2;
					}
				}
				else
				{
					$arr['durum'] = 0;
				}
				
				echo json_encode($arr);
			}
			else
			{
				redirect(baseurl("home"));
			}
		}
		
	
	}


?>