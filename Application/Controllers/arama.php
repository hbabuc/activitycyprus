<?php 


	class arama extends Controller
	{
		
		function index()
		{
			$aranacak = $_GET['arama'];

			$control = $this->aktivite_model->aktivite_arama($aranacak)->totalRows();

			if ($control == 0){
				$tur = 0;
				$kayitlar = $this->kullanici_model->kullanici_ara($aranacak)->result();
			}else{
				$tur = 1;
				$kayitlar = $this->aktivite_model->aktivite_arama($aranacak)->result();
			}
			
		
			$data = array(
				"arama_sonuc" => $kayitlar,
				"tur" => $tur
			);
			
			$veriler = array(
			
				"sayfa"=>Import::page("arama_sonuc",$data,true)
			
			);
			
			Import::Masterpage($veriler);
			

		}
	}
?>