<?php 
	
	class test extends Controller{
		
		public function index(){
			
			$postlar = Method::post();
			
			if($postlar){
				
				Validation::rules("kadi",array("html"));
				Validation::rules("sifre",array("html"));
				
				$kadi = Method::post("kadi");
				$sifre = Method::post("sifre");
				
				if(User::login($kadi,$sifre)){
					
					
					$dizi["durum"] = 1 ;
					$dizi["msg"] = "Giriş Başarılı	" ;
					
					User::row();
					
					
				}else{
					
					$dizi["durum"] = 0;
					$dizi["msg"] = "Kullanıcı Adı Veya Şifre Bulunamadı. ";
					
				}
				
			}else{
				
				$dizi["durum"] = 0 ;
				$dizi["msg"] = "Post Yapılamadı.";
				
			}
			
			echo json_encode($dizi);
			
		}
		
	}

?>