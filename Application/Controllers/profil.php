<?php 

	

	class profil extends Controller{

		

		public function index($par=null){

			if($par == null){
				
				$par = User::kadi();
				
			}

			$user = $this->profil_model->getKadi($par);
			
			if($user->totalRows()<1){

				redirect("404");

			}else{

				$row = $user->row();
					
				$data = array(

					"row"=>$row,
					"aglar"=>$this->profil_model->userSosyal($row->id)->result(),
					"takipedilen"=>$this->profil_model->takip($row->id)->result(),
					"takipeden"=>$this->profil_model->takipci($row->id)->result(),
					"arkadaslar"=>$this->profil_model->userArkadas($row->id)->result()

				);
				
				$veriler = array(

					"sayfa"=>Import::page("sayfalar/profil/profil",$data,true)

				);

				Import::masterpage($veriler);

			}

		}

		public function edit($par = "kisisel"){

			if(!User::check()){

				redirect("login");

			}

			$userid = User::id();
			
			$user = $this->profil_model->getid(User::id())->row();

			if($par=="sosyal"){

				$sayfa = "sayfalar/profil/sosyal";

				$title = "Sosyal Ağlar";

			}else if($par=="iletisim"){

				$sayfa = "sayfalar/profil/iletisim";

				$title = "İletişim Bilgilerin";

			}else if($par=="resimdegis"){

				$sayfa = "sayfalar/profil/resimdegis";

				$title = "Resmini Değiştir";

			}else if($par=="sifredegis"){

				$sayfa = "sayfalar/profil/sifredegis";

				$title = "Şifreni Değiştir";

			}else{

				$sayfa = "sayfalar/profil/edit";

				$title = "Kişisel Bilgiler";

			}

			$icsayfa = array(

				"user"=>$user,
				"sosyalaglar"=>$this->profil_model->sosyalaglar()->result(),
				"ksosyal"=>$this->profil_model->ksosyal(User::id())->result(),

			);

			$data = array(

				"user"=>$user,
				"sayfa"=>Import::page($sayfa,$icsayfa,true),
				"title" => $title
				
			);

			$veriler = array(
			
				"sayfa"=>Import::page("sayfalar/profil/kalip",$data,true)
				
			);
			
			Import::masterpage($veriler);
			
		}



		public function doEdit(){
			
			if(Method::post()){
				
				Validation::rules("adi",array("reqired","trim","maxchar"=>20,"injection","html"),"Adınız ");
				Validation::rules("soyadi",array("reqired","trim","maxchar"=>15,"injection","html"),"Soyadınız");
				Validation::rules("hakkinda",array("trim","injection","html"),"Hakkınızda Alanı :");

				$error = Validation::error("string");

				if(!$postlar["cinsiyet"]){

					$postlar["cinsiyet"] = 0; 

				}

				if($error){
					
					Warning::set($error,"warning");

				}else{
					
					$postlar = Method::post();
					
					if($this->profil_model->updateUser($postlar,User::id())){
						
						Warning::set("Veriler Başarıyla Güncellendi. " , "success");

					}else{

						Warning::Set("Veritabanına bağlanırken bir sorunla karşılaştık.");

					}

				}

			}else{

				Warning::set("Güvenlik Duvarı ! ","warning","register");

			}

		}
		
		public function insertSosyal(){
		
			$postlar = Method::post();

			$aglar = Method::post("aglar");

			$link = Method::post("link");

			if($postlar){

				Validation::rules("link",array("reqired","trim","maxchar"=>70,"html"),"Link Alanı ");

				$error = Validation::error("string");

				if($error){

					Warning::set($error,"warning");

				}else{

					$postlar = Method::post();

					if($this->profil_model->insertSosyal($link,$aglar)){

						Warning::set("Sosyal Ağınız Başarıyla Eklendi . " , "success");

					}else{

						Warning::Set("Veritabanına bağlanırken bir sorunla karşılaştık.");

					}

				}

			}else{

				Warning::set("Güvenlik Duvarı ! ","warning","register");

			}

		}

		

		public function deleteSosyal($id =0){
			
			if( $id == 0 ){
				
				Warning::set("Güvenlik Duvarı ! ","info","profil/edit/sosyal");
				
			}else{

				if($this->profil_model->checkSosyal($id,User::id())->totalRows()<1){

					Warning::set("Güvenlik Duvarı ! ","info","profil/edit/sosyal");

				}else{

					if($this->profil_model->deleteSosyal($id)){

						Warning::set("Sosyal Ağınız Başarıyla Silindi.","success");

					}else{

						Warning::Set("Veritabanına bağlanırken bir sorunla karşılaştık.");

					}

				}

			}

		}

		public function doResimDegis(){

			if($_FILES){

				$dosyaid = Dosya::profilImg();

				if(Message::get()){

					if($this->profil_model->insertImg($dosyaid,User::id())){

						Warning::set("Resminiz Başarıyla Güncellendi .");

					}else{

						Warning::Set("Veritabanına bağlanırken bir sorunla karşılaştık.");

					}

				}else{

					Warning::set("Lütfen uygun formatta bir resim girişi yapınız <span>gif|jpeg|png</span>");

				}

			}else{

				Warning::set("Güvenlik Duvarı ! ");

			}

		}
		
		public function doEditIletisim(){
			
			$postlar = Method::post();

			if($postlar){

				Validation::rules("email",array("reqired","trim","email","injection","html"),"Email Adresiniz ");
				Validation::rules("tel",array("reqired","trim","tel","injection","html"),"Telefon Numaranız ");

				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");

				}else{

					$postlar = Method::post();

					if($this->profil_model->updateUser($postlar,User::id())){

						Warning::set("Veriler Başarıyla Güncellendi. " , "success");

					}else{

						Warning::Set("Veritabanına bağlanırken bir sorunla karşılaştık.");

					}

				}

			}else{
				
				Warning::set("Güvenlik Duvarı ! ","warning","register");

			}

		}

		public function doEditSifre(){
			
			$postlar = Method::post();
			
			$sifre = sha1(md5($postlar["sifre"]));
			
			##  2 Yeni Şifre Eşleşiyo mu Kontrol edildi.
			if( $postlar["yeni_sifre1"] == $postlar["yeni_sifre2"]){

				## Kullanıcının veritabanındaki mevcut şifresi kontrol edildi
				if( $this->profil_model->sifre_control(User::id(),$sifre)->totalRows() > 0 ){

					$yenisifre = sha1(md5($postlar["yeni_sifre1"]));

					$userid = User::id();

					if ( $this->profil_model->sifre_degistir($yenisifre,$userid)){

						Warning::set("Şifreniz Değiştirildi","success");

					}else{

						Warning::set("Fonksiyon Hatası","warning");

					}

				}else{

					Warning::set("Mevcut Şifrenizi yanlış girdiniz","warning");

				}

			}else{

				Warning::set("Girdiğiniz 2 Şifre Aynı Değil","warning");

			}

		}

		public function arkadasEkle(){

			$postlar = Method::post();

			$userid = User::id();

			if($postlar){

				$kontrol =  $this->profil_model->checkKadi($postlar["kadi"]);

				if($kontrol){

					if($kontrol->totalRows()<1){

						$array["durum"]=0;

						$array["msg"] = "Kontrol Hatası";

					}else{

						$kid = $kontrol->row()->id;

						$checkArkadas =  $this->profil_model->checkArkadas($kid,$userid);

						if($checkArkadas){

							if($checkArkadas->totalRows()<1){

								if($this->profil_model->arkadasEkle($kid,$userid)){

									$dataid = DB::insertId();

									$array["durum"]=1;

									$userkadi = User::kadi();

									Bildirim::set($kid,2,'<div class="col-md-12 "><p><a href="'.baseurl($userkadi).'">'.$userkadi.'</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>

									<div class="col-md-12">

										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>

									</div>

									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="'.$dataid.'" data-uid="'.$kid.'" data-gid="'.$userid.'"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="'.$dataid.'" data-uid="'.$kid.'" data-gid="'.$userid.'"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>');

								}else{

									$array["durum"]=0;

									$array["msg"] = "Eklenme Hatası";

								}

							}else{

								$array["durum"]=2;

								$array["msg"] = "Zaten Arkadaşlar";

							}

						}else{

							$array["durum"]=0;

							$array["msg"] = "Arkadaş Kontrol Hatası";

						}

					}

				}else{

					$array["durum"]=0;

					$array["msg"] = "Sorgu Hatası";

				}

			}else{

				Warning::set("Güvenlik Duvarı ! ","","home");

			}

			echo json_encode($array);

		}

		public function arkadasSil(){

			$userid = User::id();

			$postlar = Method::post();

			if($postlar){
				
				$kontrol =  $this->profil_model->checkKadi($postlar["kadi"]);

				if($kontrol){

					if($kontrol->totalRows()<1){

						$array["durum"]=0;

						$array["msg"] = "Kontrol Hatası";

					}else{

						$kid = $kontrol->row()->id;

						$checkArkadas =  $this->profil_model->checkArkadas($kid,$userid);

						if($checkArkadas){

							if($checkArkadas->totalRows()<1){

								$array["durum"] = 0;

								$array["msg"] = "Zaten Arkadaş Değilsiniz";

							}else{

								if($this->profil_model->arkadasSil($kid,$userid)){

									$array["durum"] = 1;

									$array["msg"] = "Arkadaş Silindi";

								}else{

									$array["durum"] = 0;

									$array["msg"] = "SilinirkenBir hata İle Karşılaştık";

								}

							}

						}else{

							$array["durum"]=0;

							$array["msg"] = "Arkadaş Kontrol Hatası";

						}

					}

				}else{

					$array["durum"]=0;

					$array["msg"] = "Sorgu Hatası";

				}

			}else{

				Warning::set("Güvenlik Duvarı ! ","","home");

			}

			echo json_encode($array);

		}

		public function istekSil(){

			$userid = User::id();

			$gonderenkadi = User::kadi();

			$postlar = Method::post();

			if($postlar){

				$kontrol =  $this->profil_model->checkKadi($postlar["kadi"]);

				if($kontrol){

					if($kontrol->totalRows()<1){

						$array["durum"]=0;

						$array["msg"] = "Kontrol Hatası";

					}else{

						$row = $kontrol->row();

						$kid = $row ->id;

						$userkadi = $row ->kadi;

						$checkArkadas =  $this->profil_model->checkArkadas($kid,$userid);

						if($checkArkadas){

							if($checkArkadas->totalRows()<1){

								$array["durum"] = 0;

								$array["msg"] = "Zaten Arkadaş Değilsiniz";

							}else{

								if($this->profil_model->arkadasSil($kid,$userid)){

									$metin = str_replace("'","&#39;",'<a href="'.baseurl($gonderenkadi).'">'.$gonderenkadi.'</a> kullanıcısı sizi arkadaş olarak eklemek istiyor.');

									if($this->profil_model->bildirimSil($metin,$kid)){

										$array["durum"] = 1;

										$array["msg"] = "Arkadaş Silindi - ";

									}else{
										
										$array["durum"] = 1;

										$array["msg"] = "Bildirim Silinemedi";

									}

								}else{
									
									$array["durum"] = 0;

									$array["msg"] = "SilinirkenBir hata İle Karşılaştık";

								}
								
							}

						}else{

							$array["durum"]=0;

							$array["msg"] = "Arkadaş Kontrol Hatası";

						}

					}

				}else{

					$array["durum"]=0;

					$array["msg"] = "Sorgu Hatası";

				}
				
			}else{

				Warning::set("Güvenlik Duvarı ! ","","home");

			}
			
			echo json_encode($array);

		}

		

		function arkadasKabul(){

			$postlar = Method::post();

			if($this->profil_model->arkadasKabul($postlar))

				$arr['durum'] = 1;

			else

				$arr['durum'] = 0;

			echo json_encode($arr);

		}
		
		function arkadasReddet(){

			$postlar = Method::post();

			if($this->profil_model->arkadasReddet($postlar))

				$arr['durum'] = 1;

			else

				$arr['durum'] = 0;

			echo json_encode($arr);

		}
		
		public function takipciEkle(){

			$postlar = Method::post();

			$takipciKadi = $this->db->where("id=",$postlar['takipciid'])->get("kullanici")->row()->kadi;

			unset($postlar['kadi']);

			if($this->profil_model->takipciEkle($postlar)){

				$array["durum"]=1;

				$kid = $postlar['userid'];

				Bildirim::set($kid,2,'<div class="col-md-12"><p><a href="'.baseurl($takipciKadi).'">'.$takipciKadi.'</a> kullanıcısı sizi takip etmeye başladı.</p></div>

				<div class="col-md-12">

					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>

				</div>');

			}else{

				$array["durum"]=0;


			}

			echo json_encode($array);

		}

		public function takipBirak(){

			$postlar = Method::post();

			if($this->profil_model->takipBirak($postlar)){

				$array["durum"]=1;

			}else{

				$array["durum"]=0;

			}

			echo json_encode($array);

		}

		public function takipci_cikar($takipciID){
			
			$userid = User::id();
			$this->profil_model->takipci_Birak($userid,$takipciID);
			Warning::set("Takipçilerinizden Çıkartıldı","success");
			
		}

		public function tum_arkadaslar(){

			if ( User::check()){
				
				$userid = User::id();
				$arkadaslar = $this->profil_model->userArkadas($userid)->result();
				$data = array(
					"arkadaslar"=>$arkadaslar,
				);

				$veriler = array(

					"sayfa"=>Import::page("sayfalar/profil/tumarkadaslar",$data,true)
				);

				Import::masterpage($veriler);
				
			}else{

				redirect("login");
				
			}

		}

		public function tum_takipciler($kadi = null){

			if ( User::check()){
				if (empty($kadi)){

					$userid = User::id();
					$takipciler = $this->kullanici_model->takipci_getir($userid)->result();
					$data = array(
						"takipciler"=>$takipciler,
						"title" => "Takipçileri",
						"tip" => 0
					);

					$veriler = array(

						"sayfa"=>Import::page("sayfalar/profil/tumtakipciler",$data,true)
					);

					Import::masterpage($veriler);


				}else{
		
					$takipciler = $this->kullanici_model->kadi_takipci_getir($kadi)->result();
					$data = array(
						"takipciler"=>$takipciler,
						"title" => "Takipçileri",
						"tip" => 1
					);

					$veriler = array(

						"sayfa"=>Import::page("sayfalar/profil/tumtakipciler",$data,true)
					);

					

					Import::masterpage($veriler);
				}
			}else{

				redirect("login");
			}

		}


		## Aktiviteye Katılarnları listeler
		public function katilanlari_yonet($aktiviteID){

			$userid = User::id();

			if ( $this->aktivite_model->guncelleme_control($aktiviteID,$userid)->totalRows() > 0){
				$katilanlar = $this->aktivite_model->aktiviteye_katilanlar($aktiviteID)->result();
				
				$data = array(
					"katilanlar"=>$katilanlar
				);

				$veriler = array(

					"sayfa"=>Import::page("aktivite_katilanlar",$data,true)
				);

				Import::masterpage($veriler);

			}else{
				Warning::set("Yetkiniz Olmayan Alana Giremezsiniz","warning","aktivite/aktivitelerim");
			}
			

		}

		public function tumbildirimler(){
			if ( User::check()){
				$userid = User::id();
				$bildirimler = Bildirim::getAll($userid)->result();
				$data = array(
					"bildirimler"=>$bildirimler
				);

				$veriler = array(

					"sayfa"=>Import::page("sayfalar/profil/tum_bildirimler",$data,true)
				);

				Import::masterpage($veriler);
			}else{
				redirect('login');
			}

		}

		public function takip_ettikleri($kadi = null){

			if ( User::check()){
				if (empty($kadi)){

					$userid = User::id();
					$takipciler = $this->kullanici_model->id_takip_ettikleri($userid)->result();
					$data = array(
						"takipciler"=>$takipciler,
						"title" => "Takip Ettikleri"
					);

					$veriler = array(

						"sayfa"=>Import::page("sayfalar/profil/tumtakipciler",$data,true)
					);

					Import::masterpage($veriler);


				}else{
		
					$takipciler = $this->kullanici_model->kadi_takip_ettikleri($kadi)->result();
					$data = array(
						"takipciler"=>$takipciler,
						"title" => "Takip Ettikleri"
					);

					$veriler = array(

						"sayfa"=>Import::page("sayfalar/profil/tumtakipciler",$data,true)
					);

					

					Import::masterpage($veriler);
				}
			}else{

				redirect("login");
			}

		}

		public function bildirimsil(){

			$userid = User::id();
			if( User::check()){
				Bildirim::deleteAll($userid);
				Warning::set("Bildirimleriniz Temizlendi","Success","profil");
			}else{
				redirect('/login');
			}
		}



	}



?>