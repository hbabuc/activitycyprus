<?php 
	
	class service extends controller{
	
		## Kullanıcı Girişi Yapılan Bölüm
		public function index(){
			
			$postlar = Method::request();
			
			if($postlar){
				
				Validation::rules("kadi",array("html"));
				Validation::rules("sifre",array("html"));
				
				$kadi = Method::request("kadi");
				$sifre = Method::request("sifre");
				
				if(User::login($kadi,$sifre)){
					
					$dizi = (array)User::row();
					
					$dizi["durum"] = 1 ;
					$dizi["msg"] = "Giriş Başarılı	" ;
					$dizi["sifre"] = $sifre;
					$dizi["resim"] = baseurl(UPLOADS_DIR.$dizi["resim"]);
					
				}else{
					
					$dizi["durum"] = 0;
					$dizi["msg"] = "Kullanıcı Adı Veya Şifre Bulunamadı. ";
					
				}
				
			}else{
				
				$dizi["durum"] = 0 ;
				$dizi["msg"] = "Post Yapılamadı.";
				
			}
			
			echo json_encode($dizi);
			
		}  
		## Session Açıp Yönlendirme Yapılan Bölüm
		function sessionOlustur(){
			
			$postlar = Method::get();
			
			if(User::login($postlar['kadi'],$postlar['sifre'])){
				
				Session::insert("postall",json_encode($postlar));
				redirect($postlar['url']);
				
			}else{
				
				redirect(baseurl("service/error"));
				
			}
			
		}
		## Aktivite Resmi Eklenme Bölümü
		function upload($id=0){
			
			$file = Method::post("file");
			
			$dosyaid = Dosya::base64($file);
			
			if($dosyaid){
				
				if($this->service_model->uploadImg($dosyaid,$id)){
					
					if($this->service_model->newAktiviteImg($dosyaid,$id)){
						
						redirect(baseurl("service/ok"));
						
					}else{
						
						echo "Problem is Big";
						
					}
					
				}else{
					
					"Problem is Big";
					
				}
				
			}else{
				
				echo "Problem is Big ! ";
				
			}
		
		}
		## Aktivite Ekleme Bölümü
		function aktiviteEkle(){
			
			if(User::check()){
				
				$postlar = Method::post();	
				
				Validation::rules('baslik',array('required','trim','minchar' => '4','maxchar'=>'100',"html"),'Başlık');
				Validation::rules('aciklama',array('required','trim','minchar' => '4',"html"),'Açıklama');
				Validation::rules('ucret',array('trim',"html"),'Ücret');
				Validation::rules('bastarih',array('required',"html"),'Başlangıç Tarihi');
				Validation::rules('bittarih',array('required',"html"),'Bitiş Tarihi');
				Validation::rules('adres',array('trim',"html"),'Adres');
				
				$error = Validation::error('string');
				
				$postlar = Method::post();
				
				Session::insert("PostBack",json_encode($postlar));
				
				if($error){
					
					Warning::set($error,"warning","service/ekle");
					
				}else{
				
					
					$postlar['bastarih'] = date("Y-m-d",strtotime($postlar['bastarih']));
					$postlar['bittarih'] = date("Y-m-d",strtotime($postlar['bittarih']));
					$postlar["bastarih"] .=" ". $postlar["bastarihsaati"].":00";
					$postlar["bittarih"] .=" ". $postlar["bittarihsaati"].":00";
					$postlar['seo_link'] = Convert::urlWord($postlar['baslik']);
					$postlar['seo_link'] = Convert::urlWord($postlar['baslik']);	
					$postlar['userid'] = User::id();
					
					if(!empty($postlar['ucret'])){
							if($postlar['ucret'] < 1)
								Warning::set("Ücret 0 veya 0'dan küçük olamaz.");
							else if(!is_numeric($postlar['ucret']))
								Warning::set("Sadece rakam giriniz.");
					}
					else
						$postlar['ucret'] = 0;
					
					unset($postlar['ucretdurumu']);
					unset($postlar['bittarihsaati']);
					unset($postlar['bastarihsaati']);
					
					if($this->aktivite_model->forminsert($postlar)){
						
						$insertid = $this->db->insertid();
						$gonder = array("userid" => User::id(), "aktiviteid" => $insertid, "durum" => 1,"tip" => 2);
						$this->service_model->olusturaniEkle($gonder);

						Session::insert("aktiviteid",$insertid);
						Session::delete("PostBack");
						
						redirect(baseurl("service/resimekle/$insertid"));
						
					}else{
						
						 Warning::set("Aktivite oluştururken hata oluştu");
						 
					}
					
				}
				
			}else{
				
				redirect(baseurl('service/error'));
				
			}
			
		}
		## Aktiviteye Katılma Rezervasyon İşlemi
		function aktiviteKatil(){
			
			$data = array(
			
				"aktiviteler" => $this->service_model->tumaktiviteler(0)->result()
				
			);
			
			$veriler = array(
			
				"sayfa"=>Import::page("mobil/tumliste",$data)
				
			);

		}		
		## Kullanıcı Bildirimleri Alma 
		function bildirimAl(){
			
			$postall = Method::get();
						
			if($postall){
					
				$model = $this->service_model->bildirimAl($postall["kadi"],$postall["sifre"]);
						
				if($model){
					
					if($model->totalRows()>0){
							
						$row = $model->row();
						
						$this->service_model->bildirimUpdate($row->id);
						
						$dizi["tip"] = $row->tipi;
						
						$dizi["aciklama"] = strip_tags($row->aciklama);
						
						$dizi["durum"] = 1;
						
					}else{
						
						$dizi["durum"] = 0;
						
					}
					
				}else{
					
					$dizi["durum"] = 0;
					
				}
				
			}else{
				
				$dizi["durum"] = 0;
				
			}
			
			echo json_encode($dizi);
			
			
		}
		## Aktivite Bulucu 
		function aktiviteBul(){
			
			$postlar = json_decode(Session::select("postall"),true);
			
			$data = array(
			
				"aktiviteler" => $this->service_model->yankindakiler($postlar)->result()
				
			);
			
			$veriler = array(
			
				"sayfa"=>Import::page("mobil/aktiviteBulucu",$data)
				
			);
			
		}
		## Bildirim Listesi
		function bildirimler(){
			
			Import::page("mobil/bildirimler","");
			
		}
		## Aktivite Bulucu 
		function yakinBul($page=0){
			
			$postlar = json_decode(Session::select("postall"),true);
			
			$postlar["page"] = $page;
			
			$data = array(
			
				"aktiviteler" => $this->service_model->yankindakilerPage($postlar)->result()
				
			);
			
			Import::page("mobil/yakinliste",$data);
			
		}
		## Android Uygulaamnın İçerik Idsini Aldığı Yer
		function resimekle(){
			
			if(User::check()){
				
				$aktiviteID = Session::select("activiteid");
				
				$data = array("aktiviteID" => $aktiviteID);
								
			}else{
				
				redirect(baseurl('login'));
				
			}
			
		}
		## Tüm Aktivitelerin Listelendiği Yerler
		function tumListe(){
						
			$postall = json_decode(Session::select("postall"));
			$data = array(
			
				"aktiviteler" => $this->service_model->tumaktiviteler(0,$postall->lat,$postall->lng)->result()
				
			);
			
			$veriler = array(
			
				"sayfa"=>Import::page("mobil/tumliste",$data)
				
			);
			
		}
		## Aktivite Detay Rasgele Getir
		function detayRasgele(){
			
			if(User::check()){
				
				$user = User::row();
				
			}
			
			$model = $this->service_model->getDetayRasgele($user->id);
			
			$totalRows = $model->totalRows();

			//echo $model->stringQuery();
			
			$row = $model->row();
			
			if($totalRows>0){
			
				$id = $row->id;
			
			}else{
				
				$id= 0;
				
			}
			$data = array(
				"row" => $row,
				"user"=>$user,
				"totalRows"=>$totalRows,
				"yorumlar"=>$this->detay_model->yorumcek($id)->result(),
				"oylama"=>$this->detay_model->oncedenPuanVermismi(array("aktiviteid"=>$id,"userid"=>$user->id))
			);
			
			Import::page("mobil/detay",$data);
			 
		}
		## Aktivite Detay Page
		function detay($id){
			
			if(User::check()){
				
				$user = User::row();
				
			}
			
			$modal =  $this->service_model->getDetay($id,$user->id);
			
			$data = array(
				"row" =>$modal->row(),
				"totalRows" =>$modal->totalRows(),
				"user"=>$user,
				"aktiviteresim"=>$this->detay_model->resimcek($id)->result(),
				"yorumlar"=>$this->detay_model->yorumcek($id)->result(),
				"oylama"=> $this->detay_model->oncedenPuanVermismi(array("aktiviteid"=>$id,"userid"=>$user->id))
			);
			
			Import::page("mobil/detay",$data);
			
		}
		## Ajax Extra Sayfa İsteği
		function eksayfa($page){
			
			$data = array(
			
				"aktiviteler" => $this->service_model->tumaktiviteler($page)->result()
				
			);
			
			Import::page("mobil/ekliste",$data);
						
		}//service_model->user_activity($userid)->

		public function eksayfa_user($page){
			
			$userid = User::id();
			$data = array(
			
				"aktiviteler" => $this->service_model->user_activity($page,$userid)->result()
				
			);
			
			Import::page("mobil/ekliste_user",$data);
						
		} 

		public function eksayfa_davetiye($page){
			
			$userid = User::id();
			$data = array(
			
				"aktiviteler" => $this->service_model->user_davetiye($page,$userid)->result()
				
			);
			
			Import::page("mobil/ekliste",$data);
						
		} 

		## Ajax Extra Sayfa Yakındakiler İsteği
		function eksayfaYakin($page){
			
			$postlar = json_decode(Session::select("postall"),true);
			
			$postlar["page"] = $page;
			
			$data = array(
			
				"aktiviteler" => $this->service_model->yankindakilerPage($postlar)->result()
				
			);
			
			Import::page("mobil/eksayfaYakin",$data);
						
		}
		## Ekleme Formunun Açıldığı Yer
		function ekle(){
			
			$data = array(
				"yaslar" => $this->aktivite_model->yas_getir()->result(),
				"kategoriler" => $this->aktivite_model->kategori_getir()->result()
			);
			
			Import::page("mobil/aktiviteekleview",$data);
		
		}
		## Eklenilen Kritere Göre Davet İşlemleri
		public function control(){
			
			$aktiviteID = Session::select("aktiviteid");
			
			$tur = $this->aktivite_model->single($aktiviteID);

			if ( $tur->tur == 1){
				
				redirect(baseurl('aktivite/arkadaslar'));
				
			}else if( $tur->tur == 2){
				
				redirect(baseurl('aktivite/takipciler'));
				
			}else if( $tur->tur == 0){
				
				Session::delete("aktiviteid");
				
				if(empty($tur)){
					
					redirect(baseurl("aktivite/aktivitelerim"));
					
				}else{
					
					redirect(baseurl($tur->seo_link."/".$tur->id));
					
				}
				
			}	
			
		}
		## Kullanıcı Profil Sayfası 
		public function profil($kadi=null){
			
			$user = User::row();
			
			if($kadi==null){
				
				$kadi = $user->kadi;
				
			}
			
			$user = $this->profil_model->getKadi($kadi);
			
			if($user->totalRows()<1){
				
				redirect("service/page404");
				
			}
			
			$data = array(
			
				"user" => $user->row()
			
			);
			
			Import::page("mobil/profil",$data);
			
			
		}		
		## Uygulamada İşlem Başarılıysa 
		function ok(){
			
		}
		## Uygulamada Bir Problem Varsa
		function error(){
			
		}

		public function arkadaslari($userid){
			$this->profil_model->userArkadas($userid);
		}
		## User id sinde göre aktivite getirme
		public function user_activity(){
			$userid = User::id();
			$data = array(
			
				"aktiviteler" => $this->service_model->user_activity(0,$userid)->result()
				
			);
			
			Import::page("mobil/kullanicinin_aktiviteleri",$data);
		}
		## User id'nin davetyeleri ve Rezarvasyon
		public function user_davetactivity(){
			$userid = User::id();
			$data = array(
			
				"aktiviteler" => $this->service_model->user_davetiye(0,$userid)->result()
				
			);
			
			//echo "<pre>"; print_r($data);echo "</pre>";
			Import::page("mobil/kullanici_davetyeleri",$data);
		}
		
	}
?>