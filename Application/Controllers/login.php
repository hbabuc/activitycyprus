<?php 

	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookResponse;
	use Facebook\FacebookSDKException;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;
	use Facebook\Entities\AccessToken;
	use Facebook\HttpClients\FacebookCurlHttpClient;
	use Facebook\HttpClients\FacebookHttpable;
	
	class login extends Controller{
		
		public $firstname = null;
		public $lastname = null;
		
		public function index(){
			
			if (User::check()){
				Warning::set("Zaten giriş yapmışsınız","danger","profil");
			}else{
				if(isset($_SERVER["HTTP_REFERER"])){
					
					Session::insert("RefererPage",$_SERVER["HTTP_REFERER"]);
				
				}
							
				$data = array();
				
				$veriler = array(
				
					"sayfa"=>Import::page("login",$data,true)
				
				);
				
				Import::masterpage($veriler);
			}
			
		
		}
		
		public function forgotmypassword(){
			if (User::check()){
				Warning::set("Zaten giriş yapmışsınız","danger","profil");
			}else{
							
				$data = array();
				
				$veriler = array(
				
					"sayfa"=>Import::page("forgotmypassword",$data,true)
				
				);
				
				Import::masterpage($veriler);
			}
		}
		
		public function sendForgottenPasswordToDamnEmail(){
			if($_POST){
			$posts = Method::post();
			
			Validation::rules("email",array("html","injection"));
			
			
			$password = $this->login_model->getForgottenPassword($posts["email"]);
			
			
			Email::to($posts["email"]) 
				 ->subject('Activity Cyprus Şifremi Unuttum')
				 ->message('
					<a href="'.baseurl('login/newpassword?e='.$posts["email"].'&s='.$password->sifre).'">Şifrenizi değiştirmek için tıklayın.</a>
					
					<p>Activity Cyprus iyi günler diler.</p>
					')
				 ->send();
				 
			output(Email::error());
			
			Warning::set("Şifre yenileme isteği emailinize gönderildi.","","login");
			}
		}
		
		public function newpassword(){
			
			$gets = Method::get();
			
			Validation::rules("e",array("html","injection"));
			Validation::rules("s",array("html","injection"));
			
			if($this->login_model->newPasswordControl($gets["e"],$gets["s"]) > 0){
				// email and password true then
				
				$data = array("email" => $gets["e"]);
				
				$veriler = array(
				
					"sayfa"=>Import::page("changeForgottenPassword",$data,true)
				
				);
				
				Import::masterpage($veriler);
				
			}
		}
		
		public function changeForgottenPasswordLastStep(){
			
			$posts = Method::post();
			
			Validation::rules("pass",array("html","injection","reqired","trim"));
			Validation::rules("passAgain",array("html","injection","reqired","trim"));
			
			
			if($posts["pass"] == $posts["passAgain"]){
				$this->login_model->changeOldPassword($posts["email"],$posts["pass"]);
				Warning::set("Şifreniz değiştirildi.","","login");
			}
			else
				Warning::set("Şifreleriniz aynı değil, tekrar deneyin.");
			
		}
		
		public function doLogin(){
			
			$postlar = Method::post();
			$kadi = $postlar["kadi"];
			$sifre = $postlar["sifre"];
			
			if($postlar){
				
				if(User::login($kadi,$sifre)){
					
					if(Session::select("RefererPage")){
						
						redirect(Session::select("RefererPage"));
						
					}else{
						
						redirect(baseurl("home"));
						
					}
					
				}else{
					
					Warning::set("Girilen kriterlerde kullanıcı bulunamadı.");
					
				}
				
			}else{
				Warning::set("Güvenlik Duvarı !");		
			}	
		}
		
		public function out(){
			
			User::logout();
			redirect("login");
			
		}
		
		public function fblogin(){
			
			FacebookSession::setDefaultApplication( '1928047870755625','d0f7666b7ec39716e0a31120da089208' );
			$helper = new FacebookRedirectLoginHelper('http://aktivite.merkezyazilim.com/login/fblogin' );
			
			try {
			  $session = $helper->getSessionFromRedirect();
			} catch( FacebookRequestException $ex ) {
			  // When Facebook returns an error
			} catch( Exception $ex ) {
			  // When validation fails or other local issues
			}
			
			if ( isset( $session ) ) {
				
				$request = new FacebookRequest(
				  $session,
				  'GET',
				  '/me',
				  array(
					'fields' => 'id,name,email,birthday'
				  )
				);
				
				$response = $request->execute();
				$graphObject = $response->getGraphObject();
				$postlar["fbid"] = $graphObject->getProperty('id');             
				$fbid = $graphObject->getProperty('id');
				
				if($this->login_model->fbcontrol($fbid)->totalRows()<1){
					
					$fbfullname = $graphObject->getProperty('name'); 
					$this->isimayir($fbfullname);
					$postlar["adi"] = $this->firstname;
					$postlar["soyadi"] = $this->lastname;
					$postlar["email"] = $graphObject->getProperty('email');    
					
					$postlar["dtarihi"] = date("Y-m-d",strtotime($graphObject->getProperty('birthday')));    
					if($graphObject->getProperty('gender')=="male"){
						
						$postlar["cinsiyet"]=1;
						
					}else{
						
						$postlar["cinsiyet"]=0;
						
					}
					
					$url = "https://graph.facebook.com/$fbid/picture?type=large";
					Session::insert("FbID",$fbid);           
					$resimadi = "$fbid.jpg";
					
					$postlar["resim"] = Dosya::getResim($url,$resimadi);
					
					$postlar["onay"] = 1;
					
					if($this->login_model->insertUser($postlar)){
						
						Session::insert("fbid",$fbid);
						Warning::Set("Son bir adım kaldı . Lütfen bir kullanıcı adı sifre girin","success","login/fbusername/");
						
					}else{
						
						Warning::set("Bir problemle karşılaştık. Lütfen daha sonra tekrar deneyin.");
						
					}
					
				}else{
					
					if(User::fblogin($fbid))
						echo "ok";
					else
						echo "bok";
					redirect(baseurl("home"));
					
				}
				
			} else {
				$loginUrl = $helper->getLoginUrl(["email","user_about_me","user_birthday"]);
				redirect($loginUrl); 
					
			}
			
		}
		
		public function isimayir($name=null){
			
			$ayir = explode(" ",$name);
			
			$sayi = count($ayir);
			
			$this->lastname = $ayir[$sayi-1];
			
			for($x = 0 ; $x<$sayi-1;$x++){
				
				$this->firstname.=$ayir[$x]." ";
				
			}
						
		}
		
		public function fbusername(){
						
			$data = array();
			
			$veriler = array(
			
				"sayfa"=>Import::page("fbusername",$data,true)
			
			);
			
			Import::masterpage($veriler);
			
		}
		
		public function onay(){
			
			$getler = Method::get();
			
			Validation::rules("validation",array("html","injection"));
			
			$validation = Method::get("validation");
			
			$this->login_model->onay($validation);
			
			Warning::set("Kullanıcınız Onaylandı ","success","login");
			
		}
		
		public function dofbUsername(){
			
			$postlar = Method::post();

			if($postlar){
				
				Validation::rules("kadi",array("reqired","trim","maxchar"=>15,"minchar"=>4),"Kullanıcı Adı ");
				Validation::rules("sifre",array("reqired","trim"),"Şifreniz");
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					if($this->login_model->kadiVarmi($postlar["kadi"])->totalRows()<1){
						
						$postlar["sifre"] = sha1(md5($postlar["sifre"]));
						
						if($this->login_model->updateUser($postlar,Session::select("fbid"))){
							
							User::fblogin(Session::select("fbid"));
							Warning::set("Kayıt Başarıyla Sağlandı.","success","home");
							
						}else{
							
							Warning::set("Veritabanına Baglanırken Bir Hatayla Karşılaştık.");
							
						}
						
					}else{
						
						Warning::set("Kullanıcı Adı Kullanılıyor. ","warning");
						
					}
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı ! ","warning","register");
				
			}
			
		}
	
	}

?>