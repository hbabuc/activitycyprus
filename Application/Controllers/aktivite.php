﻿<?php

    class aktivite extends Controller{
        
		public function test()
		{
			$id = 126;
			echo $this->kullanici_model->arkadas_getir($id)->stringQuery();
		}
		public function ekle(){
			
			 if(User::check()){
				
				$data = array(
					"yaslar" => $this->aktivite_model->yas_getir()->result(),
					"kategoriler" => $this->aktivite_model->kategori_getir()->result()
				);
				
				$veriler = array(
				
					"sayfa"=>Import::page("post-ad",$data,true)
				
				);
				
				Import::Masterpage($veriler);
				
			}
			else
			{
				redirect(baseurl('login'));
			}
			
		}

		## Resim Güncelleme Sayfası
		public function resim_guncelle($id){
			 if(User::check()){

				$userid = User::id();
				if ( $this->aktivite_model->guncelleme_control($id,$userid)->totalRows() > 0){
				$data = array(
					"resimler" => $this->aktivite_model->aktivite_resimleri($id,$userid)->result(),
				);
				
				$veriler = array(
				
					"sayfa"=>Import::page("resim-guncelle",$data,true)
				
				);
				
				Import::Masterpage($veriler);
			}else{
				Warning::set("Yetkiniz olmayan alana giremezsiniz","warning","aktivite/aktivitelerim");
			}
				
			}
			else
			{
				redirect(baseurl('login'));
			}
		}

		public function resim_sil($id,$aktiviteid){
			echo $id."li dosya silinecek";
			$veriler = $this->aktivite_model->resim_sil($id,$aktiviteid);
			print_r($veriler);
			//@unlink(baseurl(UPLOADS_DIR.$veriler[0]->adi));
	
		}
		
		public function aktivitelerim(){
			 if(User::check()){
				$userid = User::id();
				$data = array(
					"yaslar" => $this->aktivite_model->yas_getir()->result(),
					"kategoriler" => $this->aktivite_model->kategori_getir()->result(),
					"aktivitelerim" => $this->aktivite_model->kullanici_aktiviteleri($userid)->result()
				);
				
				$veriler = array(
				
					"sayfa"=>Import::page("kullanici_aktivite",$data,true)
				
				);
				
				Import::Masterpage($veriler);
				
			}
			else
			{
				redirect(baseurl('login'));
			}
			
		}

		public function kisi_cikar($silinecek_userid,$activiteID){

			if( User::check()){
				$aktivitenin_sahibi = User::id();
				if ( $this->aktivite_model->guncelleme_control($activiteID,$aktivitenin_sahibi)->totalRows() > 0){
					if ($aktivitenin_sahibi == $silinecek_userid){

						Warning::set("Kendinizi Çıkartamazsınız.","warning","profil/katilanlari_yonet/{$activiteID}");
					
					}else{

					
						$aktivite = $this->db->select("seo_link,baslik")->where("id = ",$activiteID)->row();
						print_r($aktivite);
						Bildirim::set($silinecek_userid,6,"<a href=".baseurl($aktivite->seo_link."/".$activiteID).">{$aktivite->baslik} </a> aktivitesinden çıkarıldın.. ");
						$this->aktivite_model->kisi_cikar($silinecek_userid,$activiteID);

						Warning::set("Belirnen Kişi Listeden Çıkartıldı.","success","profil/katilanlari_yonet/{$activiteID}");
					
					}
				}else{

					Warning::set("Yetkiniz Olmadığı alana giremezsiniz","danger","profil/katilanlari_yonet/{$activiteID}");
				}
			}else{
				redirect('login');
			}

		}

		public function aktivite_guncelle($id){
			$userid = User::id();
			if ( $this->aktivite_model->guncelleme_control($id,$userid)->totalRows() > 0){
				$data = array(
					"yaslar" => $this->aktivite_model->yas_getir()->result(),
					"kategoriler" => $this->aktivite_model->kategori_getir()->result(),
					"aktivite" => $this->aktivite_model->single($id)
				);
				
				$veriler = array(
				
					"sayfa"=>Import::page("aktivite_guncelle",$data,true)
				
				);
				
				Import::Masterpage($veriler);
			}else{
				Warning::set("Yetkiniz olmayan alana giremezsiniz!!","warning","aktivite/aktivitelerim");
			}
		}

		## id si gelen resmi mevcut aktivitenin kapak resmi yapar
		public function ana_resim_yap($resimid,$akviviteid){
			$userid = User::id();
			if ( $this->aktivite_model->guncelleme_control($akviviteid,$userid)->totalRows() > 0){
				$this->aktivite_model->kapak_resmi_degistir($resimid,$akviviteid);
				redirect("aktivite/resim_guncelle/{$akviviteid}");
			}else{
				Warning::set("Yetkiniz olmayan alana giremezsiniz!!","warning","aktivite/aktivitelerim");
			}
		}

		public function formUpdate($id){
			$postlar = Method::post();
			$postlar['bastarih'] = date("Y-m-d H:i:s",strtotime($postlar['bastarih']));
			$postlar['bittarih'] = date("Y-m-d H:i:s",strtotime($postlar['bittarih']));
			Validation::rules('baslik',array('required','trim','minchar' => '4','maxchar'=>'100',"html"),'Başlık');
			Validation::rules('aciklama',array('required','trim','minchar' => '4',"html"),'Açıklama');
			Validation::rules('ucret',array('trim',"html"),'Ücret');
			Validation::rules('bastarih',array('required',"html"),'Başlangıç Tarihi');
			Validation::rules('bittarih',array('required',"html"),'Bitiş Tarihi');
			Validation::rules('adres',array('trim','required',"html"),'Adres');
			$postlar['seo_link'] = Convert::urlWord($postlar['baslik']);	
			$postlar['userid'] = User::id();
			
			$error = Validation::error('string');
			if($error){
				Warning::set($error,"warning","aktivite/ekle");
			}
			else
			{	
				$userid = User::id();
				$postlar = Method::post();
				$postlar['seo_link'] = Convert::urlWord($postlar['baslik']);
				if($this->aktivite_model->formUpdate($postlar,$id,$userid)){
					Warning::set("Başarıyla Güncellendi","success","aktivite/aktivitelerim");
				}
				else{
					 Warning::set("Aktivite güncellenirken hata oluştu","warning","aktivite/aktivitelerim");
					
				}
			}
		}



		## Resimlerix Post Edildiği Metot
		public function resimekle(){
			if(User::check()){
				$aktiviteID = Session::select("activiteid");
				
				$data = array("aktiviteID" => $aktiviteID);
				
				$veriler = array(
				
					"sayfa"=>Import::page("resimekle",$data,true)
				);
				
					Import::Masterpage($veriler);

				}else{
					redirect(baseurl("login"));
				}
				
		}
		
		## Aktivite Ekleme

		public function forminsert(){
			if(User::check()){
				
				$postlar = Method::post();	
				Validation::rules('baslik',array('required','trim','minchar' => '4','maxchar'=>'100',"html"),'Başlık');
				Validation::rules('aciklama',array('required','trim','minchar' => '4',"html"),'Açıklama');
				Validation::rules('ucret',array('trim',"html"),'Ücret');
				Validation::rules('bastarih',array('required',"html"),'Başlangıç Tarihi');
				Validation::rules('bittarih',array('required',"html"),'Bitiş Tarihi');
				Validation::rules('adres',array('trim','required',"html"),'Adres');
				$error = Validation::error('string');
				if($error){
					Warning::set($error,"warning","aktivite/ekle");
				}
				else
				{
				
					$postlar = Method::post();
					$postlar["onay"] = 0;
					Session::insert("postback",json_encode($postlar));
					$postlar['bastarih'] = date("Y-m-d H:i:s",strtotime($postlar['bastarih']));
					$postlar['bittarih'] = date("Y-m-d H:i:s",strtotime($postlar['bittarih']));
					if ($postlar['bastarih'] > $postlar['bittarih']){
						unset($postlar['bastarih']);
						unset($postlar['bittarih']);
						Session::insert("postback",json_encode($postlar));
						Warning::set("Başlangıç Tarihi Bitişten küçük olmalı","warning","aktivite/ekle");
					}else{

						$postlar['seo_link'] = Convert::urlWord($postlar['baslik']);
						$postlar['seo_link'] = Convert::urlWord($postlar['baslik']);	
						$postlar['userid'] = User::id();
						if(!empty($postlar['ucret'])){
							if($postlar['ucret'] < 1)
								Warning::set("Ücret 0 veya 0'dan küçük olamaz.");
							else if(!is_numeric($postlar['ucret']))
								Warning::set("Sadece rakam giriniz.");
						}
						else
							$postlar['ucret'] = 0;
						
						if(empty($postlar['lat']) or empty($postlar['lng'])){
							Warning::set("Lütfen formun üstündeki haritadan aktivitenin tam konumu tıklayarak işaretleyiniz.","danger","aktivite/ekle");
						}

						if($this->aktivite_model->forminsert($postlar)){
							$insertid = $this->db->insertid();

							$gonder = array("userid" => User::id(), "aktiviteid" => $insertid, "durum" => 1, "tip"=> 2);
							$this->aktivite_model->olusturaniEkle($gonder);
							Session::insert("aktiviteid",$insertid);
							Session::delete("postback");
							redirect(baseurl('aktivite/resimekle'));

						}
						else{
							 Warning::set("Aktivite oluştururken hata oluştu");
							
						}

					}
					
				}
				
			}else{
				redirect(baseurl('login'));
			}
		}
			

		public function yeni_resim_ekle($aktiviteid){
			if(User::check()){
				$userid = User::id();
				if ( $this->aktivite_model->guncelleme_control($aktiviteid,$userid)->totalRows() > 0){
					$id = Dosya::upload();
					$this->aktivite_model->aktivite_resim($aktiviteid,$id);
					if ($this->db->where('resim=',0,"AND")->where('id = ',$aktiviteid)->get('aktivite') >  0 ){
						$this->db->where('id = ', $aktiviteid)->update('aktivite', array('resim' => $id));
					}
					Warning::set("Yeni resim Başarıyla eklendi","success","aktivite/resim_guncelle/$aktiviteid");
				}else{
					Warning::set("Yetkiniz olmayan alana giremezsiniz","warning","aktivite/aktivitelerim");
				}
				
			}
			else
			{
				redirect('/login');
			}
		}	

		## Aktiviteye Resim Ekleme 
		public function resUploads(){
			if(User::check()){
				$aktiviteID = Session::select("aktiviteid");
				$id = Dosya::upload();
				$this->aktivite_model->aktivite_resim($aktiviteID,$id);
				$this->aktivite_model->resimKaydet($aktiviteID,$id);
			}
			else
			{
				redirect('/login');
			}
		
		}


		public function aktivite_sil($aktiviteid){
			$userid = User::id();
			## Gelen Aktivite id üye id si ile kontrol ediliyor.
			if ( $this->aktivite_model->guncelleme_control($aktiviteid,$userid)->totalRows() > 0){

				## Aktiviteyi Silme
				$katilanlar = $this->aktivite_model->aktivite_katilim_control($aktiviteid)->result();
				$davetiye_sayisi = $this->aktivite_model->davetiye_control($aktiviteid)->totalRows();

				if (count($katilanlar) > 1){
					print_r($davetiye_sayisi);
					//$this->aktivite_model->aktivite_sil($aktiviteid,$userid);
					Warning::set('Aktiviteye Katılanlar Var.İptal Edemezsin!','danger','aktivite/aktivitelerim');
				}else{
					$this->aktivite_model->aktivite_sil($aktiviteid,$userid);
					Warning::set("Aktivite Başarıyla Silindi","success","aktivite/aktivitelerim");
					
					
				}


			}else{
				Warning::set("Yetkiniz olmayan alana giremezsiniz","warning","aktivite/aktivitelerim");
			}
		}

		public function control(){
			$aktiviteID = Session::select("aktiviteid");
			$tur = $this->aktivite_model->single($aktiviteID);

			if ( $tur->tur == 1){
				redirect(baseurl('aktivite/arkadaslar'));
			}else if( $tur->tur == 2){
				redirect(baseurl('aktivite/takipciler'));
			}else if( $tur->tur == 0){
				Session::delete("aktiviteid");
				//if(empty($tur)){
					redirect(baseurl("aktivite/aktivitelerim"));
				//}else{
					//redirect(baseurl($tur->seo_link."/".$tur->id));
				//}
				
			}else{
				print_r($tur);
				//Warning::set('Error','danger',baseurl($tur->seo_link."/".$tur->id));
			}
		}

		## Kullanıcının takipçilerine gidicek bildirimleri vs. ayarlaran fonksiyon
		public function takipciler(){
			$aktiviteID = Session::select("aktiviteid");
			if (empty($aktiviteID)){ redirect('aktivite/resimEkle'); }
			$aktivite_row = $this->aktivite_model->single($aktiviteID);
			if  ($aktivite_row->tur == 0){
				redirect('aktivite/resimEkle');
			}elseif ($aktivite_row->tur == 1){
				redirect('arkadaslar');
			}

			echo $userid =  User::id();
			echo $adsoyad =  User::adi()." ".User::soyadi();
			$aktivite_row = $this->aktivite_model->single($aktiviteID);
			$takipciler = $this->kullanici_model->takipci_getir($userid)->result();
			print_r($takipciler);
			foreach ($takipciler as $row) {
				Bildirim::set($row->takipciid,1,"Takip ettiğin {$adsoyad} tarafından oluşturulan {$aktivite_row->baslik} başlıklı aktiviteye davetlisin.Bu Davetiye sadece {$adsoyad} takipçilerine gönderilmiştir.");
				Bildirim::davetiyeGonder($userid,$aktivite_row->id,$row->id);
			}
			Session::delete("aktiviteid");
			redirect("{$aktivite_row->seo_link}/{$aktivite_row->id}");
		}

		## Arkadaşları seçme formunu gösterir
		public function arkadaslar(){
			$aktiviteID = Session::select("aktiviteid");
			if (empty($aktiviteID)){ redirect('aktivite/resimEkle'); }
			$aktivite_row = $this->aktivite_model->single($aktiviteID);
			if  ($aktivite_row->tur == 0){
				redirect($_SERVER['HTTP_REFERER']);
			}elseif ($aktivite_row->tur == 2){
				redirect('takipciler');
			}
			
			$userid =  User::id();
			$adsoyad =  User::adi()." ".User::soyadi();
			
			if ($aktivite_row->cinsiyet == 1){
				$arkadaslar = $this->kullanici_model->arkadas_cinsiyet_ayir($userid,1)->result();
			}elseif($aktivite_row->cinsiyet == 2){
				$arkadaslar = $this->kullanici_model->arkadas_cinsiyet_ayir($userid,0)->result();
			}else{
				$arkadaslar = $this->kullanici_model->arkadas_getir($userid)->result();
			}
			

			if(User::check()){
			$data = array("arkadaslar" => $arkadaslar);
			$veriler = array(
				"sayfa"=>Import::page("arkadas_sec",$data,true)
			
			);
			Import::Masterpage($veriler);
		}

		}



		public function davet_et($aktiviteID){
			## Aktiteye davetyesi olan ve katılanlar listelenmicek

			if (empty($aktiviteID)) { redirect("aktivite/aktivitelerim"); }
			
			if (User::check()){
				$userid = User::id();
				if ( $this->aktivite_model->guncelleme_control($aktiviteID,$userid)->totalRows() > 0){

					$userid =  User::id();
					$adsoyad =  User::adi()." ".User::soyadi();

					$aktiviterow = $this->aktivite_model->single($aktiviteID);
					
					if( $aktiviterow->cinsiyet == 1){
						/* Sadece Erkek */
						$arkadaslar = $this->kullanici_model->arkadas_cinsiyet_ayir($userid,1)->result();

					}elseif( $aktiviterow->cinsiyet == 2){

						/* Sadece Bayan */
						$arkadaslar = $this->kullanici_model->arkadas_cinsiyet_ayir($userid,0)->result();

					}elseif( $aktiviterow->cinsiyet == 0){

						/* Erkek ve Bayan */
						$arkadaslar = $this->kullanici_model->arkadas_getir($userid)->result();

					}

					$data = array("arkadaslar" => $arkadaslar);
					$veriler = array(
						"sayfa"=>Import::page("arkadas_davet",$data,true)
					
					);
					Import::Masterpage($veriler);

				}else{
					Warning::set("Yetkiniz Olmayan Alana giremezsiniz","danger","aktivite/aktivitelerim");
				}

			}else{
				redirect("login");
			}

	
		}
		## Arkadaş Ekleme Formundan gelen veriler
		public function arkadas_form_davet($aktiviteID){


				$userid =  User::id();
				if ( $this->aktivite_model->guncelleme_control($aktiviteID,$userid)->totalRows() > 0){
				if (empty($aktiviteID)){ redirect('/'); }
				$aktivite_row = $this->aktivite_model->single($aktiviteID);

					$userid =  User::id();
					$adsoyad =  User::adi()." ".User::soyadi();
					
					if ( Method::post()){
						$postlar = Method::post();
						$sayac = 0;
						## keyler bildirim gidicek user id leri
						foreach ($postlar['arkadas'] as $key => $value) {
						$status = $this->aktivite_model->katilan_kontrol($aktiviteID,$key)->totalRows();
						echo $status;
							if ( $status == 0){
								$sayac++;
								/* class=\"davet\" */
								Bildirim::set($key,3,$adsoyad.' adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. '.$aktivite_row->baslik.' başlıklı aktiviteye göz atmak için 
									<a class="davet" href="'.baseurl("$aktivite_row->seo_link/$aktivite_row->id").'">'.$aktivite_row->baslik.'</a>');
								Bildirim::davetiyeGonder($userid,$aktivite_row->id,$key);
							}
	
						}
						Warning::set("Davetiyeler gönderildi","success","aktivite/aktivitelerim");
						
					}
				
				}else{
					Warning::set("Yetkiniz Yok","danger","aktivite/aktivitelerim");
				}

			
		}/* fonksiyon sonu */

		## Arkadaş Ekleme Formundan gelen veriler
		public function arkadas_form(){
			$aktiviteID = Session::select("aktiviteid");
			if (empty($aktiviteID)){ redirect('/'); }
			$aktivite_row = $this->aktivite_model->single($aktiviteID);
			if  ($aktivite_row->tur == 0){
				redirect('');
			}elseif ($aktivite_row->tur == 2){
				redirect('takipciler');
			}else{
				$userid =  User::id();
				$adsoyad =  User::adi()." ".User::soyadi();
				
				if ( Method::post()){
					$postlar = Method::post();

					## keyler bildirim gidicek user id leri
					foreach ($postlar['arkadas'] as $key => $value) {
						Bildirim::set($key,3,"{$adsoyad} adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  {$aktivite_row->baslik} başlıklı aktiviteye göz atmak için <a href=".baseurl("$aktivite_row->seo_link/$aktivite_row->id").">Aktiviteyi İncele</a> ");
						Bildirim::davetiyeGonder($userid,$aktivite_row->id,$key);
		
					}
					Session::delete("aktiviteid");
					redirect($aktivite_row->seo_link."/".$aktivite_row->id);
				}
			}
		}
			



	public function davetiyelerim(){
		if (User::check()){
			$userid = User::id();
			$davetiyelerim = $this->aktivite_model->kullanicinin_davetiyeleri($userid)->result();

			$data = array("davetiyelerim" => $davetiyelerim);
			$veriler = array(
				"sayfa"=>Import::page("kullanici_davetiye",$data,true)
			
			);
			Import::Masterpage($veriler);
		}else{
			redirect('/');
		}
	}

	public function davetiye_sil($davetiyeID,$userid){
		$this->aktivite_model->davetiye_sil($davetiyeID,$userid);
		Warning::set("Davetyeniz Silindi","success","aktivite/davetiyelerim");
	}

	public function rezarvasyonlarim(){
		if (User::check()){
			$userid = User::id();
			$katildiklarim = $this->aktivite_model->katildigim_aktiviteler($userid)->result();
			$data = array("katildiklarim" => $katildiklarim);
			$veriler = array(
				"sayfa"=>Import::page("katildigim_aktiviteler",$data,true)
			
			);
			Import::Masterpage($veriler);
		}else{
			redirect('login');
		}
		
	}

	public function aktiviteden_cikis($aktiviteID){
		$userid = User::id();
		$silme =  $this->aktivite_model->aktiviteden_cikis($userid,$aktiviteID);
		if ($silme){
			Warning::set("Aktiviteden Ayrıldınız","success","aktivite/rezarvasyonlarim");
		}else{
			Warning::set("Bir Sorun Oluştu.Yöneticiye Haber Veriniz.","danger","aktivite/rezarvasyonlarim");
		}
	}



		
}
?>