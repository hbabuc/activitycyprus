<?php 

	class home extends Controller{

		public function index(){
				
			$data = array(

				"kategoriler" => $this->aktivite_model->kategori_getir()->result(),

				"aktiviteler" => $this->aktivite_model->aktivite_listele()->result(),

				"biten_aktiviteler" => $this->aktivite_model->tarih_sorgulari("biten_aktivite")->result(),

				"devam_edenler" => $this->aktivite_model->tarih_sorgulari("devam_edenler")->result(),

				"ucretsiz_aktivite" => $this->aktivite_model->tarih_sorgulari("ucretsiz_aktiviteler")->result(),

				"bayanlar" => $this->aktivite_model->tarih_sorgulari("bayanlar")->result(),

				"erkekler" => $this->aktivite_model->tarih_sorgulari("erkekler")->result()

			);

			$veriler = array(

				"sayfa"=>Import::page("anasayfa",$data,true)
			);

			Import::masterpage($veriler);
		}




		## Sayfalama İçin Tüm Verilerin listelendiği fonksiyon
		public function alls(){
			
			if ($_POST){

				$postlar = Method::post();
				$bitenler = $this->aktivite_model->aktivite_filtrele($postlar);
				$listeleme_sonuc = $bitenler->result();
				$listeleme_pagin = $bitenler->pagination();
				$baslik = "Filtreleme Sonuçları";
				$yas_durumu = $this->aktivite_model->yas_durumu()->result();
				
				$data = array(

					"kategoriler" => $this->aktivite_model->kategori_getir()->result(),

					"listeleme" =>$listeleme_sonuc,

					"paginate" => $listeleme_pagin,

					"baslik" => $baslik,

					"yas_durumu" => $yas_durumu

				);

			}else{

				$bitenler  =  $this->aktivite_model->aktivite_listele();
				$listeleme_sonuc = $bitenler->result();
				$listeleme_pagin = $bitenler->pagination();
				$baslik = "Tüm Aktiviteler";
				$yas_durumu = $this->aktivite_model->yas_durumu()->result();
				
				$data = array(

					"kategoriler" => $this->aktivite_model->kategori_getir()->result(),

					"listeleme" =>$listeleme_sonuc,

					"paginate" => $listeleme_pagin,

					"baslik" => $baslik,

					"yas_durumu" => $yas_durumu

				);
			}
			//print_r($listeleme_sonuc);
				
				$veriler = array(
					"sayfa"=>Import::page("list",$data,true)
				);
				Import::masterpage($veriler);

		
		}

		## Kategoriye Göre Listeleme

		public function category($seolink){

			$url_kat = $this->db->select("id")->where('seo_link=',$seolink)->get("kategoriler")->row();
			

			if ($_POST){

				$postlar = Method::post();
				unset($postlar["kategori"]);
				$bitenler = $this->aktivite_model->aktivite_filtrele($postlar);
				$listeleme_sonuc = $bitenler->result();
				$listeleme_pagin = $bitenler->pagination();
				$baslik = "Filtreleme Sonuçları";
				$yas_durumu = $this->aktivite_model->yas_durumu()->result();
				
				$data = array(

					"kategoriler" => $this->aktivite_model->kategori_getir()->result(),

					"listeleme" =>$listeleme_sonuc,

					"paginate" => $listeleme_pagin,

					"baslik" => $baslik,

					"yas_durumu" => $yas_durumu,

					"aktif_kat" => $url_kat->id

				);

			}else{


				$bitenler  =  $this->aktivite_model->category_get($seolink);
				$listeleme_sonuc = $bitenler->result();
				$listeleme_pagin = $bitenler->pagination();
				$baslik = "Kategori";
				$yas_durumu = $this->aktivite_model->yas_durumu()->result();

				$data = array(

					"kategoriler" => $this->aktivite_model->kategori_getir()->result(),

					"listeleme" =>$listeleme_sonuc,

					"paginate" => $listeleme_pagin,

					"baslik" => $baslik,

					"yas_durumu" => $yas_durumu,

					"aktif_kat" => $url_kat->id

				);


			}
			
			$veriler = array(
				"sayfa"=>Import::page("list",$data,true)
			);
			Import::masterpage($veriler);
			
		}


		## Kullanıcının konumunun alınıp sıralandığı fonksiyon
		public function konum($konum){
		  
			$yeni_konum = explode(",", $konum);
			
			$data = array(
			
				"kategoriler" => $this->aktivite_model->kategori_getir()->result(),
				"aktiviteler" => $this->aktivite_model->aktivite_listele($yeni_konum[0],$yeni_konum[1])->result(),
				"biten_aktiviteler" => $this->aktivite_model->tarih_sorgulari("biten_aktivite")->result(),
				"devam_edenler" => $this->aktivite_model->tarih_sorgulari("devam_edenler")->result(),
				"ucretsiz_aktivite" => $this->aktivite_model->tarih_sorgulari("ucretsiz_aktiviteler")->result(),
				"bayanlar" => $this->aktivite_model->tarih_sorgulari("bayanlar")->result(),
				"erkekler" => $this->aktivite_model->tarih_sorgulari("erkekler")->result()
				
			);

			$veriler = array(
				"sayfa"=>Import::page("anasayfa",$data,true)
			);

			Import::masterpage($veriler);
			
		}


	}

?>