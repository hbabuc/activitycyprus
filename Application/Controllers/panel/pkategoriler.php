<?php

	class pkategoriler extends Controller{	
		
		protected $alan = 8;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["columns"] = array("#"=>"id","Alan Adı"=>"adi","Onay Durumu"=>"onay");
			$data["DataGrid"] = $this->pkategoriler_model->getall();
			$data["tableTitle"] = "Kategoriler";
			$data["DbName"] = $this->pkategoriler_model->dbname;
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/TopluIslem/list.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Kategori"
			
			));
			
		}	
		
		
		public function sirala($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/kategoriler/list.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Kullanıcı"
			
			));
			
		}	
		
		public function doSirala(){
			
			$json = Method::post("json");
			$json = json_decode(htmlspecialchars_decode($json),true);
			$this->siraekle($json);
			
		}
		
		public function siraekle($degisgen,$kim=0){
			
			$postlar = array();
			for($x=0;$x<count($degisgen);$x++){
				if(isset($degisgen[$x]["id"])){
					
					$id = $degisgen[$x]["id"];
					
				}else{
					
					$id=0;
					
				}
				if(isset($degisgen[$x]["children"])){
					
					$alt = $degisgen[$x]["children"];
					
				}else{
					
					$alt=array();
					
				}
				if($id>0){
					
					$postlar = array(
						
						"sirasi"=>$x+1,
						"kim"=>$kim
							
					);
	
					print_r($postlar);
					echo $id;
					if($this->pkategoriler_model->updateSira($postlar,$id))
						echo "başarılı";
					else
						echo "başarısız";
					
					$this->siraekle($alt,$id);
					
				}
				
			}
			
		}
		
		public function ekle(){
			
			Yetki::insert($this->alan);
			
			$diller = $this->pkategoriler_model->dil()->result();
			
			$data = array(
				
				"title"=>"Özel Kategori Ekleme Formu",
				"titlesmall"=>"Özel bir kategori eklemek için kullanılır.",
				"diller"=>$diller,
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/kategoriler/insert.php",$data,true),	
				"class"=>"Yetki",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function delete(){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			
			if($postlar){
					
				
				if(!is_numeric($id)){
					
					Warning::set("Güvenlik Duvarı !");
					
				}else{
			
					if($this->pkategoriler_model->deleteAlan($id)){
						
						Warning::set("Yetki Başarıyla Silindi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			
			if($postlar){
				
				$dizi["kim"] = 0;
				
				for($x=0;$x<count($postlar["adi"]);$x++){
					
					$dizi["adi"] =  $postlar["adi"][$x];
					$dizi["seo_link"] = Convert::urlWord($dizi["adi"]);
					$dizi["dilid"] =  $postlar["dilid"][$x];
					
					if($this->pkategoriler_model->insert($dizi)){
						
						$id = DB::insertId();
						
						$dizi["kim"] = $id;
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
				Warning::set("Kategori Başarıyla Eklendi","success");
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
	
	}

?>