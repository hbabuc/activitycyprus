<?php

	class purunler extends Controller{	
		
		protected $alan = 8;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["tableTitle"] = "Ürünler";
			$data["DbName"] = $this->purunler_model->dbname;
			$data["columns"] = array("#"=>"id","Ürün Adı"=>"adi","Ürün Başlığı"=>"baslik","onay"=>"onay");
			$data["DataGrid"] = $this->purunler_model->getall();
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/TopluIslem/list.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Ürünler"
			
			));
			
		}	
		
		public function ekle(){
			
			Yetki::insert($this->alan);
			
			$data = array(
				
				"title"=>"Ürün Ekleme Formu",
				"titlesmall"=>""
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/urunler/insert.php",$data,true),	
				"class"=>"Yetki",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function edit(){
			
			Yetki::update($this->alan);
			
			$id = Method::get("dataGridId");
			
			if(is_numeric($id)){
				
				$model = $this->purunler_model->getid($id);
				
				if($model){
					
					$data["urun"] = $model->row();
					
				}else{
					
					Warning::set("Güvenlik Duvarı !","info");
					
				}
				
			}else{
				$data = array();
				Warning::set("Güvenlik Duvarı !","info");
				
			}
			
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/urunler/edit.php",$data,true),	
				"method"=>"Düzenle",
				"class"=>"Kullanıcı"
			
			));
			
		}
		
		public function delete(){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			
			if($postlar){
					
				
				if(!is_numeric($id)){
					
					Warning::set("Güvenlik Duvarı !");
					
				}else{
			
					if($this->purunler_model->deleteAlan($id)){
						
						Warning::set("Yetki Başarıyla Silindi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			
			if($postlar){
					
				Validation::rules("adi",array("injection","maxchar"=>50,"trim","required"),"Ürün Adı : ");
				Validation::rules("baslik",array("injection","maxchar"=>50,"trim","required"),"Ürün Başlığı : ");
				Validation::rules("aciklama",array("injection","maxchar"=>50,"trim","required"),"Ürün Açıklaması: ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
			
					if($this->purunler_model->insert($postlar)){
						
						Warning::set("Ürünler Başarıyla Eklendi.","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
	}

?>