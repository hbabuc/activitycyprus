<?php

	class pfirmalar extends Controller{	
		
		protected $alan = 12;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["columns"] = array("#"=>"id","Firma Adı"=>"adi","Telefon Numarası"=>"telefon");
			$data["DataGrid"] = $this->pfirmalar_model->getall();
			$data["tableTitle"] = "Firmalar Listesi";
			$data["DbName"] = $this->pfirmalar_model->dbname;
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/TopluIslem/list.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Firmalar"
			
			));
			
		}	
		
		public function ekle(){
			
			Yetki::insert($this->alan);
			
			$data = array(
				
				"title"=>"Firma Ekleme Formu",
				"titlesmall"=>""
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/firmalar/insert.php",$data,true),	
				"class"=>"Firma",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function edit(){
			
			Yetki::update($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			if(is_numeric($id)){
				
				$data = array(
					
					"title"=>"Firma Düzenleme Formu",
					"titlesmall"=>"",
					"row"=>$this->pfirmalar_model->get($id)->row()
				
				);
				
				Import::page("panel/MasterPage",array(
					
					"sayfa"=>Import::page("panel/sayfalar/firmalar/edit.php",$data,true),	
					"class"=>"Firma",
					"method"=>"Düzenleme"
				
				));
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function delete(){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			
			if($postlar){
					
				
				if(!is_numeric($id)){
					
					Warning::set("Güvenlik Duvarı !");
					
				}else{
			
					if($this->pfirmalar_model->deleteAlan($id)){
						
						Warning::set("Veri Başarıyla Silindi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			$postlar["konum"] = htmlspecialchars($postlar["konum"]);
			
			if($postlar){
					
				Validation::rules("adi",array("injection","maxchar"=>70,"trim","required"),"Firma Adı : ");
				Validation::rules("resim",array("injection","maxchar"=>70,"trim","required"),"Firma Resmi : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
			
					if($this->pfirmalar_model->insert($postlar)){
						
						$id = DB::insertId();
						
						Warning::set("Firma Başarıyla Eklendi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function doEdit($id){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			
			if($postlar){
					
				if(is_numeric($id)){
					
					Validation::rules("adi",array("injection","maxchar"=>70,"trim","required"),"Alan Adı : ");
					
					$hata = Validation::error("string");
					
					if($hata){
						
						Warning::set($hata,"warning");
						
					}else{
				
						if($this->pfirmalar_model->edit($postlar,$id)){
							
							$id = DB::insertId();
							
							Warning::set("Firma Başarıyla Düzenlendi","success");
							
						}else{
							
							Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
							
						}
						
					}
						
				}else{
					
					Warning::set("Güvenlik Duvarı !");
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
	}

?>