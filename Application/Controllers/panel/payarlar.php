<?php

	class payarlar extends Controller{	
		
		protected $alan = 11;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["ayar"] = $this->payarlar_model->getall()->row();
			$data["icerikler"] = $this->payarlar_model->getIcerik()->result();
			$data["sslayari"] = $this->payarlar_model->getSsl()->result();
			$data["AcikKapali"] = $this->payarlar_model->getSiteDurum()->result();
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/ayarlar/ayarlar.php",$data,true),	
				"method"=>"Düzenle",
				"class"=>"Genel Ayarlar"
			
			));
			
		}	
		
		public function editGenelAyar(){
			
			$post = Method::post();
			
			if($post){
				
				Validation::rules("adi",array("required","trim"),"Site Adı : ");
				Validation::rules("baslik",array("required","trim"),"Site Başlığı : ");
				Validation::rules("etiketler",array("required","trim"),"Site Etiketleri : ");
				Validation::rules("aciklama",array("required","trim"),"Site Açıklaması : ");
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					if($this->payarlar_model->editGenelAyar(Json::encode($post))){
						
						Warning::set("Veriler Başarıyla Güncellendi.","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function editIletisim(){
			
			$post = Method::post();
			
			if($post){
				
				Validation::rules("email",array("required","trim"),"Email Adresi : ");
				Validation::rules("telefon",array("required","trim"),"Telefon Numarası : ");
				Validation::rules("adres",array("required","trim"),"Adres : ");
				if(!$post["iframe"]){
					
					$post["iframe"] = "";
					
				}
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					if($this->payarlar_model->editIletisim(Json::encode($post))){
						
						Warning::set("Veriler Başarıyla Güncellendi.","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function editEmail(){
			
			$post = Method::post();
			
			if($post){
				
				Validation::rules("iletisim",array("required","trim","email"),"İletişim Maili : ");
				Validation::rules("replyto",array("required","trim","email"),"Reply To Maili : ");
				
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					if($this->payarlar_model->editEmail(Json::encode($post))){
						
						Warning::set("Veriler Başarıyla Güncellendi.","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function editIcerik(){
			
			$post = Method::post();
			
			if($post){
				
				Validation::rules("icerigi",array("required","trim"),"İçerik Seçimi: ");
				
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					if($this->payarlar_model->editIcerik(Json::encode($post))){
						
						Warning::set("Veriler Başarıyla Güncellendi.","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function mailTest(){
			
			$ayar = $this->payarlar_model->getall()->row();
			$email = json::decode($ayar->email);
			
			
			if(Email::To($email->iletisim)->subject('Konu')->message('Mesaj')->send()){
			
				Warning::set("Aşağıdaki İletişim Adresine Mail Gönderimi Yapıldı. Lütfen Gelen Kutusunu Ve Spawn Gönderimleri Kontrol Ediniz.","success");
				 
			}else{
				 
				 Warning::set("Bir Sorunla Karşılaştık. Lütfen Mail Ayarlarının Doğruluğunu Kontrol Ederek Tekrar Deneyin.");
				 
			}
			
		}
		
		public function editSsl(){
			
			$post = Method::post();
			
			if($post){
				
				Validation::rules("ssl",array("required","trim"),"Ssl Seçimi : ");
				
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					if($this->payarlar_model->editSsl(Json::encode($post))){
						
						Warning::set("Veriler Başarıyla Güncellendi.","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function editSiteDurumu(){
			
			$post = Method::post();
			
			if($post){
				
				Validation::rules("sitedurumu",array("required","trim"),"Site Durumu : ");
				
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					if($this->payarlar_model->editSiteDurumu(Json::encode($post))){
						
						Warning::set("Veriler Başarıyla Güncellendi.","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
	}

?>