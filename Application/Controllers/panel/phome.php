<?php

	class phome extends Controller{	

		public function index($params = ''){	
			
			$yorum = $this->phome_model->getNewYorum();
			
			$yorumSayi = $yorum->totalRows();
			
			$data["yorumlar"] = $yorum->result();
			
			$data["yorumSayi"] = $yorumSayi;
			
			$data["aktiviteSayi"] = $this->phome_model->aktiviteSayi()->totalRows();
			
			$data["resimSayi"] = $this->phome_model->resimSayi()->totalRows();
 			
			$data["kampanyaSayi"] = $this->phome_model->kampanyaSayi()->totalRows();
			
			$data["kullaniciSayi"] = $this->phome_model->kullaniciSayi()->totalRows();
			
			$data["firmaSayi"] = $this->phome_model->firmaSayi()->totalRows();
			
			$data["kategoriSayi"] = $this->phome_model->kategoriSayi()->totalRows();
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/index.php",$data,true)
			
			));
		
		}

		public function yorumSil($id){
			
			if($this->phome_model->yorumSil($id)){
				
				Warning::set("Yorum Başarıyla Kaldırıldı.","success");
				
			}else{
				
				Warning::set("Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin.");
				
			}
			
		}

		public function yorumOnlayla($id){
			
			if($this->phome_model->yorumOnlayla($id)){
				
				Warning::set("Yorum Başarıyla Onaylandı.","success");
				
			}else{
				
				Warning::set("Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin.");
				
			}
			
		}
		
	}

?>