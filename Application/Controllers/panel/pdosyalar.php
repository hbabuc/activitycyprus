<?php

	class pdosyalar extends Controller{	
		
		protected $alan = 7;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["dosyalar"] = $this->pdosyalar_model->getall()->result();
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/dosyalar/listele.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Dosyalar"
			
			));
			
		}	
		
		public function ekle(){
			
			Yetki::insert($this->alan);
			
			$data = array(
				
				"title"=>"Özel Alan Ekleme Formu",
				"titlesmall"=>"Özel bir yönetim alanı eklemek için kullanılır."
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/dosyalar/ekle.php",$data,true),	
				"class"=>"Dosya",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function delete($id){
			
			Yetki::delete($this->alan);
									
				
			if(!is_numeric($id)){
				
				Warning::set("Güvenlik Duvarı !");
				
			}else{
				
				$dosya = $this->pdosyalar_model->get($id)->row();
				
				$konum = UPLOADS_DIR.$dosya->adi;
				
				if(file_exists($konum)){
					
					unlink($konum);
					
				}
				
				$konum = UPLOADS_DIR.$dosya->kucuk;
				
				if(file_exists($konum)){
					
					unlink($konum);
					
				}
				$konum = UPLOADS_DIR.$dosya->orta;
				
				if(file_exists($konum)){
					
					unlink($konum);
					
				}
				
				if($this->pdosyalar_model->delete($id)){
					
					Warning::set("Yetki Başarıyla Silindi","success");
					
				}else{
					
					Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
					
				}
				
			}
					
		}
		
		public function lastItem(){
			
			$data = array(
			
				"resimler"=>$this->pdosyalar_model->lastItem()->result()
			
			);
			
			Import::page("panel/sayfalar/dosyalar/lastItem.php",$data);
						
		}
		
		public function searchImg(){
			
			$ara = Method::post("ara");
			
			$data = array(
			
				"resimler"=>$this->pdosyalar_model->SearchImg($ara)->result()
			
			);
			
			Import::page("panel/sayfalar/dosyalar/lastItem.php",$data);
						
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			if($_FILES){
			
				echo Dosya::upload();
				
			}			
			
		}
		
		public function filter($type){
			
			if($type=="document"){
				
				$data["dosyalar"] = $this->pdosyalar_model->getdocument()->result();
				
			}else if($type=="audio"){
				
				$data["dosyalar"] = $this->pdosyalar_model->getaudio()->result();
				
			}else if($type=="img"){
				
				$data["dosyalar"] = $this->pdosyalar_model->getimg()->result();
				
			}else{
				
				$data["dosyalar"] = $this->pdosyalar_model->getall()->result();
				
			}
			
			Yetki::select($this->alan);
			
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/dosyalar/listele.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Dosyalar"
			
			));
			
		}
		
		public function dosyadetay($id){
			
			if(is_numeric($id)){
				
				$type = "other";
				
				$satir = $this->pdosyalar_model->get($id);
				
				if($satir){
					
					$dosya = $satir->row();
					
					$type = substr($dosya->adi,-4);
					
					if($type==".jpg" || $type=="jpeg" || $type==".png" || $type==".gif"){
						
						$type="img";
						
					}
					
					$array = array(
						
						"url"=>baseurl(UPLOADS_DIR.$dosya->adi),
						"type"=>$type
					
					);
					echo json_encode($array);
					
				}else{
					
					Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
	}

?>