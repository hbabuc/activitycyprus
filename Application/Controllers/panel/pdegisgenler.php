<?php 

	Class pdegisgenler Extends Controller{
		
		protected $alan = 17;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["columns"] = array(	
			
				"#"=>"id",
				"Dili"=>"dili",	
				"Değişgen Keyi"=>"keyi",	
				"Değişgen Değeri"=>"value"
				
			);
			$data["DataGrid"] = $this->pdegisgenler_model->getall();
			$data["tableTitle"] = "Dil Ayarları";
			$data["DbName"] = $this->pdegisgenler_model->dbname;
			
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/TopluIslem/list.php",$data,true),	
				"method"=>"Listesi",
				"class"=>"Dil"
			
			));
			
		}
		
		public function ekle(){
			
			Yetki::insert($this->alan);
			
			$data = array(
				
				"title"=>"Degişgen ekleme formu ",
				"titlesmall"=>"Bir değişgen eklemek istediğinizde sadece dilin bilgilerini girin artık sisteminizde aktif olacaktır.",
				"diller"=>$this->pdegisgenler_model->getdil()->result()
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/degisgenler/insert.php",$data,true),	
				"class"=>"Değişgen",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function delete(){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			
			if($postlar){
					
				
				if(!is_numeric($id)){
					
					Warning::set("Güvenlik Duvarı !");
					
				}else{
					
					if($id<=User::yetki()){
						
						Warning::set("Kendi Yetkinizi Veya Sizden Önce Üretilmiş Bir Yetkiyi Silemezsiniz");
						
					}else{
					
						if($this->pdegisgenler_model->deleteYetki($id)){
							
							Warning::set("Yetki Başarıyla Silindi","success");
							
						}else{
							
							Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
							
						}
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function doEdit(){
			
			Yetki::update($this->alan);
			
			$postlar = Method::post();
			$yetkiId = Method::post("yetkiId");
			$bolgeler = $_POST["bolgeler"];
			
			
			if($postlar){
					
				Validation::rules("adi",array("injection","maxchar"=>20,"trim","required"),"Dil Adı : ");
				Validation::rules("kisa",array("injection","maxchar"=>4,"trim","required"),"Dil Kısaltması : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
					
					if($this->pyetki_model->deleteyetkiler($yetkiId)){
					
						$hata =0;
						
						foreach($bolgeler as $bolge){
							
							$parcala = explode(",",$bolge);
							
							$alani = $parcala[0];
							$yetkisi = $parcala[1];
							
							if($this->pdegisgenler_model->insertYetkilimi($yetkiId,$alani,$yetkisi)){
								
								$hata = 0;
								
							}else{
								
								$hata = 1;
								
							}
							
						}
						
						if($hata==1){
								
							Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
							
						}else{
							
							redirect("panel/pyetki/");
							
						}
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
				
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function edit(){
			
			Yetki::update($this->alan);
			
			$yetkiler = array();
			
			$id = Method::get("dataGridId");
			
			$model = $this->pyetki_model->yetkialanlari($id);
			
			foreach($model->result() as $row){
				
				$yetkiler[$row->alanid.",".$row->alanyetkiid] = $row->alanid.",".$row->alanyetkiid;
				
			}
			
			
			$useryetki = User::yetki();
			
			$model = $this->pdegisgenler_model->yetkialanlari($useryetki);
			
			foreach($model->result() as $row){
				
				$useryetkisi[$row->alanid.",".$row->alanyetkiid] = $row->alanid.",".$row->alanyetkiid;
				
			}
			
			$data = array(
				
				"title"=>"Özel Yetki Düzenleme Formu",
				"titlesmall"=>"Özel bir yetki düzenlemek istediğinizde sadece adını yazın ve  gerekli işlemleri yapın",
				"alanlar"=>$this->pdegisgenler_model->getalanlar()->result(),
				"alanyetkisi"=>$this->pdegisgenler_model->getalanyetkisi()->result(),
				"yetkiler"=>$yetkiler,
				"useryetkisi"=>$useryetkisi,
				"yetkiId"=>$id
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/yetki/edit.php",$data,true),	
				"class"=>"Yetki",
				"method"=>"Duzenle"
			
			));
			
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			
			if($postlar){
					
				Validation::rules("key",array("injection","maxchar"=>20,"trim","required"),"Değişgen Key : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
					
					for( $x = 0 ; $x < count($postlar["value"]) ; $x++ ){
						
						$item = array();
						
						$item["keyi"] = $postlar["key"];
						$item["dilid"] = $postlar["dilid"][$x];
						$item["value"] = $postlar["value"][$x];
						
						print_r($postlar);
						
						$this->pdegisgenler_model->insert($item);
						
						ML::insert(strtolower($postlar["dil"][$x]),$postlar["key"], $postlar["value"][$x]);
						
					}
					
					Warning::set("Veriler Başarıyla Kayıt Edildi.","success");
										
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		
	}

?>