<?php

	class palanyetkisi extends Controller{	
		
		protected $alan = 3;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["columns"] = array("#"=>"id","Alan Adı"=>"alanyetkisi");
			$data["tableTitle"] = "Alan Yetkileri";
			$data["DbName"] = $this->palanyetkisi_model->dbname;
			$data["DataGrid"] = $this->palanyetkisi_model->getall();
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/TopluIslem/list.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Kullanıcı"
			
			));
			
		}	
		
		public function ekle(){
			
			Yetki::insert($this->alan);
			
			$data = array(
				
				"title"=>"Özel Alan Yetkisi Ekleme Formu",
				"titlesmall"=>"Özel bir yönetim alanı yetkisi eklemek için kullanılır."
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/alanyetkisi/insert.php",$data,true),	
				"class"=>"Yetki",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function delete(){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			if($postlar){
					
				
				if(!is_numeric($id)){
					
					Warning::set("Güvenlik Duvarı !");
					
				}else{
			
					if($this->palanyetkisi_model->deleteAlan($id)){
						
						Warning::set("Yetki Başarıyla Silindi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			
			if($postlar){
					
				Validation::rules("alanyetkisi",array("injection","maxchar"=>50,"trim","required"),"Alan Adı : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
			
					if($this->palanyetkisi_model->insert($postlar)){
						
						$id = DB::insertId();
						
						Warning::set("Alan Başarıyla Eklendi. Alan Idsi = $id","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
	}

?>