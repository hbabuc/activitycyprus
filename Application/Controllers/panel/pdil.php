<?php 

	Class pdil Extends Controller{
		
		protected $alan = 15;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["columns"] = array(	
			
				"#"=>"id",
				"Dil Adı"=>"adi",
				"Dil Adı Kısaltması"=>"kisa",
				"Onay Durumu"=>"onay"
				
			);
			$data["DataGrid"] = $this->dil_model->getall();
			$data["tableTitle"] = "Dil Ayarları";
			$data["DbName"] = $this->dil_model->dbname;
			
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/TopluIslem/list.php",$data,true),	
				"method"=>"Listesi",
				"class"=>"Dil"
			
			));
			
		}
		public function ekle(){
			
			Yetki::insert($this->alan);
			
			$data = array(
				
				"title"=>"Dil ekleme formu ",
				"titlesmall"=>"Bir dil eklemek istediğinizde sadece dilin bilgilerini girin artık sisteminizde aktif olacaktır."	
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/dil/insert.php",$data,true),	
				"class"=>"Dil",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function delete(){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			
			if($postlar){
					
				
				if(!is_numeric($id)){
					
					Warning::set("Güvenlik Duvarı !");
					
				}else{
					
					if($id<=User::yetki()){
						
						Warning::set("Kendi Yetkinizi Veya Sizden Önce Üretilmiş Bir Yetkiyi Silemezsiniz");
						
					}else{
					
						if($this->pyetki_model->deleteYetki($id)){
							
							Warning::set("Yetki Başarıyla Silindi","success");
							
						}else{
							
							Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
							
						}
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function doEdit(){
			
			Yetki::update($this->alan);
			
			$postlar = Method::post();
			$yetkiId = Method::post("yetkiId");
			$bolgeler = $_POST["bolgeler"];
			
			
			if($postlar){
					
				Validation::rules("adi",array("injection","maxchar"=>20,"trim","required"),"Dil Adı : ");
				Validation::rules("kisa",array("injection","maxchar"=>4,"trim","required"),"Dil Kısaltması : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
					
					if($this->pyetki_model->deleteyetkiler($yetkiId)){
					
						$hata =0;
						
						foreach($bolgeler as $bolge){
							
							$parcala = explode(",",$bolge);
							
							$alani = $parcala[0];
							$yetkisi = $parcala[1];
							
							if($this->pyetki_model->insertYetkilimi($yetkiId,$alani,$yetkisi)){
								
								$hata = 0;
								
							}else{
								
								$hata = 1;
								
							}
							
						}
						
						if($hata==1){
								
							Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
							
						}else{
							
							redirect("panel/pyetki/");
							
						}
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
				
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function edit(){
			
			Yetki::update($this->alan);
			
			$yetkiler = array();
			
			$id = Method::get("dataGridId");
			
			$model = $this->pyetki_model->yetkialanlari($id);
			
			foreach($model->result() as $row){
				
				$yetkiler[$row->alanid.",".$row->alanyetkiid] = $row->alanid.",".$row->alanyetkiid;
				
			}
			
			
			$useryetki = User::yetki();
			
			$model = $this->pyetki_model->yetkialanlari($useryetki);
			
			foreach($model->result() as $row){
				
				$useryetkisi[$row->alanid.",".$row->alanyetkiid] = $row->alanid.",".$row->alanyetkiid;
				
			}
			
			$data = array(
				
				"title"=>"Özel Yetki Düzenleme Formu",
				"titlesmall"=>"Özel bir yetki düzenlemek istediğinizde sadece adını yazın ve  gerekli işlemleri yapın",
				"alanlar"=>$this->pyetki_model->getalanlar()->result(),
				"alanyetkisi"=>$this->pyetki_model->getalanyetkisi()->result(),
				"yetkiler"=>$yetkiler,
				"useryetkisi"=>$useryetkisi,
				"yetkiId"=>$id
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/yetki/edit.php",$data,true),	
				"class"=>"Yetki",
				"method"=>"Duzenle"
			
			));
			
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			
			if($postlar){
					
				Validation::rules("adi",array("injection","maxchar"=>20,"trim","required"),"Dil Adı : ");
				Validation::rules("kisa",array("injection","maxchar"=>4,"trim","required"),"Dil Kısaltması : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
			
					if($this->dil_model->insert($postlar)){
						
						$id = DB::insertId();
						
						Warning::set("Veri Başarıyla Eklendi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
		public function insertstep($id=0){
			
			Yetki::insert($this->alan);
			
			
			$useryetki = User::yetki();
			
			$model = $this->pyetki_model->yetkialanlari($useryetki);
			
			foreach($model->result() as $row){
				
				$yetkiler[$row->alanid.",".$row->alanyetkiid] = $row->alanid.",".$row->alanyetkiid;
				
			}
			
			$data = array(
				
				"title"=>"Özel Yetki Ekleme Formu - 2.Adım",
				"titlesmall"=>"Özel bir yetki eklemek istediğinizde sadece adını yazın ve  gerekli işlemleri yapın",
				"alanlar"=>$this->pyetki_model->getalanlar()->result(),
				"alanyetkisi"=>$this->pyetki_model->getalanyetkisi()->result(),
				"yetkiId"=>$id,
				"yetkiler"=>$yetkiler
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/yetki/insertstep.php",$data,true),	
				"class"=>"Yetki",
				"method"=>"Ekle"
			
			));
			
		}
		
		public function doYetkiler(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			$yetkiId = Method::post("yetkiId");
			$bolgeler = $_POST["bolgeler"];
			
			
			if($postlar){
					
				Validation::rules("yetkiId",array("injection","trim","required"),"Yetki Idsi : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
					$hata =0;
					
					foreach($bolgeler as $bolge){
						
						$parcala = explode(",",$bolge);
						
						$alani = $parcala[0];
						$yetkisi = $parcala[1];
						
						if($this->pyetki_model->insertYetkilimi($yetkiId,$alani,$yetkisi)){
							
							$hata = 0;
							
						}else{
							
							$hata = 1;
							
						}
						
						
					}
					
					if($hata==1){
							
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}else{
						
						redirect("panel/pyetki/");
						
					}
				
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
	}

?>