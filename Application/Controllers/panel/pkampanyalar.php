<?php

	class pkampanyalar extends Controller{	
		
		protected $alan = 13;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){	
			
			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["columns"] = array("#"=>"id","Kampanya Adı"=>"adi","Afis 1"=>"afis","Afis 2"=>"afis1","Kalan Bilet Sayısı"=>"kuponsayi","Başlangıç Tarihi"=>"baslangictarihi","Bitiş Tarihi"=>"bitistarihi","Onay Durmu"=>"onay");
			$data["DataGrid"] = $this->pkampanyalar_model->getall();
			$data["tableTitle"] = "Kampanyalar Listesi";
			$data["DbName"] = $this->pkampanyalar_model->dbname;
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/TopluIslem/list.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Kampanyalar"
			
			));
			
		}	
		
		public function ekle(){
			
			Yetki::insert($this->alan);
			
			$data = array(
				
				"title"=>"Firma Ekleme Formu",
				"kategoriler"=>$this->pkampanyalar_model->getKategori()->result(),
				"titlesmall"=>""
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/kampanyalar/insert.php",$data,true),	
				"class"=>"Firma",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function editImages($id=0){
			
			Yetki::update($this->alan);
			
			$data = array(
				
				"title"=>"Firma Ekleme Formu",
				"id"=>$id,
				"resimler"=>$this->pkampanyalar_model->getKampanyaImg($id)->result(),
				"titlesmall"=>""
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/kampanyalar/editImages.php",$data,true),	
				"class"=>"Firma",
				"method"=>"Ekle"
			
			));
			
		}
		
		public function doInsertImg($id=0){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			if($postlar){
				
				Validation::rules("resim",array("injection","maxchar"=>255,"trim","required"),"Resim : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
										
					if($this->pkampanyalar_model->insertImg($id,$postlar["resim"])){
						
						$kampanyaid = DB::insertId();
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
					while($kuponsayi>0){
						
						$random = $this->kuponKod();
						
						if($this->pkampanyalar_model->kuponVarmi($random,$kampanyaid)->totalRows()<1){
							
							$this->pkampanyalar_model->kuponEkle($random,$kampanyaid);
							
							$kuponsayi--;
							
						}
						
					}
					
					Warning::set("Kampanya Oluşturuldu Firma İçin Kuponlar Oluşturuldu.","success");
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function edit(){
			
			Yetki::update($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			
			if(is_numeric($id)){
				
				$veri = $this->pkampanyalar_model->get($id);
				
				$row = $veri->row();
			
				if($veri->totalRows()<1){
					
					Warning::set("Böyle Bir Veri Bulunamadı");
					
				}
				
				$data = array(
					
					"title"=>"Firma Düzenleme Formu",
					"titlesmall"=>"",
					"kategoriler"=>$this->pkampanyalar_model->getKategori()->result(),
					"row"=>$row,
					"id"=>$id
				
				);
				
				Import::page("panel/MasterPage",array(
					
					"sayfa"=>Import::page("panel/sayfalar/kampanyalar/edit.php",$data,true),	
					"class"=>"Firma",
					"method"=>"Düzenleme"
				
				));
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function delete(){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			
			if($postlar){
					
				
				if(!is_numeric($id)){
					
					Warning::set("Güvenlik Duvarı !");
					
				}else{
			
					if($this->pkampanyalar_model->deleteAlan($id)){
						
						Warning::set("Veri Başarıyla Silindi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function deleteKampanyaImg($id=0){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			if(!is_numeric($id)){
				
				Warning::set("Güvenlik Duvarı !");
				
			}else{
		
				if($this->pkampanyalar_model->deleteKampanyaImg($id)){
					
					Warning::set("Veri Başarıyla Silindi","success");
					
				}else{
					
					Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
					
				}
				
			}
				
		}
		
		public function firmaSearch(){
			
			$key = Method::post("ara");
			
			$data["firmalar"] = $this->pkampanyalar_model->searchFirma($key);
			
			if($data["firmalar"]){
				
				$data["firmalar"] = $data["firmalar"]->result();
				
				Import::page("panel/sayfalar/kampanyalar/searchFirma",$data);
				
			}else{
				
				Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
				
			}
			
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			$kuponsayi = $postlar["biletsayi"];
			unset($postlar["biletsayi"]);
			
			$postlar["seo_link"] = Convert::urlWord($postlar["adi"]);
			
			if($postlar){
				
				// print_r($postlar["aciklama"]);
				
				// preg_match_all('/<img (.*?)src="(.*?)"/',$postlar["aciklama"],$out);
				
				// print_r($out[1]);
				
				// $img = str_replace('data:image/jpeg;base64,', '', $out[1][0]);
				// $img = str_replace(' ', '+', $img);
				// $data = base64_decode($img);
				
				// echo "<br>".$data;
				// $file = UPLOADS_DIR . 'test.jpg';
				// $success = file_put_contents($file, $data);
				// if($success){
					
					// echo "Test";
					
				// }else{
					
					// echo "dgl";
					
				// }
				unset($postlar["biletsayi"]);
				unset($postlar["files"]);
				
				Validation::rules("adi",array("injection","maxchar"=>255,"trim","required"),"Kampanya Adı : ");
				Validation::rules("firmaid",array("injection","numeric","trim","required"),"Firma Seçimi : ");
				Validation::rules("baslangictarihi",array("injection","trim","required"),"Kampanya Başlangıç Tarihi : ");
				Validation::rules("bitistarihi",array("injection","trim","required"),"Kampanya Bitiş Tarihi : ");
				Validation::rules("biletsayi",array("injection","numeric","trim","required"),"Kampanya Bilet Sayısı : ");
				Validation::rules("resim",array("injection","numeric","trim","required"),"Kampanya Resmi : ");
				Validation::rules("etiketler",array("injection","trim","required"),"Kampanya Etiketi : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
					
					print_r($postlar);
					
					if($this->pkampanyalar_model->insert($postlar)){
						
						$kampanyaid = DB::insertId();
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
					while($kuponsayi>0){
						
						$random = $this->kuponKod();
						
						if($this->pkampanyalar_model->kuponVarmi($random,$kampanyaid)->totalRows()<1){
							
							$this->pkampanyalar_model->kuponEkle($random,$kampanyaid);
							
							$kuponsayi--;
							
						}
						
					}
					
					Warning::set("Kampanya Oluşturuldu Firma İçin Kuponlar Oluşturuldu.","success");
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function kuponKod($length=6){
			
			$return = "";

			$characters = "ABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";

			$num_characters = strlen($characters) - 1;

			while (strlen($return) < $length) {

				$return.= $characters[mt_rand(0, $num_characters)];

			}

			return $return;
			
		}
		
		public function doEdit($id){
			
			
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			$tamamla = $postlar["biletsayi"];
			
			if($postlar["slider"]!=1){
				
				$postlar["slider"]=0;
				
			}
			if($postlar["afis"]!=1){
				
				$postlar["afis"]=0;
				
			}
			if($postlar["afis1"]!=1){
				
				$postlar["afis1"]=0;
				
			}
			
			unset($postlar["files"]);
			unset($postlar["biletsayi"]);
			
			$biletsayi = $this->pkampanyalar_model->totalBilet($id)->totalRows();
			
			while($tamamla>$biletsayi){
				
				$random = $this->kuponKod();
						
				if($this->pkampanyalar_model->kuponVarmi($random,$id)->totalRows()<1){
					
					$this->pkampanyalar_model->kuponEkle($random,$id);
					
					$kuponsayi--;
					
				}
				$biletsayi++;
				echo "Calıstı";
				
			}
			
			if($postlar){
					
				if(is_numeric($id)){
					
					Validation::rules("adi",array("injection","maxchar"=>255,"trim","required"),"Kampanya Adı : ");
					Validation::rules("firmaid",array("injection","numeric","trim","required"),"Firma Seçimi : ");
					Validation::rules("baslangictarihi",array("injection","trim","required"),"Kampanya Başlangıç Tarihi : ");
					Validation::rules("bitistarihi",array("injection","trim","required"),"Kampanya Bitiş Tarihi : ");
					Validation::rules("biletsayi",array("injection","numeric","trim","required"),"Kampanya Bilet Sayısı : ");
					Validation::rules("resim",array("injection","numeric","trim","required"),"Kampanya Resmi : ");
					Validation::rules("etiketler",array("injection","trim","required"),"Kampanya Etiketi : ");
					
					$hata = Validation::error("string");
					
					if($hata){
						
						Warning::set($hata,"warning");
						
					}else{
				
						if($this->pkampanyalar_model->edit($postlar,$id)){
							
							$id = DB::insertId();
							
							Warning::set("Kampanya Başarıyla Düzenlendi","success");
							
						}else{
							
							Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
							
						}
						
					}
						
				}else{
					
					Warning::set("Güvenlik Duvarı !");
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
			
		}
		
	}

?>