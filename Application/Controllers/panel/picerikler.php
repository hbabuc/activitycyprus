<?php

	class picerikler extends Controller{	
		
		protected $alan = 16;
		
		protected $select = 1;
		
		protected $insert = 2;
		
		protected $update = 3;
		
		protected $delete = 4;
		
		protected $OnayKontrol = 5;
		
		public function index($params = ''){

			Yetki::select($this->alan);
			
			$data["EditKontrol"] = Yetki::kontrol($this->alan,$this->update);
			$data["RemoveKontrol"] = Yetki::kontrol($this->alan,$this->delete);
			$data["InsertKontrol"] = Yetki::kontrol($this->alan,$this->insert);
			$data["OnayKontrol"] = Yetki::kontrol($this->alan,$this->OnayKontrol);
			$data["columns"] = array("#"=>"id","Aktivite Başlığı"=>"baslik","Onay Durumu"=>"onay");
			$data["DataGrid"] = $this->picerikler_model->getall();
			$data["tableTitle"] = "Aktivite Listesi";
			$data["DbName"] = $this->picerikler_model->dbname;
			
						
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/TopluIslem/list.php",$data,true),	
				"method"=>"Listele",
				"class"=>"Kampanyalar"
			
			));
			
		}	
		
		public function ekle(){
						
			Yetki::insert($this->alan);
			
			$data = array(
				
				"title"=>"İçerik Ekleme Formu",
				"kategoriler"=>$this->picerikler_model->getKategori()->result(),
				"titlesmall"=>""
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/icerikler/insert.php",$data,true),	
				"class"=>"İçerik",
				"method"=>"Ekle"
			
			));
			
			
		}
		
		public function editImages($id=0){
			
			Yetki::update($this->alan);
			
			$data = array(
				
				"title"=>"Firma Ekleme Formu",
				"id"=>$id,
				"resimler"=>$this->pkampanyalar_model->getKampanyaImg($id)->result(),
				"titlesmall"=>""
			
			);
			
			Import::page("panel/MasterPage",array(
				
				"sayfa"=>Import::page("panel/sayfalar/kampanyalar/editImages.php",$data,true),	
				"class"=>"Firma",
				"method"=>"Ekle"
			
			));
			
		}
		
		public function doInsertImg($id=0){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			if($postlar){
				
				Validation::rules("resim",array("injection","maxchar"=>255,"trim","required"),"Resim : ");
				
				$hata = Validation::error("string");
				
				if($hata){
					
					Warning::set($hata,"warning");
					
				}else{
										
					if($this->pkampanyalar_model->insertImg($id,$postlar["resim"])){
						
						$kampanyaid = DB::insertId();
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
					while($kuponsayi>0){
						
						$random = $this->kuponKod();
						
						if($this->pkampanyalar_model->kuponVarmi($random,$kampanyaid)->totalRows()<1){
							
							$this->pkampanyalar_model->kuponEkle($random,$kampanyaid);
							
							$kuponsayi--;
							
						}
						
					}
					
					Warning::set("Kampanya Oluşturuldu Firma İçin Kuponlar Oluşturuldu.","success");
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function edit(){
			
			Yetki::update($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			$versiyon = Method::get("versiyon");
			
			$versiyonid = Method::get("versiyonid");
			
			if($versiyonid)
				
				$id = $versiyonid;
				
			if($versiyon=="true")
				
				$versiton = true;
				
			else
				
				$versiyon = false;
			
			if(is_numeric($id)){
				
				$veri = $this->picerikler_model->get($id,$versiyon);
				
				$row = $veri->row();
				
				if($veri->totalRows()<1){
					
					Warning::set("Böyle Bir Veri Bulunamadı");
					
				}
				
				$data = array(
					
					"title"=>"Firma Düzenleme Formu",
					"titlesmall"=>"",
					"kategoriler"=>$this->picerikler_model->getKategori()->result(),
					"gethasKat"=>$this->picerikler_model->gethasKat($id)->result(),
					"etiketler"=>$this->picerikler_model->gethasKeyword($id)->result(),
					"taslaklar"=>$this->picerikler_model->getVersiyon(Method::get("dataGridId"))->result(),
					"row"=>$row,
					"id"=>$id
				
				);
				
				Import::page("panel/MasterPage",array(
					
					"sayfa"=>Import::page("panel/sayfalar/icerikler/edit.php",$data,true),	
					"class"=>"Firma",
					"method"=>"Düzenleme"
				
				));
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function delete(){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			$id = Method::get("dataGridId");
			
			
			if($postlar){
					
				
				if(!is_numeric($id)){
					
					Warning::set("Güvenlik Duvarı !");
					
				}else{
			
					if($this->picerikler_model->deleteAlan($id)){
						
						Warning::set("Veri Başarıyla Silindi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
						
					}
					
				}
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function deleteKampanyaImg($id=0){
			
			Yetki::delete($this->alan);
			
			$postlar = Method::get();
			
			if(!is_numeric($id)){
				
				Warning::set("Güvenlik Duvarı !");
				
			}else{
		
				if($this->pkampanyalar_model->deleteKampanyaImg($id)){
					
					Warning::set("Veri Başarıyla Silindi","success");
					
				}else{
					
					Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
					
				}
				
			}
				
		}
		
		public function firmaSearch(){
			
			$key = Method::post("ara");
			
			$data["firmalar"] = $this->pkampanyalar_model->searchFirma($key);
			
			if($data["firmalar"]){
				
				$data["firmalar"] = $data["firmalar"]->result();
				
				Import::page("panel/sayfalar/kampanyalar/searchFirma",$data);
				
			}else{
				
				Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık. Lütfen Daha Sonra Tekrar Deneyin.");
				
			}
			
		}
		
		public function doInsert(){
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			$postlar["seo_link"] = Convert::urlWord($postlar["baslik"]);
			
			if($postlar){
				
				Session::insert("PostBack",$postlar);
				
				Validation::rules("baslik",array("required","maxchar"=>255),"Başlık :");
				Validation::rules("aciklama",array("required"),"Açıklama :");
				Validation::rules("type",array("required"),"Biçim :");
				Validation::rules("onay",array("required"),"Onay :");
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					$kategoriler = $postlar["kategoriler"];
					$etiketler = $postlar["etiketler"];
					unset($postlar["kategoriler"]);
					unset($postlar["files"]);
					unset($postlar["etiketler"]);
					
					print_r($postlar);
					
					if(count($kategoriler)<1){
							
						Warning::set("Lütfen Bir Kategori Seçiniz","warning");
						
					}
						
					if(count($etiketler)<1){
					
						Warning::set("Etiket Alanı Boş Gecilemez","warning");
						
					}
					
					if($this->picerikler_model->insert($postlar)){
						
						$LastId = DB::insertId();
						
						
						
						foreach($kategoriler as $kategori){
							
							$this->picerikler_model->insertKategori($LastId,$kategori);
							
						}
						
						$etiketler = explode(",",$etiketler);
						
						
						
						foreach($etiketler as $etiket){
							
							$icerikVarmi = $this->picerikler_model->etiketVarmi(trim($etiket));
							
							
							if($icerikVarmi->totalRows() < 1){
								
								$this->picerikler_model->etiketEkle($etiket,$LastId);
								
								$etiketId = DB::insertId();
								
								$this->picerikler_model->icerikEtiketEkle($etiketId,$LastId);
																
							}else{
								
								$row = $icerikVarmi->row();
								$id = $row->id;
								$this->picerikler_model->icerikEtiketEkle($id,$LastId);
								
							}
							
						}
						
						Session::delete("PostBack");
						
						Warning::set("İçerik Başarıyla Kayıt Edildi ","success","panel/picerikler");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin . ");
						
					}
					
				}
				
					
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
		public function kuponKod($length=6){
			
			$return = "";

			$characters = "ABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";

			$num_characters = strlen($characters) - 1;

			while (strlen($return) < $length) {

				$return.= $characters[mt_rand(0, $num_characters)];

			}

			return $return;
			
		}
		
		public function doEdit($id){
			
			
			
			Yetki::insert($this->alan);
			
			$postlar = Method::post();
			
			$postlar["seo_link"] = Convert::urlWord($postlar["baslik"]);
			
			if($postlar){
				
				Session::insert("PostBack",$postlar);
				
				Validation::rules("baslik",array("required","maxchar"=>255),"Başlık :");
				Validation::rules("aciklama",array("required"),"Açıklama :");
				Validation::rules("type",array("required"),"Biçim :");
				Validation::rules("onay",array("required"),"Onay :");
				
				$error = Validation::error("string");
				
				if($error){
					
					Warning::set($error,"warning");
					
				}else{
					
					$kategoriler = $postlar["kategoriler"];
					$etiketler = $postlar["etiketler"];
					unset($postlar["kategoriler"]);
					unset($postlar["files"]);
					unset($postlar["etiketler"]);
					
					print_r($postlar);
					
					if(count($kategoriler)<1){
							
						Warning::set("Lütfen Bir Kategori Seçiniz","warning");
						
					}
						
					if(count($etiketler)<1){
					
						Warning::set("Etiket Alanı Boş Gecilemez","warning");
						
					}
					
					if($this->picerikler_model->edit($postlar)){
						
						$LastId = DB::insertId();
						
						foreach($kategoriler as $kategori){
							
							$this->picerikler_model->insertKategori($LastId,$kategori);
							
						}
						
						$etiketler = explode(",",$etiketler);
						
						foreach($etiketler as $etiket){
							
							$icerikVarmi = $this->picerikler_model->etiketVarmi(trim($etiket));
							
							
							if($icerikVarmi->totalRows() < 1){
								
								$this->picerikler_model->etiketEkle($etiket,$LastId);
								
								$etiketId = DB::insertId();
								
								$this->picerikler_model->icerikEtiketEkle($etiketId,$LastId);
																
							}else{
								
								$row = $icerikVarmi->row();
								$id = $row->id;
								$this->picerikler_model->icerikEtiketEkle($id,$LastId);
								
							}
							
						}
						
						Session::delete("PostBack");
						
						Warning::set("İçerik Başarıyla Düzenlendi","success");
						
					}else{
						
						Warning::set("Veritabanına Bağlanırken Bir Sorunla Karşılaştık Lütfen Daha Sonra Tekrar Deneyin . ");
						
					}
					
				}
				
			}else{
				
				Warning::set("Güvenlik Duvarı !");
				
			}
			
		}
		
	}

?>