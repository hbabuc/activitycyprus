<?php 



	class register extends Controller{

	

		public function index(){

			if (User::check()){
				Warning::set("Giriş yapmışsınız.Kayıt olamazsınız","danger","profil");
			}else{

				$data = array();

				

				$veriler = array(

				

					"sayfa"=>Import::page("register",$data,true)

				

				);

				

				Import::masterpage($veriler);
			}

	
			

		}

		

		public function doRegister(){
			
			
			if (User::check()){
				Warning::set("Güvenlik Duvarı ! ","warning","profil");


			}else{
				$postlar = Method::post();



				if($postlar){

					

					Validation::rules("kadi",array("reqired","trim","maxchar"=>15,"minchar"=>4),"Kullanıcı Adı ");

					Validation::rules("sifre",array("reqired","trim"),"Şifreniz");

					Validation::rules("adi",array("reqired","trim","maxchar"=>20),"Adınız ");

					Validation::rules("soyadi",array("reqired","trim","maxchar"=>15),"Soyadınız");

					Validation::rules("email",array("reqired","trim","email"),"Email Adresiniz");

					

					$error = Validation::error("string");

					

					if($error){

						

						Warning::set($error,"warning");

						

					}else{

						

						if($this->register_model->userVarmi($postlar["kadi"],$postlar["email"])->totalRows()<1){

							

							$postlar["sifre"] = sha1(md5($postlar["sifre"]));

							print_r($postlar);

							

							if($this->register_model->insertUser($postlar)){

								Email::to($postlar["email"]) 
									 ->subject('Activity Cyprus Onay Maili')
									 ->message('
										<p> Üyeliğinizi tamamlamak için aşağıdaki bağlantıyı tıklayın. </p><br>
										<a href="'.baseurl("login/onay?validation=".$postlar["sifre"]).'">Kullanıcı Hesabını Onaylamak İçin Lütfen Linke Tıklayın.</a>
									 
									 ') 
									 ->send();
									 
								output(Email::error());
							
								Warning::set("Kayıt Başarıyla Sağlandı. Bir adım kaldı. Mailinize gönderdiğimiz aktivasyon maili ile üyeliğinizi tamamlanmış olacak.","success");

							}else{

								Warning::set("Veritabanına Baglanırken Bir Hatayla Karşılaştık.");

							}

						}else{

							Warning::set("Kullanıcı Adı Veya Email Adresi Kullanılıyor. ","warning");

						}

					}

				}else{
					
					Warning::set("Güvenlik Duvarı ! ","warning","register");

				}
				
			}
			
		}

	}



?>

			