<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
 
<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR."datatables.js"); ?>"></script>
<br><br><br>

<div class="container" style="padding-bottom:100px;">
	<?php /*print_r($bildirimler);*/ echo Warning::get(); ?>
	<div class="table-responsive">

	

	<table id="example" class="display" width="100%" cellspacing="0">
	        <thead>
	            <tr>
	                <th>id</th>
	                <th>Başlık</th>
	                <th>İşlemler</th>
	            </tr>
	        </thead>
	        <tbody>
	        <?php  foreach($aktivitelerim as $row): ?>
	                <tr>
	                	<td><?=$row->id?></td>
	                    <td><?=$row->baslik?></td>
	                    <td>
	        	            <a href="<?php  echo baseurl("aktivite/aktivite_guncelle/{$row->id}") ?>" type="button" class="btn btn-success">Güncelle</a>
	        	            <a href="<?php  echo baseurl("aktivite/resim_guncelle/{$row->id}") ?>" type="button" class="btn btn-info">Resimleri Güncelle</a>
	        	            <a href="<?php  echo baseurl("profil/katilanlari_yonet/{$row->id}") ?>" type="button" class="btn btn-success">Katılanları Yönet</a>
	        	            <a href="#"  data-link="<?php  echo baseurl("aktivite/aktivite_sil/{$row->id}") ?>" clasS="btn btn-danger swal"  style="margin-right:15px;"> Sil </a>
	        	            <a href="<?php echo baseurl("aktivite/davet_et/{$row->id}") ?>" type="button" class="btn btn-info"> Davet Et  </a>
	                    </td>
	                </tr>
	            <?php  endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
	            <script type="text/javascript">
	            	$(document).ready(function() {
	            	    $('#example').DataTable( {
	            	        "order": [[ 0, "desc" ]]
	            	    } );
	            	} );
	            </script>

</div>
<style type="text/css">
	.bildirimaciklama{
		width: 250px;
	}
	.bildirimaciklama .col-md-6{
		width: 200px;
		
	}
</style>