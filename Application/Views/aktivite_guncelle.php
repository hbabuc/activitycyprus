<?php
	 $postback = Session::select("postback");
	 
	 if($postback){
		$row = json_decode($postback); 
	 }
?>

	<!-- Submit Ad -->
	<div class="submit-ad main-grid-border">
		<div class="container">
			<h2 class="head"> Aktivite Oluştur </h2>
			<div class="post-ad-form">
				<form method="POST" action="<?php echo baseurl("aktivite/formUpdate/{$aktivite->id}")?>">
				<input type="hidden" name="lat" id="maplat" value="">
				<input type="hidden" name="lng" id="maplng" value="">
					<label><?php echo ML::select("categori") ?> <span>*</span></label>
					<select class="name" style="width:70%" name="aktivite_kat">
					  <?php foreach($kategoriler as $row): ?>
					  <option value="<?=$row->id?>" <?php  if($aktivite->aktivite_kat == $row->id){ echo 'selected'; } ?>><?=$row->adi ?></option>
					  <?php endforeach; ?>
					</select>
					<div class="clearfix"></div>

					<label><?php echo ML::select("

                    gender") ?> <span>*</span></label>
					<select class="name" style="width:70%" name="cinsiyet">
					  <option value="0" <?php  if($aktivite->cinsiyet == 0){ echo 'selected'; } ?>>Karışık</option>
					  <option value="1" <?php  if($aktivite->cinsiyet == 1){ echo 'selected'; } ?>>Sadece Erkek</option>
					  <option value="2" <?php  if($aktivite->cinsiyet == 2){ echo 'selected'; } ?>>Sadece Bayan</option>
					</select>
					<div class="clearfix"></div>
					
					<label>Başlık<span>*</span></label>
					<input type="text" name="baslik" class="name" placeholder="Aktivite Başlığı" value="<?=$aktivite->baslik?>">
					<div class="clearfix"></div>

					<label>Kapasite<span>*</span></label>
					<
					<input type="text" name="kapasite" class="name" placeholder="Aktivitinze kaç kişinin katılmasını istiyorsunuz ? " <?php if(isset($aktivite->kapasite)) echo 'value="'.$aktivite->kapasite.'"'; ?>>
					<div class="clearfix"></div>

					<label> Aktiviteniz Durumu <span>*</span></label>
					<select class="name" style="width:70%" name="tur">
					  <option value="0" <?php  if($aktivite->tur == 0){ echo 'selected'; } ?>>Herkese Açık</option>
					  <option value="1" <?php  if($aktivite->tur == 1){ echo 'selected'; } ?>>Arkadaşlara Özel</option>
					  <option value="2" <?php  if($aktivite->tur == 2){ echo 'selected'; } ?>>Takipçilere Özel</option>
					</select>
					<div class="clearfix"></div>
					
					<label>Aktivite Açıklaması <span>*</span></label>
					<textarea class="mess" placeholder="Aktiviteniz açıklayınız" name="aciklama"><?php if(isset($aktivite->aciklama)) echo $aktivite->aciklama; ?></textarea>
					<div class="clearfix"></div>
					
					<label>Ücret<span>*</span></label>
					<input type="checkbox" id="ucretdurumu" name="ucretdurumu" value="1"> Aktiviteniz ücretli mi?
					<input style="display:none;" type="text" id="ucret" name="ucret" class="name" placeholder="Kişi başı ücret" <?php if(isset($aktivite->ucret)) echo 'value="'.$aktivite->ucret.'"'; ?>>
					<div class="clearfix"></div>


					
					<label>Başlangıç Tarihi<span>*</span></label>
					<input id="datepicker" type="text" name="bastarih" placeholder="Tarih seçmek için tıklayın." <?php if(isset($aktivite->bastarih)) echo 'value="'.$aktivite->bastarih.'"'; ?>>
					<div class="clearfix"></div>
					
					<label>Bitiş Tarihi<span>*</span></label>
					<input id="datepicker2" type="text" name="bittarih" placeholder="Tarih seçmek için tıklayın." <?php if(isset($aktivite->bittarih)) echo 'value="'.$aktivite->bittarih.'"'; ?>>
					<div class="clearfix"></div>
					<div class="personal-details">
					
					<!-- HARİTA -->
					
					<label>Adres<span>*</span></label>
					<input type="text" id="adres" name="adres" class="name" placeholder="Adres" <?php if(isset($aktivite->adres)) echo 'value="'.$aktivite->adres.'"'; ?>>
					
					<label>Yaş Aralığı Seçin <span>*</span></label>
					<select class="" name="yas_aralik" style="width:70%">
					<?php  foreach($yaslar as $row): ?>
					  <option value="<?=$row->id?>" <?php  if($aktivite->yas_aralik == $row->id ){ echo 'selected'; } ?>><?=$row->baslangic?>-<?=$row->bitis?></option>
					  <?php endforeach; ?>
					</select>
					<div class="clearfix"></div>
				<div class="clearfix"></div>
						   <center><div id="map" style="width: 70%; height: 300px; position:relative; margin-left: 110px;" >
				   <img id="loadergif" src="<?php echo baseurl(UPLOADS_DIR."loader.gif");?>">
				   </div></center>

			<div class="clearfix"></div><br><br>
	

				<!--  Harita SON -->
				</div>
						
					<input type="submit" value="Güncelle">					
					<div class="clearfix"></div>
					</form>
					</div>
			</div>
		</div>	
	</div>
	<!-- // Submit Ad -->

	</div>

	  
<script src="//api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>
	<script>
	
	var myMap;
	var position,isMapsLoaded = false;

	if(navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(getCurrentPos,errorCallback);
	}
	else
	{
		position = false;
	}
	
	
	function errorCallback(){
		position = false;
	}
	
	function getCurrentPos(pos){
		if(!isMapsLoaded){
		position = pos.coords;
		$('#loadergif').hide();
		$('#map').css('height','550px');
		$('#map').empty();
		ymaps.ready(init);
		}
	}
	
	setTimeout(function(){
		if(!isMapsLoaded){
			position = false;
			$('#loadergif').hide();
			$('#map').css('height','550px');
			ymaps.ready(init);
		}
		
	},10000); 
	

	function init () {
		// Creating an instance of the map and binding it to the container with the specified ID
		// ("map").
		isMapsLoaded = true;
		var defaultCoords = [35.166170, 33.376186];
		
		myMap = new ymaps.Map('map', {
			// When initializing the map, you must specify its center and the zoom factor.
			center: (!position) ? defaultCoords : [position.latitude,position.longitude], // Москва
			zoom: 10
		}, {
			searchControlProvider: 'yandex#search'
		});
		
		if(position){
			var myPlacemark = new ymaps.Placemark([position.latitude,position.longitude], { 
				hintContent: 'Buradasınız!', balloonContent: 'Etkinliğinizi burada oluşturabilirsiniz!' 
			});

			myMap.geoObjects.add(myPlacemark);
			
			$("#maplat").val(position.latitude);
			$("#maplng").val(position.longitude);
		}
		

		myMap.events.add('click', function (e) {
			
			myMap.geoObjects.removeAll();
		// Getting the click coordinate
		var coords = e.get('coords');
		var myPlacemark = new ymaps.Placemark([coords[0],coords[1]], { 
				hintContent: 'Harika!', balloonContent: 'Etkinliğinizi burada oluşturabilirsiniz!' 
			});

			myMap.geoObjects.add(myPlacemark);

			$("#maplat").val(coords[0]);
			$("#maplng").val(coords[1]);
		//alert(coords.join(', '));
		
	});

	}
	</script>