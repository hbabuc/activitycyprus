<?php
	date_default_timezone_set('Europe/Istanbul');
	$userid = User::id();
?>


<script type="text/javascript" src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.min.js"></script>
	<div class="header">
		<div class="container" style="height: 130px;">
		<div class="col-md-6">
			<div class="logo" style="margin-top: 15px">
				<a href="/activitycyprus"><img width=700 src="<?php echo baseurl(UPLOADS_DIR."aktivitekibris.png") ?>" alt=""/></a>
			</div>
		</div> 
		<div class="col-md-4">	
		
		</div>
		<div class="col-md-5" style="margin-left: 70px; margin-top: 20px;">
					<?php if(!User::check()){ ?>
						<div class="header-right"  style="margin-top: 30px;">
						<a class="btn btn-info" href="<?=baseurl('register');?>" style="margin-right:5px">Kayıt Ol</a>
						<span></span>
						<a class="btn btn-info" href="<?=baseurl('login');?>" style="margin-right:5px">Giriş Yap </a>		
					<?php }else{ ?>
						<div class="header-right" style="margin-top: 30px;">
						<span></span>
						<div class="dropdown pull-right" style="margin-left: 5px;">
						  <button class="btn btn-info dropdown-toggle" type="button" id="profildrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<i class="fa fa-user"></i> <?=User::kadi() ?>
							<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="profildrop">
							<li><a href="<?=baseurl('profil');?>">Profil</a></li>
							<li><a href="<?=baseurl('aktivite/aktivitelerim');?>">Aktivitelerim</a></li>
							<li><a href="<?=baseurl('aktivite/davetiyelerim');?>">Davetiyelerim</a></li>
							<li><a href="<?=baseurl('aktivite/rezarvasyonlarim');?>">Rezervasyonlarim</a></li>
							<li><a href="<?=baseurl('login/out') ?>">Çıkış Yap</a></li>
						  </ul>
						</div>
						
						<div class="dropdown pull-right" style="margin-left: 5px;">
						  <button class="btn btn-info dropdown-toggle" type="button" id="aktivitedrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<i class="fa fa-globe"></i>
							Bildirimler(<?=Bildirim::getAll($userid)->totalRows() ?>)
							<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="aktivitedrop" style="width:300px;">
							<div class="list">
								  <?php
									
									$notifications = Bildirim::getWithLimit($userid,3);
									?>
									
									<?php
									if($notifications->totalRows() < 1){
										?>
										<div class="bildirim">
										<div class="col-md-3 text-center">
											<i class="fa fa-2x fa-times-circle"></i>
										</div>
										<div class="col-md-9" style="padding-bottom:10px;">
											<h4></h4>
											<p>Yeni bildirim yok!</p>

										</div>
										<div class="clearfix"></div>
										</div>
									<?php
									}
									else
									{
									foreach($notifications->result() as $n){
										?>
									<div class="bildirim" data-id="<?=$n->id ?>">
										<div class="col-md-3 text-center">
											<?php
												switch($n->tip){
													case 1:
													echo '<i class="fa fa-2x fa-rss"></i>';
													break;
													case 2:
													echo '<i class="fa fa-2x fa-user-plus"></i>';
													break;
													case 3:
													echo '<i class="fa fa-2x fa-envelope"></i>';
													break;
													case 4:
													echo '<i class="fa fa-2x fa-users"></i>';
													break;
													case 5:
													echo '<i class="fa fa-2x fa-user-times"></i>';
													break;
													case 6:
													echo '<i class="fa fa-2x fa-ban"></i>';
													break;
													case 7:
													echo '<i class="fa fa-2x fa-remove"></i>';
													break;
													
												}
											?>
										</div>
										<div class="col-md-9" style="padding-bottom:10px;">
											<h4><?=$n->adi ?></h4>										
											<h6 style="font-color: #555;font-weight:bold;font-size:11px; margin-top: -4px;"><i class="fa fa-clock-o"></i> &nbsp;<?=TimeStamp::timeConvert($n->tarih) ?></h6>
										</div>
											<?=$n->aciklama ?>
											<br/>
										
										<div class="clearfix"></div>
									</div>
								<?php
									}
									}
								  ?>
								  <a href="<?php echo baseurl('profil/tumbildirimler'); ?>" class="btn btn-success" style=" width:100%;">Tümünü Gör</a>
						    </div>
						  </ul>
						</div>
						<a class="btn btn-info pull-right" style="margin-left: 5px;" href="<?=baseurl('aktivite/ekle');?>"><i class="fa fa-paper-plane-o"></i> Aktivite Oluştur</a> 
						
						
						<span></span>
					<?php } ?>
					</div>
						</div>
					</div>
			</div>
		</div>
	<?php if(Uri::segment(1)=="home" || Uri::segment(1)==""){ ?>
		
		<div id="map" style="width: 100%; height: 400px; margin-top: 30px;"></div>
		  
		  
		  
	<?php } ?>

		<?php echo $sayfa; ?>	
	<?php if(Uri::segment(1)=="" || Uri::segment(1)=="home"){ ?>
		<div class="mobile-app">
						<div class="container">
							<div class="col-md-5 app-left">
								<img src="<?= baseurl(UPLOADS_DIR.'app.png'); ?>" alt="">
							</div>
							<div class="col-md-7 app-right">
								<h3> Activity Cyprus Mobil uygulamasını denediniz mi ?   </h3>
								<p> Activity Cyprus mobil uygulaması çok yakında Google Play ve App Store da yerini alacaktır. </p>
								<div class="app-buttons">
									<div class="app-button">
										<a href="<?php echo baseurl("app.apk") ?>"><img src="<?php echo baseurl(UPLOADS_DIR."1.png") ?>" alt=""></a>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
	<?php } ?>
		<footer style="border-top:5px solid #fff;">
		
			<div class="footer-bottom text-center">
			<div class="container">
				<div class="footer-logo">
					<a href="/activitycyprus"><span>Activity</span>Cyprus</a>
				</div>
				<div class="footer-social-icons">
					<ul>
						<li><a class="facebook" href="#"><span>Facebook</span></a></li>
						<li><a class="twitter" href="#"><span>Twitter</span></a></li>
						<li><a class="flickr" href="#"><span>Flickr</span></a></li>
						<li><a class="googleplus" href="#"><span>Google+</span></a></li>
						<li><a class="dribbble" href="#"><span>Dribbble</span></a></li>
					</ul>
				</div>
				<div class="copyrights">
					<p> © 2016 Activity Cyprus - Tüm Hakları Saklıdır.</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		</footer>
        <!--footer section end-->
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- js -->

<script type="text/javascript">

	$(".swal").click(function(){
		
		swallink = $(this).attr("data-link");
		
		swal({   
			title: "Silmek İstediğinize Eminmisiniz ?",   
			text: "Bu veri geri getirilemeyecek şekilde silinecek . ",   
			type: "warning",   
			showCancelButton: true,   
			cancelButtonText:"İptal",
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "İçeriği Sil",   
			closeOnConfirm: false 
		}, function(){   
			
			window.location.href = swallink;
			
		});
		
		return false;
		
	});

</script>
<script type="text/javascript">
		
	function linkgir(){

		var selected = $("select[name=aglar]");         
        var output = ""; 
        if(selected.val() != 0){ 
		
            output = selected.val(); 
		} 
		$("input[name=link]").removeAttr("disabled");
		$("input[name=link]").val(output);
	
	}

</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>bootstrap.min.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>bootstrap-select.js"></script>
<script>
  $(document).ready(function () {
    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>
<script type="text/javascript" src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.leanModal.min.js"></script>
<link href="<?php echo baseurl(STYLES_DIR) ?>jquery.uls.css" rel="stylesheet"/>
<link href="<?php echo baseurl(STYLES_DIR) ?>jquery.uls.grid.css" rel="stylesheet"/>
<link href="<?php echo baseurl(STYLES_DIR) ?>jquery.uls.lcd.css" rel="stylesheet"/>
<link href="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.datetimepicker.css" rel="stylesheet"/>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<!-- Source -->
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.uls.data.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.uls.data.utils.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.uls.lcd.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.uls.languagefilter.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.uls.regionfilter.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.uls.core.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.datetimepicker.min.js"></script>

<script src="<?php echo baseurl(SCRIPTS_DIR) ?>friends.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>dropzone.js"></script>
<link rel="stylesheet" href="http://www.dropzonejs.com/css/dropzone.css?v=1468421417" />

<link href="<?php echo baseurl(STYLES_DIR) ?>comment.css" rel="stylesheet"/>
 <script src="<?php echo baseurl(SCRIPTS_DIR) ?>rater.js"></script> 
 <script src="<?php echo baseurl(SCRIPTS_DIR) ?>uls-trigger.js"></script> 
 <script src="<?php echo baseurl(SCRIPTS_DIR) ?>main.js"></script>
 <script src="//api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>

	<script>
	function getCurrentPos(pos){
		var position = pos.coords;
		window.location = '<?php echo baseurl('home/konum'); ?>/' + position.latitude + ',' + position.longitude;
	}
	</script>

<style>
.sa-custom{width: 400px !important; height: 200px !important;}
</style>

			<script>
			$(document).ready(function(){
				$(".like-btn").click(function(){
					var id = $(this).attr("data-id");
					var el = $(this);
					$.ajax({
						type: 'post',
						dataType: 'json',
						url:'<?=baseurl('detay/like') ?>',
						data: "id="+id,
						success: function(data){
							if(data.oncedenOylamismi == 1)
							{
								swal("Önceden oyladınız.","","error");
							}
							else
							{
								if(data.durum == 0){
									swal({title:"Giriş yapılmadı.",type:"error"},function(){
										window.location = '<?=baseurl("login") ?>';
									});
								}
								else
								{
									el.children(".sonuc").html(data.sayi);
									swal({title: "Oylamanız kaydedildi.",   timer: 1500,   showConfirmButton: true, type: "success"});
								}
							}
						}
					});
				});
				
				$(".dislike-btn").click(function(){
					var id = $(this).attr("data-id");
					var el = $(this);
					$.ajax({
						type: 'post',
						dataType: 'json',
						url:'<?=baseurl('detay/dislike') ?>',
						data: "id="+id,
						success: function(data){
							if(data.oncedenOylamismi == 1)
							{
								swal("Önceden oyladınız.","","error");
							}
							else
							{
								if(data.durum == 0){
									swal({title:"Giriş yapılmadı.",type:"error"},function(){
										window.location = '<?=baseurl("login") ?>';
									});
									
								}
								else
								{
									el.children(".sonuc").html(data.sayi);
									swal({   title: "Oylamanız kaydedildi.",   timer: 1500,   showConfirmButton: true, type: "success"});
								}
							}
						}
					});
				});
				
				
			});
			
			
			
			$(document).ready(function(){
			$("#dropzonee").dropzone({ url: "<?php echo baseurl('aktivite/resUpload'); ?>"});
			$("#ucretdurumu").click(function(){
				$("#ucret").fadeToggle(100);
			});
			
			$("#konumbtn").click(function(){
				swal({   title: "Konum bekleniyor!",   text: "Tarayıcınızın konum ayarına izin veriniz.",   imageUrl: "<?php echo baseurl(UPLOADS_DIR."konumizni.jpg"); ?>"  });
				if(navigator.geolocation)
					navigator.geolocation.getCurrentPosition(getCurrentPos);
			});
			
		});
			
			$(document).ready(function(){
				$(".arkadaslikKabul").click(function(){
					var bu = $(this);
					$.ajax({
						type: 'post',
						dataType: "json",
						url:'<?=baseurl('profil/arkadasKabul') ?>',
						data: 'userid=' + bu.attr("data-uid") + "&gonderenid=" + bu.attr("data-gid") + "&id=" + bu.attr("data-id") + "&bildirimid=" + bu.parent().parent().attr("data-id"),
						success: function(data){
							if(data.durum == 1){
								swal({title: "Arkadaş eklendi.",type:"success",timer:1500},function(){
									window.location.href="";
								});
							}
							else if(data.durum == 0){
								swal("arkadaş olunurken hata oluştu","","error");
							}
						},
						error: function(data){
							swal("İnternet bağlantınızı kontrol edin","","error");
						}
						
					});
				});
				
				$(".arkadaslikReddet").click(function(){
					var bu = $(this);
					$.ajax({
						type: 'post',
						dataType: "json",
						url:'<?=baseurl('profil/arkadasReddet') ?>',
						data: 'userid=' + bu.attr("data-uid") + "&gonderenid=" + bu.attr("data-gid") + "&id=" + bu.attr("data-id") + "&bildirimid=" + bu.parent().parent().attr("data-id"),
						success: function(data){
							if(data.durum == 1){
								swal({title: "Arkadaş reddedildi.",type:"success",timer:1500},function(){
									window.location.href="";
								});
							}
							else if(data.durum == 0){
								swal("arkadaş olunurken hata oluştu","","error");
							}
						},
						error: function(data){
							swal(" İnternet bağlantınızı kontrol edin","","error");
						}
						
					});
					
				});
				
			});

		<?php
			## BÜYÜK HARİTA AKTİVİTE/EKLE SAYFASINDA DEİLSE ÇEKİLSİN
			if(Uri::segment(1)=="" || Uri::segment(1)=="home"){
				
				?>
					ymaps.ready(initForHome);
		

							function initForHome () {
							
							var defaultCoords = [35.166170, 33.376186];
							
							var myMap2 = new ymaps.Map('map', {
								// When initializing the map, you must specify its center and the zoom factor.
								center:  defaultCoords, // Москва
								zoom: 10
							}, {
								searchControlProvider: 'yandex#search'
							});
					
					
					<?php
						$marks = 0;
						$kordinatlar = Bildirim::haritaCek()->result();
						foreach($kordinatlar as $k){
							?>
							
							 var p<?=$marks ?> = new ymaps.Placemark([ <?=$k->lat.",".$k->lng ?>], {iconContent: '<?=$k->baslik ?>'}, {
									draggable : false,  
									preset: "islands#blueStretchyIcon"  
							 });
							 p<?=$marks++ ?>.events.add("click",function(){
								  window.location = "<?=baseurl($k->seo_link."/".$k->id) ?>";
							 });
							 
							<?php
						}
						
						echo ' myMap2.geoObjects';
						
						for($i = 0; $i < $marks; $i++){
							echo '.add(p'.$i.')';
						}
						echo ";";
						
					?>
					

						
					}
				
				<?php
			}
		?>
		
		</script>
		<style type="text/css">
			.rating,.rate-base-layer,.rate-select-layer,.rate-hover-layer{
				font-size: 35px;
			}
		</style>
		<link rel="stylesheet" href="<?php echo baseurl(STYLES_DIR); ?>flexslider.css" media="screen" />
		  <script defer src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.flexslider.js"></script>
		  <style type="text/css">
		  
		  	.search-form .form-group {
		  	  float: right !important;
		  	  transition: all 0.35s, border-radius 0s;
		  	  width: 32px;
		  	  height: 32px;
		  	  background-color: #fff;
		  	  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
		  	  border-radius: 25px;
		  	  border: 1px solid #ccc;
		  	}
		  	.search-form .form-group input.form-control {
		  	  padding-right: 20px;
		  	  border: 0 none;
		  	  background: transparent;
		  	  box-shadow: none;
		  	  display:block;
		  	}
		  	.search-form .form-group input.form-control::-webkit-input-placeholder {
		  	  display: none;
		  	}
		  	.search-form .form-group input.form-control:-moz-placeholder {
		  	  /* Firefox 18- */
		  	  display: none;
		  	}
		  	.search-form .form-group input.form-control::-moz-placeholder {
		  	  /* Firefox 19+ */
		  	  display: none;
		  	}
		  	.search-form .form-group input.form-control:-ms-input-placeholder {
		  	  display: none;
		  	}
		  	.search-form .form-group:hover,
		  	.search-form .form-group.hover {
		  	  width: 100%;
		  	  border-radius: 4px 25px 25px 4px;
		  	}
		  	.search-form .form-group span.form-control-feedback {
		  	  position: absolute;
		  	  top: -1px;
		  	  right: -2px;
		  	  z-index: 2;
		  	  display: block;
		  	  width: 34px;
		  	  height: 34px;
		  	  line-height: 34px;
		  	  text-align: center;
		  	  color: #3596e0;
		  	  left: initial;
		  	  font-size: 14px;
		  	}

		  </style>
