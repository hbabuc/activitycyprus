<div class="container" style="margin-top:100px;">
	<div class="panel panel-success">
	  <div class="panel-heading">Hangi Arkadaşlarını Aktiviteye Davet Etmek İstersin ? </div>
	  <div class="panel-body" style="padding:20px;">
	  <form action="<?php  echo baseurl('aktivite/arkadas_form'); ?>" method="POST">
	    <?php   
	    foreach ($arkadaslar as $row) { 
	    	if ( $row->userid == User::id()):
	    		?>
	    		  <div class="col-lg-3">
	    		    <div class="input-group">
	    		      <span class="input-group-addon">
	    		        <input type="checkbox" aria-label="..." name="arkadas[<?=$row->gonderenidsi?>]">
	    		      </span>
	    		      <img src="<?=baseurl(UPLOADS_DIR).$row->gonderenimg ?>" style="height:100px; width:100px;">
	    		      <?=$row->gonderenadisoyadi; ?>
	    		    </div><!-- /input-group -->
	    		  </div><!-- /.col-lg-6 -->
	    		<?php 	
	    		else: ?>

	    		<div class="col-lg-3">
	    			  <div class="input-group">
	    			    <span class="input-group-addon">
	    			      <input type="checkbox" aria-label="..." name="arkadas[<?=$row->useridsi?>]">
	    			    </span>
	    			    <img src="<?=baseurl(UPLOADS_DIR).$row->userimage ?>" style="height:100px; width:100px;">
	    			    <?=$row->useradisoyadi; ?>
	    			  </div><!-- /input-group -->
	    			</div><!-- /.col-lg-6 -->
	    			<?php
	    		endif;
	    	}
	    ?>
	  </div>
	  <div class="panel-footer"><input type="submit" value="Davet Et" class="btn btn-info"></div>
	  
	  </form>
	</div>

</div>