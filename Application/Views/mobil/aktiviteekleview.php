<?php date_default_timezone_set('Europe/Istanbul'); ?>
<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'materialize.min.css') ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'bootalert.css') ?>"  media="screen,projection"/>
	<link href="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.datetimepicker.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
</head>
<body>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'materialize.min.js') ?>"></script>
	<script src="//api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>
	<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.datetimepicker.min.js"></script>
	  
	  
	  
	<form method="POST" action="<?php echo baseurl('service/aktiviteEkle')?>">
		<div id="map" style="width: 100%; height: 300px; position:relative;" ></div>
		<?php
			$allpost = json_decode(Session::select("postall"));
			
			 $postback = Session::select("PostBack");
			 if($postback){
				$postback = json_decode($postback); 
			 }
		?>
		<div class="container" style="padding: 10px;">
			<div class="row">
				<div class="input-field col s12">
					<label for="email">^^ Haritadan aktiviteniz için adres işaretleyin ^^</label>
				</div>
				<div class="input-field col s12">
					<?=Warning::get(); ?>
				</div>
			</div>
			<br/>
			<input type="hidden" name="lat" id="maplat" value="">
			<input type="hidden" name="lng" id="maplng" value="">
			<br/>
			<div class="row">
				<div class="input-field col s12">
					<select name="aktivite_kat">
						<?php foreach($kategoriler as $row): ?>
							<option value="<?=$row->id?>"><?=$row->adi ?></option>
						<?php endforeach; ?>
					</select>
					<label>Kategori Seçiniz</label>
				</div>
			</div>
			<br/>
			<div class="row">
				<div class="input-field col s12">
					<div class="switch">
						<label>
							<input type="checkbox" id="kriterdurumu" value="0"> 
							<span class="lever"></span>Daha Fazla Kriter
						</label>
					</div>
				</div>
			</div>
			<br/>
			<br/>
			<br/>
			<div class="kriter" style="display:none;">
				<div class="row">
					<div class="input-field col s12">
						<select name="yas_aralik">
							<option value="0">Lütfen Bir Yaş Grubu Seçin</option>
							<?php  foreach($yaslar as $row): ?>
								<option value="<?=$row->id?>"><?=$row->baslangic?>-<?=$row->bitis?></option>
							<?php endforeach; ?>
						</select>
						<label>Yaş Aralığı Seçiniz</label>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="input-field col s12">
						<input type="text" id="adres" name="adres" class="name" placeholder="Adres Tarifini Giriniz" <?php if(isset($postback->adres)) echo 'value="'.$postback->adres.'"'; ?>>
						<label for="email">Adres Tarifi</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<select name="cinsiyet">
							<option value="0">Herkes</option>
							<option value="1">Sadece Erkek</option>
							<option value="2">Sadece Bayan</option>
						</select>
						<label>Kimler gelebilir ?</label>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="input-field col s12">
						<input style="display:none; margin-top: 50px;" type="text" id="ucret" name="ucret" class="name" placeholder="Kişi başı ücret" <?php if(isset($postback->ucret)) echo 'value="'.$postback->ucret.'"'; ?>>
						<label for="ucret">Aktivitem ...</label>  
						<div class="switch" style="float:left; margin-left: 20px; ">
							<label>
								Ücretsiz
								<input type="checkbox" id="ucretdurumu" name="ucretdurumu" value="0"> 
								<span class="lever"></span>
								Ücretli
							</label>
						</div>
					</div>
				</div>
				<br/><br/>
				<div class="row">
					<div class="input-field col s12">
						<input type="text" name="kapasite" class="name" placeholder="Aktivitinize kaç kişinin katılmasını istiyorsunuz ? " <?php if(isset($postback->kapasite)) echo 'value="'.$postback->kapasite.'"'; ?>>
						<label for="email">Kapasite</label>
					</div>
				</div>
				<br/>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input type="text" name="baslik"  placeholder="Aktivite Başlığı" <?php if(isset($postback->baslik)) echo 'value="'.$postback->baslik.'"'; ?>>
					<label for="email">Başlık</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<textarea class="materialize-textarea" placeholder="Aktiviteniz açıklayınız" name="aciklama"><?php if(isset($postback->aciklama)) echo $postback->aciklama; ?></textarea>
					<label for="aciklama">Aktivite Açıklaması</label>
				</div>
				</div>
			<br/>	
			<div class="row">
				<div class="col s12">
					<label for="email">Başlangıç Tarihi</label>
				</div>
				<div class="input-field col s12">
					<input type="date" name="bastarih" placeholder="Tarih seçmek için tıklayın." <?php if(isset($postback->bastarih)) echo 'value="'.$postback->bastarih.'"';  else  echo 'value="'.date("Y-m-d").'"'; ?>>
					<label for="email"></label>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<label for="email">Başlangıç Saati</label>
				</div>
				<div class="input-field col s12">
					<input type="time" name="bastarihsaati"  placeholder="Tarih seçmek için tıklayın." <?php if(isset($postback->bastarihsaati)) {echo 'value="'.$postback->bastarihsaati.'"'; }else{ echo 'value="'.date("H:i").'"';} ?>>
					<label for="email"></label>
				</div>
			</div>
			<br/>
			<div class="row">
				<div class="col s12">
					<label for="email">Bitiş Tarihi</label>
				</div>
				<div class="input-field col s12">
					<input type="date" name="bittarih" placeholder="Tarih seçmek için tıklayın." <?php if(isset($postback->bittarih)) echo 'value="'.$postback->bittarih.'"'; else  echo 'value="'.date("Y-m-d").'"'; ?>>
					<label for="email"></label>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<label for="email">Bitiş Saati</label>
				</div>
				<div class="input-field col s12">
					<input type="time" name="bittarihsaati" placeholder="Tarih seçmek için tıklayın." <?php if(isset($postback->bittarihsaati)) echo 'value="'.$postback->bittarihsaati.'"'; else echo 'value="'.date("H:i",(strtotime(date("H:i")) + 60*60)).'"'; ?>>
				</div>
			</div>
			<br><br>	
			<center>
				<div class="row">
					<button class="btn waves-effect waves-light btn-large col s12" type="submit">Aktiviteyi Ekle
					</button>
				</div>
			</center>
			<br/><br/><br/>
		  </div>
	</form>
					
	<script type="text/javascript">
		
		var myMap;
		var position,isMapsLoaded = false;
		ymaps.ready(init);
		

		function init () {
			
			isMapsLoaded = true;
			<?php
			if(isset($allpost->lat) && isset($allpost->lng)){
				?>
				var position = [<?=$allpost->lat.','.$allpost->lng ?>]; 
				<?php
			}
			else
			{
				?>
				var position = [39.144467065269545, 35.05445864369768];
				<?php
			}
			
			?>
			
			myMap = new ymaps.Map('map', {
				center:  position,
				zoom: 6
			}, {
				searchControlProvider: 'yandex#search'
			});
			
			if(position){
				
				var myPlacemark = new ymaps.Placemark([position[0],position[1]], {iconContent: 'Etkinliğiniz burada!'}, {
						draggable : false,  
						preset: "islands#blueStretchyIcon"  
				 });

				myMap.geoObjects.add(myPlacemark);
				
				$("#maplat").val(position[0]);
				$("#maplng").val(position[1]);
				
			}
			

			myMap.events.add('click', function (e) {
				
					myMap.geoObjects.removeAll();
				// Getting the click coordinate
					var coords = e.get('coords');
					var myPlacemark = new ymaps.Placemark([coords[0],coords[1]], {iconContent: 'Etkinliğiniz burada!'}, {
							draggable : false,  
							preset: "islands#blueStretchyIcon"  
					 });
					 

					myMap.geoObjects.add(myPlacemark);

					$("#maplat").val(coords[0]);
					$("#maplng").val(coords[1]);
				//alert(coords.join(', '));
				
			});

		}
		
		$(document).ready(function(){
			
			$("#datepicker").datetimepicker({
				
				format:'d-m-Y H:i',
				minDate: 0,
				minTime: 0,
				lang: 'tr'
				
			});
		
			$("#datepicker2").datetimepicker({
				
				format:'d-m-Y H:i',
				onShow: function(ct){
					this.setOptions({
						formatDate:'d-m-Y',
						formatTime:'H:i',
						minDate: $("#datepicker").val().split(' ')[0],
						minTime: $("#datepicker").val().split(' ')[1].trim()
						});
				},
				lang: 'tr'
				
			});

			
			$("#ucretdurumu").click(function(){
				
					$("#ucret").stop().slideToggle(1000);
					
			});

			$("#kriterdurumu").click(function(){
				
					$(".kriter").stop().slideToggle(1000);
					
			});
			
			$("select").material_select();
			
		});
		
	</script>			
</body>
</html>