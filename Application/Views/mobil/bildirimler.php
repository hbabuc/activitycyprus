﻿<?php $postall = json_decode(Session::select("postall")); ?>
<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'materialize.min.css') ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'bootalert.css') ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'../font-awesome/css/font-awesome.css') ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'sweetalert.css') ?>"  media="screen,projection"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
</head>
<body>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'materialize.min.js') ?>"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'sweetalert.min.js') ?>"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'sweetalert-dev.js') ?>"></script>
	<script src="//api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>	
	
	
	<div class="container" style="margin-top:20px;">
		<div class="row">
			<div style="padding:0px 30px">
				<?php $row = User::row(); ?>
				
				<?php $notifications = Bildirim::getWithLimit($row->id,10); ?>
						
					<?php if($notifications->totalRows() < 1){ ?>
					
							<div class="bildirim">
								<div class="col s3 center-align">
									<i class="fa fa-2x fa-times-circle"></i>
								</div>
								<div class="col s9" style="padding-bottom:10px;">
									<h4></h4>
									<p>Yeni bildirim yok!</p>

								</div>
								<div class="clearfix"></div>
							</div>
							
						<?php }else{ ?>
							
							<?php foreach($notifications->result() as $n){ ?>
							
							<div class="bildirim" data-id="<?=$n->id ?>">
							
								<div class="col s3 center-align icon">
									<?php
									
										switch($n->tip){
											case 1:
											echo '<i class="fa fa-2x fa-rss"></i>';
											break;
											case 2:
											echo '<i class="fa fa-2x fa-user-plus"></i>';
											break;
											case 3:
											echo '<i class="fa fa-2x fa-envelope"></i>';
											break;
											case 4:
											echo '<i class="fa fa-2x fa-users"></i>';
											break;
											case 5:
											echo '<i class="fa fa-2x fa-user-times"></i>';
											break;
											
										}
										
									?>
								</div>
								
								<div class="col 9" style="padding-bottom:10px;">
									<p style="color:#444;font-weight:bold;"style="margin-top:-10px;"><?=$n->adi ?></p>										
									<p style="font-color: #555;font-weight:bold;font-size:11px; margin-top: -4px;"><i class="fa fa-clock-o"></i> &nbsp;<?=TimeStamp::timeConvert($n->tarih) ?></p>
								</div>
								<div class="col s12" data-id="<?=$n->id ?>">
									<?php 										
										
										$girdiler = array(
										
											'#<a class="davet" href="http://(.*?)/(.*?)/(.*?)">(.*?)</a>#',
											'#<a href="http://(.*?)/(.*?)">(.*?)</a>#',
										
										);
										
										$ciktilar = array(
											
											'<a class="davet" href="http://$1/service/detay/$3">$4</a>',
											'<a href="http://$1/service/profil/$2">$3</a>',
										
										);
										
										echo preg_replace($girdiler,$ciktilar,$n->aciklama);
										
										 
										
									 ?>
								</div>
								<br/>
									
								<div class="clearfix"></div>
							</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>	
	<style type="text/css">
		.bildirim{
			
			border-bottom : 1px solid #cecece;
			padding-bottom:20px;
			padding-top:20px;
			color:#555;
		}
		.bildirim .icon i {
			
			color:#2a6b8c;
			line-height:50px;
			
		}
	</style>

	<script type="text/javascript">
		$(document).ready(function(){
				$(".arkadaslikKabul").click(function(){

					var bu = $(this);
					$.ajax({
						type: 'post',
						dataType: "json",
						url:'<?=baseurl('profil/arkadasKabul') ?>',
						data: 'userid=' + bu.attr("data-uid") + "&gonderenid=" + bu.attr("data-gid") + "&id=" + bu.attr("data-id") + "&bildirimid=" + bu.parent().parent().attr("data-id"),
						success: function(data){
							if(data.durum == 1){
								swal({title: "Arkadaş eklendi.",type:"success",timer:1500},function(){
									window.location.href="";
								});
							}
							else if(data.durum == 0){
								swal("Arkadaş olunurken hata oluştu","","error");
							}
						},
						error: function(data){
							swal("İnternet bağlantınızı kontrol edin","","error");
						}
						
					});
				});
				
				$(".arkadaslikReddet").click(function(){
				
					var bu = $(this);

					$.ajax({
						type: 'post',
						dataType: "json",
						url:'<?=baseurl('profil/arkadasReddet') ?>',
						data: 'userid=' + bu.attr("data-uid") + "&gonderenid=" + bu.attr("data-gid") + "&id=" + bu.attr("data-id") + "&bildirimid=" + bu.parent().parent().attr("data-id"),
						success: function(data){
							if(data.durum == 1){
								swal({title: "Arkadaş reddedildi.",type:"success",timer:1500},function(){
									window.location.href="";
								});
							}
							else if(data.durum == 0){
								swal("Arkadaş olunurken hata oluştu","","error");
							}
						},
						error: function(data){
							swal("İnternet bağlantınızı kontrol edin","","error");
						}
						
					});
					
				});
				
			});
	</script>
</body>
</html>