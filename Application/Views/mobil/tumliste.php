<?php
	//print_r(Session::selectAll());

	 $postback = Session::select("postback");
	 
	 if($postback){
		$row = json_decode($postback); 
	 }
?>
<!DOCTYPE html>
<html>
    <head>
	
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
		<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'materialize.min.css') ?>"  media="screen,projection"/>
		<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'bootalert.css') ?>"  media="screen,projection"/>


		<link href="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.datetimepicker.css" rel="stylesheet"/>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
    </head>
    <body>
		
		<pre>
			<?php /* print_r($aktiviteler); */ ?>
		</pre>
		<?php foreach($aktiviteler as $aktivite){ ?>
			<div class="container">
				<div class="card">
					<div class="card-image waves-effect waves-block waves-light" style="position:relative;">
						<img class="activator" src="<?php echo baseurl(UPLOADS_DIR.$aktivite->resmi) ?>">
						<div style="position:absolute;right:0px;top:20px; background:rgba(0,0,0,.7);padding:10px 20px;color:#fff;font-weight:bold;"> <?php 
								
								if($aktivite->distances<1000){
									
									echo $aktivite->distances." Metre";
									
								}else{
									
									echo round($aktivite->distances/1000)." KM";
									
								}
							
							?></div>
					</div>
					<div class="card-content">
						<span class="card-title activator grey-text text-darken-4"><?php echo $aktivite->baslik ?><i class="material-icons right">more_vert</i></span>
						<p>
							<div class="row" style="margin-top:40px;">
							<a class="waves-effect waves-light btn col s12" href="<?php echo baseurl("service/detay/".$aktivite->id) ?>">Aktiviteye Git</a></div>
						</p>
						</div>
					<div class="card-reveal">
						<span class="card-title grey-text text-darken-4"><?php echo $aktivite->baslik ?><i class="material-icons right">close</i></span>
						<p><?php echo $aktivite->aciklama ?></p>
						<div class="row" style="bottom:0px;" >
							<a class="waves-effect waves-light btn col s12 " href="<?php echo baseurl("service/detay/".$aktivite->id) ?>"><i class="material-icons left">send</i>Detaya Git</a>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="yenile">
		
		</div>
		
		
		
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'materialize.min.js') ?>"></script>
		<script src="//api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>
		<script type="text/javascript">
		
			var page = 1;
			
			var iceriksayi = 12;
			
			var kactanbaslar = page * iceriksayi;
				
			$(window).scroll(function(){
				
				console.log("Yukarudan Yüksekliği : "+ $(window).scrollTop());
				
				var mesafe = $("body").height() - $(window).scrollTop();
				
				
				
				if(mesafe < 1000){
					
					$.ajax({
						
						type:"POST",
						url:"<?=baseurl("service/eksayfa/") ?>"+kactanbaslar,
						success:function(cevap){
							
							$(".yenile").html(cevap);
							$(".yenile").eq(0).removeClass("yenile");
							
						}
						
					});
					
					page++; 
					
					kactanbaslar = page * iceriksayi;
					
				}
				
			});
						
			
		</script>
    </body>
</html>