<?php $postall = json_decode(Session::select("postall")); ?>
<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'materialize.min.css') ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'bootalert.css') ?>"  media="screen,projection"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
</head>
<body>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'materialize.min.js') ?>"></script>
	<script src="//api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>	
	<div id="map" style="width: 100%; height:100% !important;position:absolute; "></div>
	
	<script type="text/javascript">
	
		$(document).ready(function(){
		  $('.carousel').carousel();
		});
		  $(document).ready(function(){
			$('.materialboxed').materialbox();
		  });
        
	
		
		ymaps.ready(initForHome);


		function initForHome () {
				
				var defaultCoords = [<?php if($postall->lat) echo $postall->lat ?>,<?php if($postall->lng) echo $postall->lng ?>];
				
				var myMap2 = new ymaps.Map('map', {
					// When initializing the map, you must specify its center and the zoom factor.
					center:  defaultCoords, // ??????
					zoom: 11
				}, {
					searchControlProvider: 'yandex#search'
				});
		
				var myPlacemark1 = new ymaps.Placemark([<?php if(isset($postall->lat)){ echo $postall->lat; } ?>,<?php if(isset($postall->lng)){ echo $postall->lng; } ?>], {iconContent: 'Senin Konumun'}, {
						draggable : false,  
						preset: "islands#greenStretchyIcon"  
				 });

		
				<?php
					$marks = 0;
					$kordinatlar = $aktiviteler;
					foreach($kordinatlar as $k){
						?>
						
						 var p<?=$marks ?> = new ymaps.Placemark([ <?=$k->lat.",".$k->lng ?>], {iconContent: '<?=$k->baslik ?>'}, {
								draggable : false,  
								preset: "islands#blueStretchyIcon"  
						 });
						 p<?=$marks++ ?>.events.add("click",function(){
							  window.location = "<?=baseurl("service/detay/".$k->id) ?>";
						 });
						 
						<?php
					}
					
					echo ' myMap2.geoObjects';
					
					for($i = 0; $i < $marks; $i++){
						echo '.add(p'.$i.')';
					}
					echo ".add(myPlacemark1)";
					echo ";";
					
				?>

			
		}
				

		
		
	</script>			
</body>
</html>