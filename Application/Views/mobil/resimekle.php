<?php
	//print_r(Session::selectAll());

	 $postback = Session::select("postback");
	 
	 if($postback){
		$row = json_decode($postback); 
	 }
?>
<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'materialize.min.css') ?>"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'bootalert.css') ?>"  media="screen,projection"/>
      
		<link rel="stylesheet" href="http://www.dropzonejs.com/css/dropzone.css?v=1468421417" />
		
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'materialize.min.js') ?>"></script>
	  <script src="<?php echo baseurl(SCRIPTS_DIR) ?>dropzone.js"></script>
		<div class="container">
		
		<div class="input-field col s12">
		  <?=Warning::get(); ?>
		</div>
		<form method="POST" style="background: url(<?=baseurl(UPLOADS_DIR.'uploadd.png') ?>) no-repeat top center" action="<?php echo baseurl('service/resUploads/'); ?>"  class="dropzone">
		<br/>
		<br/>
		  <div class="fallback" >
			<input name="file" type="file"  id="uploaddd" multiple="multiple" />
		  </div>
		</form>
		
		<br/>
		
		<form method="POST" enctype="multipart/form-data">
		<br/>
		<br/>
		  <div >
			<input name="file" type="file"  id="uploaddd" multiple="multiple" />
		  </div>
		</form>
		
		<br/><br/>
		<center>
		<a href="<?php echo baseurl('service/control') ?>">
		   <button class="btn waves-effect waves-light" name="action">Resimleri Ekle
			<i class="material-icons right">send</i>
		  </button>
		  </a>
		</center>
		</div>
		
	<script type="text/javascript">
		$(document).ready(function(){
			$("#dropzonee").dropzone({ url: "<?php echo baseurl('service/resUpload'); ?>"});
			$("#uploaddd").on("touchstart",function(){
				$(this).trigger("click");
			});
		});
	</script>			
    </body>
  </html>
