<?php 
	$postall = json_decode(Session::select("postall"));
	date_default_timezone_set('Europe/Istanbul'); 
		
?>
<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'materialize.min.css') ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'bootalert.css') ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'sweetalert.css') ?>"  media="screen,projection"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
</head>
<body style="padding-bottom:100px;">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'materialize.min.js') ?>"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'sweetalert.min.js') ?>"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'sweetalert-dev.js') ?>"></script>
	<script src="<?php echo baseurl(SCRIPTS_DIR) ?>rater.js"></script> 
	<script src="//api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>	
	<?php if($totalRows<1){ ?>
		<div class="container">
			<?php if(Uri::segment(2)=="detayRasgele"){ ?>
				<h5 style="text-align:center">5 km yakınınızda başlamış hiç bir aktivite bulunamadı</h5>
				
			<?php }else{ ?>
				<h5 style="text-align:center">Bir sebepten dolayı iceriğe ulaşılamıyor</h5>
			<?php } ?>
		</div>
	<?php }else{ ?>
	<div id="map" style="width: 100%; height: 300px; position:relative;" ></div>
	
	<?php if(Uri::segment(2)=="detayRasgele"){ ?>
		<div style="position:fixed;bottom:0px;width:100%;z-index:99">
			<div class="row" style="margin-bottom:0px;">
				<a href="" class="btn waves-effect waves-light col s12"  style="height:50px;line-height:45px;"><i class="material-icons">track_changes</i> <span style="margin-top:-50px;">Random Getir</span></a>
			</div>
		</div>
	<?php } ?>
	<div class="container" style="margin-top:20px;">
		<div class="row">
			<div class="col s8">
				<a class="chip" href="<?php echo baseurl("service/profil/".$row->kadi) ?>">
					<img src="<?php echo baseurl(UPLOADS_DIR.$row->kresim) ?>" alt="Contact Person">
					<?=ucfirst(strtolower($row->adisoyadi)) ?>
				</a>
			</div>
			<div class="col s4 right-align">
				<a href="" class="chip">
					<?=ucfirst(strtolower($row->katseo)) ?>
				</a>
			</div>
			<div class="col s12" style="position:relative;">
				<img src="<?php echo baseurl(UPLOADS_DIR.$row->aktiviteresmi) ?>" class="col s12">
				<div style="position:absolute;right:0px;top:20px; background:rgba(0,0,0,.7);padding:10px 20px;color:#fff;font-weight:bold;"> 
					
					<?php
								
						if($row->distances<1000){
							
							echo $row->distances." Metre";
							
						}else{
							
							echo round($row->distances/1000)." KM";
							
						}
					
					?>
					
				</div>
			</div>
			<div class="col s12" style="padding-bottom:40px;">
				<h3 style="color:#555" class="center-align"><?=$row->baslik ?></h3>
				<p class="center-align"><?=$row->aciklama ?></p>
			</div>
			<div class="col s12">
			
				<?php 
				$a = "6";
				if(User::check() && ($row->katilmismi < 1 && $row->rezervasyonuVarmi < 1 && (strtotime(date("d-m-Y H:i:s")) < strtotime($row->bastarih)))) 
					echo '<button class="waves-effect waves-light btn-large col s6 " id="rezervasyon">Rezervasyon</button>';
				
				else if(User::check() && ($row->katilmismi < 1 && (strtotime(date("d-m-Y H:i:s")) > strtotime($row->bastarih) && (strtotime(date("d-m-Y H:i:s")) < strtotime($row->bittarih))))) 
					echo '<button class="waves-effect waves-light btn-large col s6 " id="katil">Katıl</button>';
				
				else if(User::check() && ($row->rezervasyonuVarmi > 0 && (strtotime(date("d-m-Y H:i:s")) < strtotime($row->bastarih)))) 
					echo '<button class="waves-effect waves-light btn-large col s6 red " id="iptalet">İptal Et</button>';
				else 
					$a = "12";
				?>
				<div class="col s<?=$a ?>">
					<a href="javascript:;" class="waves-effect waves-light btn-large col s12 red" id="sikayet">Şikayet Et</a>
				</div>
			</div>
			<div class="col s12" style="margin-top:50px;">
				<h5 class="center-align" style="color:#555;margin-bottom:50px;"><span style="padding-bottom:20px;border-bottom:2px solid #555;">Yorumlar</span></h5>
				<?php foreach($yorumlar as $yorum){ ?>
					<div class="col s12 m8 offset-m2 l6 offset-l3">
						<div class="card-panel grey lighten-5 z-depth-1">
							<div class="row valign-wrapper">
								<div class="col s2">
									<a href="<?php echo baseurl("detay/profil/".$yorum->kadi) ?>">
										<img src="<?=baseurl(UPLOADS_DIR.$yorum->kullaniciresim) ?>" alt="" class="circle responsive-img">
									</a>
								</div>
								<div class="col s10">
									<span class="black-text">
										<h6 style="font-weight:bold;"><a style="color:#444;" href="<?php echo baseurl("detay/profil/".$yorum->kadi) ?>"><?=$yorum->adisoyadi ?> </a>- <small><?php echo TimeStamp::timeConvert($yorum->zaman); ?></small></h6>
										<?=$yorum->yorum ?>
									</span>
								</div>
							</div>
						</div>
					</div> 
				<?php 
				} 
				
				if($row->katilmismi > 0) echo '<button type="button" id="yorumekle" class="waves-effect waves-light btn-large col s12" data-toggle="modal" data-target="#yorummodal">Yorum Ekle</button>'; 
				
				?>
			</div>
			<div>
			  <!-- ÖDEME -->
			  <div id="modal1" class="modal">
				<div class="modal-content">
				  <h4>Ödeme Noktası</h4>
				  <center><img style="width:100%" src="<?=baseurl(UPLOADS_DIR.'master.png') ?>"></center>
				</div>
				<div class="modal-footer">
				  <a href="#!" id="odeme" class=" modal-action modal-close waves-effect waves-green btn-flat">Ödeme</a>
				</div>
			  </div>
			  
          <!-- Şikayet -->
			  <div id="sikayetmodal" class="modal">
				<div class="modal-content">
				  <h4>Şikayet Yaz</h4>
				  <br/><br/>
					<div id="sikayetekle">
						<select id="sikayettipi" class="input-field">
							<option value="1">Beni veya birini rahatsız ediyor</option>
							<option value="2">Uygunsuz içerik</option>
							<option value="3">Katılım</option>
						</select>
						<br/>
						<textarea placeholder="Şikayetiniz" name="addcomment" id="sikayettextarea" class="materialize-textarea input-field" cols="30" rows="5"></textarea>
						<br/>
					</div>
							
				</div>
				<div class="modal-footer">
				  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Kapat</a>
				  <a href="#!" id="sikayetgonder" class=" modal-action waves-effect waves-green btn-flat">Gönder</a>
				</div>
			  </div>
			  
			  <!-- Yorum -->
			  <div id="yorummodal" class="modal">
				<div class="modal-content">
				  <h4>Yorum Yaz</h4>
				  <br/>
					<?php
						if($oylama < 1){
							?>
							<center><span style="font-weight: bold;">Aktiviteyi değerlendirin :</span> 
							<div class="rating" style="font-size: 30px; " data-rate-value=0></div>
							</center>
							<?php
						}
					?>
					<br/>
					
					<form id="formm">
							<input type="hidden" id="yorumhidden" name="yorum"/>
							<input type="hidden" id="derecehidden" value="0" name="derece"/>
							<input type="hidden" value="<?=Uri::segment(-1) ?>" name="aktiviteid"/>
						  </form>
							
						<div id="addcomment">
							<textarea placeholder="Yorumunuz" name="addcomment" id="yorumtextarea" class=".input-field materialize-textarea" cols="30" rows="5"></textarea>
							<br/>
						</div>
							
							
				</div>
				<div class="modal-footer">
				  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Kapat</a>
				  <a href="#!" id="yorumgonder" class=" modal-action waves-effect waves-green btn-flat">Yorum Ekle</a>
				</div>
			  </div>
			  
          
			</div>
		</div>	
		
		
		
	</div>
	
	<?php
		if(User::check() && $row->userid == User::id()){
			?>
			<div style="z-index:9999; position: fixed; bottom: 0; width:100%;">
			<div class="row" style="margin-bottom: 0px !important;">
				<button class="col s6 waves-effect waves-light btn-large red">İçeriği Düzenle</button>
				<button class="col s6 waves-effect waves-light btn-large">Katılanları İncele</button>
				</div>
			</div>
			<?php
		}
	?>	
		
	
	
	<script type="text/javascript">
	
		$(document).ready(function(){
			$('select').material_select();
			$('.carousel').carousel();
			$('.materialboxed').materialbox();
			$("#sikayet").click(function(){
				$("#sikayetmodal").openModal();
			});
			$("#yorumekle").click(function(){
				$("#yorummodal").openModal();
			});
			
			$(".rating").rate({
				max_value: 10,
				step_size: 1
			});
			
		  });
        
	
		var myMapvar,rate;
		var position,isMapsLoaded = false;
		ymaps.ready(init);
		

		function init () {
			
			isMapsLoaded = true;
			
			var position = 
			<?php if(isset($row->lat) && isset($row->lng)) {echo '['.$row->lat.','.$row->lng.']'; } ?>
			
			myMap = new ymaps.Map('map', {
				center:  position,
				zoom: 6
			}, {
				searchControlProvider: 'yandex#search'
			});
			
				
			var myPlacemark = new ymaps.Placemark([position[0],position[1]], {iconContent: 'Etkinlik Konumu'}, {
					draggable : false,  
					preset: "islands#blueStretchyIcon"  
			 });

			myMap.geoObjects.add(myPlacemark);
			
			
			
			<?php
				
			if(isset($postall->lat)){
				?>
				var myPlacemark1 = new ymaps.Placemark([<?php if(isset($postall->lat)){ echo $postall->lat; } ?>,<?php if(isset($postall->lng)){ echo $postall->lng; } ?>], {iconContent: 'Senin Konumun'}, {
					draggable : false,  
					preset: "islands#greenStretchyIcon"  
				 }); 

				myMap.geoObjects.add(myPlacemark1);
				 
				ymaps.route([
					{ type: 'viaPoint', point: [<?=$postall->lat.",".$postall->lng ?>] },
					{ type: 'wayPoint', point: [position[0],position[1]] }
				], {
					mapStateAutoApply: true
				}).then(function (route) {
					route.getPaths().options.set({
						// you can make settings for route graphics
						strokeColor: 'e1172c',
						opacity: 0.9
					 });
					 // adding the route to the map
					 
					 myMap.geoObjects.add(route);
				 });
				
				<?php
			}
			
			?>
			

		}
		
		
		$(document).ready(function(){
				var hangiButon;
				
				$("#yorumgonder").click(function(){
					
					rate = $(".rating").rate("getValue").toString();
					
					if($("#yorumtextarea").val().trim() == "")
					{
						swal("Yorum alanı boş geçilemez!","","error");
					}
					else if($("#yorumtextarea").val().trim().length < 2){
						swal("Yorum alanı 2 karakterden az olamaz","","error");
					}
					else
					{
						$("#yorumhidden").val($("#yorumtextarea").val());
						$("#derecehidden").val(rate);
						$.ajax({
							type: 'post',
							dataType: "json",
							url:'<?=baseurl('detay/yorumgonder') ?>',
							data: $("#formm").serialize(),
							success: function(data){
								if(data.durum == 1){
									swal({title:"Yorumunuz eklendi.", type:"success"},function(){
										window.location.href = "";
									});
								}
								else if(data.durum == 2){ 
									swal("Yorum gönderilirken bir hata oluştu, lütfen daha sonra tekrar deneyiniz.","","error");
								}
								else if(data.durum == 0){
									swal({title:"Yorum göndermek için giriş yapmalısınız.",type:"error"},function(){
										window.location = "<?=baseurl("login") ?>";
									});	
								}
							}
						});
						
					}
					
				});
				
					//KATIL BUTONU
					
					$("#katil").click(function(){
						hangiButon = 1;
						<?php
							if($row->ucret != 0)
								echo '$("#modal1").openModal();';
							else
								echo '$("#odeme").trigger("click");';
						?>
						
					});
					
					$("#rezervasyon").click(function(){
						hangiButon = 0;
						<?php
							if($row->ucret != 0)
								echo '$("#modal1").openModal();';
							else
								echo '$("#odeme").trigger("click");';
						?>
						
					});
						
					$("#iptalet").click(function(){
							$.ajax({
								type: 'post',
								dataType: "json",
								url:'<?=baseurl('detay/rezervasyonIptal') ?>',
								data: "aktiviteid=<?=Uri::segment(-1) ?>",
								success: function(data){
									if(data.durum == 1){
										swal({title:"Rezervasyonu iptal ettin!",type:"success", timer: 1500},function(){
											window.location.href="";
										});
									}
									else if(data.durum == 2)
									{
										swal({title:"Lütfen giriş yapınız.",text:"Girişe yönlendiriliyor.",type:"success"},function(){
											window.location ="<?=baseurl("login") ?>";
										});
									}
									else if(data.durum == 0)
									{
										swal("İptal ederken bir hata oluştu, lütfen daha sonra tekrar dene!", "","error");
									}
								}
							});
						});
						
						$("#odeme").click(function(){
							if(hangiButon == 1)
							{
								
								$.ajax({
									type: 'post',
									dataType: "json",
									url:'<?=baseurl('detay/katil') ?>',
									data: "aktiviteid=<?=Uri::segment(-1) ?>&lat=<?=$postall->lat ?>&lng=<?=$postall->lng ?>",
									success: function(data){
										if(data.durum == 1){
											swal({title:"Aktiviteye katıldın!",type:"success", timer: 1500},function(){
												window.location.href="";
											});
										}
										else if(data.durum == 2)
										{
											var uzaklik = (data.distance < 1000) ? Math.floor(data.distance) + " metre" : Math.floor(data.distance / 1000) + " km";
											
											swal({title: "Uzaktasınız",text:"Aktiviteden yaklaşık " + uzaklik + " uzaktasın, aktiviteye katılmak için aktivite adresinin en az 200 metre yakınında olmalısın!"});
										}
										else if(data.durum == 3)
										{
											swal("Aktivitenin limiti dolduğundan aktiviteye katılamazsın !", "","error");
										}
										else if(data.durum == 4)
										{
											swal({title:"Aktiviteye katılmak için kullanıcı girişi yapmalısın!",type:"error"},function(){
												window.location = '<?=baseurl("login") ?>';
											});
										}
										else if(data.durum == 0)
										{
											swal("Aktiviteye katılırken bir hata oluştu, lütfen daha sonra tekrar dene!", "","error");
										}
									}
								});
							}
							else
							{
								$.ajax({
									type: 'post',
									dataType: "json",
									url:'<?=baseurl('detay/rezervasyon') ?>',
									data: "aktiviteid=<?=Uri::segment(-1) ?>",
									success: function(data){
										if(data.durum == 1){
										swal({title:"Rezervasyonun Alındı!",type:"success", timer: 1500},function(){
											window.location.href="";
										});
										}
										else if(data.durum == 3)
										{
											swal("Aktivitenin limiti dolduğundan aktiviteye katılamazsın !", "","error");
										}
										else if(data.durum == 4)
										{
											swal({title:"Aktiviteye katılmak için kullanıcı girişi yapmalısın!",type:"error"},function(){
												window.location = '<?=baseurl("login") ?>';
											});
										}
										else if(data.durum == 0)
										{
											swal("Aktiviteye katılırken bir hata oluştu, lütfen daha sonra tekrar dene!", "","error");
										}
									}
								});
							
							}
							
						});
					
					
					$("#sikayetgonder").click(function(){
						if($("#sikayettextarea").val().trim().length < 5){
							swal("Şikayet alanı 5 harften küçük olamaz. Neden 5 diye sormayın bilmiyorum.");
						}
						else{
							$.ajax({
								type: 'post',
								dataType: "json",
								url:'<?=baseurl('detay/sikayetgonder') ?>',
								data: "aktiviteid=<?=Uri::segment(-1) ?>&tip=" + $("#sikayettipi").val() + "&aciklama=" + $("#sikayettextarea").val().trim(),
								success: function(data){
									if(data.durum == 1){
										swal({title:"Şikayetin alındı.",text:"En kısa zamanda yöneticiler tarafından değerlendirilip gerekeni yapılacak.",timer:1500,type:"success"},function(){
											window.location.href="";
										});
									}
									else if(data.durum == 2)
									{
										swal("Şikayet gönderilirken bir hata oluştu","","error");
									}
									else if(data.durum == 0){
										swal({title:"şikayet gönderebilmek için giriş yapmalısın.",type:"error"});
									}
								}
							});
						}
						
					});
			});
		
	</script>			
	<?php } ?>
</body>
</html>