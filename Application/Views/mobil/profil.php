
<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'materialize.min.css') ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?=baseurl(STYLES_DIR.'bootalert.css') ?>"  media="screen,projection"/>
	<link href="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.datetimepicker.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
</head>
<body style="background:#eeE;">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR.'materialize.min.js') ?>"></script>
	<script src="//api-maps.yandex.ru/2.1/?lang=tr_TR" type="text/javascript"></script>
	<script src="<?php echo baseurl(SCRIPTS_DIR) ?>jquery.datetimepicker.min.js"></script>
	  
	
	<div style="position:relative">
		<img src="<?php echo baseurl(UPLOADS_DIR) ?>profile-bg.png" style="postion:absolute;top:0px;left:0px; width:100%;" alt=""/>
		<div style="position:absolute;top:0px;left:0px;margin-top:20px;z-index:99;">
			<div class="container">
				<div class="row" style="color:#555;">
					<div class="col s8 offset-s2">
						<div style="background:#fff;padding:20px;border:1px solid #eee;">
							<img src="<?php echo baseurl(UPLOADS_DIR.$user->resmi) ?>" style="width:100%;" alt=""/>
						</div>
					</div>
					<div class="col s12 center-align" style="background:#fff;margin-top:20px;padding:20px;">
						<div class="col s12">
							<h3  style="border-bottom:1px solid #555;padding-bottom:30px; "><?php echo $user->adi." ".$user->soyadi ?></h3>
						</div>
						<div class="col s4 center-align" >
							<h4><?=$user->aktivitesayi ?> <i class="material-icons">room</i> </h4>
							<p>Aktiviteleri</p>
						</div>
						<div class="col s4 center-align">
							<h4><?=$user->aktivitesayi ?> <i class="material-icons">input</i></h4>
							<p>Katıldıkları</p>
						</div>
						<div class="col s4 center-align">
							<h4><?=$user->aktivitesayi ?> <i class="material-icons">comment</i></h4>
							<p>Yorumları</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>