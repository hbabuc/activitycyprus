<?php if(count($aktiviteler)>0){ ?>

	<?php foreach($aktiviteler as $aktivite){ ?>

		<div class="container">

			<div class="card">

				<div class="card-image waves-effect waves-block waves-light" style="position:relative;">

					<img class="activator" src="<?php echo baseurl(UPLOADS_DIR.$aktivite->resmi) ?>">

	

				</div>

				<div class="card-content">

					<span class="card-title activator grey-text text-darken-4"><?php echo $aktivite->baslik ?><i class="material-icons right">more_vert</i></span>

					<p>

						<div class="row" style="margin-top:40px;">

						<a class="waves-effect waves-light btn col s12" href="<?php echo baseurl("service/detay/".$aktivite->id) ?>">Aktiviteye Git</a></div>

					</p>

					</div>

				<div class="card-reveal">

					<span class="card-title grey-text text-darken-4"><?php echo $aktivite->baslik ?><i class="material-icons right">close</i></span>

					<p><?php echo $aktivite->aciklama ?></p>

					<div class="row" style="bottom:0px;" >

						<a class="waves-effect waves-light btn col s12 " href="<?php echo baseurl("service/detay/".$aktivite->id) ?>"><i class="material-icons left">send</i>Detaya Git</a>

					</div>

				</div>

			</div>

		</div>

	<?php } ?>

	<div class="yenile">

	

	</div>

<?php }else{ ?>

	<div class="container">

		

		<h4	style="color:#555;text-align:center;">Aktif Tüm Aktiviteleri Listelik .</h4>

	

	</div>

<?php } ?>