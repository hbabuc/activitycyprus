<?php
	$siteayari = json_decode($ayar->siteayari);

	$baslik = $siteayari->baslik;
	$aciklama = $siteayari->aciklama;

   header("Content-type: text/xml; charset=utf8");

	function xml_entities($string) {
		return str_replace(
			array("&",     "<",    ">",    '"',      "'"),
			array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), 
			$string
		);
	}
	
   echo '<?xml version="1.0" encoding="utf-8"?>
   <rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/">
   <channel>
      <title>'.$baslik.'</title>
      <link>'.baseurl().'</link>
      <language>tr</language>
      <description>'.$aciklama.' </description>
      <generator>Bekir Samet Şirinel http://www.sametsirinel.com</generator>';
   
      
         
         foreach ($items  as $row){
			 
			$pubDate= date("D, d M Y H:i:s T", strtotime($row->baslangictarihi));
             echo '<item>
               <title>'.xml_entities($row->adi).'</title>
			   <guid isPermaLink="true">'.baseurl().'kampanyalar/'.$row->seo_link.'/'.$row->id.'</guid>
               <link>'.baseurl().'kampanyalar/'.$row->seo_link.'/'.$row->id.'</link>
               <description><![CDATA['.xml_entities(strip_tags(htmlspecialchars_decode( $row->kisaaciklama))).']]></description>
               <pubDate>'.$pubDate.'</pubDate>
            </item>';
         }
		 
   echo '</channel>
   </rss>';?>
