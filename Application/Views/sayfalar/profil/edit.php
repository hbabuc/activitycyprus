<form class="form-horizontal" method="post" action="<?php echo baseurl("profil/doEdit") ?>" role="form">
			<?php echo Warning::get(); ?>
			<div class="form-group">
			  <label class="col-lg-3 control-label">Adınız :</label>
			  <div class="col-lg-9">
				<input class="form-control" name="adi" value="<?php echo $user->adi?>" type="text">
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-lg-3 control-label">Soyadınız :</label>
			  <div class="col-lg-9">
				<input class="form-control" name="soyadi" value="<?php echo $user->soyadi?>"type="text">
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-3 control-label">Cinsiyet :</label>
			  <div class="col-md-9">
				<label for="kadin">
					<input type="radio" id="kadin" <?php if($user->cinsiyet==0){ echo "checked"; } ?> name="cinsiyet" value="0"/> Kadın
				</label>
				<label for="erkek">
					<input type="radio" id="erkek" <?php if($user->cinsiyet==1){ echo "checked"; } ?> name="cinsiyet" value="1"/> Erkek
				</label>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-3 control-label">Hakkınzıda :</label>
			  <div class="col-md-9">
				<textarea name="hakkinda" class="form-control" id="" cols="30" rows="10"><?php echo $user->hakkinda?></textarea>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-3 control-label"></label>
			  <div class="col-md-9">
				<input class="btn btn-info" value="Profili Düzenle" type="submit">
				<span></span>
				<input class="btn btn-default" value="İptal Et" type="reset">
			  </div>
			</div>
		  </form>
		