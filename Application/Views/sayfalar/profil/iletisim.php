<form class="form-horizontal" method="post" action="<?php echo baseurl("profil/doEditIletisim") ?>" role="form">
	<?php echo Warning::get(); ?>
	<div class="form-group">
	  <label class="col-lg-3 control-label">Email :</label>
	  <div class="col-lg-9">
		<input class="form-control" name="email" value="<?php echo $user->email?>" type="text">
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label">Telefon Numaranız :</label>
	  <div class="col-md-9">
		<input class="form-control" name="tel" value="<?php echo $user->tel?>" type="text">
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label"></label>
	  <div class="col-md-9">
		<input class="btn btn-info" value="Profili Düzenle" type="submit">
		<span></span>
		<input class="btn btn-default" value="İptal Et" type="reset">
	  </div>
	</div>
  </form>
		