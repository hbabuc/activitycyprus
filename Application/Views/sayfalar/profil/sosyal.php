<form class="form-horizontal" method="post" action="<?php echo baseurl("profil/insertSosyal") ?>" role="form">
	<?php echo Warning::get(); ?>
	<div class="form-group">
	  <label class="col-lg-3 control-label">Sosyal Ağ :</label>
	  <div class="col-lg-9">
		<select name="aglar" clasS="form-control" id="" onchange="linkgir()">
			<option value="">Lütfen Bir Sosyal Ağ Seçiniz</option>
			<option  disabled>---------------------</option>
			<?php foreach($sosyalaglar as $ag){ ?>
				<option value="<?php echo  $ag->link ?>" ><?php echo  $ag->ismi ?></option>
			<?php } ?>
		</select>
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-lg-3 control-label">Linkiniz :</label>
	  <div class="col-lg-9">
		<input class="form-control" disabled name="link" type="text">
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-md-3 control-label"></label>
	  <div class="col-md-9">
		<input class="btn btn-info" value="Sosyal Ağı Ekle" type="submit">
		<span></span>
		<input class="btn btn-default" value="İptal Et" type="reset">
	  </div>
	</div>
</form>
<div class="col-md-offset-1 col-md-11">
	<table class="table">
		<thead>
			<tr>
				<th>Soyal Ağ</th>
				<th>Linkiniz</th>
				<th>İşlemler</th>
			</tr>
		</thead>
		<tbody>
			<?php if(count($ksosyal)<1){ ?>
				<tr>
					<td colspan="3" class="text-center">Herhangi Bir Sosyal Ağnız Bulunmamakta.</td>
				</tr>
			<?php }else{ ?>
				<?php foreach($ksosyal as $userag){ ?>
					<tr>
						<td><?php echo $userag->ismi; ?></td>
						<td><?php echo $userag->link; ?></td>
						<td><a href="#" class="swal" data-link="<?php echo baseurl("profil/deleteSosyal/".$userag->id) ?>" style="text-decoration:none;color:#e1172c"><i class="fa fa-trash"></i> Sil </a></td>
					</tr>
				<?php } ?>
			<?php } ?>
		</tbody>
	</table>
</div>

		