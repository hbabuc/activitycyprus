<div class="profilimg">

	<img src="<?php echo baseurl(UPLOADS_DIR) ?>profile-bg.png" alt=""/>

	

</div>

<div class="profilinfo">

	<div class="container">

		<div class="whitebg">

			<div class="row">

				<div class="col-md-2 col-lg-2 col-sm-4 col-xs-12">

					<div class="basresim">

						<img src="<?php echo baseurl(UPLOADS_DIR.$row->resmi); ?>" style="width:120px;height:120px" alt=""/>

					</div>

				</div>

				<div class="col-xs-10 col-xs-offset-1 col-lg-10 col-md-10 col-sm-8 col-md-offset-0" style="padding-bottom:20px;">

					<div class="row">

						<div class="col-md-4" style="margin-top:30px;">

							<h3><?php echo $row->adi." ".$row->soyadi ?> <?php if($row->id == User::id()){ ?><a style="color:#555;" href="<?php echo baseurl("profil/edit") ?>"><i class="fa fa-edit" title="Profili Düzenle"></i></a><?php } ?></h3>

							<p>Kayıt Tarihi : <?php echo date("d-m-Y",strtotime($row->ktarihi)) ?></p>

						</div>

						<div class="col-md-8" style="margin-top:40px;">

							<?php if(User::check()){ ?>

								<?php if($row->id != User::id()){ ?>

									<?php if($row->takiptemi<1){ ?>

										<a href="JavaScript:;" clasS="btn btn-info pull-right" id="takipEt" style="margin-right:15px;"><i class="fa fa-rss"></i> Takip Et</a>

									<?php }else{ ?>

										<a href="JavaScript:;" clasS="btn btn-info pull-right" id="takipBirak" style="margin-right:15px;"><i class="fa fa-rss"></i> Takibi Bırak</a>

									<?php } ?>

									<?php if($row->arkadasmi<1){ ?>

										<a href="JavaScript:;" clasS="btn btn-info pull-right" id="arkadasEkle" style="margin-right:15px;"><i class="fa fa-user-plus"></i> Arkadaş Ekle</a>

									<?php }else{ ?>

										<?php if($row->arkadasdurum==0){ ?>

											<a href="JavaScript:;" clasS="btn btn-info pull-right" id="istekIptal" style="margin-right:15px;"><i class="fa fa-user-plus"></i> İsteğini İptal Et</a>

										<?php }else{ ?>

											<a href="JavaScript:;" clasS="btn btn-info pull-right" id="arkadasCikar" style="margin-right:15px;"><i class="fa fa-users"></i> Arkadaşlarımdan Çıkar</a>

										<?php } ?>

									<?php } ?>

								<?php } ?>

							<?php }else{

								?>

								<a href="<?=baseurl("login") ?>" clasS="btn btn-info pull-right" style="margin-right:15px;"><i class="fa fa-rss"></i> Takibi Bırak</a>

								<a href="<?=baseurl("login") ?>" clasS="btn btn-info pull-right" style="margin-right:15px;"><i class="fa fa-user-plus"></i> Arkadaş Ekle</a>

								<?php

							} ?>

						</div>

						<div class="clearfix"></div>

					</div>

					<div class="clearfix"></div>

				</div>

				<div class="clearfix"></div>

			</div>	

		</div>

		<?php echo Warning::get(); ?>
		
		<div class="row">

			<div class="col-md-12 text-center">

				<div class="whitebox">

					<div class="col-md-2">

						<div class="profilkalip">

							<div class="icon"><i class="fa fa-map-signs  fa-2x"></i></div>

							<div class="yazi">

								<h3><?php echo $row->aktivitesayi ?></h3>

								<p>Aktivite</p>

							</div>

						</div>

					</div>

					

					<div class="col-md-2">

						<div class="profilkalip">

							<div class="icon"><i class="fa fa-sign-in fa-2x"></i></div>

							<div class="yazi">

								<h3><?php echo $row->katilimsayi ?></h3>

								<p>Katıldıkları</p>

							</div>

						</div>

					</div>

					

					<div class="col-md-2">

						<div class="profilkalip">

							<div class="icon"><i class="fa fa-rss fa-2x"></i></div>

							<div class="yazi">

								<h3><?php  echo $row->takipciler?></h3>

								<p>Takipçiler</p>

							</div>

						</div>

					</div>

					

					<div class="col-md-2">

						<div class="profilkalip">

							<div class="icon"><i class="fa fa-eye fa-2x"></i></div>

							<div class="yazi">

								<h3><?php  echo $row->takipler?></h3>

								<p>Takipler</p>

							</div>

						</div>

					</div>

					

					<div class="col-md-2">

						<div class="profilkalip">

							<div class="icon"><i class="fa fa-user fa-2x"></i></div>

							<div class="yazi">

								<h3><?php  echo $row->arkadassayi ?></h3>

								<p> <a href="<?php baseurl('profil/tum_arkadaslar') ?>">Arkadaşlar </a> </p>

							</div>

						</div>

					</div>

					

					<div class="col-md-2">

						<div class="profilkalip">

							<div class="icon"><i class="fa fa-comments-o fa-2x"></i></div>

							<div class="yazi">

								<h3><?php echo $row->yorumsayi ?></h3>

								<p>Yorumları</p>

							</div>

						</div>

					</div>

					

					<div class="clearfix"></div>

				</div>

			</div>

			<?php if(count($aglar)>0){ ?>

				<div class="col-md-12 text-center" >

					<div class="whitebox">

						<?php foreach($aglar as $ag){ ?>

							<a href="<?php echo $ag->linki ?>" class="icon-button <?php echo $ag->ismi ?>"><i class="fa <?php echo $ag->icon ?>"></i><span></span></a>

						<?php } ?>

					</div>

				</div>

			<?php } ?>

			<?php if( $row->hakkinda){ ?>

				<div class="col-md-12">

					<div class="whitebox">

						<div class="row">

							<div class="col-md-4">

								<h3>Kullanıcı Hakkında  </h3>

								<?php  echo htmlspecialchars_decode($row->hakkinda);  ?>

							</div>

							<div class="col-md-8" style="border-left:1px solid #eee;">
								<h3>Arkadaşları <small> ( <?php echo count($arkadaslar); ?> ) </small>   </h3>

								<div class="row">

									<?php if(count($arkadaslar)<1){ ?>

										<div class="col-md-12" style="color:#666;margin-top:20px;">

											Kullanıcının arkadaşı bulunmamakta.

										</div>

									<?php }else{ ?>

										<?php foreach($arkadaslar as $arkadas){ ?>

											<?php if($arkadas->userid == $row->id){ ?>

												<div class="col-md-2">

													<div class="row">

														<a href="<?php echo baseurl($arkadas->gonderenkadi) ?>" title="<?php echo $arkadas->gonderenadisoyadi ?>">

															<img width=100 height=100 src="<?php echo baseurl(UPLOADS_DIR.$arkadas->gonderenResim) ?>" clasS="img-rounded col-xs-12" alt="<?php echo $arkadas->gonderenadisoyadi ?>"/>

														</a>

													</div>

												</div>

											<?php }else{ ?>

												<div class="col-md-2">

													<div class="row">

														<a href="<?php echo baseurl($arkadas->userkadi) ?>" title="<?php echo $arkadas->useradisoyadi ?>">

															<img width=100 height=100 src="<?php echo baseurl(UPLOADS_DIR.$arkadas->userResim) ?>" clasS="img-rounded col-xs-12" alt="<?php echo $arkadas->useradisoyadi ?>"/>

														</a>

													</div>

												</div>

											<?php } ?>

										<?php } ?>

									<?php } ?>

								</div>

							</div>

						</div>

					</div>

				</div>

			<?php } ?>

			<div class="col-md-6" style="margin-bottom:100px;">

				<div class="whitebox">

					<h3>Takip Ettikleri <small> ( <?php echo count($takipedilen) ?> ) </small> 
					<?php $user_kadi = User::kadi(); ?>
					<small class="pull-right" style="margin-top:10px;margin-right:10px;font-size:14px;"><a href="<?php echo baseurl("profil/takip_ettikleri/{$row->kadi}") ?>">Tümünü Gör </a></small>

					</h3>
				
					<div class="row">

						<?php if(count($takipedilen)<1){ ?>

							<div class="col-md-12 text-center" style="color:#666;margin-top:20px;">

								Herhangi Bir Kullanıcıyı Takip Etmiyor .

							</div>

						<?php }else{ ?>

							<?php foreach($takipedilen as $takipet){ ?>

								<div class="col-md-2">

									<div class="row">

										<a href="<?php echo baseurl($takipet->kadi) ?>" title="<?php echo $takipet->adisoyadi ?>">

											<img src="<?php echo baseurl(UPLOADS_DIR.$takipet->resmi) ?>" clasS="img-rounded col-xs-12" alt="<?php echo $takipet->adisoyadi ?>"/>

										</a>

									</div>

								</div>

							<?php } ?>

						<?php } ?>

					</div>

				</div>

			</div>

			<div class="col-md-6">

				<div class="whitebox">

					<h3>Takip Edenler <small> ( <?php echo count($takipeden) ?> ) </small>

					<?php  if( $row->kadi == $user_kadi ){ ?>
					 <small class="pull-right" style="margin-top:10px;margin-right:10px;font-size:14px;"><a href="<?php echo baseurl("profil/tum_takipciler") ?>">Tümünü Gör</a></small>
					 <?php  }else{ ?>
					 <small class="pull-right" style="margin-top:10px;margin-right:10px;font-size:14px;"><a href="<?php echo baseurl("profil/tum_takipciler/".$row->kadi) ?>">Tümünü Gör</a></small>
					 <?php } ?>
					 </h3>

					<div class="row">

					<?php if(count($takipeden)<1){ ?>

							<div class="col-md-12 text-center" style="color:#666;margin-top:20px;">

								Herhangi Bir Kullanıcı Takip Etmiyor .

							</div>

						<?php }else{ ?>

							<?php foreach($takipeden as $takiped){ ?>

								<div class="col-md-2">

									<div class="row">

										<a href="<?php echo baseurl($takiped->kadi) ?>" title="<?php echo $takiped->adisoyadi ?>">

											<img src="<?php echo baseurl(UPLOADS_DIR.$takiped->resmi) ?>" clasS="img-rounded col-xs-12" alt="<?php echo $takiped->adisoyadi ?>"/>

										</a>

									</div>

								</div>

							<?php } ?>

						<?php } ?>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>



<script type="text/javascript">



		$(document).ready(function(){

			

	



		$("#arkadasEkle").click(function(){

			

			$.ajax({

				

					type: 'post',

					dataType: "json",

					url:'<?=baseurl('profil/arkadasekle') ?>',

					data: "kadi=<?=$row->kadi ?>",

					success: function(data){

						

						if(data.durum == 1){

							

							swal({title:"Arkadaşlık isteği gönderildi",type:"success", timer: 1500},function(){

								window.location.href="";

							});

							

						}else if(data.durum==2){

							

							swal("Kullanıcının Kabul Etmesi Bekleniyor.","","error");

							

						}else{

							

							swal("Arkadaş eklerken hata oluştu","","error");

							

						}

						

					}

					

				});

				

		});



		$("#arkadasCikar").click(function(){

			

			swal({   

			

				title: "Eminmisiniz ?",   

				text: "Bu işlemi yaptığınızda kullanıcı ile arkadaşlığınız bitecek",   

				type: "warning",   

				showCancelButton: true,   

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "Arkadaşlıktan Çıkar ",   

				cancelButtonText: "Hayır , iptal et",   

				closeOnConfirm: false,   

				closeOnCancel: false 

			

			}, function(isConfirm){   

			

				if (isConfirm) {     

				

					$.ajax({

						

						type: 'post',

						dataType: "json",

						url:'<?=baseurl('profil/arkadasSil') ?>',

						data: "kadi=<?=$row->kadi ?>",

						success: function(data){

							

							if(data.durum == 1){

								

								swal({

									

									title:"Artık arkadaş değilsiniz.",

									type:"success", 

									timer: 1500

									

								},function(){

									

									window.location.href="";

									

								});

								

							}else if(data.durum==2){

								

								swal("Kullanıcının Kabul Etmesi Bekleniyor.","","error");

								

							}else{

								

								swal("Arkadaş eklerken hata oluştu","","error");

								

							}

							

						}

						

					});

				

				} else {

					

					swal({

						

						title:"İptal Edildi",

						text: "Merak etmeyin arkadaşlığınız güvende !",

						timer:1500, 

						type: "error"

						

					});   

					

				} 

			

			});

			

				

		});

		

		$("#istekIptal").click(function(){

			

			swal({   

			

				title: "Eminmisiniz ?",   

				text: "Bu işlemi yaptığınızda kullanıcıya gönderilen istek iptal edilir",   

				type: "warning",   

				showCancelButton: true,   

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "İsteği kaldır ",   

				cancelButtonText: "Hayır , iptal et",   

				closeOnConfirm: false,   

				closeOnCancel: false 

			

			}, function(isConfirm){   

			

				if (isConfirm) {     

				

					$.ajax({

						

						type: 'post',

						dataType: "json",

						url:'<?=baseurl('profil/istekSil') ?>',

						data: "kadi=<?=$row->kadi ?>",

						success: function(data){

							

							if(data.durum == 1){

								

								swal({

									

									title:"Artık arkadaş değilsiniz.",

									type:"success", 

									timer: 1500

									

								},function(){

									

									window.location.href="";

									

								});

								

							}else if(data.durum==2){

								

								swal("Kullanıcının Kabul Etmesi Bekleniyor.","","error");

								

							}else{

								

								swal("Arkadaş eklerken hata oluştu","","error");

								

							}

							

						}

						

					});

				

				} else {

					

					swal({

						

						title:"İptal Edildi",

						text: "Merak etmeyin arkadaşlığınız güvende !",

						timer:1500, 

						type: "error"

						

					});   

					

				} 

			

			});

			

				

		});

		

		$("#takipEt").click(function(){

			<?php

				if(User::check()){

					?>

					$.ajax({

						type: 'post',

						dataType: "json",

						url:'<?=baseurl('profil/takipciEkle') ?>',

						data: "userid= <?=$row->id ?>" + "&takipciid=<?=User::id() ?>" + "&kadi=<?=$row->kadi ?>",

						success: function(data){

							if(data.durum == 1){

								swal({title: "Takip edildi.",type:"success",timer:1500},function(){

									window.location.href="";

								});

							}

							else if(data.durum == 0){

								swal("Takip ederken bir hata oluştu","","error");

							}

						},

						error: function(data){

							swal(" İnternet bağlantınızı kontrol edin","","error");

						}

						

					});

					<?php

				}

				else

				{

					?>

					swal({title:"Kullanıcı girişi yapılmadı.",type:"error"},function(){

						window.location = "<?=baseurl("login") ?>";

					});

					<?php

				}

			?>

			

		});

		

		$("#takipBirak").click(function(){

			

			swal({   

			

				title: "Eminmisiniz ?",   

				text: "Bu işlemi yaptığınızda bu kullanıcıyı takip etmeyi bırakacaksınız.",   

				type: "warning",   

				showCancelButton: true,   

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "Takipten Çıkar ",   

				cancelButtonText: "Hayır , iptal et",   

				closeOnConfirm: false,   

				closeOnCancel: false 

			

			}, function(isConfirm){   

			

				if (isConfirm) {     

				

					$.ajax({

						

						type: 'post',

						dataType: "json",

						url:'<?=baseurl('profil/takipBirak') ?>',

						data: "userid=<?=$row->id ?>&takipciid=<?=User::id() ?>",

						success: function(data){

							

							if(data.durum == 1){

								

								swal({

									

									title:"Takipten çıkarıldı.",

									type:"success", 

									timer: 1500

									

								},function(){

									

									window.location.href="";

									

								});

								

							}else if(data.durum == 0){

								

								swal("Takipten çıkarırken hata oluştu","","error");

								

							}

							

						}

						

					});

				

				} else {

					

					swal({

						

						title:"İptal Edildi",

						text: "İşlem iptal edildi !",

						timer:1500, 

						type: "error"

						

					});   

					

				} 

			

			});

			

				

		});

		

		

	});

		

		

		

</script>			