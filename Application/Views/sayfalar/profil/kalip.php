<div class="profilimg">
	<img src="<?php echo baseurl(UPLOADS_DIR) ?>profile-bg.png" alt=""/>
</div>
<div style="background:#efefef;padding:20px 0px;">
	<div class="container" style="padding-top: 60px;background:#fff;padding:20px 50px;margin-top:-125px;position:relative;z-index:99;">
	  <h1 class="page-header">Profili Düzenle <small class="pull-right"><?php echo $title ?></small></h1>
		  <div class="row">
			<!-- left column -->
			<div class="col-md-4 col-sm-6 col-xs-12">
				<ul class="list-group" style="overflow:hidden">
					<div class="row" style="position:relative">
						<a href="<?php echo baseurl("profil/edit/resimdegis") ?>" class="editimg">
							<i class="fa fa-edit"></i> Resmi Düzenle
						</a>
						<img src="<?php echo baseurl(UPLOADS_DIR.$user->resmi) ?>" class="avatar col-xs-12" alt="avatar">
					</div>
				  <li class="list-group-item"><a href="<?php echo baseurl("profil/edit/kisisel") ?>">Kişisel Bilgiler</a></li>
				  <li class="list-group-item"><a href="<?php echo baseurl("profil/edit/sosyal") ?>">Sosyal Ağlar</a></li>
				  <li class="list-group-item"><a href="<?php echo baseurl("profil/edit/iletisim") ?>">İletişim Bilgileri</a></li>
				  <li class="list-group-item"><a href="<?php echo baseurl("profil/edit/sifredegis") ?>">Şifreni Değiştir</a></li>
				</ul>
			</div>
			<!-- edit form column -->
			<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
				<?php echo $sayfa ?>
		    </div>
		  </div>
	</div>
</div>