﻿<form class="form-horizontal" method="post" action="<?php echo baseurl("profil/doEditSifre") ?>" role="form">
	<?php echo Warning::get(); ?>
	<div class="form-group">
	  <label class="col-lg-3 control-label">Mevcut Şifreniz :</label>
	  <div class="col-lg-9">
		<input class="form-control" name="sifre" type="text">
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-lg-3 control-label">Yeni Şifreniz :</label>
	  <div class="col-lg-9">
		<input class="form-control" name="yeni_sifre1" type="text">
	  </div>
	</div>
	<div class="form-group">
	  <label class="col-lg-3 control-label">Yeni Şifreniz (Tekrar):</label>
	  <div class="col-lg-9">
		<input class="form-control" name="yeni_sifre2" type="text">
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-md-3 control-label"></label>
	  <div class="col-md-9">
		<input class="btn btn-info" value="Şifreni Değiştir" type="submit">
		<span></span>
		<input class="btn btn-default" value="İptal Et" type="reset">
	  </div>
	</div>
  </form>
		