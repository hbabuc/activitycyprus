<div class="container">
<?php if (empty($arkadaslar)): ?>
	<div class="alert alert-info">Hiç Arkadaşınız Yok </div>
<?php endif; ?>
<br><br>
<h3>Tüm Arkadaşların</h3>
<div class="row">
	<?php  foreach($arkadaslar as $arkadas): ?>

		<div class="col-sm-6 col-md-4">
		  <div class="thumbnail">
		    <img src="<? echo baseurl(UPLOADS_DIR.$row->userimg) ?>" alt="<?=$row->adi?>" style="width:250px; height:250px;">
		    <div class="caption">
		      <h3><?=$row->adi ?></h3>
		      <p> <?=$row->email ?> Kullanıcı adıyla </p>
		      <p> <a href="#" class="btn btn-primary" role="button">Takipçilerimden Çıkar </a> </p>
		    </div>
		  </div>
		</div>

	<?php endforeach; ?>
	</div>
</div>