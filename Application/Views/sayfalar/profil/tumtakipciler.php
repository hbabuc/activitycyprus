<?php 
	$userid = User::id();
?>
<div class="container">
<?php  echo Warning::get(); ?>
<?php if (empty($takipciler)): ?>
	<div class="alert alert-info">Hiç Takipçiniz Yok </div>
<?php endif; ?>
<br><br>
	<h3>   <?=$title ?> </h3>
	<div class="row">
	<?php  foreach($takipciler as $row): ?>	
			  <div class="col-sm-6 col-md-4">
			    <div class="thumbnail">
			      <img src="<? echo baseurl(UPLOADS_DIR.$row->userimg) ?>" alt="<?=$row->adi?>" style="width:250px; height:250px;">
			      <div class="caption">
			        <h3> <a href="<?php echo baseurl("{$row->kadi}"); ?>"><?=$row->adi ?></a> </h3>
			        <p>E Mail: <?=$row->email ?> </p>
			      </div>
			      <?php  if($tip == 0): ?>
			     <a href="#"  data-link="<?php echo baseurl("profil/takipci_cikar/{$row->id}") ?>" clasS="swal"> Takipçilerimden Çıkar </a>
			 	<?php  endif; ?>
			    </div>
			  </div>
		<?php endforeach;  ?>

	</div>
</div>