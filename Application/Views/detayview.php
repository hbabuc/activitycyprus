<?php date_default_timezone_set('Europe/Istanbul'); ?>
<div class="banner text-center">
	  <div id="map" style="width: 100%; height:400px; margin-top: -45px"></div>
	</div>
	<!--single-page-->
	<div class="single-page main-grid-border">
		<div class="container">
			<ol class="breadcrumb" style="margin-bottom: 5px;">
				<li><a href="<?=baseurl("home") ?>">Anasayfa</a></li>
				<li><a href="<?=baseurl("home/alls/tum_aktiviteler") ?>">Aktiviteler</a></li>
				<li><a href="<?=baseurl("home/category/".$veriler->katseo) ?>"><?=$veriler->katadi ?></a></li>
				<li class="active"><?=$veriler->baslik ?></li>
			</ol>
			<div class="product-desc">
				<div class="col-md-7 product-view">
					<h2><?=$veriler->baslik ?></h2>
					<p> <i class="glyphicon glyphicon-map-marker"></i><a href="#"><?=$veriler->adres ?></a>| <?=$veriler->bastarih.' / '.$veriler->bittarih ?></p>
					<div class="flexslider">
					
						<ul class="slides">
						<?php
							foreach($resimler as $res){
								?>
								<li data-thumb="<?php echo baseurl(UPLOADS_DIR).$res->resimadi; ?>">
									<img src="<?php echo baseurl(UPLOADS_DIR).$res->resimadi; ?>" style="width:100%"/>
								</li>
								<?php
							}
							
						?>
							
						</ul>
					</div>
					<br/>
					<div class="col-md-12 product-details" style="margin-top: -20px">
					<div class="col-md-2">
							<p style="padding-top: 5px;">Başlık</p>
							<p style="padding-top: 5px;">Açıklama</p> 
						</div>
						<div class="col-md-10">
							<h4><?=$veriler->baslik ?></h4>
							<p><?=$veriler->aciklama ?></p>
						</div>	
					</div>
					<div class="clearfix"></div>
					<br>
					<div class="col-md-12">
						<div style="border-top:1px solid #eee"></div>
					</div>
					<div class="clearfix"></div><br>
					
					
					<!--COMMENTSSSS -->
					<?php
					foreach($yorumlar as $yorum){
					?>
					<div class="container" style="width:100%;">
					<div class="row" style="width:100%;">
						<div class="" style="width:100%;">
							<div class="panel panel-white post panel-shadow">
								<div class="post-heading">
									<div class="col-md-2">
										<img  src="<?=baseurl(UPLOADS_DIR.$yorum->kullaniciresim) ?>" class="img-circle avatar pull-left" alt="user profile image">
									
									</div>
									<div class="pull-left col-md-6">
										<div class="title h5 pull-left">
											<a href="<?=baseurl($yorum->kadi) ?> "><b><?=$yorum->adisoyadi ?></b></a>
										</div>
										<h6 class="text-muted time pull-left" style="margin-top:12px;margin-left:15px;"><?=TimeStamp::timeConvert($yorum->zaman) ?></h6>
										<div class="clearfix"></div>
									</div>
									<div class="col-md-4">
										<div class="pull-right">
											<a class="btn btn-default stat-item like-btn" data-id="<?=$yorum->id ?>">
												<i class="fa fa-thumbs-up icon pull-left" style="margin-right:3px"></i><div class="sonuc pull-left"><?=$yorum->ilike ?></div>
											</a>
											<a class="btn btn-default stat-item dislike-btn" data-id="<?=$yorum->id ?>">
												<i class="fa fa-thumbs-down icon pull-left" style="margin-right:5px"></i><div class="sonuc pull-left"><?=$yorum->idislike ?></div>
											</a>
										</div>
									</div>
								</div> 
								<div class="col-md-offset-2 col-md-10" style="margin-top:-50px">
									<p style="padding-left:10px;"><?=$yorum->yorum ?> </p>
								</div>
									
							</div>
						</div>
						
							
						</div>
					</div>
					
					<?php
					}
					?>
					<br/>
					<?php if($veriler->katilmismi > 0) echo '<button type="button" id="yorumekle" class="btn btn-info btn-lg pull-right" data-toggle="modal" data-target="#modalimiz">Yorum Ekle</button>'; ?>
					<!-- Modal -->
					
					<div class="modal fade" id="modalimiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="relative" ><div class="loading" id="modalLoader"><img src="<?=baseurl(UPLOADS_DIR."/loader.gif") ?>"><br/><div>Konum Bekleniyor...</div><br/><button id="iptal" class="btn-info btn btn-lg" type="button">İptal</button></div>
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							
							<h4 id="myModalLabel">Yorum yazın</h4><br/>
							<?php
								if($oylama < 1){
									?>
									<center><span style="font-weight: bold;">Aktiviteyi değerlendirin :</span> 
									<div class="rating" style="font-size: 30px; " data-rate-value=0></div>
									</center>
									<?php
								}
							?>
						  </div>
						  <div class="modal-body">
						  
						  <form id="formm">
							<input type="hidden" id="yorumhidden" name="yorum"/>
							<input type="hidden" id="derecehidden" value="0" name="derece"/>
							<input type="hidden" value="<?=Uri::segment(-1) ?>" name="aktiviteid"/>
						  </form>
							
						<div id="addcomment">
							<textarea placeholder="Yorumunuz" name="addcomment" id="yorumtextarea" class="form-control" cols="30" rows="5"></textarea>
							<br/>
						</div>
							
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-info" data-dismiss="modal">Kapat</button>
							<button type="button" id="yorumgonder" class="btn btn-info">Yorumu Gönder</button>
						  </div>
						</div>
						</div>
					  </div>
					</div>
					
					
					<!-- SIKAYET MODAL -->
					<!-- Modal -->
					
					<div class="modal fade" id="sikayetmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							
							<h3 id="myModalLabel">Şikayet Yaz</h3><br/>
						
						  </div>
						  <div class="modal-body">
						  
							
						<div id="sikayetekle">
							<select id="sikayettipi" class="form-control">
								<option value="1">Beni veya birini rahatsız ediyor</option>
								<option value="2">Uygunsuz içerik</option>
								<option value="3">Katılım</option>
							</select>
							<br/>
							<textarea placeholder="Şikayetiniz" name="addcomment" id="sikayettextarea" class="form-control" cols="30" rows="5"></textarea>
							<br/>
						</div>
							
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-info" data-dismiss="modal">Kapat</button>
							<button type="button" id="sikayetgonder" class="btn btn-info">Şikayet Gönder</button>
						  </div>
						</div>
					  </div>
					</div>
					<!-- UCRET MODAL -->
					<div class="modal fade" id="ucretmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							
							<h3 id="myModalLabel">Ücret</h3><br/>
						
						  </div>
						  <div class="modal-body">
							 
								<h4>Ücret burada ödenecek</h4>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-info" data-dismiss="modal">Kapat</button>
							<button type="button" id="odeme" class="btn btn-info">Öde</button>
						  </div>
						</div>
					  </div>
					</div>
				</div>
				<div class="col-md-5 product-details-grid">
					<div class="item-price">
						<div class="product-price">
							<p class="p-price"><?php echo ML::select("price") ?></p>
							<h3 class="rate" id="ucretttt"><?php if($veriler->ucret == 0){ echo ML::select("free");} else {echo "<img width='16' src='".baseurl(UPLOADS_DIR."tl.png")."'> ".$veriler->ucret;} ?></h3>
							<div class="clearfix"></div>
						</div>
						<div class="condition">
							<p class="p-price">Kategori</p>
							<h4><?=$veriler->katadi ?></h4> 
							<div class="clearfix"> </div>
						</div>
						<div class="itemtype">
							<p class="p-price"><?php echo ML::select("point") ?></p>
							<h4> <?php echo (empty($veriler->oylama)) ? ' - / 10 ' : $veriler->oylama ." / 10 " ?> </h4>
							<div class="clearfix"></div>
						</div>
						<div class="condition">
							<p class="p-price"><?php echo ML::select("participants") ?></p>
							<h4>
							<?php 
								switch($veriler->cinsiyet){
									case 0: echo "Herkese Açık";break;
									case 1: echo "Erkek";break;
									case 2: echo "Bayan";break;
								}
							?>
							</h4> 
							<div class="clearfix"> </div>
						</div>
						<?php if(!empty(trim($veriler->yasaraligi))){
							?>
							<div class="condition">
							<p class="p-price"><?php echo ML::select("agerange") ?></p>
							<h4>
							<?=$veriler->yasaraligi ?>
							</h4> 
							<div class="clearfix"> </div>
							</div>
							<?php
						}
						
						
						if(!empty(trim($veriler->kapasite))){
							?>
							<div class="condition">
							<p class="p-price"><?php echo ML::select("limit") ?></p>
							<h4>
							<?=$veriler->kapasite ?>
							</h4> 
							<div class="clearfix"> </div>
							</div>
						<?php
						}
						?>
					</div>
						<div class="tips">
						<?php 
						if(User::check() && ($veriler->katilmismi < 1 && $veriler->rezervasyonuVarmi < 1 && (strtotime(date("d-m-Y H:i:s")) < strtotime($veriler->bastarih)))) 
							echo '<button class="btn-lg btn-success btn col-xs-12" id="rezervasyon">Rezervasyon</button>';
						
						else if(User::check() && ($veriler->katilmismi < 1 && (strtotime(date("d-m-Y H:i:s")) > strtotime($veriler->bastarih) && (strtotime(date("d-m-Y H:i:s")) < strtotime($veriler->bittarih))))) 
							echo '<button class="btn-lg btn-success btn col-xs-12" id="katil">Katıl</button>';
						
						else if(User::check() && ($veriler->rezervasyonuVarmi > 0 && (strtotime(date("d-m-Y H:i:s")) < strtotime($veriler->bastarih)))) 
							echo '<button class="btn-lg btn-danger btn col-xs-12" id="iptalet">Rezervasyonu İptal Et</button>';
						
						?>
						<div class="clearfix"></div>
						<button class="btn-lg btn-danger btn col-xs-12" data-toggle="modal" data-target="#sikayetmodal" style="margin-top: 10px;" id="sikayet">Şikayet Et</button>
						<div class="clearfix"></div>	
						<br/><br/>
						</div> 
				</div>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!--//single-page-->

	<script type="text/javascript">
	var myMap,isMapLoaded = false;
	
	$(document).ready(function(){
		ymaps.ready(init).done(function(){
			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(getPos);
			}
		});
		
		 
	});
	
	function getPos(pos){
		if(isMapLoaded){
			var myPlacemark2 = new ymaps.Placemark([pos.coords.latitude,pos.coords.longitude],
			{ 
				iconContent: 'Senin Konumun'
			},{
            // Options. The placemark's icon will stretch to fit its contents.
            preset: 'islands#greenStretchyIcon'
			}); 

			myMap.geoObjects.add(myPlacemark2);
			
			ymaps.route([
				{ type: 'viaPoint', point: [pos.coords.latitude,pos.coords.longitude] },
				{ type: 'wayPoint', point: [<?=$veriler->lat.','.$veriler->lng ?>] }
			], {
				mapStateAutoApply: true
			}).then(function (route) {
				route.getPaths().options.set({
					// you can make settings for route graphics
					strokeColor: 'e1172c',
					opacity: 0.9
				 });
				 // adding the route to the map
				 
				 myMap.geoObjects.add(route);
			 });
		}
		
	}
	
	function init () {
		// Creating an instance of the map and binding it to the container with the specified ID
		// ("map").
		
		var defaultCoords = [<?=$veriler->lat.','.$veriler->lng ?>];
		
		myMap = new ymaps.Map('map', {
			// When initializing the map, you must specify its center and the zoom factor.
			center: defaultCoords, // Москва
			zoom: 6
		}, {
			searchControlProvider: 'yandex#search'
		});
		
		var myPlacemark = new ymaps.Placemark([<?=$veriler->lat.','.$veriler->lng ?>],
			{ 
				iconContent: '<?=$veriler->baslik ?>',
                hintContent: 'Aktivite Burada!'
			},{
            // Options. The placemark's icon will stretch to fit its contents.
            preset: 'islands#blueStretchyIcon'
			}); 

			myMap.geoObjects.add(myPlacemark);
			isMapLoaded = true;
			
	}
	
	$(document).ready(function(){
				var rate,hangiButon;
				
				$(".rating").rate({
					max_value: 10,
					step_size: 1
				});
				
				$("#yorumgonder").click(function(){
					
					rate = $(".rating").rate("getValue").toString();
					
					if($("#yorumtextarea").val().trim() == "")
					{
						swal("Yorum alanı boş geçilemez!","","error");
					}
					else if($("#yorumtextarea").val().trim().length < 2){
						swal("Yorum alanı 2 karakterden az olamaz","","error");
					}
					else
					{
						$("#yorumhidden").val($("#yorumtextarea").val());
						$("#derecehidden").val(rate);
						$.ajax({
							type: 'post',
							dataType: "json",
							url:'<?=baseurl('detay/yorumgonder') ?>',
							data: $("#formm").serialize(),
							success: function(data){
								if(data.durum == 1){
									swal({title:"Yorumunuz eklendi.", type:"success"},function(){
										window.location.href = "";
									});
								}
								else if(data.durum == 2){ 
									swal("Yorum gönderilirken bir hata oluştu, lütfen daha sonra tekrar deneyiniz.","","error");
								}
								else if(data.durum == 0){
									swal({title:"Yorum göndermek için giriş yapmalısınız.",type:"error"},function(){
										window.location = "<?=baseurl("login") ?>";
									});	
								}
							}
						});
						
					}
					
				});
				
					//KATIL BUTONU
					
					$("#katil").click(function(){
						hangiButon = 1;
						rezKatil();
						
					});
					
					$("#rezervasyon").click(function(){
						hangiButon = 0;
						rezKatil();
						
					});
						
					$("#iptalet").click(function(){
							$.ajax({
								type: 'post',
								dataType: "json",
								url:'<?=baseurl('detay/rezervasyonIptal') ?>',
								data: "aktiviteid=<?=Uri::segment(-1) ?>",
								success: function(data){
									if(data.durum == 1){
										swal({title:"Rezervasyonu iptal ettin!",type:"success"},function(){
											window.location.href="";
										});
									}
									else if(data.durum == 2)
									{
										swal({title:"Lütfen giriş yapınız.",text:"Girişe yönlendiriliyor.",type:"success"},function(){
											window.location ="<?=baseurl("login") ?>";
										});
									}
									else if(data.durum == 0)
									{
										swal("İptal ederken bir hata oluştu, lütfen daha sonra tekrar dene!", "","error");
									}
								}
							});
						});
						
						function getCurrentPos2(pos){
							$("#katil").html("Lütfen bekleyiniz...");
							$.ajax({
								type: 'post',
								dataType: "json",
								url:'<?=baseurl('detay/katil') ?>',
								data: "aktiviteid=<?=Uri::segment(-1) ?>&lat=" + pos.coords.latitude + "&lng=" + pos.coords.longitude,
								success: function(data){
									if(data.durum == 1){
									swal({title:"Aktiviteye katıldın!",type:"success"},function(){
										window.location.href="";
									});
									}
									else if(data.durum == 2)
									{
										var uzaklik = (data.distance < 1000) ? Math.floor(data.distance) + " metre" : Math.floor(data.distance / 1000) + " km";
										swal("Aktiviteden yaklaşık " + uzaklik + " uzaktasın, aktiviteye katılmak için aktivite adresinin en az 10 kilometre yakınında olmalısın!","","error");
									}
									else if(data.durum == 3)
									{
										swal("Aktivitenin limiti dolduğundan aktiviteye katılamazsın !", "","error");
									}
									else if(data.durum == 4)
									{
										swal({title:"Aktiviteye katılmak için kullanıcı girişi yapmalısın!",type:"error"},function(){
											window.location = '<?=baseurl("login") ?>';
										});
									}
									else if(data.durum == 0)
									{
										swal("Aktiviteye katılırken bir hata oluştu, lütfen daha sonra tekrar dene!", "","error");
									}
								}
							});
						}
						
						function rezKatil(){
							if(hangiButon == 1)
							{
								$("#katil").html("Konum bekleniyor...");
								if(navigator.geolocation)
								{
									navigator.geolocation.getCurrentPosition(getCurrentPos2);
								}
								else
									swal("Tarayıcınız konum bildirimini desteklemiyor!","","warning");
								
							}
							else
							{
								$.ajax({
									type: 'post',
									dataType: "json",
									url:'<?=baseurl('detay/rezervasyon') ?>',
									data: "aktiviteid=<?=Uri::segment(-1) ?>",
									success: function(data){
										if(data.durum == 1){
										swal({title:"Aktivite için rezervasyonun yapıldı!",type:"success"},function(){
											window.location.href="";
										});
										}
										else if(data.durum == 3)
										{
											swal("Aktivitenin limiti dolduğundan aktiviteye katılamazsın !", "","error");
										}
										else if(data.durum == 4)
										{
											swal({title:"Aktiviteye katılmak için kullanıcı girişi yapmalısın!",type:"error"},function(){
												window.location = '<?=baseurl("login") ?>';
											});
										}
										else if(data.durum == 0)
										{
											swal("Aktiviteye katılırken bir hata oluştu, lütfen daha sonra tekrar dene!", "","error");
										}
									}
								});
							
							}
						}
						
					
					
					$("#sikayetgonder").click(function(){
						if($("#sikayettextarea").val().trim().length < 5){
							swal("Şikayet alanı 5 harften küçük olamaz. Neden 5 diye sormayın bilmiyorum.");
						}
						else{
							$.ajax({
								type: 'post',
								dataType: "json",
								url:'<?=baseurl('detay/sikayetgonder') ?>',
								data: "aktiviteid=<?=Uri::segment(-1) ?>&tip=" + $("#sikayettipi").val() + "&aciklama=" + $("#sikayettextarea").val().trim(),
								success: function(data){
									if(data.durum == 1){
										swal({title:"Şikayetin alındı.",text:"En kısa zamanda yöneticiler tarafından değerlendirilip gerekeni yapılacak.",type:"success"},function(){
											window.location.href="";
										});
									}
									else if(data.durum == 2)
									{
										swal("Şikayet gönderilirken bir hata oluştu","","error");
									}
									else if(data.durum == 0){
										swal({title:"şikayet gönderebilmek için giriş yapmalısın.",type:"error"});
									}
								}
							});
						}
						
					});
					
					
			});
	

	</script>
