<?php
	 $postback = Session::select("postback");
	 
	 if($postback){
		$postData = json_decode($postback); 
	 }

?>

<div class="banner text-center">
	  <center><div id="map" style="width: 100%; height: 400px; position:absolute; margin-top:-45px" >
				   <img id="loadergif" src="<?php echo baseurl(UPLOADS_DIR."loader.gif");?>">
				   </div></center>
	</div>
	<!-- Submit Ad -->
	<div class="submit-ad main-grid-border">
		<div class="container">
			<h2 class="head"> Aktivite Oluştur </h2>
			<div class="post-ad-form">
			<?php echo Warning::get(); ?>
				<form method="POST" action="<?php echo baseurl('aktivite/forminsert')?>">
				<input type="hidden" name="lat" id="maplat" value="">
				<input type="hidden" name="lng" id="maplng" value="">
					
					
					<label>Başlık<span>*</span></label>
					<input type="text" name="baslik" class="name" placeholder="Aktivite Başlığı" 
					 <?php if(isset($postData->baslik)) echo 'value="'.$postData->baslik.'"'; ?> >					
					<div class="clearfix"></div>
					<label>Aktivite Açıklaması <span>*</span></label>
					<textarea class="mess" placeholder="Aktiviteniz açıklayınız" name="aciklama"><?php if(isset($postData->aciklama)) echo $postData->aciklama; ?></textarea>
					<div class="clearfix"></div>
					
					<label>Adres<span>*</span></label>
					<input type="text" id="adres" name="adres" class="name" placeholder="Adres" <?php if(isset($postData->adres)) echo 'value="'.$postData->adres.'"'; ?>>
					
					
					<label>Başlangıç Tarihi<span>*</span></label>
					<input id="datepicker" type="text" name="bastarih" placeholder="Tarih seçmek için tıklayın." <?php if(isset($postData->bastarih)) echo 'value="'.$postData->bastarih.'"'; ?>>
					<div class="clearfix"></div>
					
					<label>Bitiş Tarihi<span>*</span></label>
					<input id="datepicker2" type="text" name="bittarih" placeholder="Tarih seçmek için tıklayın." <?php if(isset($postData->bittarih)) echo 'value="'.$postData->bittarih.'"'; ?>>
					<div class="clearfix"></div>
					<div class="personal-details">
					
					<!-- HARİTA -->
					
					<label>Kapasite<span> </span></label>
					<input type="text" name="kapasite" class="name" placeholder="Aktivitenize maksimum kaç kişinin katılmasını istiyorsunuz ? " <?php if(isset($postData->kapasite)) echo 'value="'.$postData->kapasite.'"'; ?>>
					<div class="clearfix"></div>
					
					<label>Ücret<span></span></label>
					<input type="checkbox" id="ucretdurumu" value="1"> Aktiviteniz ücretli mi?
					<input style="display:none;" type="text" id="ucret" name="ucret" class="name" placeholder="Kişi başı ücret" <?php if(isset($postData->ucret)) echo 'value="'.$postData->ucret.'"'; ?>>
					<div class="clearfix"></div>
						
					
					<label style="display:none;">Mesafe Sınırı<span></span></label>
					<input  style="display:none;" type="checkbox" id="mesafedurumu" name="mesafe" value="1"> 
					<input style="display:none;" type="text" id="mesafe" name="mesafe" class="name" placeholder="Katılanların Mesafe Sınırı " <?php if(isset($postData->mesafe)) echo 'value="'.$postData->mesafe.'"'; ?>>
					<div class="clearfix"></div>
					
					<label>Yaş Aralığı Seçin <span></span></label>
					<select class="" name="yas_aralik" style="width:70%">
					
					<?php  foreach($yaslar as $row): ?>
					  <option value="<?=$row->id?>"><?=$row->baslangic?>-<?=$row->bitis?></option>
					  <?php endforeach; ?>
					</select>
					<div class="clearfix"></div>
					<label> Aktivite Durumu <span></span></label>
					<select class="name" style="width:70%" name="tur">
					  <option value="0">Herkese Açık</option>
					  <option value="1">Arkadaşlara Özel</option>
					  <option value="2">Takipçilere Özel</option>
					</select>
					<div class="clearfix"></div>
					<label>Kategori Seç <span></span></label>
					<select class="name" style="width:70%" name="aktivite_kat">
					  <?php foreach($kategoriler as $row): ?>
					  <option value="<?=$row->id?>"><?=$row->adi ?></option>
					  <?php endforeach; ?>
					</select>
					<div class="clearfix"></div>

					<label>Kimler katılabilir ? <span></span></label>
					<select class="name" style="width:70%" name="cinsiyet">
					  <option value="0">Hepsi</option>
					  <option value="1">Sadece Erkek</option>
					  <option value="2">Sadece Bayan</option>
					</select>
					<div class="clearfix"></div>
				<div class="clearfix"></div>
						   

			<div class="clearfix"></div><br><br>
	

				<!--  Harita SON -->
				</div>
						
					<input type="submit" value="Oluştur">					
					<div class="clearfix"></div>
					</form>
					</div>
			</div>
		</div>	
	</div>

	<script type="text/javascript">
	
	$(document).ready(function(){
		$("#mesafedurumu").click(function(){
			$("#mesafe").stop().fadeToggle(100);
		});
	});
	
	var myMap;
	var position,isMapsLoaded = false;

	if(navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(getCurrentPos,errorCallback);
	}
	else
	{
		position = false;
	}
	
	
	function errorCallback(){
		position = false;
	}
	
	function getCurrentPos(pos){
		if(!isMapsLoaded){
		position = pos.coords;
		$('#loadergif').hide();
		$('#map').css('height','400px');
		$('#map').empty();
		ymaps.ready(init);
		}
	}
	
	setTimeout(function(){
		if(!isMapsLoaded){
			position = false;
			$('#loadergif').hide();
			$('#map').css('height','400px');
			ymaps.ready(init);
		}
		
	},5000); 
	

	function init () {
		// Creating an instance of the map and binding it to the container with the specified ID
		// ("map").
		isMapsLoaded = true;
		var defaultCoords = [35.166170, 33.376186];
		
		myMap = new ymaps.Map('map', {
			// When initializing the map, you must specify its center and the zoom factor.
			center: (!position) ? defaultCoords : [position.latitude,position.longitude], // Москва
			zoom: 10
		}, {
			searchControlProvider: 'yandex#search'
		});
		
		
		var myPlacemark = new ymaps.Placemark([35.166170, 33.376186],
		{ 
			iconContent: 'Aktivitenizin adresini haritaya tıklayarak belirtin.'
		},{
		// Options. The placemark's icon will stretch to fit its contents.
		preset: 'islands#blueStretchyIcon'
		}); 

		myMap.geoObjects.add(myPlacemark);
		
		if(position){
			
			myMap.geoObjects.removeAll();
			
			var myPlacemark = new ymaps.Placemark([position.latitude,position.longitude],
			{ 
				iconContent: 'Aktiviteniz Burada!'
			},{
			// Options. The placemark's icon will stretch to fit its contents.
			preset: 'islands#blueStretchyIcon'
			}); 

			myMap.geoObjects.add(myPlacemark);
			
			
			$("#maplat").val(position.latitude);
			$("#maplng").val(position.longitude);
		}
		

		myMap.events.add('click', function (e) {
			
			myMap.geoObjects.removeAll();
			// Getting the click coordinate
			var coords = e.get('coords');
			
			var myPlacemark = new ymaps.Placemark([coords[0],coords[1]],
			{ 
				iconContent: 'Aktiviteniz Burada!'
			},{
			// Options. The placemark's icon will stretch to fit its contents.
			preset: 'islands#blueStretchyIcon'
			}); 

			myMap.geoObjects.add(myPlacemark);

			$("#maplat").val(coords[0]);
			$("#maplng").val(coords[1]);
		//alert(coords.join(', '));
		
	});

	}
	


	
	</script>