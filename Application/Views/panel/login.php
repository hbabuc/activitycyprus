<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Activity Cyprus</title>

    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo baseurl(STYLES_DIR) ?>../font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/animate.css" rel="stylesheet">
    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

			<div class="col-md-12" style="max-width: 500px;">
			<center><h2 class="font-bold"><span style="color:#222;">Activity Cyprus</span><br /> <small>Admin Panel Girişi</small> </h2></center>
				

                <div class="ibox-content">
                    <form class="m-t" role="form" action="<?php echo baseurl("panel/plogin/send") ?>" method="post">
						<?php echo Warning::get(); ?>
						<div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Kullanıcı Adı" required="*">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="sifre" placeholder="Şifre" required="">
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Giriş Yap</button>
                        
                            <center><small>Tüm Hakları Saklıdır. © 2016</small></center>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
