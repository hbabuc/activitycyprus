<?php 

	if(!User::check() || User::yetki()<1){
		
		User::logout();
		redirect(baseurl("panel/plogin"));
		
	}

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Activity Cyprus Yönetim Paneli</title>

    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo baseurl(STYLES_DIR) ?>../font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/animate.css" rel="stylesheet">
    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/style.css" rel="stylesheet">
    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo baseurl(STYLES_DIR) ?>panel/summernote/summernote.css" rel="stylesheet">

</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?php echo baseurl(UPLOADS_DIR).User::kucukresim() ?>" style="width:50px;height:50px;">
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo User::adisoyadi() ?></strong>
                             </span> <span class="text-muted text-xs block"><?php echo User::yetkiadi() ?> <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php echo baseurl("panel/puser/edit/?dataGridType=dataGridTypeEdit&dataGridId=".User::id());?>">Profil</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo baseurl("panel/plogin/logout/") ?>">Çıkış Yap</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        AC
                    </div>
                </li>
                <li <?php if(Uri::segment(2)=="phome"){ echo ' class="active"'; } ?>>
                    <a href="<?php echo baseurl("panel/phome/index/") ?>"><i class="fa fa-home"></i> <span class="nav-label">Anasayfa</span></a>
                </li>
				<?php if(Yetki::check(16)){ ?>
					<li <?php if(Uri::segment(2)=="picerikler"){ echo ' class="active"'; } ?>>
						<a href="<?php echo baseurl("panel/picerikler/") ?>"><i class="fa fa-briefcase"></i> <span class="nav-label">Aktiviteler</span></a>
						
					</li>
				<?php } ?>
	
				<?php if(Yetki::check(14)){ ?>
					<li <?php if(Uri::segment(2)=="pyorumlar"){ echo ' class="active"'; } ?>>
						<a href="<?php echo baseurl("panel/pyorumlar/index/") ?>"><i class="fa fa-comments"></i> <span class="nav-label">Yorumlar</span></a>
					</li>
				<?php } ?>
				
				<?php if(Yetki::check(2)){ ?>
					<li <?php if(Uri::segment(2)=="puser"){ echo ' class="active"'; } ?>>
						<a href="<?php echo baseurl("panel/puser/index/") ?>"><i class="fa fa-users"></i> <span class="nav-label">Kullanıcılar</span></a>
					</li>
				<?php } ?>
				<?php if(Yetki::check(7)){ ?>
					<li <?php if(Uri::segment(2)=="pdosyalar"){ echo ' class="active"'; } ?>>
						<a href="<?php echo baseurl("panel/pdosyalar/index/") ?>"><i class="fa fa-folder-open-o"></i> <span class="nav-label">Galeri</span></a>
					</li>
				<?php } ?>
				
				<?php if(Yetki::check(2) || Yetki::check(11)){ ?>
					<li <?php if(Uri::segment(2)=="pyetki" || Uri::segment(2)=="payarlar"){ echo ' class="active"'; } ?>>
						<a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Site Ayarları</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							
							<?php if(Yetki::check(11)){ ?>
							<li><a href="<?php echo baseurl("panel/payarlar/") ?>">Genel Ayarlar</a></li>
							<?php } ?>
							
						</ul>
					</li>
				<?php } ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
	
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="#">
                <div class="form-group">
                    <input type="text" placeholder="Ara..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Activity Cyprus Yönetim Paneline Hoşgeldiniz.</span>
                </li>
                <li>
                    <a href="<?php echo baseurl("panel/plogin/logout/") ?>">
                        <i class="fa fa-sign-out"></i> Çıkış Yap
                    </a>
                </li>
            </ul>

        </nav>
        </div>

		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-lg-10">
				<h2>Activity Cyprus Yönetim Paneli</h2>
				
			</div>
			<div class="col-lg-2">

			</div>
		</div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <?php echo $sayfa; ?>
        </div>
        <div class="footer">
            <div>
                <strong>Copyright</strong> ActivityCyprus.com 2016
            </div>
        </div>

    </div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/jquery-2.1.1.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/bootstrap.min.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/plugins/dropzone/dropzone.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/inspinia.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/plugins/pace/pace.min.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/plugins/clipboard/clipboard.min.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/plugins/nestable/jquery.nestable.js"></script>
<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/main.js"></script>

<script src="<?php echo baseurl(SCRIPTS_DIR) ?>panel/plugins/summernote/summernote.min.js"></script>
<script>

    $(document).ready(function (){

        new Clipboard('.btnaaa');
		
		$(".part2").hide();
		$(".part4").hide();

    });

</script>

<script>

	$(document).ready(function(){
		
		var summer = $('.summernote');
		
		summer.summernote({
			
			height: 300,
			lang: 'ko-tr',
			minHeight: 500,
			maxHeight: null,
			focus: false,
			toolbar: [

				['style', [ "style"]],
				['fontname', ["fontname"]],
				['fontsize', ['fontsize']],
				['bold', ['bold', 'italic', 'underline',"hr", 'clear']],
				["link",["hello","link","unlink"]],
				['height', ['height']],
				['table', ['table']],
				["Misc",["undo","redo"]],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph',]],
				["Misc",["fullscreen","codeview","help"]]
			],
			buttons:{
			
				hello:function(context){
					
					var ui = $.summernote.ui;
					
					var button = ui.button({
						contents: '<i class="fa fa-picture-o"/>',
						tooltip: 'Resim Seç ',
						click: function (e) {
							
							$("#ResimSec").modal();
							
						}
					});
					
					return button.render();
					
				}
					
			}
			  
		});
		
	});
	
   function calistir(){
	   
	   $('.summernote').summernote('insertImage', "<?php echo baseurl(UPLOADS_DIR."profil.jpg") ?>");
		
	}
		
	$(function(){
		
		$(".gosummer").click(function(){
			
			$("textarea[name=aciklama]").html($(".note-editable").html());
			$("#postform").submit();
		});
		
	})
</script>

<script>
	
	
	Dropzone.options.myAwesomeDropzone = {
		
		init: function() {
			
			this.on("complete", function(file) { 

				$.ajax({
					
					type:"POST",
					url:"<?php echo baseurl("panel/pdosyalar/lastItem"); ?>",
					success:function(cevap){
						
						$("#lastItem").html(cevap);
						
					}
				
				});
			
			});
	
		}
	
	};
	
	function searchImg(){
		
		$.ajax({
			
			type:"POST",
			url:"<?php echo baseurl("panel/pdosyalar/searchImg") ?>",
			data:"ara="+$("input[name=ImgSearch]").val(),
			success:function(cevap){
				
				$("#lastItem").html(cevap);
				
			}
			
			
		});
		
	}
	
	<?php  if(Uri::segment(2)=="pkategoriler" && Uri::segment(3)=="sirala"){ ?>
		
		 $(document).ready(function(){

			 var updateOutput = function (e) {
				 var list = e.length ? e : $(e.target),
				output = list.data('output');
				 if (window.JSON) {
					 output.val(window.JSON.stringify(list.nestable('serialize')));
				 } else {
					 output.val('JSON browser support required for this demo.');
				 }
				 $.ajax({
					 
					 type:"POST",
					 url:"<?php echo baseurl("panel/pkategoriler/doSirala") ?>",
					 data:"json="+window.JSON.stringify(list.nestable('serialize')),
					 dataType:"json",
					 success:function(cevap){
						 
						 alert("Başarılı");
						 
					 }
					 
				 })
			 };
			 
			 $('#nestable2').nestable({
				 group: 1
			 }).on('change', updateOutput);
			 
			 updateOutput($('#nestable2').data('output', $('#nestable2-output')));

		 });
		 
	<?php } ?>
	
	function firmaAra(){
		
		var kelime = $("input[name=firmaAra]").val();
		
		$.ajax({
			
			type:"POST",
			url:"<?php echo baseurl("panel/pkampanyalar/firmaSearch/") ?>",
			data:"ara="+kelime,
			success:function(cevap){
				
				$("#firmasonuc").html(cevap);
				
			}
			
		})
		
	}
	
	function firmaSec(id){
		
		var firmaimg = $(".firmaimg"+id).attr("src");		
		var firmaadi = $(".firmaadi"+id).html();		
		
		$(".firmaimg").attr("src",firmaimg);
		$(".firmaadi").html(firmaadi);
		$("input[name=firmaid]").val(id);
		
	}
	
	function selectImg(id){
		
		var firmaimg = $(".selectImg"+id).attr("src");
		
		$(".selectImg").attr("src",firmaimg);
		$("input[name=resim]").val(id);
		
	}
	
	function firmakaydet(){
		
		$(".part1").slideToggle();
		$(".part2").slideToggle();
		
	}
	
	function ImgToggle(){
		
		$(".part3").slideToggle();
		$(".part4").slideToggle();
		
	}
	
	function firmaTekrarSec(){
		
		$(".part1").slideToggle();
		$(".part2").slideToggle();
		
		$("input[name=firmaAra]").val("");
		$("#firmasonuc").html("<h2 style='color:#555;font-weight:bold;text-align:center'>Yukarıdaki arama kutusunu kullanarak firma araması yapabilirsiniz.</h2><div class='clearfix'></div>");
		
	}
	
</script>

</body>

</html>
