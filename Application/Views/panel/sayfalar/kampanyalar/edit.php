			<?php echo Warning::get();  ?>
			<div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?php echo $title ?> <small><?php echo $titlesmall ?></small></h5>
                            <div class="ibox-tools">
                               <a href="<?php echo baseurl("panel/".Uri::segment(2)."/") ?>">Geri Dön</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form method="post" id="postform" action="<?php echo baseurl("panel/".Uri::segment(2)."/doEdit/$id") ?>" class="form-horizontal">
								<div class="part2">
									<div class="form-group">
										<div class="col-sm-12">
											<button type="button" class="btn btn-primary col-xs-12" data-toggle="modal" data-target="#myModal">
												Kampanya İçin Bir Firma Seçiniz
											</button>
										</div>
									</div>
									<div class="hr-line-dashed"></div>
								</div>
								<div class="part1">
									<label class="col-sm-2 control-label">Firma Bilgileri</label>
									<div class="col-xs-6">
										<div class="row">
											<div class="col-md-4">
												<img src="<?php echo baseurl(UPLOADS_DIR.$row->firmaresim) ?>" class="col-xs-12 firmaimg"/>
											</div>
											<div class="col-md-8">
												<h2 class="firmaadi" style="color:#555;font-weight:bold;"><?php echo $row->firmadi ?></h2>
												<p><a href="JavaScript:;" class="btn btn-primary" onclick="firmaTekrarSec()">Tekrar Seç</a></p>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="hr-line-dashed"></div>
								</div>
								<div class="part4">
									<div class="form-group">
										<div class="col-sm-12">
											<?php Dosya::modalButton(); ?>
										</div>
									</div>
									<div class="hr-line-dashed"></div>
								</div>
								<div class="part3">
									<label class="col-sm-2 control-label">Kampanya Resmi</label>
									<div class="col-xs-6">
										<div class="row">
											<div class="col-md-4">
												<img src="<?php echo baseurl(UPLOADS_DIR.$row->resim) ?>" class="col-xs-12 selectImg"/>
											</div>
											<div class="col-md-8">
												<p><a href="JavaScript:;" class="btn btn-primary" onclick="ImgToggle()">Tekrar Seç</a></p>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="hr-line-dashed"></div>
								</div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Firma Kategorisi</label>
                                    <div class="col-sm-10">
										<select class="form-control" name="kategoriid">
											<?php foreach($kategoriler as $kategori){ ?>
												<option <?php if($kategori->id==$row->kategoriid){ echo "selected"; } ?> value="<?php echo $kategori->id ?>"><?php echo $kategori->adi ?></option>
											<?php } ?>
										</select>
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Kampanya Adı Giriniz</label>
                                    <div class="col-sm-10">
										<input type="hidden" name="firmaid" value="<?php echo $row->firmaid ?>" class="form-control">
										<input type="hidden" name="resim" value="<?php echo $row->resimid ?>" class="form-control">
										<input type="text" value="<?php echo $row->adi ?>" name="adi" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Kampanya İndirim Miktarı</label>
                                    <div class="col-sm-10">
										<input type="text" value="<?php echo $row->indirimMiktari ?>" name="indirimMiktari" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Kampanya Başlangıç Tarihi</label>
                                    <div class="col-sm-10">
										<input type="date" value="<?php echo $row->baslangictarihi ?>" name="baslangictarihi" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Kampanya Bitiş Tarihi</label>
                                    <div class="col-sm-10">
										<input type="date" value="<?php echo $row->bitistarihi ?>" name="bitistarihi" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Tamamlamak İstediğiniz Bilet Sayısı</label>
                                    <div class="col-sm-10">
										<input type="number" value="<?php echo $row->biletsayi ?>" name="biletsayi" value="1" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Kampanya Etiketleri</label>
                                    <div class="col-sm-10">
										<input type="text" value="<?php echo $row->etiketler ?>" name="etiketler" value="1" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Kısa Kampanya Açıklaması</label>
                                    <div class="col-sm-10">
										<textarea name="kisaaciklama" value="1" class="form-control"><?php echo $row->kisaaciklama ?></textarea>
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Özel Alanlar</label>
                                    <div class="col-sm-10">
										<label>
											<input type="checkbox" <?php if($row->slider==1){ echo 'checked'; } ?> value="1" name="slider"> Slider  
										</label>
										<label>
											<input type="checkbox" <?php if($row->afis==1){ echo 'checked'; } ?> value="1" name="afis"> Afiş 1  
										</label>
										<label>
											<input type="checkbox" <?php if($row->afis1==1){ echo 'checked'; } ?> value="1" name="afis1"> Afiş 2  
										</label>
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
								
								<textarea name="aciklama" id="" cols="30" rows="10" style="display:none;"><?php echo $row->aciklama ?></textarea>
								<div class="ibox-content no-padding">
									<div class="summernote"><?php echo $row->aciklama ?></div>
								</div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">İptal</button>
                                        <a href="<?php echo baseurl("panel/pkampanyalar/editImages/$id"); ?>" class="btn btn-info pull-right" type="button">Kampanya Resimleri</a>
                                        <button class="btn btn-primary gosummer" type="button">Veriyi Güncelle</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
								<i class="fa fa-search modal-icon"></i>
								<h4 class="modal-title">Firma Ara</h4>
								<input type="text" name="firmaAra" placeholder="Filitrelemek İçin Firmanın Adını Giriniz" onkeyup="firmaAra()" class="form-control">
							</div>
							<div class="modal-body" id="firmasonuc">
								<h2 style="color:#555;font-weight:bold;text-align:center">Yukarıdaki arama kutusunu kullanarak firma araması yapabilirsiniz.</h2>
								<div class="clearfix"></div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Kapat</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="firmakaydet()">Firmayı Kaydet</button>
							</div>
						</div>
					</div>
				</div>
				
			<?php Dosya::modal(); ?>