
		<?php echo Warning::get(); ?>
		<div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?php echo $tableTitle ?></h5>
                            <div class="ibox-tools">
                                <a href="<?php echo baseurl(Uri::segment(1)."/".Uri::segment(2)."/".Uri::segment(3)."/") ?>">
                                    Geri D�n
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-5 m-b-xs">
									<?php if($InsertKontrol){ ?>
										<a href="<?php echo baseurl("panel/".Uri::segment(2)."/ekle") ?>" class="btn btn-primary pull-left" style="margin-right:5px;"><i class="fa fa-plus"></i> Yeni Veri Ekle</a>
									<?php } ?>
									<form id="TopluIslemForm" class="pull-left" action="<?php echo baseurl("panel/topluislem/") ?>" method="post">
										<input type="hidden" id="TopluIslemHidden" name="TopluIslemHidden" value=""/>
										<input type="hidden" id="TobluIslemDbName" name="TobluIslemDbName" value="<?php echo  $DbName ?>"/>
										<input type="hidden" id="TobluIslemType"  name="TobluIslemType" value=""/>
										<?php if($RemoveKontrol){ ?>
											<button type="button" class="btn btn-danger sSil"><i class="fa fa-trash"></i> Se�ilenleri Sil</button>
										<?php } ?>
										<?php if($OnayKontrol){ ?>
											<button type="button" class="btn btn-info oDegis"><i class="fa fa-trash"></i> Se�inlen Onay De�i�</button>
										<?php } ?>
									</form>
								</div>
                                <div class="col-sm-3 pull-right">
									<form action="" method="post">
										<div class="input-group">	
											<input type="hidden" name="dataGridType" value="dataGridSearch">
											<input type="text" placeholder="Tabloda Aray�n" name="query" class="input-sm form-control"> 
											<span class="input-group-btn">
												<button type="submit" class="btn btn-sm btn-primary"> Ara !</button> 
											</span>
										</div>
									</form>
                                </div>
                            </div>
                            <div class="table-responsive">
                               <?php echo DataGrid::sql($DataGrid)->remove($RemoveKontrol,baseurl("panel/".Uri::segment(2)."/delete/"))->edit($EditKontrol,baseurl("panel/".Uri::segment(2)."/edit/"))->columns($columns)->create(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>