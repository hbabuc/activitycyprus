			<?php echo Warning::get(); ?>
			<div class="row">
                <div class="col-lg-9">
					
					<input type="text" value="" placeholder="Bir başlık girin" name="adi" class="form-control" style="margin-bottom:30px;">
				
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <form method="post" id="postform" action="<?php echo baseurl("panel/".Uri::segment(2)."/doInsert") ?>" class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-12">
										<input type="hidden" name="firmaid" value="" class="form-control">
										<input type="hidden" name="resim" value="" class="form-control">
									</div>
                                </div>
								
								<div style="background:#eee"><textarea name="aciklama" id="" cols="30" rows="10" style="display:none;"></textarea></div>
								<div class="ibox-content no-padding">
									<div class="summernote"></div>
								</div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">Sil</button>
                                        <button class="btn btn-primary gosummer" type="button">Veriyi Ekle</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
							<h3>Etiketler</h3>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="firmaid" value="" class="form-control">
								</div>
								<div class="clearfix"></div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
						<div class="ibox-content">
							<h3>Yayımla</h3>
							<div class="form-group">
								<div class="col-md-6">
									<label for="onay1" style="padding:20px;font-size:16px;">
										<input type="radio" name="onay" style="background:#cecece;height:15px;width:15px;margin-top:15px;" checked='checked' value="1" id="onay1"> Yayınla
									</label>
								</div>
								<div class="col-md-6">
									<label for="onay2" style="padding:20px;font-size:16px;">
										<input type="radio" name="onay" style="background:#cecece;height:15px;width:15px;margin-top:15px;" value="1" id="onay2"> Taslak
									</label>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-6">
									<button class="btn col-md-12 btn-primary gosummer" type="button">İçeriği Ekle</button>
								</div>
								<div class="col-sm-6">
									<button class="btn btn-white col-md-12" type="submit">Sil</button>
								</div>
							</div>
							<div class="clearfix"></div>
                        </div>
					</div>
                    <div class="ibox float-e-margins">
						<div class="ibox-content">
							<h3>Biçim</h3>
							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="type1" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" style="background:#cecece;height:15px;width:15px;margin-top:15px;" checked='checked' value="1" id="type1"> <i class="fa fa-thumb-tack fa-2x" style="margin:10px;"></i> Standart
									</label>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<label for="type2" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" style="background:#cecece;height:15px;width:15px;margin-top:15px;"  value="1" id="type2"> <i class="fa fa-photo fa-2x" style="margin:10px;"></i> Görsel
									</label>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<label for="type3" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" style="background:#cecece;height:15px;width:15px;margin-top:15px;"  value="1" id="type3"> <i class="fa fa-film fa-2x" style="margin:10px;"></i> Video
									</label>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<label for="type4" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" style="background:#cecece;height:15px;width:15px;margin-top:15px;"  value="1" id="type4"> <i class="fa fa-link fa-2x" style="margin:10px;"></i> Bağlantı
									</label>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<label for="type5" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" style="background:#cecece;height:15px;width:15px;margin-top:15px;" value="1" id="type5"> <i class="fa fa-sliders fa-2x" style="margin:10px;"></i> Galeri
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
                        </div>
					</div>
                    <div class="ibox float-e-margins">
						<div class="ibox-content">
							<h3>Resim</h3>
							<div class="part3">
								<div class="form-group">
									<div class="col-sm-12">
										<?php Dosya::modalButton(); ?>
									</div>
								</div>
								<div class="hr-line-dashed"></div>
							</div>
							<div class="part4">
								<div class="row">
									<img src="" class="col-xs-12 selectImg"/>
									<div class="col-md-12" style="margin-top:20px;">
										<p><a href="JavaScript:;" class="btn btn-primary col-md-12" onclick="ImgToggle()">Tekrar Seç</a></p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
                        </div>
					</div>
				</div>
            </div>
			<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
								<i class="fa fa-search modal-icon"></i>
								<h4 class="modal-title">Firma Ara</h4>
								<input type="text" name="firmaAra" placeholder="Filitrelemek İçin Firmanın Adını Giriniz" onkeyup="firmaAra()" class="form-control">
							</div>
							<div class="modal-body" id="firmasonuc">
								<h2 style="color:#555;font-weight:bold;text-align:center">Yukarıdaki arama kutusunu kullanarak firma araması yapabilirsiniz.</h2>
								<div class="clearfix"></div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Kapat</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="firmakaydet()">Firmayı Kaydet</button>
							</div>
						</div>
					</div>
				</div>
			<?php Dosya::modal(); ?>