			<?php echo Warning::get(); ?>
			<div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?php echo $title ?> <small><?php echo $titlesmall ?></small></h5>
                            <div class="ibox-tools">
                               <a href="<?php echo baseurl("panel/".Uri::segment(2)."/") ?>">Geri Dön</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form method="post" id="postform" action="<?php echo baseurl("panel/".Uri::segment(2)."/doInsertImg/$id") ?>" class="form-horizontal">
								<div class="part3">
									<div class="form-group">
										<div class="col-sm-12">
											<?php Dosya::modalButton(); ?>
										</div>
									</div>
									<div class="hr-line-dashed"></div>
								</div>
								<div class="part4">
									<label class="col-sm-2 control-label">Kampanya Resmi</label>
									<div class="col-xs-6">
										<div class="row">
											<div class="col-md-4">
												<img src="" class="col-xs-12 selectImg"/>
											</div>
											<div class="col-md-8">
												<p><a href="JavaScript:;" class="btn btn-primary" onclick="ImgToggle()">Tekrar Seç</a></p>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="hr-line-dashed"></div>
									<input type="hidden" name="resim" value="" class="form-control">
								</div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">İptal</button>
                                        <button class="btn btn-primary gosummer" type="button">Veriyi Ekle</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row">
				<?php foreach($resimler as $resim){ ?>
					<div class="col-md-4">
						<div class="thumbnail">
							<div class="row">
							<img src="<?php echo baseurl(UPLOADS_DIR.$resim->resimadi) ?>" class="col-xs-12"/>
							</div>
							<br/>
							
							<a href="<?php echo baseurl("panel/pkampanyalar/deleteKampanyaImg/".$resim->id); ?>" class="btn btn-danger col-xs-12">Resmi Kaldır</a>
							
							<br/>
							<div class="clearfix"></div>
						</div>
					</div>
				<?php } ?>
			</div>
			<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
								<i class="fa fa-search modal-icon"></i>
								<h4 class="modal-title">Firma Ara</h4>
								<input type="text" name="firmaAra" placeholder="Filitrelemek İçin Firmanın Adını Giriniz" onkeyup="firmaAra()" class="form-control">
							</div>
							<div class="modal-body" id="firmasonuc">
								<h2 style="color:#555;font-weight:bold;text-align:center">Yukarıdaki arama kutusunu kullanarak firma araması yapabilirsiniz.</h2>
								<div class="clearfix"></div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Kapat</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="firmakaydet()">Firmayı Kaydet</button>
							</div>
						</div>
					</div>
				</div>
			<?php Dosya::modal(); ?>