			<?php echo Warning::get(); ?>
			<div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?php echo $title ?> <small><?php echo $titlesmall ?></small></h5>
                            <div class="ibox-tools">
                               <a href="<?php echo baseurl("panel/".Uri::segment(2)."/") ?>">Geri Dön</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form method="post" action="<?php echo baseurl("panel/".Uri::segment(2)."/doEdit/".$row->id) ?>" class="form-horizontal">
							
								<div class="part4">
									<div class="form-group">
										<div class="col-sm-12">
											<?php Dosya::modalButton(""); ?>
										</div>
									</div>
									<div class="hr-line-dashed"></div>
								</div>
								<div class="part3">
									<label class="col-sm-2 control-label">Kampanya Resmi</label>
									<div class="col-xs-6">
										<div class="row">
											<div class="col-md-4">
												<img src="<?php echo baseurl(UPLOADS_DIR.$row->resimadi) ?>" class="col-xs-12 selectImg"/>
											</div>
											<div class="col-md-8">
												<p><a href="JavaScript:;" class="btn btn-primary" onclick="ImgToggle()">Tekrar Seç</a></p>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="hr-line-dashed"></div>
								</div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Firma Adı Giriniz</label>
                                    <div class="col-sm-10">
										<input type="text" name="adi" value="<?php echo $row->adi ?>" class="form-control">
										<input type="hidden" name="resim" value="<?php echo $row->resimid ?>" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Firma Adresi</label>
                                    <div class="col-sm-10">
										<input type="text" name="adres" value="<?php echo $row->adres ?>" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Firma Telefonu</label>
                                    <div class="col-sm-10">
										<input type="text" name="telefon" value="<?php echo $row->telefon ?>" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Firma Web Sitesi</label>
                                    <div class="col-sm-10">
										<input type="text" name="web" value="<?php echo $row->web ?>" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
									<label class="col-sm-2 control-label">Firma Konumu</label>
                                    <div class="col-sm-10">
										<input type="text" name="konum" value="<?php echo htmlspecialchars($row->konum) ?>" class="form-control">
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">İptal</button>
                                        <button class="btn btn-primary" type="submit">Veriyi Ekle</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			<?php Dosya::modal(); ?>