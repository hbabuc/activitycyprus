			<?php echo Warning::get(); ?>
			<div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?php echo $title ?> <small><?php echo $titlesmall ?></small></h5>
                            <div class="ibox-tools">
                               <a href="<?php echo baseurl("panel/".Uri::segment(2)."/") ?>">Geri Dön</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form method="post" action="<?php echo baseurl("panel/".Uri::segment(2)."/doInsert") ?>" class="form-horizontal">
                                <div class="form-group">
									<label class="col-sm-2 control-label">Key</label>
                                    <div class="col-sm-10"><input type="text" name="key" class="form-control"></div>
                                </div>
								<div class="hr-line-dashed"></div>
								<?php foreach($diller as $dil){ ?>
									<div class="form-group">
										<input type="hidden" value="<?php echo $dil->kisa ?>" name="dil[]" class="form-control">
										<input type="hidden" value="<?php echo $dil->id ?>" name="dilid[]" class="form-control">
										<label class="col-sm-2 control-label">Value ( <?php echo $dil->kisa ?> )<br/></label>
										<div class="col-sm-10"><textarea name="value[]" id="" class="form-control" cols="30" rows="10"></textarea></div>
									</div>
									<div class="hr-line-dashed"></div>
								<?php } ?>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">İptal</button>
                                        <button class="btn btn-primary" type="submit">Veriyi Ekle</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>