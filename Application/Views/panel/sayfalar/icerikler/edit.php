		<form	 method="post" id="postform" action="<?php echo baseurl("panel/".Uri::segment(2)."/doEdit") ?>" class="form-horizontal">
			<?php echo Warning::get(); ?>
			
			<?php $postback = Session::select("PostBack"); ?>
			
			<?php
				
				$etiketi = "";
				
				if(!$postback){
					
					$postback = json_decode(json_encode($row),true);
					
				} 

			?>
			<div class="row">
                <div class="col-lg-9">
					<input type="text" placeholder="Bir başlık girin" name="baslik" value="<?php if($postback){ echo $postback["baslik"]; } ?>" class="form-control" style="margin-bottom:30px;">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
							<div class="form-group">
								<div class="col-sm-12">
									<input type="hidden" name="resim" value="<?php if($postback){ echo $postback["resim"]; } ?>" class="form-control">
									<input type="hidden" name="baglantiid" value="<?php if($postback){ echo $id; } ?>" class="form-control">
								</div>
							</div>
							<div style="background:#eee"><textarea name="aciklama" id="" cols="30" rows="10" style="display:none;"><?php if($postback){ echo ($postback["aciklama"]); } ?></textarea></div>
							<div class="ibox-content no-padding">
								<div class="summernote"><?php if($postback){ echo ($postback["aciklama"]); } ?></div>
							</div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
							<h3>Etiketler</h3>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="etiketler" value="<?php foreach($etiketler as $etiket){
										
										$etiketi .= $etiket->adi.",";
										
									}
									echo substr($etiketi,0,-1);
									?>" class="form-control">
								</div>
								<div class="clearfix"></div>
							</div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins bicim bicim3">
                        <div class="ibox-content">
							<h3>Video Kodu</h3>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="video" value="<?php if($postback){ echo htmlspecialchars($postback["video"]); } ?>" class="form-control">
									<input type="hidden" name="tarih" value="<?php echo date("Y-m-d H:i:s") ?>" class="form-control">
								</div>
								<div class="clearfix"></div>
							</div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins bicim bicim4">
                        <div class="ibox-content">
							<h3>Paylaşım Linki</h3>
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="link" value="<?php if($postback){ echo $postback["link"]; } ?>" class="form-control">
								</div>
								<div class="clearfix"></div>
							</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
						<div class="ibox-content">
							<h3>Yayımla</h3>
							<div class="form-group">
								<div class="col-md-6">
									<label for="onay1" style="padding:20px;font-size:16px;">
										<input type="radio" name="onay" style="background:#cecece;height:15px;width:15px;margin-top:15px;" checked='checked' value="1" id="onay1"> Yayınla
									</label>
								</div>
								<div class="col-md-6">
									<label for="onay2" style="padding:20px;font-size:16px;">
										<input type="radio" name="onay" style="background:#cecece;height:15px;width:15px;margin-top:15px;" value="1" id="onay2"> Taslak
									</label>
								</div>
								<div class="clearfix"></div>
								<div class="hr-line-dashed"></div>
								<div class="col-md-12 text-center">
									<a href="#myModal" class="onceki btn btn-warning col-md-12" data-toggle="modal" data-target="#myModal">Önceki Versiyonları İncele</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-6">
									<button class="btn col-md-12 btn-primary gosummer" type="button">İçeriği Ekle</button>
								</div>
								<div class="col-sm-6">
									<button class="btn btn-white col-md-12" type="submit">Sil</button>
								</div>
							</div>
							<div class="clearfix"></div>
                        </div>
					</div>
                    <div class="ibox float-e-margins">
						<div class="ibox-content">
							<h3>Biçim</h3>
							<div class="form-group BicimType">
								<div class="col-md-10 col-md-offset-1">
									<label for="type1" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" <?php if($postback && $postback["type"]==1){ echo "checked='checked'"; } ?> style="background:#cecece;height:15px;width:15px;margin-top:15px;" checked='checked' value="1" id="type1"> <i class="fa fa-thumb-tack fa-2x" style="margin:10px;"></i> Standart
									</label>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<label for="type2" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" <?php if($postback && $postback["type"]==2){ echo "checked='checked'"; } ?> style="background:#cecece;height:15px;width:15px;margin-top:15px;"  value="2" id="type2"> <i class="fa fa-photo fa-2x" style="margin:10px;"></i> Görsel
									</label>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<label for="type3" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type"<?php if($postback && $postback["type"]==3){ echo "checked='checked'"; } ?> style="background:#cecece;height:15px;width:15px;margin-top:15px;"  value="3" id="type3"> <i class="fa fa-film fa-2x" style="margin:10px;"></i> Video
									</label>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<label for="type4" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" <?php if($postback && $postback["type"]==4){ echo "checked='checked'"; } ?> style="background:#cecece;height:15px;width:15px;margin-top:15px;"  value="4" id="type4"> <i class="fa fa-link fa-2x" style="margin:10px;"></i> Bağlantı
									</label>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<label for="type5" style="padding:0 20px;font-size:15px;">
										<input type="radio" name="type" <?php if($postback && $postback["type"]==5){ echo "checked='checked'"; } ?> style="background:#cecece;height:15px;width:15px;margin-top:15px;" value="5" id="type5"> <i class="fa fa-sliders fa-2x" style="margin:10px;"></i> Galeri
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
                        </div>
					</div>
                    <div class="ibox float-e-margins">
						<div class="ibox-content">
							<h3>Kategoriler</h3>
							<div class="form-group">
								<?php foreach($kategoriler as $kategori){ ?>
									<div class="col-md-10 col-md-offset-1">
										<label for="kat<?php echo $kategori->id ?>" style="padding:0 20px;font-size:15px;">
										
											<input type="checkbox" <?php if($postback && isset($gethasKat)){
												
												foreach($gethasKat as $postkat){
													
													if($postkat->katid == $kategori->id)
														
														echo "checked";
													
												}
												
											} ?> name="kategoriler[]" style="background:#cecece;height:15px;width:15px;margin-top:15px;" value="<?php echo $kategori->id ?>" id="kat<?php echo $kategori->id ?>">  <?php echo $kategori->adi ?>
										</label>
									</div>
								<?php } ?>
							</div>
							<div class="clearfix"></div>
                        </div>
					</div> 
					<div class="ibox float-e-margins bicim bicim2">
						<div class="ibox-content">
							<h3>Resim</h3>
							<div class="part3">
								<div class="form-group">
									<div class="col-sm-12">
										<?php Dosya::modalButton(); ?>
									</div>
								</div>
								<div class="hr-line-dashed"></div>
							</div>
							<div class="part4">
								<div class="row">
									<img src="" class="col-xs-12 selectImg"/>
									<div class="col-md-12" style="margin-top:20px;">
										<p><a href="JavaScript:;" class="btn btn-primary col-md-12" onclick="ImgToggle()">Tekrar Seç</a></p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
                        </div>
					</div>
				</div>
            </div>
			<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
								<i class="fa fa-search modal-icon"></i>
								<h4 class="modal-title">Versiyonları İncele</h4>
							</div>
							<div class="modal-body" id="firmasonuc">
								<table class="table">
									<thead>
										<tr>
											<th>Versiyon Tarihi</th>
											<th>Versiyona Git</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($taslaklar as $taslak){ ?>
											<tr>
												<td><?php echo $taslak->tarih ?></td>
												<?php 
												
													if($taslak->baglantiid==0){
													
														$editid = $taslak->id;
													
													}else{
														
														$editid = $taslak->baglantiid;
															
													}
													
												?>
												<td><a href="<?php echo baseurl("panel/".Uri::segment(2)."/".Uri::segment(3)."/?dataGridType=dataGridTypeEdit&dataGridId=".$editid."&versiyon=true&versiyonid=".$taslak->id); ?>" class="btn btn-info">Versiyonu İncele</a></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
								<div class="clearfix"></div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Kapat</button>
							</div>
						</div>
					</div>
				</div>
			<?php Dosya::modal(); ?>
		</form>
		<?php if($postback){ ?>
		
			<script type="text/javascript">
			
					
					
					setTimeout(function(){
						
						
						$(".bicim").hide()
						$(".bicim<?php echo $postback["type"]; ?>").show();
						
					},500);
					
			</script>
		
		<?php } ?>
		
		