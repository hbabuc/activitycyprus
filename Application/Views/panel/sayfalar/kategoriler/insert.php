		<?php echo Warning::get(); ?>
		<form method="post" action="<?php echo baseurl("panel/".Uri::segment(2)."/doInsert") ?>" class="form-horizontal">
			<div class="row">
                <div class="col-lg-12">
					<div class="tabs-container">
                        <ul class="nav nav-tabs">
							<?php $sayi=1; ?>
							<?php foreach($diller as $dil){ ?>
								<li <?php if($sayi==1){ echo 'class="active"'; } ?>><a data-toggle="tab" href="#<?php echo $dil->kisa ?>"> <?php echo $dil->adi ?></a></li>
								<?php $sayi++; ?>
							<?php } ?>
                        </ul>
                        <div class="tab-content">
							<?php $sayi=1; ?>
							<?php foreach($diller as $dil){ ?>
								<div id="<?php echo $dil->kisa ?>" class="tab-pane <?php if($sayi==1){ echo 'active'; } ?>">
									<div class="panel-body" style="padding-top:50px">
										<div class="ibox float-e-margins">
											<div class="form-group">
												<label class="col-sm-2 control-label">Bir Kategori Adı Giriniz (<?php echo $dil->kisa ?>)</label>
												<div class="col-sm-10">
													<input type="text" name="adi[]" class="form-control">
													<input type="hidden" name="dilid[]" value="<?php echo $dil->id ?>" class="form-control">
												</div>
											</div>
											<div class="hr-line-dashed"></div>
											<div class="form-group">
												<div class="col-sm-4 col-sm-offset-2">
													<button class="btn btn-white" type="submit">İptal</button>
													<button class="btn btn-primary" type="submit">Veriyi Ekle</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php $sayi++; ?>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
		</form>
		<?php Dosya::iconModal(); ?>