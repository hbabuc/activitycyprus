
        <div class="row">
		<div class="col-lg-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Aktivite Sayısı</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins"><?=$aktiviteSayi;?></h1>
						<small>Sitedeki tüm aktivitelerin sayısı</small>
					</div>
				</div>
            </div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Kullanıcı Sayısı</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins"><?php echo $kullaniciSayi ?></h1>
						<small>Sitedeki tüm kullanıcıların sayısı</small>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Kategori Sayısı</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins"><?php echo $kategoriSayi ?></h1>
						<small>Sitedeki tüm kategorilerin sayısı</small>
					</div>
				</div>
            </div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Resim Sayısı</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins"><?php echo $resimSayi ?></h1>
						<small>Sitedeki tüm resimlerin sayısı</small>
					</div>
				</div>
            </div>
			
			
        </div>
		