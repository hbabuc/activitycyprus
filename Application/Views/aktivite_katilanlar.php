<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
 
<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR."datatables.js"); ?>"></script>
<br><br><br>

<div class="container">
	<?php /* echo "<pre>"; print_r($katilanlar); echo "</pre>"; */echo Warning::get(); ?>
	<div class="table-responsive">
	<table id="table_id" class="display table" >
	    <thead>
	        <tr>
	            <th>Resmi</th>
	            <th>Adı Soyadı</th>
	            <th>Katılma Durumu</th>
	            <th>İşlemler</th>
	        </tr>
	    </thead>
	    <tbody>
	    <?php  foreach($katilanlar as $row): ?>
	        <tr>
	        	<td> <img src="<?php echo baseurl(UPLOADS_DIR.$row->adi)?>" style="height:50px; width:50px;"> </td>
	        	<td> <a href="<?php echo baseurl($row->kadi) ?>"><?=$row->adisoyadi?></a> </td>
	            <td> <?php 
	            if( $row->durum == 2){   
	            		echo "<span class='label label-success'> Katılıyor </span>"; 
	            	}elseif( $row->durum == 1) { 
	            		echo "<span class='label label-warning'> Rezervli </span>"; 
	            	}elseif( $row->durum == 0){
	            		echo "<span class='label label-danger'> Davetyesi Var </span>"; 
	            		} ?> </td>
	            <td> 
	            	<a href="#" class="swal" data-link="<?php echo baseurl("aktivite/kisi_cikar/{$row->userid}/{$row->aktiviteid}") ?>"> [ Aktiviteden Çıkar ] </a> 
	            </td>

	        </tr>
	    <?php  endforeach; ?>
	    </tbody>
	</table>
	</div>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#table_id').DataTable();
		} );
	</script>
</div>