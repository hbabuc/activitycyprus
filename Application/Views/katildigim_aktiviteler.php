<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR."datatables.js"); ?>"></script>
<br><br><br>
<div class="container">
	<?php echo Warning::get(); ?>
	<div class="table-responsive">
	<table id="table_id" class="display table" >
	    <thead>
	        <tr>
	            <th>Resim</th>
	            <th>Başlık</th>
	            <th>İşlemler</th>
	        </tr>
	    </thead>
	    <tbody>
	  
	    <?php  foreach($katildiklarim as $row): ?>
	        <tr>
	        	<td>
	        		<img src="<? echo baseurl(UPLOADS_DIR.$row->adi) ?>" style="height:100px; width:100px;">
	        	</td>
	        	<td><?=$row->baslik?></td>
	        	<td> <a href="#" class="swal" data-link="<?php echo baseurl("aktivite/aktiviteden_cikis/{$row->aktiviteid}") ?>">[ Rezarvasyon İptali ]</a> </td>
	        </tr>
	    <?php  endforeach; ?>
	    </tbody>
	</table>
	</div>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#table_id').DataTable();
		} );
	</script>
</div>