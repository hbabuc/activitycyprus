<?php 
	$aktiviteID = Uri::segment(-1);
	$aktivite_model = new aktivite_model();  
?>
<div class="container" style="margin-top:100px;">
	<div class="panel panel-success">
	  <div class="panel-heading"> Seçili Aktiviteye Davetyesi olmayan arkadaşların </div>
	  <div class="panel-body" style="padding:20px;">
	  <form action="<?php  echo baseurl("aktivite/arkadas_form_davet/".Uri::segment(-1)); ?>" method="POST">
	    <?php   

	    //print_r($arkadaslar);
	    if( empty($arkadaslar)){
	    	echo "<div class='alert alert-success'>Malesef Kayıt Bulunamadı </div>";
	    }
	    foreach ($arkadaslar as $row) { 
	    	if ( $row->userid == User::id()){
	    		if($aktivite_model->katilan_kontrol($aktiviteID,$row->gonderenidsi)->totalRows() == 0): 
	    		?>
	    		  <div class="col-lg-3">
	    		    <div class="input-group">
	    		      <span class="input-group-addon">

						 <input type="checkbox" aria-label="..." name="arkadas[<?=$row->gonderenidsi?>]">
					 

	    		      </span>
	    		      <img src="<?=baseurl(UPLOADS_DIR).$row->gonderenimg ?>" style="height:100px; width:100px;">
	    		     	<a href="<?=baseurl($row->gonderenkadi)?>"> <?=$row->gonderenadisoyadi; ?></a>
	    		    </div><!-- /input-group -->
	    		  </div><!-- /.col-lg-6 -->
	    		<?php 	
	    		endif;
	    		}else{
	    			if($aktivite_model->katilan_kontrol($aktiviteID,$row->useridsi)->totalRows() == 0):
	    			?>
	    		<div class="col-lg-3">
	    			  <div class="input-group">
	    			    <span class="input-group-addon">
	    			      <input type="checkbox" aria-label="..." name="arkadas[<?=$row->useridsi?>]">
	    			    </span>
	    			    <img src="<?=baseurl(UPLOADS_DIR).$row->userimage ?>" style="height:100px; width:100px;">
	    			   <a href="<?=baseurl($row->userkadi)?>">  <?=$row->useradisoyadi; ?> </a>
	    			  </div><!-- /input-group -->
	    			</div><!-- /.col-lg-6 -->
	    			<?php
	    			endif;
	    		}
	    	}/* foreach end */
	    ?>
	  </div>
	  <div class="panel-footer"><input type="submit" value="Davet Et" class="btn btn-info"></div>
	  
	  </form>
	</div>

</div>