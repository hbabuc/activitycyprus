<?php  $userid = User::id();  ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
<script type="text/javascript" src="<?=baseurl(SCRIPTS_DIR."datatables.js"); ?>"></script>
<br><br><br>

<div class="container" style="padding-bottom:100px;">
	<?php  echo Warning::get(); ?>
	<div class="table-responsive">
	<table id="table_id" class="display table" >
	    <thead>
	        <tr>
	            <th>Resmi</th>
	            <th>Aktivite</th>
	            <th>Davetyeni Gönderen</th>
	            <th>İşlemler</th>
	        </tr>
	    </thead>
	    <tbody>
	    <?php  foreach($davetiyelerim as $row): ?>
	        <tr>
	        	<td> <img src="<?= baseurl(UPLOADS_DIR.$row->aktimage) ?>" style="height:100px; width:100px;"> </td>
	        	<td> <b> <a href="<?php echo baseurl($row->seo_link."/".$row->aktiviteid) ?>"> <?=$row->baslik ?> </a> </b> </td>
	        	<td> <a href="<?=baseurl($row->kadi)?>"> <?=$row->useradsoyadi ?> </a> </td>
	        	<td> 
	        	<a href="#" class="swal"  data-link="<?php echo baseurl("aktivite/davetiye_sil/{$row->id}/{$userid}") ?>">[ Davetiye Sil ] </a> 

	        	</td>
	        </tr>
	    <?php  endforeach; ?>
	    </tbody>
	</table>
	</div>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#table_id').DataTable();
		} );
	</script>
</div>