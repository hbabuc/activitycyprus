<?php $user_row = User::row(); ?>
<!-- content-starts-here -->
<div class="content">
	<div class="categories">
		<div class="container">
			<div class="row">
				<?php 
				## Kategori Listeleme
				foreach($kategoriler as $row): ?>
					<div class="col-md-2 focus-grid">
						<a href="<?php  echo baseurl("home/category/{$row->seo_link}") ?>">
							<div class="focus-border">
								<div class="focus-layout">
									<div class="focus-image"><i class="<?=$row->icon?>"></i></div>
									<h4 class="clrchg"><?=$row->adi?></h4>
								</div>
							</div>
						</a>
					</div>
					<?php  endforeach; ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
			<center><div class="container"><a href="#" class="btn btn-warning" id="konumbtn"> Konumuna Göre Sırala </a></center>
			<div class="clearfix"></div>
			<div class="container">

			
	<!-- Furnitures -->
	<div class="total-ads main-grid-border">
		<div class="container">
			<div class="ads-grid">
				<div class="row">
					<div class="ads-display col-md-12">
						<div class="wrapper">					
						<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						  <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
							<li role="presentation" class="active">
							  <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">
								<span class="text">Tüm Aktiviteler</span>
							  </a>
							</li>
							<li role="presentation" class="next">
							  <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">
								<span class="text">Biten Aktiviteler</span>
							  </a>
							</li>
							<li role="presentation">
							  <a href="#samsa" role="tab" id="samsa-tab" data-toggle="tab" aria-controls="samsa">
								<span class="text"> Devam Edenler </span>
							  </a>
							</li>
						<li role="presentation">
						  <a href="#ucretsiz" role="tab" id="ucretsiz-tab" data-toggle="tab" aria-controls="ucretsiz">
							<span class="text"> Ücretsizler </span>
						  </a>
						</li>
						
						<?php  if( User::check()):  

								if( $user_row->cinsiyet == 1){	?>

									<li role="presentation">
									  <a href="#erkekler" role="tab" id="erkekler-tab" data-toggle="tab" aria-controls="erkekler">
										<span class="text"> Erkek Aktiviteleri </span>
									  </a>
									</li>

						<?php	}elseif($user_row->cinsiyet == 0){ ?>

									<li role="presentation">
									  <a href="#bayanlar" role="tab" id="bayanlar-tab" data-toggle="tab" aria-controls="bayanlar">
										<span class="text"> Bayan Aktiviteleri </span>
									  </a>
									</li>

						<?php	}else{

								}
						?>

						<?php  else: ?>
							<li role="presentation">
							  <a href="#bayanlar" role="tab" id="bayanlar-tab" data-toggle="tab" aria-controls="bayanlar">
								<span class="text"> Bayan Aktiviteleri </span>
							  </a>
							</li>
							<li role="presentation">
							  <a href="#erkekler" role="tab" id="erkekler-tab" data-toggle="tab" aria-controls="erkekler">
								<span class="text"> Erkek Aktiviteleri </span>
							  </a>
							</li>
						<?php endif; ?>

						  </ul>

						  <div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
								 <div>
									<div id="container">
										<div class="clearfix"></div>
									<ul class="list">
										<?php 
										## Aktivite 1. listeleme
										if (empty($aktiviteler)){
											echo "<div class='alert alert-warning'>".ML::select("icerikyok")."</div>";
										}

										foreach($aktiviteler as $row): ?>
											<a href="<?=baseurl("{$row->seo_link}/{$row->id}")?>" class="col-md-6" style="height:180px !important;">
												<li style="height:160px;">
												<img style="height:100px; width:100px;" src="<?php echo baseurl(UPLOADS_DIR); ?><?=$row->resmi?>" title="" alt="" />
												<section class="list-left">
												<h5 class="title"><?=$row->baslik?></h5>
												<span class="adprice"><?
												if ($row->ucret == 0)
													echo "".ML::select("free")."";
												else
													echo $row->ucret." TL";
												?>  </span>
												<p class="catpath"><?=$row->kategorisi?> aktivitesi <?=$row->kadi?> tarafından oluşturuldu </p>
												</section>

												<section class="list-right">
												<span class="date"><?=$row->bastarih?></span>
												<span class="cityname"></span>
												</section>
												<div class="clearfix"></div>
												</li> 
											</a>
										<?php  endforeach; ?>
										<div class="clearfix"></div>
										<a href="<?php echo baseurl('home/alls')  ?>" class="btn btn-info"> Tümünü Gör </a>
										</ul>
									</div>
								</div>
								<div class="clearfix"></div>	
							</div>
							<div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
							 <div>
								<div id="container">
									<div class="clearfix"></div>
								<ul class="list">
		
									<?php 

									if (empty($biten_aktiviteler)){
										echo "<div class='alert alert-warning'>Henüz İçerik Eklenmemiş</div>";
									}
									

									foreach($biten_aktiviteler as $row): ?>
									<a href="<?=baseurl("{$row->seo_link}/{$row->id}")?>" class="col-md-6" style="height:180px !important;">
										<li style="height:160px;">
										<img style="height:100px; width:100px;" src="<?php echo baseurl(UPLOADS_DIR); ?><?=$row->resmi?>" title="" alt="" />
										<section class="list-left">
										<h5 class="title"><?=$row->baslik?></h5>
										<span class="adprice"><?
												if ($row->ucret == 0)
													echo "".ML::select("free")."";
												else
													echo $row->ucret." TL";
												?>  </span>
										<p class="catpath"><?=$row->kategorisi?> aktivitesi <?=$row->kadi?> tarafından oluşturuldu </p>
										</section>

										<section class="list-right">
										<span class="date"><?=$row->bittarih?> tarihinde sonlandı</span>
										<span class="cityname"></span>
										</section>
										<div class="clearfix"></div>
										</li> 
									</a>
									<?php endforeach; ?>
									<div class="clearfix"></div>
									<a href="<?php echo baseurl('home/alls')  ?>" class="btn btn-info"> Tümünü Gör </a>
								</ul>
							</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="samsa" aria-labelledby="samsa-tab">
							  <div>
							<div id="container">
									<div class="clearfix"></div>
								<ul class="list">
								
									
										<?php 
										if (empty($devam_edenler)){
											echo "<div class='alert alert-warning'>Henüz İçerik Eklenmemiş</div>";
										}

										foreach($devam_edenler as $row): ?>
										<a href="<?=baseurl("{$row->seo_link}/{$row->id}")?>" class="col-md-6" style="height:180px !important;">
											<li style="height:160px;">
											<img style="height:100px; width:100px;" src="<?php echo baseurl(UPLOADS_DIR); ?><?=$row->resmi?>" title="" alt="" />
											<section class="list-left">
											<h5 class="title"><?=$row->baslik?></h5>
											<span fclass="adprice"><?
												if ($row->ucret == 0)
													echo "".ML::select("free")."";
												else
													echo $row->ucret." TL";
												?>  </span>
											<p class="catpath"><?=$row->kategorisi?> aktivitesi <?=$row->kadi?> tarafından oluşturuldu </p>
											</section>

											<section class="list-right">
											<span class="date"><?=$row->bittarih?> 'e kadar devam ediyor.</span>
											<span class="cityname"></span>
											</section>
											<div class="clearfix"></div>
											</li> 
										</a>
										<?php endforeach; ?>
										<div class="clearfix"></div>
										<a href="<?php echo baseurl('home/alls')  ?>" class="btn btn-info"> Tümünü Gör </a>
									</a>
								</ul>
							</div>
								</div>
							</div><!--  Listeleme Div Sonu -->

							<div role="tabpanel" class="tab-pane fade" id="ucretsiz" aria-labelledby="ucretsiz-tab">
							  <div>
							<div id="container">
									<div class="clearfix"></div>
								<ul class="list">
										<?php 
										if (empty($ucretsiz_aktivite)){
											echo "<div class='alert alert-warning'>Henüz İçerik Eklenmemiş</div>";
										}
										foreach($ucretsiz_aktivite as $row): ?>
										<a href="<?=baseurl("{$row->seo_link}/{$row->id}")?>" class="col-md-6" style="height:180px !important;">
											<li style="height:160px;">
											<img style="height:100px; width:100px;" src="<?php echo baseurl(UPLOADS_DIR); ?><?=$row->resmi?>" title="" alt="" />
											<section class="list-left">
											<h5 class="title"><?=$row->baslik?></h5>
											<span fclass="adprice"><?
												if ($row->ucret == 0)
													echo "".ML::select("free")."";
												else
													echo $row->ucret." TL";
												?>  </span>
											<p class="catpath"><?=$row->kategorisi?> aktivitesi <?=$row->kadi?> tarafından oluşturuldu </p>
											</section>

											<section class="list-right">
											<span class="date">Bitiş Tarihi: <?=$row->bittarih?></span>
											<span class="cityname"></span>
											</section>
											<div class="clearfix"></div>
											</li> 
										</a>
										<?php endforeach; ?>
										<div class="clearfix"></div>
									
								<div class="clearfix"></div>
								<a href="<?php echo baseurl('home/alls')  ?>" class="btn btn-info"> Tümünü Gör </a>
									
								</ul>
							</div>
								</div>
							</div><!--  Listeleme Div Sonu -->


							<?php  if (User::check()): 
									
									if($user_row->cinsiyet == 1){
										/* Erkek */
										?>

										<div role="tabpanel" class="tab-pane fade" id="erkekler" aria-labelledby="erkekler-tab">
										  <div>
										<div id="container">
												<div class="clearfix"></div>
											<ul class="list">
													<?php ;
													if (empty($erkekler)){
														echo "<div class='alert alert-warning'>Henüz İçerik Eklenmemiş</div>";
													}
													foreach($erkekler as $row): ?>
													<a href="<?=baseurl("{$row->seo_link}/{$row->id}")?>" class="col-md-6" style="height:180px !important;">
														<li style="height:160px;">
														<img style="height:100px; width:100px;" src="<?php echo baseurl(UPLOADS_DIR); ?><?=$row->resmi?>" title="" alt="" />
														<section class="list-left">
														<h5 class="title"><?=$row->baslik?></h5>
														<span fclass="adprice"></span>
														<p class="catpath"><?=$row->kategorisi?> aktivitesi <?=$row->kadi?> tarafından oluşturuldu </p>
														</section>

														<section class="list-right">
														<span class="date">Bitiş Tarihi: <?=$row->bittarih?></span>
														<span class="cityname"></span>
														</section>
														<div class="clearfix"></div>
														</li> 
													</a>
													<?php endforeach; ?>
													<div class="clearfix"></div>
												
											<div class="clearfix"></div>
											<a href="<?php echo baseurl('home/alls')  ?>" class="btn btn-info"> Tümünü Gör </a>
												
											</ul>
										</div>
											</div>
										</div><!--  Listeleme Div Sonu -->

										<?php

									}elseif($user_row->cinsiyet == 0){
										/* Bayan */
										?>

								<div role="tabpanel" class="tab-pane fade" id="bayanlar" aria-labelledby="bayanlar-tab">
								  <div>
								<div id="container">
										<div class="clearfix"></div>
									<ul class="list">
											<?php ;
											if (empty($bayanlar)){
												echo "<div class='alert alert-warning'>Henüz İçerik Eklenmemiş</div>";
											}
											foreach($bayanlar as $row): ?>
											<a href="<?=baseurl("{$row->seo_link}/{$row->id}")?>" class="col-md-6" style="height:180px !important;">
												<li style="height:160px;">
												<img style="height:100px; width:100px;" src="<?php echo baseurl(UPLOADS_DIR); ?><?=$row->resmi?>" title="" alt="" />
												<section class="list-left">
												<h5 class="title"><?=$row->baslik?></h5>
												<span fclass="adprice"><?
												if ($row->ucret == 0)
													echo "".ML::select("free")."";
												else
													echo $row->ucret." TL";
												?>  </span>
												<p class="catpath"><?=$row->kategorisi?> aktivitesi <?=$row->kadi?> tarafından oluşturuldu </p>
												</section>

												<section class="list-right">
												<span class="date">Bitiş Tarihi: <?=$row->bittarih?></span>
												<span class="cityname"></span>
												</section>
												<div class="clearfix"></div>
												</li> 
											</a>
											<?php endforeach; ?>
											<div class="clearfix"></div>
										
									<div class="clearfix"></div>
									<a href="<?php echo baseurl('home/alls')  ?>" class="btn btn-info"> Tümünü Gör </a>
										
									</ul>
								</div>
									</div>
								</div><!--  Listeleme Div Sonu -->
										<?php

									}else{

									}
							?>

							<?php  else: ?>

								<div role="tabpanel" class="tab-pane fade" id="bayanlar" aria-labelledby="bayanlar-tab">
								  <div>
								<div id="container">
										<div class="clearfix"></div>
									<ul class="list">
											<?php ;
											if (empty($bayanlar)){
												echo "<div class='alert alert-warning'>Henüz İçerik Eklenmemiş</div>";
											}
											foreach($bayanlar as $row): ?>
											<a href="<?=baseurl("{$row->seo_link}/{$row->id}")?>" class="col-md-6" style="height:180px !important;">
												<li style="height:160px;">
												<img style="height:100px; width:100px;" src="<?php echo baseurl(UPLOADS_DIR); ?><?=$row->resmi?>" title="" alt="" />
												<section class="list-left">
												<h5 class="title"><?=$row->baslik?></h5>
												<span fclass="adprice"><?
												if ($row->ucret == 0)
													echo "".ML::select("free")."";
												else
													echo $row->ucret." TL";
												?>  </span>
												<p class="catpath"><?=$row->kategorisi?> aktivitesi <?=$row->kadi?> tarafından oluşturuldu </p>
												</section>

												<section class="list-right">
												<span class="date">Bitiş Tarihi: <?=$row->bittarih?></span>
												<span class="cityname"></span>
												</section>
												<div class="clearfix"></div>
												</li> 
											</a>
											<?php endforeach; ?>
											<div class="clearfix"></div>
										
									<div class="clearfix"></div>
									<a href="<?php echo baseurl('home/alls')  ?>" class="btn btn-info"> Tümünü Gör </a>
										
									</ul>
								</div>
									</div>
								</div><!--  Listeleme Div Sonu -->

								<div role="tabpanel" class="tab-pane fade" id="erkekler" aria-labelledby="erkekler-tab">
								  <div>
								<div id="container">
										<div class="clearfix"></div>
									<ul class="list">
											<?php ;
											if (empty($erkekler)){
												echo "<div class='alert alert-warning'>Henüz İçerik Eklenmemiş</div>";
											}
											foreach($erkekler as $row): ?>
											<a href="<?=baseurl("{$row->seo_link}/{$row->id}")?>" class="col-md-6" style="height:180px !important;">
												<li style="height:160px;">
												<img style="height:100px; width:100px;" src="<?php echo baseurl(UPLOADS_DIR); ?><?=$row->resmi?>" title="" alt="" />
												<section class="list-left">
												<h5 class="title"><?=$row->baslik?></h5>
												<span fclass="adprice"></span>
												<p class="catpath"><?=$row->kategorisi?> aktivitesi <?=$row->kadi?> tarafından oluşturuldu </p>
												</section>

												<section class="list-right">
												<span class="date">Bitiş Tarihi: <?=$row->bittarih?></span>
												<span class="cityname"></span>
												</section>
												<div class="clearfix"></div>
												</li> 
											</a>
											<?php endforeach; ?>
											<div class="clearfix"></div>
										
									<div class="clearfix"></div>
									<a href="<?php echo baseurl('home/alls')  ?>" class="btn btn-info"> Tümünü Gör </a>
										
									</ul>
								</div>
									</div>
								</div><!--  Listeleme Div Sonu -->

							<?php  endif; ?>

							</div>



						  </div>
						</div>
					</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>	
	</div>
	
			
		<!--	
			
	<div class="mobile-app">
		<div class="container">
			<div class="col-md-5 app-left">
				<a href="mobileapp.html"><img src="<?php echo baseurl(UPLOADS_DIR) ?>app.png" alt=""></a>
			</div>
			<div class="col-md-7 app-right">
				<h3>Resale App is the <span>Easiest</span> way for Selling and buying second-hand goods</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam auctor Sed bibendum varius euismod. Integer eget turpis sit amet lorem rutrum ullamcorper sed sed dui. vestibulum odio at elementum. Suspendisse et condimentum nibh.</p>
				<div class="app-buttons">
					<div class="app-button">
						<a href="#"><img src="<?php echo baseurl(UPLOADS_DIR) ?>1.png" alt=""></a>
					</div>
					<div class="app-button">
						<a href="#"><img src="<?php echo baseurl(UPLOADS_DIR) ?>2.png" alt=""></a>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	-->
		<!--footer section end-->	
