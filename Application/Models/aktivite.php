<?php 

	class aktivite_model extends Model{

		## Aktivite Listeleme Fonksiyonu
		public $limit = 10;
		
		public function aktivite_listele($lat = 0,$lng = 0){
			
			if(!Uri::segment(4)){
				
				$page = 0;
				
			}else{
				
				$page = Uri::segment(-1);
				
			}
		
				if ($lat != 0 && $lng!=0){
					return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
					->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
					->Join('kullanici','kullanici.id = aktivite.userid','inner')->limit($page,$this->limit)
					->select("aktivite.*, distance(aktivite.lat,$lat,aktivite.lng,$lng) as distances, kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
					->where('aktivite.onay = 1')
					->OrderBy("distances","ASC")
					->get('aktivite');
				}else{
					return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
					->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
					->Join('kullanici','kullanici.id = aktivite.userid','inner')->limit($page,$this->limit)
					->select("aktivite.*, kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
					->where('aktivite.onay = 1') 
					->OrderBy("aktivite.id","DESC")
					->get('aktivite');				
				}

		}

		public function yas_durumu(){
			return $this->db->get('yaslar');
		}

		public function aktivite_filtrele($postlar){

			$sql = "aktivite.onay = 1";
			if($postlar['ucretdurumu'] == 1){

				$sql = "$sql AND aktivite.ucret > 0 ";

			}elseif($postlar['ucretdurumu'] == 0){

				$sql = "$sql AND aktivite.ucret = 0 ";
			}

			if( isset($postlar["kategori"])){
				if(is_numeric($postlar["kategori"])){
					$katid = $postlar["kategori"];
					$sql = "$sql AND aktivite.aktivite_kat = $katid ";
				}

			}
			
			if(is_numeric($postlar["yas"])){
				$yas = $postlar["yas"];
				$sql = "$sql AND aktivite.yas_aralik = $yas";
			}

			return $this->db->query("SELECT aktivite.*, kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi FROM aktivite LEFT JOIN dosyalar ON dosyalar.id = aktivite.resim INNER JOIN kategoriler ON kategoriler.id = aktivite.aktivite_kat INNER JOIN kullanici ON kullanici.id = aktivite.userid WHERE  $sql  ORDER BY aktivite.id DESC LIMIT 0 , 10 ");



		}
		public function aktivite_arama($search){
			return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
			->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
			->Join('kullanici','kullanici.id = aktivite.userid','inner')
			->select("aktivite.*, kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
			->where("aktivite.baslik LIKE '%$search%' OR")
			->get('aktivite');
		}

		## Aktivitenin Mevcut resimlerini listeler.
		public function aktivite_resimleri($id,$userid){
			return $this->db
			->Join('aktivite_resim','aktivite.id = aktivite_resim.aktivite_id','inner')
			->Join('dosyalar','dosyalar.id = aktivite_resim.dosya_id','inner')
			->where("aktivite.id= {$id} AND aktivite.userid = {$userid}")->get("aktivite");
		}

		public function resim_sil($resimid,$aktiviteid){
			## Gelen resim Aktivitenin ana resmimi 
			if ( $this->db->where('resim=',$resimid)->get('aktivite')->totalRows() > 0){
				Warning::set("Aktivitenin Kapak resmini değiştirdikten sonra silebilirsiniz.","info","aktivite/resim_guncelle/$aktiviteid");

			}else{
				## Resim Silinir
				DB::where('dosya_id = ', $resimid)->delete('aktivite_resim');
				DB::where('id = ',$resimid)->delete("dosyalar");
				$dosyaadi = DB::where('id = ', $resimid)->get('dosyalar')->row();
				@$silinecek_dosya = baseurl(UPLOADS_DIR.$dosyaadi->adi);
				$sil = File::delete($silinecek_dosya); 
				Warning::set("Resim Başarıyla silindi","success","aktivite/resim_guncelle/$aktiviteid");
			}

			
		}

		## Aktivitenin Kapak resmini değiştirir.
		public function kapak_resmi_degistir($dosyaid,$aktiviteid){
			return $this->db->where('id=',$aktiviteid)->update('aktivite', array('resim' => $dosyaid));
		}

		## Güncelleme için aktivitenin oturumu açık üyenin olduğu kontrol ediliyor.
		public function guncelleme_control($aktiviteid,$userid){
			return $this->db->where('id=',$aktiviteid," AND")
			->where('userid=',$userid)
			->get("aktivite");
		}

		public function aktiviteye_katilanlar($aktiviteID){
			return $this->db
			->select("davetiye.*, kullanici.*, dosyalar.*, concat(kullanici.adi,' ',kullanici.soyadi) as adisoyadi")
			->Join("kullanici","kullanici.id = davetiye.userid","inner")
			->Join("dosyalar","dosyalar.id = kullanici.resim ","left")
			->where("davetiye.aktiviteid = ",$aktiviteID,"AND")
			//->where("davetiye.tip =",3)
			->get("davetiye");
		}
		## Kullanıcıcın id sine göre kullanıcının aktivitleri listelenicek
		public function kullanici_aktiviteleri($userid){
			return $this->db->where('userid = ',$userid)->get("aktivite");
		}

		public function single($aktiviteid){
			return $this->db->where('id = ', $aktiviteid)->get('aktivite')->row();
		}

		##Tarihe Göre Sıralama Sorguları
		public function tarih_sorgulari($tur){
			
			if(Uri::segment(2)=="konum" || !Uri::segment(3)){
				
				$page = 0;
				
			}else{
				
				$page = Uri::segment(-1);
				
			}
			
			switch ($tur) {

				## Aktivite Bitiş tarihine göre sorgulandı
				case 'biten_aktivite':
					return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
					->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
					->Join('kullanici','kullanici.id = aktivite.userid','inner')
					->select("aktivite.*,  kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
					->where('aktivite.bittarih < curdate() AND aktivite.onay = 1 ')->limit($page,$this->limit)
					->OrderBy("id DESC")
					->get('aktivite');
					break;

				## Devam  eden aktivitelerin sorgusu
				case 'devam_edenler':
					return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
					->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
					->Join('kullanici','kullanici.id = aktivite.userid','inner')
					->select("aktivite.*,  kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
					->where('aktivite.bastarih < ', date("Y-m-d H:i:s") ,' AND')
					->where('aktivite.bittarih > ', date("Y-m-d H:i:s") ,' AND')->limit($page,$this->limit)
					->where('aktivite.onay = 1 ')
					->OrderBy("id DESC")
					->get('aktivite');
				break;
					## Ücretsizn aktivitelerin sorgusu
				case 'ucretsiz_aktiviteler':
					return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
					->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
					->Join('kullanici','kullanici.id = aktivite.userid','inner')
					->select("aktivite.*,  kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
					->where('ucret = 0')->limit($page,5)
					->OrderBy("id DESC")
					->get('aktivite');
				break;

				## Sadece Bayan Aktiviteleri
				case 'bayanlar':
					return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
					->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
					->Join('kullanici','kullanici.id = aktivite.userid','inner')
					->select("aktivite.*,  kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
					->where('aktivite.cinsiyet =',2)->limit($page,5)
					->OrderBy("id DESC")
					->get('aktivite');
				break;

				## Sadece Erkek Aktiviteleri
				case 'erkekler':
					return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
					->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
					->Join('kullanici','kullanici.id = aktivite.userid','inner')
					->select("aktivite.*,  kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
					->where('aktivite.cinsiyet =',1)->limit($page,5)
					->OrderBy("id DESC")
					->get('aktivite');
				break;

				default:
					# code...
					break;
			}
		}

		public function category_get($seolink){
			
			if(!Uri::segment(-1)){
				
				$page = 0;
				
			}else{
				
				$page = Uri::segment(-1);
				
			}
			return $this->db->Join('dosyalar','dosyalar.id = aktivite.resim','left')
			->Join('kategoriler','kategoriler.id = aktivite.aktivite_kat','inner')
			->Join('kullanici','kullanici.id = aktivite.userid','inner')
			->select("aktivite.*,  kategoriler.adi as kategorisi ,kullanici.kadi as kadi ,ifNull(dosyalar.adi,'noimg.gif') AS resmi")
			->where('kategoriler.seo_link = ',$seolink)->limit($page,$this->limit)
			//->where('aktivite.aktivite_kat =',$id)
			->OrderBy("id DESC")
			->get('aktivite');
		}

		## Aktivite Formu için Yaş Getirme
		public function yas_getir(){

			return $this->db->get('yaslar');

		}

		## Aktivite Formu için Kategori Getirme
		public function kategori_getir(){

			return $this->db->OrderBy("sirasi ASC")->get('kategoriler');

		}

		public function kategori_idAl($seolink){
			return $this->db->select("id")->where('seo_link')->get("kategoriler");
		}

		## Aktivite Ekle
		public function forminsert($postlar){

			return $this->db->insert('aktivite',$postlar);

		}

		## Aktivite Güncelle
		public function formUpdate($postlar,$id,$userid){
			if (empty($id) OR $id == 0){
				return 'Hata Parametrelerle uğraşmayın';
			}else{
				return $this->db->where('id=',$id," AND ")->where("userid= ",$userid)->update('aktivite',$postlar);
			}
		}

		## Aktivite Tablosuna Ana Resmi Ekler
		public function resimKaydet($id,$resid){

			return $this->db->query('UPDATE aktivite SET resim='.$resid.' where id='.$id);

		}

		public function aktivite_resim($aktiviteID,$dosyaid){

			return $this->db->query("INSERT INTO `aktivite_resim`(`id`, `aktivite_id`, `dosya_id`) VALUES (NULL,'$aktiviteID','$dosyaid')");

		}
		## Aktivite Silinmeden Önce Katılan Varmı Kontrol Edilir.
		public function aktivite_katilim_control($aktiviteid){
			return $this->db->where('aktiviteid =',$aktiviteid," AND")
			->where("durum=",2)
			->get("davetiye");
		}

		public function davetiye_control($aktiviteid){
			return $this->db->where('aktiviteid =',$aktiviteid)
			->get("davetiye");
		}

		public function aktivite_sil($aktiviteid,$userid){
			
			$aktiviterow = $this->db->where("id=",$aktiviteid,"AND ")->where("userid=",$userid)->get("aktivite")->row();
			$aktivite_resim =  $this->db->where('aktivite_id =',$aktiviteid)->get("aktivite_resim");
			foreach ($aktivite_resim->result() as $row) {
			
				$dosyaadi = $this->db->where('id =',$row->dosya_id)->get('dosyalar')->result();
				@$silinecek_dosya = UPLOADS_DIR.$dosyaadi[0]->adi;
				echo $silinecek_dosya;
				File::delete($silinecek_dosya);
				$this->db->where('id=', $row->dosya_id)->delete("dosyalar");
				$this->db->where('dosya_id=',$row->dosya_id)->delete("aktivite_resim");
			}
			$davetliler = $this->db->where("aktiviteid =",$aktiviteid)->get("davetiye")->result();
			foreach ($davetliler as $davetli) {
				echo $davetli->userid;
				Bildirim::set($davetli->userid,7,"Davet Edildiğin {$aktiviterow->baslik} aktivitesi silindiğinden dolayı davetiyen iptal edildi ");
			}
			$davetli_sil = $this->db->where("aktiviteid =",$aktiviteid)->delete("davetiye");
			$this->db->where("aktiviteid =",$aktiviteid)->delete("katilim");
			return $this->db->where('id=',$aktiviteid,"AND")->where('userid=',$userid)->delete("aktivite");
		}

		public function kisi_cikar($silinecek_userid,$aktiviteID){
			$this->db->query("DELETE FROM `davetiye` WHERE aktiviteid = '$aktiviteID' AND userid = '$silinecek_userid'");
		}

		public function katilan_kontrol($aktiviteID,$userid){
			return $this->db
			->Join("kullanici","kullanici.id = davetiye.userid ","inner")
			->where("davetiye.userid =",$userid,"AND")
			->where("davetiye.aktiviteid =",$aktiviteID)
			->get("davetiye");
		}	

		public function kullanicinin_davetiyeleri($userid){
			return $this->db
			->select("aktivite.*, kullanici.*, davetiye.*, dosyalar.adi as aktimage, concat(kullanici.adi,' ',kullanici.soyadi) as useradsoyadi")
			->Join("aktivite","aktivite.id = davetiye.aktiviteid","inner")
			->Join("dosyalar","dosyalar.id = aktivite.resim","left")
			->Join("kullanici","davetiye.gonderenid = kullanici.id","left")
			->where("davetiye.userid =",$userid," AND")
			->where("davetiye.durum=",0)
			->get("davetiye");

		}

		public function davetiye_sil($davetiyeID,$userid){
			return $this->db->where('id=',$davetiyeID,"AND")->where('userid=',$userid,"AND")->where("durum=",0)->delete('davetiye');
		}

		public function katildigim_aktiviteler($userid){
			return $this->db
			->Join('aktivite',"aktivite.id = davetiye.aktiviteid","inner")
			->Join('dosyalar','dosyalar.id = aktivite.resim','left')
			->where('davetiye.userid=',$userid,"AND")
			->where('davetiye.durum =',1)
			->get('davetiye');
		}

		public function aktiviteden_cikis($userid,$aktiviteID){
			return $this->db
			->where("userid=",$userid,"AND")
			->where("aktiviteid=",$aktiviteID,"AND")
			->where("durum=",1)
			->delete("davetiye");
		}

		## aktiviteyiOlusturaniKendiAktivitesineEkleme
		public function olusturaniEkle($array){
			$this->db->insert('davetiye',$array);
		}


	}



?>