<?php
	class detay_model extends Model{
		
		function verileri_cek($id,$seolink,$userid=0){
			return $this->db->select("aktivite.*,kategoriler.adi as katadi,kategoriler.seo_link as katseo,concat(yaslar.baslangic,' - ',yaslar.bitis) as yasaraligi ,(select ROUND(SUM(oylama.derece) / COUNT(oylama.derece)) from oylama where oylama.derece > 0 and oylama.aktiviteid=aktivite.id) as oylama, kullanici.kadi,kullanici.hakkinda,
			(select ifnull(dosyalar.kucuk,'noimg.gif') from dosyalar inner join kullanici on dosyalar.id=kullanici.resim where kullanici.id=aktivite.userid) as kresim,
			(select count(davetiye.id) from davetiye where davetiye.userid=$userid and davetiye.aktiviteid=aktivite.id and davetiye.tip=2) as katilmismi,
			(select count(davetiye.id) from davetiye where davetiye.aktiviteid=aktivite.id and davetiye.userid=$userid and davetiye.tip=1) as rezervasyonuVarmi")
			->join("yaslar","yaslar.id=aktivite.yas_aralik","left")
			->join("kullanici","kullanici.id=aktivite.userid","inner")
			->join("kategoriler","kategoriler.id=aktivite.aktivite_kat","inner")
			->where("aktivite.id=",$id,"and")->where("aktivite.seo_link=",$seolink,"and")
			->where("aktivite.onay=","1")->get('aktivite');
		} 
		
		function resimcek ($id){ 
			return $this->db->select("aktivite_resim.*,ifnull(dosyalar.adi,'noimg.gif')as resimadi")->join("dosyalar","dosyalar.id = aktivite_resim.dosya_id")->where("aktivite_id=",$id)->get("aktivite_resim");
		}
		
		function yorumcek($id){
			return $this->db->select("oylama.*,concat(kullanici.adi,' ',kullanici.soyadi) as adisoyadi,
			kullanici.kadi,kullanici.onay,
			ifnull(dosyalar.adi,'noimg.gif') as kullaniciresim,
			(select count(yorum_begeni.id) from yorum_begeni where yorum_begeni.yorumid = oylama.id and yorum_begeni.tur = '1') as ilike,
			(select count(yorum_begeni.id) from yorum_begeni where yorum_begeni.yorumid = oylama.id and yorum_begeni.tur = '0') as idislike")
			->join('kullanici','kullanici.id=oylama.userid','inner')
			->join('dosyalar','dosyalar.id=kullanici.resim','left')
			->where("oylama.onay=",1,"and")
			->where("kullanici.onay=",1,"and")
			->where("oylama.aktiviteid=",$id)
			->orderBy("oylama.zaman",'desc')
			->get("oylama");
		}
		
		function like_ekle($yorumid,$userid){
			return $this->db->insert("yorum_begeni",array('tur'=>1,'userid'=>$userid,'yorumid'=>$yorumid));
		}
		
		function likelar($yorumid){
			return $this->db->where('tur=',1,"and")->where("yorumid=",$yorumid)->get("yorum_begeni");
		}
		
		function dislike_ekle($yorumid,$userid){
			return $this->db->insert("yorum_begeni",array('tur'=>0,'userid'=>$userid,'yorumid'=>$yorumid));
		}
		
		function dislikelar($yorumid){
			return $this->db->where('tur=',0,"and")->where("yorumid=",$yorumid)->get("yorum_begeni");
		}
		
		function yorumKaydet($postlar){
			return $this->db->insert("oylama",$postlar);
		} 
		
		function oncedenPuanVermismi($postlar){
			return $this->db->where("oylama.aktiviteid=",$postlar["aktiviteid"],"and")->where("oylama.userid=",$postlar['userid'],"and")->where("oylama.derece >",0)->select("oylama.*")->get("oylama")->totalRows();
		}
		
		function distanceControl($postlar){
			return $this->db->having("distances < 10000")->where("aktivite.id=",$postlar['aktiviteid'])->select("distance(aktivite.lat,".$postlar['lat'].",aktivite.lng,".$postlar['lng'].") as distances")->get("aktivite")->totalRows();
		}
		
		function limitControl($postlar){
			return $this->db->query("SELECT (SELECT COUNT(davetiye.id) FROM davetiye WHERE davetiye.tip=1 and davetiye.tip=2 and davetiye.aktiviteid=".$postlar['aktiviteid'].") as limitt,aktivite.kapasite FROM aktivite HAVING limitt < aktivite.kapasite")->totalRows();
		}
		
		function cinsiyetControl($postlar){
			$a = $this->db->query("SELECT kullanici.cinsiyet,(SELECT aktivite.cinsiyet from aktivite where aktivite.id=".$postlar['aktiviteid'].") as aktiviteCinsiyet from kullanici where kullanici.id=".$postlar['userid'])->row();
			if($a->aktiviteCinsiyet == 0)
				return true;
			else if($a->aktiviteCinsiyet == $a->cinsiyet)
				return true;
			else 
				return false;
		}

		function katil($postlar){
			return $this->db->insert("davetiye",$postlar); 
		}
		
		function rezervasyon($postlar){
			return $this->db->insert("davetiye",$postlar); 
		}
		
		function sikayetekle($postlar){
			return $this->db->insert("sikayet",$postlar);
		}
		
		
		function distanceCek($postlar){
			return $this->db->where("aktivite.id=",$postlar['aktiviteid'])->select("distance(aktivite.lat,".$postlar['lat'].",aktivite.lng,".$postlar['lng'].") as distance")->get("aktivite")->row();
		}
		
		function rezervasyonIptal($userid,$aktiviteID){ 
			return $this->db
			->where("userid=",$userid,"AND")
			->where("aktiviteid=",$aktiviteID,"AND")
			->where("durum=",1)
			->delete("davetiye");
		}
		

	}
?>
