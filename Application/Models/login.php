<?php 

	class login_model extends Model{
		
		public function fbcontrol($fbid=null){
			
			return $this->db->where("fbid=",$fbid)->get("kullanici");
			
		}
		
		public function insertUser($postlar =array()){
			
			return $this->db->insert("kullanici",$postlar);
			
		}
		
		public function updateUser($postlar =array(),$fbid){
			return $this->db->where("fbid=",$fbid)->update("kullanici",$postlar);
			
		}
		
		public function kadiVarmi($kadi=null){
		
			return $this->db->where("kadi=",$kadi)->get("kullanici");
			
		}
		
		public function onay($sifre){
			
			return $this->db->where("sifre=",$sifre,"and")->where("onay=",0)->update("kullanici",array("onay"=>1));
			
		}
		
		public function getForgottenPassword($email){
			return $this->db->where("email=",$email)->get("kullanici")->row();
		}
			
		public function newPasswordControl($e,$s){
			return $this->db->where("email=",$e,"and")->where("sifre=",$s)->get("kullanici")->totalRows();
		}
		
		public function changeOldPassword($e,$p){
			return $this->db->where("email=",$e)->update("kullanici",array("sifre"=>sha1(md5($p))));
		}
	}

?>