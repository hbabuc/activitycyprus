<?php 

	

	class service_model extends Controller{

		

		public function uploadImg($resimid,$id){

			

			return $this->db->where("id=",$id)->update("aktivite",array("resim"=>$resimid));

			

		}
		
		public function olusturaniEkle($array){
			$this->db->insert('davetiye',$array);
		}

		

		public function newAktiviteImg($resimid,$id){

			

			return $this->db->insert("aktivite_resim",array("aktivite_id"=>$id,"dosya_id"=>$resimid));

			

		}

		

		public function tumaktiviteler($page,$lat=0,$lng=0){

			$postall = json_decode(Session::select("postall"));
			$lat = $postall->lat;
			$lng = $postall->lng;

			return $this->db->select("aktivite.*,ifnull(dosyalar.adi,'noimg.gif') as resmi,distance(aktivite.lat,$lat,aktivite.lng,$lng) as distances")->join("dosyalar","dosyalar.id = aktivite.resim","left")->limit($page,12)->orderBy("distances","asc")->get("aktivite");

			

		}

		

		public function getDetay($id,$userid){
			
			$postall = json_decode(Session::select("postall"));
			$lat = $postall->lat;
			$lng = $postall->lng;
			

			return $this->db->select("aktivite.*,kategoriler.adi as katadi,kategoriler.seo_link as katseo,concat(yaslar.baslangic,' - ',yaslar.bitis) as yasaraligi ,(select ROUND(SUM(oylama.derece) / COUNT(oylama.derece)) from oylama where oylama.derece > 0 and oylama.aktiviteid=aktivite.id) as oylama, kullanici.kadi,kullanici.hakkinda,distance(aktivite.lat,$lat,aktivite.lng,$lng) as distances	,
				

			ifnull(dosyalar.adi,'noimg.gif') as aktiviteresmi,concat(kullanici.adi,' ',kullanici.soyadi) as adisoyadi,

			ifnull((select dosyalar.kucuk from dosyalar left join kullanici on dosyalar.id=kullanici.resim where kullanici.id=aktivite.userid),'noimg.gif') as kresim,

			(select count(davetiye.id) from davetiye where davetiye.userid='$userid' and davetiye.aktiviteid=aktivite.id and tip=2) as katilmismi,

			(select count(davetiye.id) from davetiye where davetiye.aktiviteid=aktivite.id and davetiye.userid=$userid and davetiye.tip=1) as rezervasyonuVarmi")

			->join("yaslar","yaslar.id=aktivite.yas_aralik","left")

			->join("dosyalar","dosyalar.id = aktivite.resim","left")

			->join("kullanici","kullanici.id=aktivite.userid","inner")

			->join("kategoriler","kategoriler.id=aktivite.aktivite_kat","inner")

			->where("aktivite.id=",$id,"and")->where("aktivite.onay=","1")

			->get('aktivite');

			

		}

		

		public function getDetayRasgele($userid,$range=5000){

			date_default_timezone_set('Europe/Istanbul'); 
			$postall = json_decode(Session::select("postall"));
			$lat = $postall->lat;
			$lng = $postall->lng;

			return $this->db->select("aktivite.*,kategoriler.adi as katadi,kategoriler.seo_link as katseo,concat(yaslar.baslangic,' - ',yaslar.bitis) as yasaraligi ,(select ROUND(SUM(oylama.derece) / COUNT(oylama.derece)) from oylama where oylama.derece > 0 and oylama.aktiviteid=aktivite.id) as oylama, kullanici.kadi,kullanici.hakkinda,distance(aktivite.lat,$lat,aktivite.lng,$lng) as distances	,
				

			ifnull(dosyalar.adi,'noimg.gif') as aktiviteresmi,concat(kullanici.adi,' ',kullanici.soyadi) as adisoyadi,

			ifnull((select dosyalar.kucuk from dosyalar left join kullanici on dosyalar.id=kullanici.resim where kullanici.id=aktivite.userid),'noimg.gif') as kresim,
			(select count(davetiye.id) from davetiye where davetiye.userid='$userid' and davetiye.aktiviteid=aktivite.id and tip=2) as katilmismi,

			(select count(davetiye.id) from davetiye where davetiye.aktiviteid=aktivite.id and davetiye.userid=$userid and davetiye.tip=1) as rezervasyonuVarmi")

			->join("yaslar","yaslar.id=aktivite.yas_aralik","left")

			->join("dosyalar","dosyalar.id = aktivite.resim","left")

			->join("kullanici","kullanici.id=aktivite.userid","inner")

			->join("kategoriler","kategoriler.id=aktivite.aktivite_kat","inner")
			->having("distances < $range")
			->where("aktivite.bastarih <",date("Y-m-d H:i:s"),"and")
			->where("aktivite.bittarih >",date("Y-m-d H:i:s"),"and")
			->where("aktivite.onay=","1")
			->get('aktivite');

			

		}

		

		public function bildirimAl($kadi,$sifre){

			

			$sifre = sha1(md5($sifre));

			

			return $this->db->select("bildirim.*,radiolar.adi as tipi")->join("radiolar","radiolar.degeri = bildirim.tip","inner")->join("kullanici","kullanici.id=bildirim.userid","inner")->where("radiolar.type=",2,"and")->where("kullanici.kadi=",$kadi,"and")->where("kullanici.sifre=",$sifre,"and")->where("bildirim.durum=",1)->orderBy("id","desc")->get("bildirim");

			

		}

		

		public function bildirimUpdate($id){

			

			return $this->db->where("id=",$id)->update("bildirim",array("durum"=>0));

			

		}

		

		public function yankindakiler($postlar){

			

			return $this->db->select("aktivite.*,distance(aktivite.lat,".$postlar['lat'].",aktivite.lng,".$postlar['lng'].") as distances")->orderBy("distances","asc")->get("aktivite");

			

		}

		

		public function yankindakilerPage($postlar){

			

			return $this->db->select("aktivite.*,ifnull(dosyalar.adi,'noimg.gif') as resmi,distance(aktivite.lat,".$postlar['lat'].",aktivite.lng,".$postlar['lng'].") as distances")->join("dosyalar","dosyalar.id = aktivite.resim","left")->limit($postlar["page"],12)->orderBy("distances","asc")->get("aktivite");

			

		}

		## Kullanıcının oluştruğu aktiviteler
		public function user_activity($page,$userid){

			return $this->db
			->select("aktivite.*,ifnull(dosyalar.adi,'noimg.gif') as resmi")
			->join("dosyalar","dosyalar.id = aktivite.resim","left")
			->where("aktivite.userid=",$userid)
			->limit($page,12)
			->get("aktivite");

		}

		## Üyenin Davetyeleri
		public function user_davetiye($page,$userid)
		{
			return $this->db
			->select("aktivite.*,ifnull(dosyalar.adi,'noimg.gif') as resmi")
			->Join("aktivite","aktivite.id = davetiye.aktiviteid ","inner")
			->Join("dosyalar","dosyalar.id = aktivite.resim ","left")
			->where("davetiye.userid=",$userid,"AND")
			->where("davetiye.tip = 2")
			->limit($page,12)
			->get("davetiye");

		}

		

	}



?>