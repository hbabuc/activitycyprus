<?php 
	
	class home_model extends Model{
		
		public function get(){
			
			return $this->db->get("kullanici");
			
		}
		
		public function getid($id=0){
			
			return $this->db->join("dosyalar","dosyalar.id = kullanici.resim","left")->select("kullanici.*,IFNULL(dosyalar.adi,'noimg.gif') as resimadi")->where("kullanici.id=",$id)->get("kullanici");
			
		}
		
		public function insert($postlar){
			
			return $this->db->insert("kullanici",$postlar);
			
		}
		
		public function update($postlar=array(),$id=0){
			
			return $this->db->where("id=",$id)->update("kullanici",$postlar);
			
		}
		

		
	}

?>