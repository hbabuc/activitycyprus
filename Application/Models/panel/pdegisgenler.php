<?php 

	class pdegisgenler_model extends Controller{
		
		public $dbname = "degisgenler";
		
		public function getall(){
			
			return $this->db->select($this->dbname.".id,".$this->dbname.".keyi,".$this->dbname.".value,dil.adi as dili")->join("dil",$this->dbname.".dilid = dil.id","inner")->orderBy($this->dbname.".id","desc")->get($this->dbname);
			
		}
		
		public function insert($postlar){
			
			return $this->db->insert($this->dbname,$postlar);
			
		}
		
		public function doEdit($postlar,$id){
			
			return $this->db->where("id=",$id)->update($this->dbname,$postlar);
			
		}
		
		public function delete($id){
			
			return $this->db->where("id=",$id)->delete($this->dbname);
			
		}
		
		public function getdil(){
			
			return $this->db->get("dil");
			
		}
		
	}

?>