<?php 

	class dil_model extends Controller{
		
		public $dbname = "dil";
		
		public function getall(){
			
			return $this->db->orderBy($this->dbname.".id","desc")->get($this->dbname);
			
		}
		
		public function insert($postlar){
			
			return $this->db->insert($this->dbname,$postlar);
			
		}
		
		public function doEdit($postlar,$id){
			
			return $this->db->where("id=",$id)->update($this->dbname,$postlar);
			
		}
		
		public function delete($id){
			
			return $this->db->where("id=",$id)->delete($this->dbname);
			
		}
		
	}

?>