<?php 

	class picerikler_model extends Controller{
		
		public $dbname = "aktivite";
		
		
		public function getall(){
			
			return $this->db->orderBy($this->dbname.".id","desc")->get($this->dbname);
			
		}
		
		public function getKategori(){
			
			return $this->db->orderBy("kategoriler.adi","asc")->where("baglantiid=",0)->select("id,adi")->get("kategoriler");
			
		}
		
		public function gethasKat($id){
			
			return $this->db->where("icerikid=",$id)->get("icerik_kat");
			
		}
		
		public function gethasKeyword($id){
			
			return $this->db->select("etiketler.adi as adi")->where("icerikid=",$id)->join("etiketler","etiketler.id = icerik_etiket.etiketid")->get("icerik_etiket");
			
		}
		
		public function get($id,$versiyon=false){
			
			if($versiyon == true){
					
				return $this->db->where("id=",$id)->orderBy("id","desc")->get($this->dbname);
					
			}else{
				
				return $this->db->where("baglantiid=",$id,"or")->where("id=",$id)->orderBy("id","desc")->get($this->dbname);
				
			}
			
		}
		
		public function getVersiyon($id){
			
			return $this->db->where("baglantiid=",$id,"or")->where("id=",$id)->orderBy("id","desc")->get($this->dbname);
			
		}
		
		public function insert($postlar=array()){
			
			return  $this->db->insert($this->dbname,$postlar);
			
		}
		
		public function edit($postlar=array()){
			
			return  $this->db->insert($this->dbname,$postlar);
			
		}
		
		public function insertKategori($id,$katid){
			
			return  $this->db->insert("icerik_kat",array("icerikid"=>$id,"katid"=>$katid));
			
		}
		
		public function deleteAlan($id){
			
			$this->db->where("id=",$id)->delete($this->dbname);
			return $this->db->where("baglantiid=",$id)->delete($this->dbname);
			
		}
		
		public function dil(){
			
			return $this->db->get("dil");
			
		}
		
		public function etiketVarmi($etiket){
			
			return $this->db->where("adi=",$etiket)->get("etiketler");
			
		}
		
		public function etiketEkle($etiket){
			
			return $this->db->insert("etiketler",array("adi"=>$etiket,"tarih"=>date("Y-m-d H:i:s")));
			
		}
		
		public function icerikEtiketEkle($etiketid,$id){
			
			return $this->db->insert("icerik_etiket",array("etiketid"=>$etiketid,"icerikid"=>$id,"tarih"=>date("Y-m-d H:i:s")));
			
		}
		
		public function etiketUpdate($id,$sayi){
			
			return $this->db->where("id=",$id)->update("etiketler",array("sayi"=>$sayi+1));
			
		}
		
	}

?>