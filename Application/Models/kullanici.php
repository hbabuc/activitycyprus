<?php 
	
	class kullanici_model extends Model
	{
		
	
		public function kullanici_ara( $search){
			return $this->db->select("kullanici.*, 
				ifnull(kullanici.resim,'noimg.gif') as userimg,
				dosyalar.adi as userimg,
				concat(kullanici.adi,' ',kullanici.soyadi) as useradisoyadi")
			->Join("dosyalar","dosyalar.id  = kullanici.resim ","left")
			->where("kullanici.adi LIKE '%$search%' OR kullanici.kadi LIKE '%$search%' OR kullanici.soyadi LIKE '%$search%' ")
			->get("kullanici");
		}

		## id si verilen üyenin takipçisini getirir.	
		public function takipci_getir($userid)
		{
			return $this->db->select("kullanici.*, 
				ifnull(kullanici.resim,'noimg.gif') as userimg,
				dosyalar.adi as userimg ")->Join('kullanici','kullanici.id = kullanicitakip.takipciid','inner')
			->Join('dosyalar','dosyalar.id = kullanici.resim','left')
			->where('kullanicitakip.userid = ',$userid)
			->get('kullanicitakip');
		}

		public function kadi_takipci_getir($kadi)
		{

			$user = $this->db->where('kadi=',$kadi)->get("kullanici");
			$userid = $user->row()->id;
			return $this->db->select("kullanici.*, 
				ifnull(kullanici.resim,'noimg.gif') as userimg,
				dosyalar.adi as userimg ")->Join('kullanici','kullanici.id = kullanicitakip.takipciid','inner')
			->Join('dosyalar','dosyalar.id = kullanici.resim','left')
			->where('kullanicitakip.userid = ',$userid)
			->get('kullanicitakip');
		}

		public function id_takip_ettikleri($userid)
		{

			return $this->db->select("kullanici.*, 
				ifnull(kullanici.resim,'noimg.gif') as userimg,
				dosyalar.adi as userimg ")
			->Join('kullanici','kullanici.id = kullanicitakip.userid','inner')
			->Join('dosyalar','dosyalar.id = kullanici.resim','left')
			->where('kullanicitakip.takipciid = ',$userid)
			->get('kullanicitakip');
		}


		public function kadi_takip_ettikleri($kadi)
		{

			$user = $this->db->where('kadi=',$kadi)->get("kullanici");
			$userid = $user->row()->id;
			return $this->db->select("kullanici.*, 
				ifnull(kullanici.resim,'noimg.gif') as userimg,
				dosyalar.adi as userimg ")->Join('kullanici','kullanici.id = kullanicitakip.userid','inner')
			->Join('dosyalar','dosyalar.id = kullanici.resim','left')
			->where('kullanicitakip.takipciid = ',$userid)
			->get('kullanicitakip');
		}

		## id si verilen üyenin arkadaşlarını getirir.	
		public function arkadas_getir($userid)
		{
			return $this->db->select("arkadaslar .* , user.kadi as userkadi,user.id as useridsi, gonderen.id as gonderenidsi, concat(user.adi,' ',user.soyadi) as useradisoyadi , ifNull(userdosya.adi, 'noimg.gif') as userimage,
				gonderen.kadi as gonderenkadi, concat(gonderen.adi, ' ',gonderen.soyadi) as gonderenadisoyadi, ifNull(gonderendosya.adi, 'noimg.gif') as gonderenimg
				")
			->Join('kullanici as user','user.id = arkadaslar.userid','inner')
			->Join('kullanici as gonderen','gonderen.id = arkadaslar.gonderenid','inner')
			->Join('dosyalar as userdosya','userdosya.id = user.resim','left')
			->Join('dosyalar as gonderendosya','gonderendosya.id = gonderen.resim','left')
			->where('arkadaslar.userid = ',$userid,"AND")
			->where('arkadaslar.durum =','1','OR')
			->where('arkadaslar.gonderenid = ',$userid, "AND")
			->where('arkadaslar.durum =','1','AND')
			->get('arkadaslar');
		}



		public function arkadas_cinsiyet_ayir($userid, $cinsiyet)
		{
			/* 1 Erkek <=> 0 Bayan */

			return $this->db->select("arkadaslar .* , user.kadi as userkadi,user.id as useridsi, gonderen.id as gonderenidsi, concat(user.adi,' ',user.soyadi) as useradisoyadi , ifNull(userdosya.adi, 'noimg.gif') as userimage,
				gonderen.kadi as gonderenkadi, concat(gonderen.adi, ' ',gonderen.soyadi) as gonderenadisoyadi, ifNull(gonderendosya.adi, 'noimg.gif') as gonderenimg
				")
			->Join('kullanici as user','user.id = arkadaslar.userid','inner')
			->Join('kullanici as gonderen','gonderen.id = arkadaslar.gonderenid','inner')
			->Join('dosyalar as userdosya','userdosya.id = user.resim','left')
			->Join('dosyalar as gonderendosya','gonderendosya.id = gonderen.resim','left')
			->where('gonderen.cinsiyet =',$cinsiyet,'AND')
			->where('arkadaslar.userid = ',$userid,"AND")
			->where('arkadaslar.durum =','1','OR')
			->where('arkadaslar.gonderenid = ',$userid, "AND")
			->where('arkadaslar.durum =','1','AND')
			->where('user.cinsiyet =',$cinsiyet)
			->get('arkadaslar');
		}
	}

?>