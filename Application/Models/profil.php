<?php

	

	class profil_model extends Model{
		
		## Kullanıcı Adına Göre Kullanıcı Getirme
		public function getKadi($kadi){

			

			if(User::check()){

				$userid = User::id();

			

				return $this->db->select("kullanici.*,
					ifnull(dosyalar.adi,'noimg.gif') as resmi,(select count(aktivite.id) from aktivite where aktivite.userid=kullanici.id) as aktivitesayi,
					(select count(oylama.id) from oylama where userid=kullanici.id) as yorumsayi,
					(select count(davetiye.id) from davetiye where davetiye.userid=kullanici.id) as katilimsayi,
					(select count(kullanicitakip.id) from kullanicitakip where kullanicitakip.takipciid=kullanici.id) as takipler,
					(select count(kullanicitakip.id) from kullanicitakip where kullanicitakip.userid=kullanici.id) as takipciler,
					(select count(ark.id) from arkadaslar as ark where 
					ark.userid=kullanici.id and  ark.durum = '1' or 
					ark.gonderenid = kullanici.id and ark.durum = '1') as arkadassayi,
					
					(select count(arkadaslarmi.id) from arkadaslar as arkadaslarmi where 
					arkadaslarmi.userid=kullanici.id and arkadaslarmi.gonderenid='$userid' or 
					arkadaslarmi.gonderenid=kullanici.id and  arkadaslarmi.userid='$userid') as arkadasmi,

					(select arkadasmilar.durum from arkadaslar as arkadasmilar where 
					arkadasmilar.userid=kullanici.id and arkadasmilar.gonderenid='$userid' or 
					arkadasmilar.gonderenid=kullanici.id and  arkadasmilar.userid='$userid') as arkadasdurum,

					(select count(kullanicitakip.id) from kullanicitakip where userid=kullanici.id and takipciid='$userid') as takiptemi")->

				join("dosyalar","dosyalar.id = kullanici.resim","left")->

				where("kullanici.kadi=",$kadi)->

				get("kullanici");

				

			}else{

				

				return $this->db->select("kullanici.*,

					ifnull(dosyalar.adi,'noimg.gif') as resmi,(select count(aktivite.id) from aktivite where aktivite.userid=kullanici.id) as aktivitesayi,

					(select count(oylama.id) from oylama where userid=kullanici.id) as yorumsayi,

					(select count(davetiye.id) from davetiye where davetiye.userid=kullanici.id) as katilimsayi,

					(select count(kullanicitakip.id) from kullanicitakip where kullanicitakip.takipciid=kullanici.id) as takipler,

					(select count(kullanicitakip.id) from kullanicitakip where kullanicitakip.userid=kullanici.id) as takipciler,

					

					(select count(ark.id) from arkadaslar as ark where 

					ark.userid=kullanici.id and  ark.durum = '1' or 

					ark.gonderenid = kullanici.id and ark.durum = '1') as arkadassayi")->

				join("dosyalar","dosyalar.id = kullanici.resim","left")->

				where("kullanici.kadi=",$kadi)->

				get("kullanici");

				

			}

			

		}
		
		function getFriends($username){
			return $this->db->select("id")->where("kadi=",$username)->get("kullanici");
		}

		## İdye Göre Kullanıcı Getirme
		public function getid($id=0){

			

			return $this->db->select("kullanici.*,ifnull(dosyalar.adi,'noimg.gif') as resmi")->join("dosyalar","dosyalar.id = kullanici.resim","left")->where("kullanici.id=",$id)->get("kullanici");

	

		}

		## Kullanıcı Editleme
		public function updateUser($postlar=array(),$id=0){

			

			return $this->db->where("id=",$id)->update("kullanici",$postlar);

		

		}

		## Sistemdeki Sosyal Ağlar
		public function sosyalaglar(){



			return $this->db->get("sosyalaglar");



		}

		## Kullanıcı Sosyal Ağları
		public function ksosyal($userid=0){



			return $this->db->select("kullanici_sosyal.id,kullanici_sosyal.link,sosyalaglar.ismi")->join("sosyalaglar","sosyalaglar.id = kullanici_sosyal.sosyalid","inner")->where("kullanici_sosyal.userid=",$userid)->get("kullanici_sosyal");



		}

		## Sosyal Ağ Ekleme
		public function insertSosyal($link=null,$sosyal=null){



			return $this->db->query("insert into kullanici_sosyal set link='$link',userid='".User::id()."',sosyalid=(select id from sosyalaglar where link='$sosyal' limit 1)");



		}

		## Kullanıcıya Ait Sosyal Ağ Varmı Kontrolü
		public function checkSosyal($id=0,$userid=0){

			

			return $this->db->where("id=",$id,"and")->where("userid=",$userid)->get("kullanici_sosyal");

			

		}

		## Kullanıcı Sosyal Ag Silme Alanı
		public function deleteSosyal($id=0){

			

			return $this->db->where("id=",$id)->delete("kullanici_sosyal");

			

		}

		## Profil Resim Upload
		public function insertImg($dosyaid,$userid){



			return $this->db->where("id=",$userid)->update("kullanici",array("resim"=>$dosyaid));

		

		}

		## Mevcut Şifre Kontrolü
		public function sifre_control($userid,$sifre){

			

			return $this->db->where("userid=",$userid,"and")->where("sifre=",$sifre)->get("kullanici");

		}

		## Şifre Değiştirme Fonksiyonu
		public function sifre_degistir($yenisifre,$userid){



			return $this->db->where("id=",$userid)->update("kullanici",array("sifre"=>$yenisifre));

		}

		## Kullanıcı Sosyal Ağları
		public function userSosyal($id){

			

			return $this->db->where("kullanici_sosyal.userid=",$id)->

			select("sosyalaglar.ismi,sosyalaglar.icon,kullanici_sosyal.id,kullanici_sosyal.link as linki")->

			join("sosyalaglar","sosyalaglar.id = kullanici_sosyal.sosyalid","inner")->

			get("kullanici_sosyal");

			

		}

		## Kullacının Takip Ettikleri
		public function takip($id=0){

			

			return $this->db->select("kullanicitakip.*,ifnull(dosyalar.kucuk,'noimg.gif') as resmi,kullanici.kadi,concat(kullanici.adi,' ',kullanici.soyadi) as adisoyadi")->

			where("kullanicitakip.takipciid=",$id)->

			join("kullanici","kullanici.id = kullanicitakip.userid","inner")->

			join("dosyalar","dosyalar.id = kullanici.resim","left")->

			limit(24)->

			get("kullanicitakip");

			

		}

		## Kullanıcıyı Takip Edenler 
		public function takipci($id=0){

			

			return $this->db->select("kullanicitakip.*,ifnull(dosyalar.kucuk,'noimg.gif') as resmi,kullanici.kadi,kullanici.adi,concat(kullanici.adi,' ',kullanici.soyadi) as adisoyadi")->

			where("kullanicitakip.userid=",$id)->

			join("kullanici","kullanici.id = kullanicitakip.takipciid","inner")->

			join("dosyalar","dosyalar.id = kullanici.resim","left")->

			limit(24)->

			get("kullanicitakip");

			

		}

		## Kullanıcı Kontrol Et
		public function checkKadi($kadi){

			

			return $this->db->where("kadi=",$kadi)->get("kullanici");

			

		}

		## Arkadaş Ekleme İsteği
		public function arkadasEkle($userid=0,$gonderenid=0){

			

			return $this->db->insert("arkadaslar",array("userid"=>$userid,"gonderenid"=>$gonderenid));

			

		}

		## Arkadaşlarmı Kontrolü
		public function checkArkadas($userid=0,$gonderenid=0){

			

			return $this->db->where("userid=",$userid,"and")->where("gonderenid=",$gonderenid,"or")->

			where("userid=",$gonderenid,"and")->where("gonderenid=",$userid)->get("arkadaslar");

			

		}

		## Arkadaş Silme Alanı Ve İstek Silme Alanı
		public function arkadasSil($userid,$gonderenid){

			

			return $this->db->where("userid=",$userid,"and")->where("gonderenid=",$gonderenid,"or")->

			where("userid=",$gonderenid,"and")->where("gonderenid=",$userid)->delete("arkadaslar");

			

		}

		## Bildirim Textine Göre Bildirim Silme
		public function bildirimSil($text,$userid){

			

			return $this->db->where("aciklama=",$text,"and")->where("userid=",$userid)->delete("bildirim");

			

		}

		function userArkadas($userid){

			return $this->db->
			select("arkadaslar.*,
			concat(user.adi,' ',user.soyadi) as useradisoyadi,
			user.kadi as userkadi,
			ifnull(userdosya.adi,'noimg.gif') as userResim,
			concat(gonderen.adi,' ',gonderen.soyadi) as gonderenadisoyadi,
			gonderen.kadi as gonderenkadi,
			ifnull(gonderendosya.adi,
			'noimg.gif') as gonderenResim")
			->Join('kullanici as user','user.id = arkadaslar.userid','inner')
			->Join('kullanici as gonderen','gonderen.id = arkadaslar.gonderenid','inner')
			->Join('dosyalar as userdosya','userdosya.id = user.resim','left')
			->Join('dosyalar as gonderendosya','gonderendosya.id = gonderen.resim','left')
			->where('arkadaslar.userid = ',$userid,"AND")
			->where('arkadaslar.durum =','1','OR')
			->where('arkadaslar.gonderenid = ',$userid, "AND")
			->where('arkadaslar.durum =','1','AND')
			->get("arkadaslar");

		}

		function arkadasKabul($postlar){

			$this->arkadasEkleBildirimiDegistir($postlar);
			return $this->db->where("id=",$postlar['id'],"and")
			->where("userid=",$postlar["userid"],"and")
			->where("gonderenid=",$postlar["gonderenid"])
			->update("arkadaslar",array("durum"=>1));

		}
		
		function arkadasEkleBildirimiDegistir($postlar){

			$gonderenKadi = $this->db->where("id=",$postlar['gonderenid'])->get("kullanici")->row()->kadi;
			$userKadi = $this->db->where("id=",$postlar['userid'])->get("kullanici")->row()->kadi;
			$this->db->insert("bildirim",array("userid"=>$postlar['gonderenid'],"durum"=>0,"tip"=>4,"aciklama"=>'<p><a href="'.baseurl($userKadi).'">'.$userKadi.'</a> adlı kullanıcı ile arkadaş oldun.</p>'));
			return $this->db->where("id=",$postlar['bildirimid'])->update("bildirim",array("aciklama"=> '<p><a href="'.baseurl($gonderenKadi).'">'.$gonderenKadi.'</a> adlı kullanıcı ile arkadaş oldun.</p>', "tip"=>"4"));

		}

		function arkadasReddet($postlar){

			$this->arkadasReddetBildirimiDegistir($postlar);
			return $this->db->where("id=",$postlar['id'],"and")
			->where("userid=",$postlar["userid"],"and")
			->where("gonderenid=",$postlar["gonderenid"])
			->delete("arkadaslar");

		}

		function arkadasReddetBildirimiDegistir($postlar){

			$gonderenKadi = $this->db->where("id=",$postlar['gonderenid'])->get("kullanici")->row()->kadi;
			return $this->db->where("id=",$postlar['bildirimid'])->update("bildirim",array("aciklama"=> '<p><a href="'.baseurl($gonderenKadi).'">'.$gonderenKadi.'</a> adlı kullanıcının arkadaşlık isteğini reddettin.</p>', "tip"=>"5"));

		}
		
		function takipciEkle($postlar){

			return $this->db->insert("kullanicitakip",$postlar);

		}

		function takipBirak($postlar){

			return $this->db->where("userid=",$postlar['userid'],"and")->where("takipciid=",$postlar['takipciid'])->delete("kullanicitakip");

		}

		public function takipci_Birak($userid,$takipciID){

			return $this->db->where("userid=",$userid,"and")->where("takipciid=",$takipciID)->delete("kullanicitakip");

		}
		
	}
	
?>