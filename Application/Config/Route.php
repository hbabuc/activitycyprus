<?php
//----------------------------------------------------------------------------------------------------
// ROUTE 
//----------------------------------------------------------------------------------------------------
//
// Author     : Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
// Site       : www.znframework.com
// License    : The MIT License
// Copyright  : Copyright (c) 2012-2016, ZN Framework
//
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
// Open Page
//----------------------------------------------------------------------------------------------------
//
// Genel Kullanımı: Başlangıçta varsayılan açılış sayfasını sağlayan Controller dosyasıdır.
// Dikkat edilirse açılış sayfası welcome.php'dir ancak bu işlemi yapan home.php	          
// Controller dosyasıdır.																  					
//
//----------------------------------------------------------------------------------------------------
$config['Route']['openPage']	= 'home';

//----------------------------------------------------------------------------------------------------
// Show 404
//----------------------------------------------------------------------------------------------------
//
// Genel Kullanımı: Geçersiz URI adresi girildiğinde yönlendirilmek istenen URI yoludur.   					
//
//----------------------------------------------------------------------------------------------------
$config['Route']['show404']		= '';

//----------------------------------------------------------------------------------------------------
// Pattern Type
//----------------------------------------------------------------------------------------------------
//
// Bu ayar Change URI ayarına yazılacak desenin türünü belirler.
//
// @key string patternType: special, classic
//
// special: Config/Regex.php dosyasında yer alan karakterlerin kullanımlarıdır.
// classic: Düzenli ifadelerdeki standart karakterlerin kullanımlarıdır. 	
//	      						
//----------------------------------------------------------------------------------------------------
$config['Route']['patternType']	= 'classic';

//----------------------------------------------------------------------------------------------------
// Change Uri
//----------------------------------------------------------------------------------------------------
//
// URI adreslerine rota vermek için kullanılır.
//
// Kullanım: @key -> yeni adres, @value -> eski adres										  
//    																			           																		  
// array																					  
// (																						  														  
//     'anasayfa'     => 'home/index'														      
// );																				      
//	      						
//----------------------------------------------------------------------------------------------------
$aktivite = DB::where("seo_link=",Uri::segment(1),"and")->where("onay=",1)->get("aktivite")->totalRows();
$user = DB::where("kadi=",Uri::segment(1),"and")->where("onay=",1)->get("kullanici")->totalRows();
if($aktivite>0){

	$config['Route']['changeUri'] 	= array
	(
		'/^'.Uri::segment(1).'/'=>'detay/index/'.Uri::segment(1)."/".Uri::segment(2),
	);

}else if($user>0){
			
	$config['Route']['changeUri'] 	= array
	(
		'/^'.Uri::segment(1).'/'=>'profil/index/'.Uri::segment(1)."/",
	);
	
}else if(is_file(CONTROLLERS_DIR.URI::segment(1).'.php')){
	
	$config['Route']['changeUri'] 	= array
	(
		'/^panel$/'=>'panel/',
		'/^panel\/$/'=>'panel/',
		'/^404'=>'page404',
		'/changeLangue\//'=>"changeLangue/index/"
	);
	
}else{
	
	$config['Route']['changeUri'] 	= array
	(
		'/^panel$/'=>'panel/',
		'/^panel\/$/'=>'panel/',
		'/^404'=>'page404',
		'/changeLangue\//'=>"changeLangue/index/"
	);
	
}