<?php 

	// Identity Setting
	
		$config["DataGrid"]["identityColumn"] = "id";
		$config["DataGrid"]["DataGridMethod"] = "get";
	
	// Identity Setting

	
	// Table Settings 
		
		$config["DataGrid"]["tableBorder"] = 0;
		$config["DataGrid"]["tableClass"] = "table";
		$config["DataGrid"]["tableStyle"] = "";
		$config["DataGrid"]["tableId"] = "";
	
	// Table Settings 
	
	
	// EditButton Settings
	
		$config["DataGrid"]["editButtonName"] = "Düzenle";
		$config["DataGrid"]["editButtonClass"] = "btn btn-info";
		$config["DataGrid"]["editButtonStyle"] = "";
		
	// EditButton Settings
	
	
	// RemoveButton Settings
		
		$config["DataGrid"]["removeButtonName"] = "Sil";
		$config["DataGrid"]["removeButtonClass"] = "btn btn-danger";
		$config["DataGrid"]["removeButtonStyle"] = "";
	
	// RemoveButton Settings
	
	
	// SearchButton Settings
		
		$config["DataGrid"]["searchButtonName"] = "Search";
		$config["DataGrid"]["searchButtonClass"] = "btn btn-success";
		$config["DataGrid"]["searchButtonStyle"] = "";
	
	// SearchButton Settings
	
	// SearchInput Settings
		
		$config["DataGrid"]["searchInputPlaceholder"] = "Tabloda Arayın . . .";
		$config["DataGrid"]["searchInputClass"] = "form-control";
		$config["DataGrid"]["searchInputStyle"] = "";
	
	// SearchInput Settings
	
	// SearchInput Settings
		
		$config["DataGrid"]["editFormInput"] = "Tabloda Arayın . . .";
		$config["DataGrid"]["editFormInputClass"] = "form-control";
		$config["DataGrid"]["editFormInputStyle"] = "";
	
	// SearchInput Settings
	
	
	
	
?>