-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Anamakine: localhost:3306
-- Üretim Zamanı: 08 Ara 2016, 22:57:53
-- Sunucu sürümü: 5.5.52-cll
-- PHP Sürümü: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Veritabanı: `merkezya_aktivite`
--


-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `aktivite`
--

CREATE TABLE IF NOT EXISTS `aktivite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `baslik` varchar(150) COLLATE utf8_turkish_ci NOT NULL,
  `aciklama` text COLLATE utf8_turkish_ci NOT NULL,
  `resim` int(11) NOT NULL,
  `cinsiyet` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `ucret` int(11) NOT NULL,
  `bastarih` datetime NOT NULL,
  `bittarih` datetime NOT NULL,
  `onay` tinyint(1) NOT NULL DEFAULT '1',
  `lat` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `lng` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `seo_link` varchar(150) COLLATE utf8_turkish_ci NOT NULL,
  `vitrin` tinyint(1) NOT NULL,
  `yas_aralik` int(11) NOT NULL,
  `aktivite_kat` int(11) NOT NULL,
  `adres` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `kapasite` int(11) NOT NULL,
  `tur` int(11) NOT NULL,
  `mesafe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=297 ;

--
-- Tablo döküm verisi `aktivite`
--

INSERT INTO `aktivite` (`id`, `baslik`, `aciklama`, `resim`, `cinsiyet`, `userid`, `ucret`, `bastarih`, `bittarih`, `onay`, `lat`, `lng`, `seo_link`, `vitrin`, `yas_aralik`, `aktivite_kat`, `adres`, `kapasite`, `tur`, `mesafe`) VALUES
(227, 'FIFA 16', 'FIFA 16 oynuyoruz.', 234, 0, 133, 0, '2016-08-09 16:48:00', '2016-08-10 16:48:00', 1, '40.75902887410603', '29.945026333037074', 'fifa-16', 0, 2, 4, 'kocaeli', 6, 0, 0),
(223, 'Can dostlarla geziyoruz.', 'Akşam yürüyüşü.', 226, 0, 127, 0, '2016-08-01 19:04:00', '2016-08-24 18:04:00', 1, '39.76023703623711', '30.523584160760457', 'can-dostlarla-geziyoruz.', 0, 3, 11, 'Eskişehir', 5, 0, 0),
(213, 'Gençlik otobüsü', 'gençlik otobüsü Berlin&amp;#039;de', 213, 0, 127, 0, '2016-08-31 07:30:00', '2016-09-08 11:43:00', 1, '52.496387881939235', '13.379991059354975', 'genclik-otobusu', 0, 2, 1, 'Berlin', 50, 0, 0),
(218, 'Sezen Aksu', 'bu guzel konsere  hepinizi beklıyoruz', 221, 0, 130, 200, '2016-08-02 14:44:00', '2016-08-02 20:44:00', 1, '40.222939300029175', '33.901920509375', 'sezen-aksu', 0, 3, 7, 'İstanbul / Bostancı gösteri merkezi', 200, 0, 0),
(206, 'Maç var', 'Beyler maç var', 204, 1, 127, 0, '2016-08-26 18:00:00', '2016-08-26 19:30:00', 1, '39.87559524188814', '32.87348735407926', 'mac-var', 0, 3, 1, 'Ankara', 22, 0, 0),
(201, 'bisiklet yarışı', 'yarış var:)', 200, 0, 127, 0, '2016-09-02 15:16:00', '2016-10-02 19:16:00', 1, '40.376651187624866', '28.88195235907431', 'bisiklet-yarisi', 0, 2, 2, 'bursa mudanya', 10, 1, 0),
(210, 'Konser', 'Rockn roll', 208, 0, 127, 250, '2016-08-05 19:23:00', '2016-08-05 01:23:00', 1, '40.94332830588999', '28.853792778651865', 'konser', 0, 2, 1, 'istanbul', 30, 0, 0),
(226, 'Konya şekeriyle çay içelim:)', 'Sadece can sıkıntısı', 228, 0, 127, 0, '2016-08-10 16:15:00', '2016-08-18 16:15:00', 1, '39.60804711003758', '28.853792778651865', 'konya-sekeriyle-cay-icelim', 0, 3, 8, 'Konya', 10, 0, 0),
(209, 'Sualtı topu', 'Sualtı topu yarışı', 207, 1, 127, 0, '2016-08-17 11:14:00', '2016-08-17 14:14:00', 1, '38.61431951901499', '27.43439841782696', 'sualti-topu', 0, 3, 2, 'manisa', 15, 0, 0),
(228, 'TrendSetter İstanbul', 'Yurt dışında tasarımcılarımızı tanıtmak üzere bir proje hazırlıyoruz. İstanbul’da gerçekleşen moda haftalarına dünyadan satın almacıların gelmelerini bekliyoruz ancak bu gerçekleşmiyor. Biz de dünyaya açılmaya karar verdik. Arzu Kaprol, Hakan Yıldırım, Dilek Hanif, Cengiz Abazoğlu dünyada isimlerini duyurmuş modacılar..........', 235, 0, 131, 150, '2016-08-05 16:47:00', '2016-08-07 16:48:00', 1, '39.60804711003758', '28.853792778651865', 'trendsetter-istanbul', 0, 4, 9, 'Adres: Halaskargazi Cad. Sait Kuran İş Mrk. No: 145 Kat: 4 Şişli / İstanbul', 150, 0, 0),
(207, 'Counter strike', 'Counter strike oynuyoruz', 205, 1, 127, 0, '2016-08-19 20:17:00', '2016-08-19 01:17:00', 1, '36.841431734116675', '30.77869283041971', 'counter-strike', 0, 2, 4, 'Antalya', 6, 1, 0),
(225, 'Atayı Ziyaret', 'Atamızı Ziyarete Gidiyoruz.', 227, 0, 126, 0, '2016-08-01 16:09:00', '2016-08-03 16:09:00', 1, '39.92544926037234', '32.83592767952451', 'atayi-ziyaret', 0, 2, 1, 'Anıtkabir', 10, 0, 0),
(212, 'Kızlarla kahve keyfi', 'Sabah kahvesi içiyoruz', 212, 2, 127, 7, '2016-08-12 09:32:00', '2016-08-12 10:32:00', 1, '36.82132897959789', '28.268793603085033', 'kizlarla-kahve-keyfi', 0, 2, 1, 'marmaris', 5, 1, 0),
(205, 'Karadeniz Gezisi', 'Bi karadeniz havası alalım.', 203, 0, 127, 120, '2016-08-18 09:52:00', '2016-08-24 09:52:00', 1, '41.02471034608388', '40.518528161996315', 'karadeniz-gezisi', 0, 2, 1, 'Rize', 30, 0, 0),
(276, 'Çocuklar  Sporla Büyüsün', 'Çocuklar Sporla Büyüsün  : Nerede ya da Nasıl  yüzme öğrenebilirim sorusuna verilecek ilk cevap tabi ki su olan bir yerde.  Yüzme öğrenirken öncelikle kişi kendine güvenmelidir. Korkmanıza gerek yok, yüzme kolay öğrenilecek bir spordur.', 270, 0, 124, 50, '2016-08-08 09:30:00', '2016-08-13 19:00:00', 1, '40.0', '28.0', 'cocuklar-sporla-buyusun', 0, 1, 5, 'Aqua Club Dolphin Cemalpaşa Cad., 34517 Esenyurt/İstanbul, Türkiye', 50, 0, 0),
(216, 'Moda Haftası', 'Moda haftası başlıyor.', 229, 0, 127, 0, '2016-08-16 14:31:00', '2016-08-18 14:31:00', 1, '52.496387881939235', '28.859209199999995', 'moda-haftasi', 0, 3, 9, 'İstanbul', 30, 0, 0),
(208, 'Kayısı festivali', 'Kayısı topluyoruz', 206, 0, 127, 0, '2016-08-21 11:04:00', '2016-08-31 11:04:00', 1, '38.341967262051924', '38.32596119599109', 'kayisi-festivali', 0, 2, 1, 'Malatya', 50, 0, 0),
(234, 'şişşst sessiz olun kitap okuyoruz:)', 'Kitap sevenler burada', 243, 2, 127, 0, '2016-08-19 09:42:00', '2016-08-23 09:42:00', 1, '39.60804711003758', '28.853792778651865', 'sissst-sessiz-olun-kitap-okuyoruz', 0, 3, 6, 'van', 22, 0, 10),
(233, 'Mahallemizde komşularla sohbet var:)', 'Mahallemizde komşularla sohbet var:)', 240, 0, 127, 0, '2016-08-11 14:30:00', '2016-08-18 17:30:00', 1, '39.60804711003758', '28.853792778651865', 'mahallemizde-komsularla-sohbet-var', 0, 3, 3, 'samsun', 32, 0, 0),
(235, 'Ava Gidiyoruz', 'Polatlıya tavşan avına gidiyoruz', 242, 1, 126, 0, '2016-08-05 09:43:00', '2016-08-02 09:43:00', 1, '39.60804711003758', '32.14062176233323', 'ava-gidiyoruz', 0, 4, 1, 'Ankara Polatlı', 10, 0, 0),
(237, 'Ankara Alışveriş Festivali', 'Ankara Ticaret Odası (ATO) tarafından düzenlenen Ankara Alışveriş Festivali moda günlerine ev sahipliği yapacak. Haydi arkadaşlar alışverişe :)', 244, 0, 124, 0, '2016-08-07 10:13:00', '2016-08-09 10:13:00', 1, '38.28373084123223', '38.28443911244765', 'ankara-alisveris-festivali', 0, 3, 10, 'Ankara/Kızılay', 300, 0, 0),
(240, 'Sobee:)', 'SOBE', 245, 0, 127, 0, '2016-08-10 11:19:00', '2016-08-10 18:19:00', 1, '40.22134950241714', '28.840994460351524', 'sobee', 0, 1, 4, 'Bursa', 12, 0, 100),
(293, 'Kole Yapalım', 'Kole Yapalım', 310, 0, 154, 0, '2016-09-22 01:14:00', '2016-09-28 01:14:00', 1, '39.16157508960785', '34.18653872182265', 'kole-yapalim', 0, 1, 1, 'Kole Yapalım', 22, 0, 0),
(296, 'Ghgg', 'Hhhhh', 0, 0, 154, 0, '2016-10-03 14:21:00', '2016-10-03 15:21:00', 1, '38.68213', '39.1964507', 'ghgg', 0, 0, 2, '', 0, 0, 0),
(290, 'Türk Ticaret  Sunumu', 'türkticaret net sunumu', 303, 0, 127, 0, '2016-08-08 13:00:00', '2016-08-09 15:00:00', 1, '40.222750436489044', '28.867095972799213', 'turk-ticaret-sunumu', 0, 2, 1, 'TeknoTeknt', 50, 1, 0),
(291, 'Domates topluyoruz', 'Domates toplamaya herkesi bekliyorum hava sıcak bayılanları traktörle bizzat kendim hastaneye götürücem yövmiye 30 lira', 304, 0, 97, 0, '2016-08-12 05:00:00', '2016-08-11 17:50:00', 1, '40.4868241', '29.7223843', 'domates-topluyoruz', 0, 2, 12, 'Elbeyli İznik', 30, 0, 0),
(292, 'test', 'test', 309, 0, 154, 0, '2016-09-23 01:05:00', '2016-09-30 01:05:00', 1, '39.05886406455031', '31.94532778432264', 'test', 0, 1, 1, 'test', 22, 0, 0),
(266, 'Koşu yarışı', 'Sahilde koşu yarışı yapılacaktır', 265, 0, 127, 0, '2016-08-02 07:37:00', '2016-08-08 16:37:00', 1, '40.2229692', '28.859209199999995', 'kosu-yarisi-t', 0, 2, 2, 'Bursa', 12, 0, 0);

--
-- Tetikleyiciler `aktivite`
--
DROP TRIGGER IF EXISTS `aktivite_sil`;
DELIMITER //
CREATE TRIGGER `aktivite_sil` AFTER DELETE ON `aktivite`
 FOR EACH ROW BEGIN

delete from oylama where oylama.aktiviteid = old.id;
delete from dosyalar where dosyalar.id = old.resim;
delete from davetiye where davetiye.aktiviteid =old.id;
delete from sikayet where sikayet.aktiviteid = old.id;

END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `aktivite_resim`
--

CREATE TABLE IF NOT EXISTS `aktivite_resim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aktivite_id` int(11) NOT NULL,
  `dosya_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=64 ;

--
-- Tablo döküm verisi `aktivite_resim`
--

INSERT INTO `aktivite_resim` (`id`, `aktivite_id`, `dosya_id`) VALUES
(1, 265, 262),
(2, 265, 263),
(3, 267, 264),
(4, 266, 265),
(5, 268, 266),
(6, 269, 267),
(7, 272, 268),
(8, 227, 234),
(9, 223, 226),
(10, 213, 213),
(11, 218, 221),
(12, 206, 204),
(13, 201, 200),
(14, 210, 208),
(15, 226, 228),
(16, 209, 207),
(17, 228, 235),
(18, 207, 205),
(19, 225, 227),
(20, 212, 212),
(21, 205, 203),
(22, 216, 229),
(23, 208, 206),
(24, 234, 243),
(25, 233, 240),
(26, 235, 242),
(27, 237, 244),
(28, 240, 245),
(29, 264, 260),
(30, 266, 265),
(31, 276, 270),
(32, 278, 271),
(33, 278, 272),
(34, 279, 273),
(35, 281, 282),
(46, 284, 293),
(45, 283, 292),
(44, 283, 291),
(43, 283, 290),
(47, 285, 294),
(48, 287, 295),
(49, 288, 296),
(50, 289, 297),
(51, 289, 298),
(52, 290, 299),
(53, 290, 300),
(54, 290, 301),
(55, 290, 302),
(56, 290, 303),
(57, 291, 304),
(58, 292, 307),
(59, 292, 308),
(60, 292, 309),
(61, 293, 310);

--
-- Tetikleyiciler `aktivite_resim`
--
DROP TRIGGER IF EXISTS `aktivite_resim_sil`;
DELIMITER //
CREATE TRIGGER `aktivite_resim_sil` AFTER DELETE ON `aktivite_resim`
 FOR EACH ROW DELETE FROM dosyalar WHERE dosyalar.id = OLD.dosya_id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `alanlar`
--

CREATE TABLE IF NOT EXISTS `alanlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alanadi` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=18 ;

--
-- Tablo döküm verisi `alanlar`
--

INSERT INTO `alanlar` (`id`, `alanadi`) VALUES
(1, 'Yetkiler'),
(2, 'Kullanıcılar'),
(3, 'Alanlar'),
(6, 'Alan Yetkisi'),
(7, 'Resim Ve Dosya Galeri'),
(8, 'Kategoriler'),
(9, 'Ürünler'),
(10, 'Odemeler'),
(11, 'Genel Ayarlar'),
(12, 'Firmalar'),
(13, 'Kampanyalar'),
(14, 'Yorumlar'),
(15, 'Dil Ayarları'),
(16, 'İçerikler'),
(17, 'Değişken Ayarı');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `alanyetkisi`
--

CREATE TABLE IF NOT EXISTS `alanyetkisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alanyetkisi` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=6 ;

--
-- Tablo döküm verisi `alanyetkisi`
--

INSERT INTO `alanyetkisi` (`id`, `alanyetkisi`) VALUES
(1, 'Görüntüleme Yetkisi'),
(2, 'Veri Ekleme Yetkisi'),
(3, 'Veri Güncelleme Yetkisi'),
(4, 'Veri Silme Yetkisi'),
(5, 'Onay Değiştir');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `arkadaslar`
--

CREATE TABLE IF NOT EXISTS `arkadaslar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `gonderenid` int(11) NOT NULL,
  `durum` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=37 ;

--
-- Tablo döküm verisi `arkadaslar`
--

INSERT INTO `arkadaslar` (`id`, `userid`, `gonderenid`, `durum`) VALUES
(32, 97, 127, 1),
(2, 126, 124, 1),
(3, 126, 97, 1),
(4, 126, 127, 1),
(36, 154, 126, 0),
(6, 127, 124, 1),
(9, 97, 124, 0),
(35, 137, 126, 0),
(10, 137, 127, 0),
(12, 130, 127, 1),
(13, 133, 127, 1),
(14, 133, 124, 1),
(16, 127, 137, 1),
(18, 127, 130, 1),
(19, 148, 97, 1),
(20, 127, 131, 1),
(34, 154, 127, 0),
(31, 97, 130, 1),
(23, 127, 135, 1),
(25, 127, 138, 1),
(26, 127, 139, 1),
(27, 127, 153, 1),
(28, 127, 150, 1),
(29, 127, 151, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `ayarlar`
--

CREATE TABLE IF NOT EXISTS `ayarlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteayari` text COLLATE utf8_turkish_ci NOT NULL,
  `iletisim` text COLLATE utf8_turkish_ci NOT NULL,
  `email` text COLLATE utf8_turkish_ci NOT NULL,
  `icerik` text COLLATE utf8_turkish_ci NOT NULL,
  `sslayari` text COLLATE utf8_turkish_ci NOT NULL,
  `sitedurum` text COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=2 ;

--
-- Tablo döküm verisi `ayarlar`
--

INSERT INTO `ayarlar` (`id`, `siteayari`, `iletisim`, `email`, `icerik`, `sslayari`, `sitedurum`) VALUES
(1, '{"adi":"Aktivite Türkiye ","baslik":"Şimdi Aktivite Zamanı","aciklama":"burası site açıklaması seo için önemli","etiketler":"burası site etiketleri google seo için önemli"}', '{"telefon":"05348500000","email":"info@lonnca.com","adres":"xx","iframe":"xx"}', '{"iletisim":"aktivite@merkezyazilim.com","replyto":"aktivite@merkezyazilim.com","gonderenadres":"aktivite@merkezyazilim.com","gonderenadresisim":"Aktivite Türkiye","mailhost":"mail.merkezyazilim.com","mailkullanici":"aktivite@merkezyazilim.com","mailsifre":"14021402","mailport":"587","mailencode":"tsl"}', '{"icerigi":"1"}', '{"ssl":"1"}', '{"sitedurumu":"1"}');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `bildirim`
--

CREATE TABLE IF NOT EXISTS `bildirim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `durum` tinyint(1) NOT NULL,
  `tip` int(11) NOT NULL,
  `aciklama` text COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=154 ;

--
-- Tablo döküm verisi `bildirim`
--

INSERT INTO `bildirim` (`id`, `userid`, `durum`, `tip`, `aciklama`, `tarih`) VALUES
(31, 136, 1, 2, '<div class="col-md-12 "><p><a href="http://aktivite.merkezyazilim.com/unname">unname</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>\n\n									<div class="col-md-12">\n\n										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n									</div>\n\n									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="142" data-uid="136" data-gid="123"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="142" data-uid="136" data-gid="123"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>', '2016-08-01 20:32:40'),
(55, 146, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/agaoglu">agaoglu</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 12:30:29'),
(43, 128, 1, 3, 'Metin Ağaoğlu adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Ava Gidiyoruz başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/ava-gidiyoruz/235">Ava Gidiyoruz</a>', '2016-08-02 07:12:57'),
(89, 127, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-04 11:11:56'),
(42, 97, 0, 3, 'Metin Ağaoğlu adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Ava Gidiyoruz başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/ava-gidiyoruz/235">Ava Gidiyoruz</a>', '2016-08-02 09:03:01'),
(52, 97, 0, 7, 'Davet Edildiğin Mfkkdkd aktivitesi silindiğinden dolayı davetiyen iptal edildi ', '2016-08-02 11:59:19'),
(53, 126, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/compistiyer">compistiyer</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 12:49:32'),
(59, 127, 0, 7, 'Davet Edildiğin Bilişim Sohbetleri aktivitesi silindiğinden dolayı davetiyen iptal edildi ', '2016-08-02 13:37:05'),
(61, 128, 1, 7, 'Davet Edildiğin Bilişim Sohbetleri aktivitesi silindiğinden dolayı davetiyen iptal edildi ', '2016-08-02 13:33:47'),
(135, 97, 0, 3, 'Metin Ağaoğlu adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  TURKTICARET.NET Proje Sunumları başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turkticaret.net-proje-sunumlari/283>Aktiviteyi İncele</a> ', '2016-08-08 07:09:11'),
(40, 137, 1, 4, '<p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 11:49:53'),
(85, 126, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/unname">unname</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:36:29'),
(82, 126, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:36:29'),
(38, 133, 1, 3, 'sibel şahin adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Mahallemizde komşularla sohbet var:) başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/mahallemizde-komsularla-sohbet-var/233">Mahallemizde komşularla sohbet var:)</a>', '2016-08-02 06:38:45'),
(37, 130, 1, 3, 'sibel şahin adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Mahallemizde komşularla sohbet var:) başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/mahallemizde-komsularla-sohbet-var/233">Mahallemizde komşularla sohbet var:)</a>', '2016-08-02 06:38:45'),
(132, 97, 0, 7, 'Davet Edildiğin TürkTicaretNet Sunum aktivitesi silindiğinden dolayı davetiyen iptal edildi ', '2016-08-08 07:09:57'),
(134, 124, 1, 3, 'Metin Ağaoğlu adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  TURKTICARET.NET Proje Sunumları başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turkticaret.net-proje-sunumlari/283>Aktiviteyi İncele</a> ', '2016-08-07 12:19:19'),
(56, 97, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/compistiyer">compistiyer</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 13:17:27'),
(54, 126, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/compistiyer">compistiyer</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-02 12:30:16'),
(32, 136, 1, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/unname">unname</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-01 20:32:49'),
(33, 128, 1, 3, 'Bekir Samet  Şirinel adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Profesionelliğin yeni adı başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/profesionelligin-yeni-adi/232">Profesionelliğin yeni adı</a>', '2016-08-01 20:44:52'),
(34, 97, 0, 3, 'Bekir Samet  Şirinel adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Profesionelliğin yeni adı başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/profesionelligin-yeni-adi/232">Profesionelliğin yeni adı</a>', '2016-08-02 09:03:11'),
(50, 127, 0, 7, 'Davet Edildiğin resim deneme aktivitesi silindiğinden dolayı davetiyen iptal edildi ', '2016-08-02 11:04:54'),
(47, 130, 1, 3, 'yasemin imrek adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Ankara Alışveriş Festivali başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/ankara-alisveris-festivali/237">Ankara Alışveriş Festivali</a>', '2016-08-02 07:17:51'),
(88, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 08:34:25'),
(57, 146, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/hbabuc">hbabuc</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 13:17:27'),
(75, 150, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-03 08:40:00'),
(78, 126, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:36:29'),
(77, 97, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-03 08:43:07'),
(80, 126, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 11:36:29'),
(87, 126, 0, 3, 'yasemin imrek adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Çocuklar  Sporla Büyüsün başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/cocuklar-sporla-buyusun/276">Çocuklar  Sporla Büyüsün</a>', '2016-08-05 11:32:19'),
(74, 137, 1, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-02 22:59:03'),
(65, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/burcin">burcin</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-03 08:40:00'),
(76, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/hbabuc">hbabuc</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-03 08:45:58'),
(81, 126, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/hbabuc">hbabuc</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:36:29'),
(90, 97, 0, 2, '<div class="col-md-12 "><p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>\n\n									<div class="col-md-12">\n\n										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n									</div>\n\n									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="7" data-uid="97" data-gid="124"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="7" data-uid="97" data-gid="124"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>', '2016-08-04 11:28:43'),
(66, 127, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/burcin">burcin</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-02 13:41:58'),
(67, 126, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/burcin">burcin</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 13:49:39'),
(84, 97, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/agaoglu">agaoglu</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-04 07:32:27'),
(83, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/agaoglu">agaoglu</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-04 07:32:24'),
(91, 97, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-04 11:28:35'),
(68, 146, 1, 2, '<div class="col-md-12 "><p><a href="http://aktivite.merkezyazilim.com/burcin">burcin</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>\n\n									<div class="col-md-12">\n\n										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n									</div>\n\n									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="149" data-uid="146" data-gid="150"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="149" data-uid="146" data-gid="150"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>', '2016-08-02 13:43:24'),
(69, 146, 1, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/burcin">burcin</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-02 13:43:26'),
(70, 97, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/burcin">burcin</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 13:46:31'),
(73, 150, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/agaoglu">agaoglu</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 13:49:39'),
(71, 97, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/burcin">burcin</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-02 13:46:13'),
(72, 150, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/hbabuc">hbabuc</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-02 13:46:31'),
(99, 124, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/unname">unname</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-04 20:11:57'),
(100, 137, 1, 2, '<div class="col-md-12 "><p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>\n\n									<div class="col-md-12">\n\n										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n									</div>\n\n									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="10" data-uid="137" data-gid="127"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="10" data-uid="137" data-gid="127"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>', '2016-08-05 08:33:21'),
(94, 97, 0, 2, '<div class="col-md-12 "><p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>\n\n									<div class="col-md-12">\n\n										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n									</div>\n\n									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="9" data-uid="97" data-gid="124"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="9" data-uid="97" data-gid="124"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>', '2016-08-04 11:28:24'),
(95, 97, 0, 3, 'Metin Ağaoğlu adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Langırt Turnuvası başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/langirt-turnuvasi/275">Langırt Turnuvası</a>', '2016-08-04 11:28:20'),
(96, 97, 0, 6, '<a href=http://aktivite.merkezyazilim.com/langirt-turnuvasi/275>Langırt Turnuvası </a> aktivitesinden çıkarıldın.. ', '2016-08-04 11:28:53'),
(97, 97, 0, 3, 'Metin Ağaoğlu adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Langırt Turnuvası başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/langirt-turnuvasi/275">Langırt Turnuvası</a>', '2016-08-04 11:29:03'),
(98, 124, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/unname">unname</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-04 14:06:16'),
(101, 124, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 08:34:25'),
(131, 126, 0, 7, 'Davet Edildiğin TürkTicaretNet Sunum aktivitesi silindiğinden dolayı davetiyen iptal edildi ', '2016-08-07 19:18:49'),
(129, 97, 0, 3, 'Metin Ağaoğlu adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  TürkTicaretNet Sunum başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turkticaretnet-sunum/282>Aktiviteyi İncele</a> ', '2016-08-08 07:10:07'),
(104, 130, 1, 4, '<p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 10:21:59'),
(128, 130, 1, 3, 'Samet Şirinel adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Test Aktivitesi başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/test-aktivitesi/281">Test Aktivitesi</a>', '2016-08-07 11:03:34'),
(136, 127, 0, 3, 'Metin Ağaoğlu adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  TURKTICARET.NET Proje Sunumları başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turkticaret.net-proje-sunumlari/283>Aktiviteyi İncele</a> ', '2016-08-07 12:20:48'),
(105, 130, 1, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 08:37:33'),
(106, 133, 1, 4, '<p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 08:38:26'),
(109, 133, 1, 4, '<p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 08:42:03'),
(113, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/derman">derman</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 10:21:59'),
(126, 127, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/unname">unname</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-07 10:03:41'),
(107, 133, 1, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 08:37:55'),
(108, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/omer">omer</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 08:38:26'),
(112, 137, 1, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 08:45:03'),
(110, 133, 1, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/yasmin">yasmin</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 08:41:53'),
(111, 124, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/omer">omer</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 08:42:03'),
(127, 126, 0, 3, 'Samet Şirinel adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Test Aktivitesi başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/test-aktivitesi/281">Test Aktivitesi</a>', '2016-08-07 19:18:59'),
(116, 97, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/derman">derman</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:26:28'),
(125, 130, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/unname">unname</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-06 19:32:14'),
(149, 154, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-09-27 12:47:15'),
(138, 126, 0, 3, 'Samet Şirinel adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  Türk Ticaret  Sunumu başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turk-ticaret-sunumu/284>Aktiviteyi İncele</a> ', '2016-08-08 06:32:38'),
(117, 150, 1, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 10:29:05'),
(118, 127, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/hbabuc">hbabuc</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 10:33:02'),
(119, 97, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:25:48'),
(123, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/hbabuc">hbabuc</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:23:51'),
(124, 130, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/hbabuc">hbabuc</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:26:28'),
(139, 124, 0, 3, 'Samet Şirinel adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  Türk Ticaret  Sunumu başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turk-ticaret-sunumu/284>Aktiviteyi İncele</a> ', '2016-08-16 07:08:38'),
(120, 97, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 11:23:32'),
(121, 127, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/agaoglu">agaoglu</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-08-05 10:35:16'),
(122, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/hbabuc">hbabuc</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-08-05 11:23:46'),
(140, 130, 1, 3, 'Samet Şirinel adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  Türk Ticaret  Sunumu başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turk-ticaret-sunumu/284>Aktiviteyi İncele</a> ', '2016-08-08 06:20:10'),
(141, 97, 0, 3, 'sibel şahin adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  Türk Ticaret  Sunumu başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turk-ticaret-sunumu/290>Aktiviteyi İncele</a> ', '2016-08-08 09:43:54'),
(142, 126, 0, 3, 'sibel şahin adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  Türk Ticaret  Sunumu başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turk-ticaret-sunumu/290>Aktiviteyi İncele</a> ', '2016-08-08 09:57:13'),
(143, 137, 1, 3, 'sibel şahin adlı arkadaşın arkadaşlarına özel aktivite oluşturdu ve senide davet ediyor. Aktivite  Türk Ticaret  Sunumu başlıklı aktiviteye göz atmak için <a href=http://aktivite.merkezyazilim.com/turk-ticaret-sunumu/290>Aktiviteyi İncele</a> ', '2016-08-08 09:42:59'),
(144, 126, 0, 3, 'sibel şahin adlı arkadaşın oluşturduğuı aktiviteye senide davet ediyor. Koşu yarışı başlıklı aktiviteye göz atmak için \n									<a class="davet" href="http://aktivite.merkezyazilim.com/kosu-yarisi-t/266">Koşu yarışı</a>', '2016-08-08 09:57:03'),
(146, 127, 0, 7, 'Davet Edildiğin Kole Yapalım aktivitesi silindiğinden dolayı davetiyen iptal edildi ', '2016-10-20 06:42:27'),
(147, 127, 0, 7, 'Davet Edildiğin Kole Yapalım aktivitesi silindiğinden dolayı davetiyen iptal edildi ', '2016-10-20 06:42:17'),
(148, 154, 0, 2, '<div class="col-md-12"><p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> kullanıcısı sizi takip etmeye başladı.</p></div>\n\n				<div class="col-md-12">\n\n					<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n				</div>', '2016-09-27 12:47:04'),
(152, 137, 1, 2, '<div class="col-md-12 "><p><a href="http://aktivite.merkezyazilim.com/agaoglu">agaoglu</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>\n\n									<div class="col-md-12">\n\n										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n									</div>\n\n									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="35" data-uid="137" data-gid="126"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="35" data-uid="137" data-gid="126"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>', '2016-10-15 11:56:31'),
(150, 154, 0, 2, '<div class="col-md-12 "><p><a href="http://aktivite.merkezyazilim.com/sibel">sibel</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>\n\n									<div class="col-md-12">\n\n										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n									</div>\n\n									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="34" data-uid="154" data-gid="127"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="34" data-uid="154" data-gid="127"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>', '2016-09-27 12:46:44'),
(151, 127, 0, 4, '<p><a href="http://aktivite.merkezyazilim.com/unname">unname</a> adlı kullanıcı ile arkadaş oldun.</p>', '2016-09-27 12:47:15'),
(153, 154, 1, 2, '<div class="col-md-12 "><p><a href="http://aktivite.merkezyazilim.com/agaoglu">agaoglu</a> kullanıcısı sizi arkadaş olarak eklemek istiyor. </p></div>\n\n									<div class="col-md-12">\n\n										<div style="margin:10px 0px ;border-bottom:1px solid #cecece"></div>\n\n									</div>\n\n									<div clasS=" col-md-6 col s6">  <a class="btn btn-success col-xs-12 arkadaslikKabul col s12"  href="#" id="kabulet" data-id="36" data-uid="154" data-gid="126"><i class="fa fa-check"></i> Kabul Et </a> <div class="clearfix"></div> </div><div class="col-md-6 col s6"><a class="btn btn-danger col-xs-12 arkadaslikReddet red col s12"  href="#" id="kabulet" data-id="36" data-uid="154" data-gid="126"><i class="fa fa-remove"></i> Reddet </a><div class="clearfix"></div></div><div class="clearfix"></div>', '2016-11-14 11:44:22');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `davetiye`
--

CREATE TABLE IF NOT EXISTS `davetiye` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gonderenid` int(11) NOT NULL,
  `aktiviteid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `durum` int(11) NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tip` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=313 ;

--
-- Tablo döküm verisi `davetiye`
--

INSERT INTO `davetiye` (`id`, `gonderenid`, `aktiviteid`, `userid`, `durum`, `tarih`, `tip`) VALUES
(94, 0, 192, 124, 1, '2016-07-29 10:03:08', 1),
(194, 0, 232, 123, 1, '2016-08-02 11:46:13', 2),
(307, 124, 276, 123, 0, '2016-09-15 01:29:18', 0),
(175, 126, 225, 127, 0, '2016-08-01 13:24:16', 0),
(248, 0, 266, 97, 1, '2016-08-02 13:40:00', 1),
(132, 127, 201, 126, 2, '2016-08-01 10:15:32', 0),
(117, 0, 207, 128, 1, '2016-08-01 08:02:15', 1),
(84, 130, 192, 126, 2, '2016-07-29 10:02:19', 0),
(96, 0, 175, 127, 1, '2016-07-29 10:10:34', 1),
(116, 0, 205, 127, 1, '2016-08-01 06:58:35', 1),
(312, 0, 296, 154, 1, '2016-10-03 11:21:52', 2),
(153, 0, 208, 123, 1, '2016-08-01 11:26:13', 1),
(156, 0, 218, 130, 1, '2016-08-01 11:49:16', 1),
(306, 0, 291, 97, 1, '2016-08-11 19:52:40', 2),
(124, 0, 213, 127, 1, '2016-08-01 08:48:26', 1),
(103, 0, 199, 97, 1, '2016-07-29 12:01:00', 1),
(222, 0, 241, 97, 1, '2016-08-02 09:05:26', 2),
(198, 0, 233, 127, 1, '2016-08-02 06:37:48', 1),
(245, 0, 266, 127, 1, '2016-08-02 13:37:49', 2),
(159, 0, 219, 123, 1, '2016-08-01 11:59:19', 2),
(160, 0, 220, 123, 1, '2016-08-01 12:03:50', 2),
(161, 0, 216, 127, 1, '2016-08-01 12:58:10', 1),
(162, 0, 218, 124, 1, '2016-08-01 12:59:36', 1),
(164, 130, 218, 127, 0, '2016-08-01 13:08:41', 0),
(165, 0, 225, 126, 2, '2016-08-01 13:14:08', 0),
(174, 126, 225, 128, 0, '2016-08-01 13:24:16', 0),
(186, 0, 228, 127, 1, '2016-08-01 13:58:17', 1),
(179, 0, 226, 124, 1, '2016-08-01 13:27:33', 1),
(182, 0, 227, 127, 1, '2016-08-01 13:50:09', 1),
(178, 0, 226, 126, 1, '2016-08-01 13:25:36', 1),
(181, 0, 227, 133, 1, '2016-08-01 13:49:48', 1),
(183, 0, 228, 131, 2, '2016-08-01 13:50:40', 0),
(184, 131, 228, 133, 0, '2016-08-01 13:53:06', 0),
(305, 127, 266, 126, 0, '2016-08-08 09:56:49', 0),
(190, 0, 229, 123, 2, '2016-08-01 14:09:58', 0),
(191, 0, 230, 123, 2, '2016-08-01 14:11:30', 0),
(193, 0, 231, 97, 2, '2016-08-01 14:19:07', 0),
(195, 123, 232, 128, 0, '2016-08-01 20:44:52', 0),
(196, 123, 232, 97, 0, '2016-08-01 20:44:52', 0),
(199, 127, 233, 126, 0, '2016-08-02 06:38:45', 0),
(217, 0, 237, 127, 1, '2016-08-02 07:19:59', 1),
(201, 127, 233, 130, 0, '2016-08-02 06:38:45', 0),
(202, 127, 233, 133, 0, '2016-08-02 06:38:45', 0),
(303, 0, 290, 123, 1, '2016-08-08 09:44:36', 1),
(204, 0, 234, 127, 2, '2016-08-02 06:43:37', 0),
(205, 0, 235, 126, 2, '2016-08-02 06:43:57', 0),
(249, 0, 266, 146, 1, '2016-08-02 13:40:17', 1),
(209, 126, 235, 128, 0, '2016-08-02 07:12:57', 0),
(210, 0, 237, 124, 2, '2016-08-02 07:15:32', 0),
(266, 124, 276, 126, 0, '2016-08-04 11:10:20', 0),
(308, 0, 292, 154, 1, '2016-09-21 22:05:26', 2),
(218, 0, 239, 123, 2, '2016-08-02 07:25:37', 0),
(235, 0, 261, 123, 1, '2016-08-02 11:50:08', 2),
(215, 124, 237, 130, 0, '2016-08-02 07:17:51', 0),
(216, 0, 233, 124, 1, '2016-08-02 07:19:27', 1),
(228, 0, 252, 97, 1, '2016-08-02 10:58:10', 2),
(224, 0, 250, 97, 1, '2016-08-02 10:48:29', 2),
(225, 0, 251, 97, 1, '2016-08-02 10:50:12', 2),
(227, 0, 240, 127, 1, '2016-08-02 10:56:28', 1),
(229, 0, 253, 97, 1, '2016-08-02 10:58:31', 2),
(230, 0, 254, 97, 1, '2016-08-02 10:59:15', 2),
(232, 0, 256, 97, 1, '2016-08-02 11:14:18', 2),
(234, 0, 261, 127, 1, '2016-08-02 11:49:14', 2),
(265, 0, 276, 124, 1, '2016-08-04 11:07:32', 2),
(299, 0, 290, 127, 1, '2016-08-08 09:42:25', 2),
(243, 0, 228, 146, 1, '2016-08-02 13:20:56', 1),
(267, 0, 266, 123, 1, '2016-08-04 11:10:33', 2),
(253, 0, 266, 97, 1, '2016-08-02 13:46:14', 2),
(304, 0, 276, 126, 1, '2016-08-08 09:54:10', 2),
(302, 127, 290, 137, 0, '2016-08-08 09:42:59', 0),
(309, 0, 293, 154, 1, '2016-09-21 22:14:57', 2),
(300, 127, 290, 97, 0, '2016-08-08 09:42:59', 0),
(294, 0, 233, 97, 1, '2016-08-08 07:08:55', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `degisgenler`
--

CREATE TABLE IF NOT EXISTS `degisgenler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyi` varchar(20) COLLATE utf8_turkish_ci NOT NULL,
  `dilid` int(11) NOT NULL,
  `value` text COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=113 ;

--
-- Tablo döküm verisi `degisgenler`
--

INSERT INTO `degisgenler` (`id`, `keyi`, `dilid`, `value`) VALUES
(1, 'activity', 1, 'Aktivite '),
(2, 'activity', 2, 'Activity'),
(3, 'aciklama', 1, 'Açıklama'),
(4, 'aciklama', 2, 'Description'),
(5, 'limit', 1, 'Kapasite'),
(6, 'limit', 2, 'Limit'),
(7, 'age range', 1, 'Yaş aralığı'),
(8, 'age range', 2, 'Age Range'),
(9, 'agerange', 1, 'Yaş aralığı'),
(10, 'agerange', 2, 'Agerange'),
(11, 'agerange', 1, 'Yaş aralığı'),
(12, 'agerange', 2, 'Age Range'),
(13, 'participants', 1, 'Katılımcılar'),
(14, 'participants', 2, 'Participants'),
(15, 'categori', 1, 'Kategori Seç'),
(16, 'categori', 2, 'Select Category'),
(17, 'point', 1, 'Puan'),
(18, 'point', 2, 'Point'),
(19, 'fee', 1, 'Ücret'),
(20, 'fee', 2, 'Fee'),
(21, 'price', 1, 'Ücret'),
(22, 'price', 2, 'Price'),
(23, 'free', 1, 'Ücretsiz'),
(24, 'free', 2, '\r\nFree'),
(25, 'icerikyok', 1, 'Henüz İçerik Eklenmemiş'),
(26, 'icerikyok', 2, 'Content is not loaded yet'),
(27, 'youremailadress', 1, 'Email Adresiniz'),
(28, 'youremailadress', 2, 'Your email adress'),
(29, 'select activity', 1, 'Aktivite Oluştur '),
(30, 'select activity', 2, 'select activity'),
(31, 'activities', 1, 'Aktiviteler'),
(32, 'activities', 2, 'Activities'),
(33, ' gender', 1, 'Cinsiyet Seç '),
(34, ' gender', 2, '\r\ngender'),
(35, 'womenactivities', 1, 'Bayan aktiviteleri'),
(36, 'womenactivities', 2, 'Womenactivities'),
(37, 'unaddedcontent', 1, 'Henüz içerik eklenmemiş'),
(38, 'unaddedcontent', 2, 'Unadded content'),
(39, '    ending_activitie', 1, 'Biten aktiviteler'),
(40, '    ending_activitie', 2, 'Ending activities'),
(41, 'endedon', 1, 'Tarihinde sonlandı'),
(42, 'endedon', 2, 'Ended on'),
(43, 'men', 1, 'Erkekler'),
(44, 'men', 2, 'Men'),
(45, 'activities', 1, 'Aktiviteler'),
(46, 'activities', 2, 'Activities'),
(47, 'all _activities', 1, 'Tüm aktiviteler'),
(48, 'all _activities', 2, 'All activities'),
(49, 'homepage', 1, 'Anasayfa'),
(50, 'homepage', 2, 'Home page'),
(51, 'userpicture', 1, 'Kullanıcı resim'),
(52, 'userpicture', 2, 'User picture'),
(53, 'add _a_comment', 1, 'Yorum ekle'),
(54, 'add _a_comment', 2, 'Add a comment'),
(55, 'send _a_comment', 1, 'Yorum gönder'),
(56, 'send _a_comment', 2, 'Send a comment'),
(57, 'writtencomplaints', 1, 'Şikayet yaz'),
(58, 'writtencomplaints', 2, 'Written complaints'),
(59, 'male', 1, 'Erkek'),
(60, 'male', 2, 'Male'),
(61, '    lady', 1, 'Bayan'),
(62, '    lady', 2, 'Lady\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n'),
(63, 'open _to_everyone', 1, 'Herkese açık'),
(64, 'open _to_everyone', 2, 'Open to everyone'),
(65, 'limit', 1, 'Limit'),
(66, 'limit', 2, 'Limit'),
(67, 'your _complaint', 1, 'Şikayetiniz'),
(68, 'your _complaint', 2, 'Your complaint'),
(69, 'unadded _content', 1, 'Henüz içerik eklenmemiş'),
(70, 'unadded _content', 2, 'Unadded Content'),
(71, 'sort _by _location', 1, 'Konuma göre sırala'),
(72, 'sort _by _location', 2, 'Sort by location'),
(73, 'free', 1, 'Ücretsiz'),
(74, 'free', 2, 'Free'),
(75, 'username', 1, 'Kullanıcı adı'),
(76, 'username', 2, 'User name'),
(77, 'your_name', 1, 'Adınız'),
(78, 'your_name', 2, 'Your name'),
(79, 'last_name', 1, 'Soy adı'),
(80, 'last_name', 2, 'Last name'),
(81, 'password', 1, 'Şifre'),
(82, 'password', 2, 'Password'),
(83, 'invite', 1, 'Davet et'),
(84, 'invite', 2, 'Invite'),
(85, 'login', 1, 'Giriş yapın'),
(86, 'login', 2, 'Login'),
(87, 'password', 1, 'Şifre'),
(88, 'password', 2, 'Password'),
(89, 'sign_ in', 1, 'Giriş yap'),
(90, 'sign_ in', 2, 'Sign in'),
(91, 'user_ name', 1, 'Kullanıcı adı'),
(92, 'user_ name', 2, 'User name'),
(93, 'password', 1, 'Şifre'),
(94, 'password', 2, 'Password'),
(95, 'register_now', 1, 'Hemen şimdi kayıt ol'),
(96, 'register_now', 2, 'Register Now'),
(97, 'password', 1, 'Şifre'),
(98, 'password', 2, 'Password'),
(99, ' all_your_friends', 1, 'Tüm arkadaşların'),
(100, ' all_your_friends', 2, '\r\nAll your friends'),
(101, 'the _user _name', 1, 'Kullanıcı adıyla'),
(102, 'the _user _name', 2, 'The user name'),
(103, 'off _the _track', 1, 'Takipten çıkar'),
(104, 'off _the _track', 2, 'Off the track'),
(105, ' social_network', 1, 'Sosyal ağ'),
(106, ' social_network', 2, '\r\nSocial network'),
(107, ' your_ links', 1, 'Linkiniz'),
(108, ' your_ links', 2, '\r\nYour links'),
(109, 'title', 2, 'Title'),
(110, 'title', 1, 'Başlık');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `dil`
--

CREATE TABLE IF NOT EXISTS `dil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adi` varchar(20) CHARACTER SET latin1 NOT NULL,
  `onay` tinyint(1) NOT NULL,
  `kisa` varchar(4) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=3 ;

--
-- Tablo döküm verisi `dil`
--

INSERT INTO `dil` (`id`, `adi`, `onay`, `kisa`) VALUES
(1, 'Türkçe', 1, 'TR'),
(2, 'English', 1, 'EN');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `dosyalar`
--

CREATE TABLE IF NOT EXISTS `dosyalar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adi` text COLLATE utf8_turkish_ci NOT NULL,
  `orta` text COLLATE utf8_turkish_ci NOT NULL,
  `kucuk` text COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=313 ;

--
-- Tablo döküm verisi `dosyalar`
--

INSERT INTO `dosyalar` (`id`, `adi`, `orta`, `kucuk`) VALUES
(127, '1469195368_34_20160409_191700_hdr-1.jpg', 'thumbs/1469195368_34_20160409_191700_hdr-1-0x0px-160x160size.jpg', 'thumbs/1469195368_34_20160409_191700_hdr-1-0x0px-50x50size.jpg'),
(126, '1469195343_49_20160314_072605.jpg', 'thumbs/1469195343_49_20160314_072605-0x0px-160x160size.jpg', 'thumbs/1469195343_49_20160314_072605-0x0px-50x50size.jpg'),
(125, '1469195257_dsc2030_21.05.2015_11_17_43.jpg', 'thumbs/1469195257_dsc2030_21.05.2015_11_17_43-0x0px-640x360size.jpg', 'thumbs/1469195257_dsc2030_21.05.2015_11_17_43-0x0px-320x180size.jpg'),
(123, '-tl.png', 'thumbs/-tl-0x0px-640x360size.png', 'thumbs/-tl-0x0px-320x180size.png'),
(124, '1469194962_1468588131_hydrangeas.jpg', 'thumbs/1469194962_1468588131_hydrangeas-0x0px-640x360size.jpg', 'thumbs/1469194962_1468588131_hydrangeas-0x0px-320x180size.jpg'),
(153, '1469442213_17_imag1576.jpg', 'thumbs/1469442213_17_imag1576-0x0px-160x160size.jpg', 'thumbs/1469442213_17_imag1576-0x0px-50x50size.jpg'),
(173, '1469559585_171.jpg', 'thumbs/1469559585_171-0x0px-640x640size.jpg', 'thumbs/1469559585_171-0x0px-320x320size.jpg'),
(129, '1469219904_imag1154.jpg', 'thumbs/1469219904_imag1154-0x0px-640x360size.jpg', 'thumbs/1469219904_imag1154-0x0px-320x180size.jpg'),
(219, '1470051338_images-2.jpg', 'thumbs/1470051338_images-2-0x0px-640x640size.jpg', 'thumbs/1470051338_images-2-0x0px-320x320size.jpg'),
(154, '', '', ''),
(155, '1469446595_41_images.jpg', 'thumbs/1469446595_41_images-0x0px-160x160size.jpg', 'thumbs/1469446595_41_images-0x0px-50x50size.jpg'),
(190, '1469773979_koala.jpg', 'thumbs/1469773979_koala-0x0px-640x640size.jpg', 'thumbs/1469773979_koala-0x0px-320x320size.jpg'),
(182, '1469645589_sukhoi-su-27.jpg', 'thumbs/1469645589_sukhoi-su-27-0x0px-640x640size.jpg', 'thumbs/1469645589_sukhoi-su-27-0x0px-320x320size.jpg'),
(270, '1470308865_4rocqiszqasrz5sc1xro_6306_ocuklar-ve-yuzme-sporu.jpg', 'thumbs/1470308865_4rocqiszqasrz5sc1xro_6306_ocuklar-ve-yuzme-sporu-0x0px-640x640size.jpg', 'thumbs/1470308865_4rocqiszqasrz5sc1xro_6306_ocuklar-ve-yuzme-sporu-0x0px-320x320size.jpg'),
(202, '1470034027_kordon3.jpg', 'thumbs/1470034027_kordon3-0x0px-640x640size.jpg', 'thumbs/1470034027_kordon3-0x0px-320x320size.jpg'),
(203, '1470034675_a7be6cfbcbf84e59c374f19cf320b180.jpg', 'thumbs/1470034675_a7be6cfbcbf84e59c374f19cf320b180-0x0px-640x640size.jpg', 'thumbs/1470034675_a7be6cfbcbf84e59c374f19cf320b180-0x0px-320x320size.jpg'),
(204, '1470035333_cankaya_tercume-e1432109020325-1024x624.jpg', 'thumbs/1470035333_cankaya_tercume-e1432109020325-1024x624-0x0px-640x640size.jpg', 'thumbs/1470035333_cankaya_tercume-e1432109020325-1024x624-0x0px-320x320size.jpg'),
(170, '1469555603_302.jpg', 'thumbs/1469555603_302-0x0px-640x640size.jpg', 'thumbs/1469555603_302-0x0px-320x320size.jpg'),
(200, '1469794718_images.jpg', 'thumbs/1469794718_images-0x0px-640x640size.jpg', 'thumbs/1469794718_images-0x0px-320x320size.jpg'),
(164, '', '', ''),
(165, '', '', ''),
(166, '1469531208_wallpapersxl-note-nature-yosemite-national-park-california-usa-picture-nr-with-195290-1280x720.jpg', 'thumbs/1469531208_wallpapersxl-note-nature-yosemite-national-park-california-usa-picture-nr-with-195290-1280x720-0x0px-640x360size.jpg', 'thumbs/1469531208_wallpapersxl-note-nature-yosemite-national-park-california-usa-picture-nr-with-195290-1280x720-0x0px-320x180size.jpg'),
(169, '1469553662_738.jpg', 'thumbs/1469553662_738-0x0px-640x360size.jpg', 'thumbs/1469553662_738-0x0px-320x180size.jpg'),
(171, '1469556042_289.jpg', 'thumbs/1469556042_289-0x0px-640x640size.jpg', 'thumbs/1469556042_289-0x0px-320x320size.jpg'),
(172, '1469556238_50.jpg', 'thumbs/1469556238_50-0x0px-640x640size.jpg', 'thumbs/1469556238_50-0x0px-320x320size.jpg'),
(174, '1469599046_609.jpg', 'thumbs/1469599046_609-0x0px-640x640size.jpg', 'thumbs/1469599046_609-0x0px-320x320size.jpg'),
(175, '1469601398_48_img_7189.jpg', 'thumbs/1469601398_48_img_7189-0x0px-160x160size.jpg', 'thumbs/1469601398_48_img_7189-0x0px-50x50size.jpg'),
(176, '1469602220_329.jpg', 'thumbs/1469602220_329-0x0px-640x640size.jpg', 'thumbs/1469602220_329-0x0px-320x320size.jpg'),
(177, '1469603429_44_12015025_670330099770487_8707627266058150352_o.jpg', 'thumbs/1469603429_44_12015025_670330099770487_8707627266058150352_o-0x0px-160x160size.jpg', 'thumbs/1469603429_44_12015025_670330099770487_8707627266058150352_o-0x0px-50x50size.jpg'),
(262, '1470145036_tulips.jpg', 'thumbs/1470145036_tulips-0x0px-640x640size.jpg', 'thumbs/1470145036_tulips-0x0px-320x320size.jpg'),
(208, '1470039930_images.png', 'thumbs/1470039930_images-0x0px-640x640size.png', 'thumbs/1470039930_images-0x0px-320x320size.png'),
(183, '1469645591_swag1.jpg', 'thumbs/1469645591_swag1-0x0px-640x640size.jpg', 'thumbs/1469645591_swag1-0x0px-320x320size.jpg'),
(205, '1470036076_images.jpg', 'thumbs/1470036076_images-0x0px-640x640size.jpg', 'thumbs/1470036076_images-0x0px-320x320size.jpg'),
(189, '1469705032_urfa.jpg', 'thumbs/1469705032_urfa-0x0px-640x640size.jpg', 'thumbs/1469705032_urfa-0x0px-320x320size.jpg'),
(206, '1470038784_indir.jpg', 'thumbs/1470038784_indir-0x0px-640x640size.jpg', 'thumbs/1470038784_indir-0x0px-320x320size.jpg'),
(207, '1470039384_images-1.jpg', 'thumbs/1470039384_images-1-0x0px-640x640size.jpg', 'thumbs/1470039384_images-1-0x0px-320x320size.jpg'),
(217, '1470048502_18_p_20160110_220814_bf.jpg', 'thumbs/1470048502_18_p_20160110_220814_bf-0x0px-160x160size.jpg', 'thumbs/1470048502_18_p_20160110_220814_bf-0x0px-50x50size.jpg'),
(220, '1470052080_188.jpg', 'thumbs/1470052080_188-0x0px-640x640size.jpg', 'thumbs/1470052080_188-0x0px-320x320size.jpg'),
(221, '1470052134_sezen-aksu-s4rutsrxgd-0.jpg', 'thumbs/1470052134_sezen-aksu-s4rutsrxgd-0-0x0px-640x640size.jpg', 'thumbs/1470052134_sezen-aksu-s4rutsrxgd-0-0x0px-320x320size.jpg'),
(212, '1470040518_134042.jpg', 'thumbs/1470040518_134042-0x0px-640x640size.jpg', 'thumbs/1470040518_134042-0x0px-320x320size.jpg'),
(213, '1470041142_indir-1.jpg', 'thumbs/1470041142_indir-1-0x0px-640x640size.jpg', 'thumbs/1470041142_indir-1-0x0px-320x320size.jpg'),
(222, '1470052740_639.jpg', 'thumbs/1470052740_639-0x0px-640x640size.jpg', 'thumbs/1470052740_639-0x0px-320x320size.jpg'),
(223, '1470053012_356.jpg', 'thumbs/1470053012_356-0x0px-640x640size.jpg', 'thumbs/1470053012_356-0x0px-320x320size.jpg'),
(224, '1470053606_505.jpg', 'thumbs/1470053606_505-0x0px-640x640size.jpg', 'thumbs/1470053606_505-0x0px-320x320size.jpg'),
(225, '1470054201_394.jpg', 'thumbs/1470054201_394-0x0px-640x640size.jpg', 'thumbs/1470054201_394-0x0px-320x320size.jpg'),
(226, '1470056806_kedi-kopek-evcil-hayvan-bakicisi.jpg', 'thumbs/1470056806_kedi-kopek-evcil-hayvan-bakicisi-0x0px-640x640size.jpg', 'thumbs/1470056806_kedi-kopek-evcil-hayvan-bakicisi-0x0px-320x320size.jpg'),
(227, '1470057224_anitkabir-slayt1.jpg', 'thumbs/1470057224_anitkabir-slayt1-0x0px-640x640size.jpg', 'thumbs/1470057224_anitkabir-slayt1-0x0px-320x320size.jpg'),
(228, '1470057538_dsc01768.jpg', 'thumbs/1470057538_dsc01768-0x0px-640x640size.jpg', 'thumbs/1470057538_dsc01768-0x0px-320x320size.jpg'),
(229, '1470058110_moda.jpg', 'thumbs/1470058110_moda-0x0px-640x640size.jpg', 'thumbs/1470058110_moda-0x0px-320x320size.jpg'),
(230, '1470058678_83_img-20160317-wa0015.jpg', 'thumbs/1470058678_83_img-20160317-wa0015-0x0px-160x160size.jpg', 'thumbs/1470058678_83_img-20160317-wa0015-0x0px-50x50size.jpg'),
(231, '', '', ''),
(232, '', '', ''),
(233, '1470058784_66_erkekprofilresimleri14.jpg', 'thumbs/1470058784_66_erkekprofilresimleri14-0x0px-160x160size.jpg', 'thumbs/1470058784_66_erkekprofilresimleri14-0x0px-50x50size.jpg'),
(234, '1470059369_img_0588.png', 'thumbs/1470059369_img_0588-0x0px-640x640size.png', 'thumbs/1470059369_img_0588-0x0px-320x320size.png'),
(235, '1470059557_moda.jpg', 'thumbs/1470059557_moda-0x0px-640x640size.jpg', 'thumbs/1470059557_moda-0x0px-320x320size.jpg'),
(236, '10154374505891204.jpg', 'thumbs/10154374505891204-0x0px-160x160size.jpg', 'thumbs/10154374505891204-0x0px-50x50size.jpg'),
(237, '10153979822827858.jpg', 'thumbs/10153979822827858-0x0px-160x160size.jpg', 'thumbs/10153979822827858-0x0px-50x50size.jpg'),
(238, '1470083609_24035854_cd91.jpg', 'thumbs/1470083609_24035854_cd91-0x0px-640x640size.jpg', 'thumbs/1470083609_24035854_cd91-0x0px-320x320size.jpg'),
(239, '10208585679393470.jpg', 'thumbs/10208585679393470-0x0px-160x160size.jpg', 'thumbs/10208585679393470-0x0px-50x50size.jpg'),
(240, '1470119659_komsu_hakki2-702x336.jpg', 'thumbs/1470119659_komsu_hakki2-702x336-0x0px-640x640size.jpg', 'thumbs/1470119659_komsu_hakki2-702x336-0x0px-320x320size.jpg'),
(241, '1077236052330005.jpg', 'thumbs/1077236052330005-0x0px-160x160size.jpg', 'thumbs/1077236052330005-0x0px-50x50size.jpg'),
(242, '1470120450_tavsan.jpg', 'thumbs/1470120450_tavsan-0x0px-640x640size.jpg', 'thumbs/1470120450_tavsan-0x0px-320x320size.jpg'),
(243, '1470120456_35097.jpg', 'thumbs/1470120456_35097-0x0px-640x640size.jpg', 'thumbs/1470120456_35097-0x0px-320x320size.jpg'),
(244, '1470122148_ankara-alisveris-festivali-nde-moda-haftasi-6441609_1770_o.jpg', 'thumbs/1470122148_ankara-alisveris-festivali-nde-moda-haftasi-6441609_1770_o-0x0px-640x640size.jpg', 'thumbs/1470122148_ankara-alisveris-festivali-nde-moda-haftasi-6441609_1770_o-0x0px-320x320size.jpg'),
(245, '1470126169_z_64f23ab3_large.jpg', 'thumbs/1470126169_z_64f23ab3_large-0x0px-640x640size.jpg', 'thumbs/1470126169_z_64f23ab3_large-0x0px-320x320size.jpg'),
(246, '1470128482_22.jpg', 'thumbs/1470128482_22-0x0px-640x640size.jpg', 'thumbs/1470128482_22-0x0px-320x320size.jpg'),
(247, '1470129112_438.jpg', 'thumbs/1470129112_438-0x0px-640x640size.jpg', 'thumbs/1470129112_438-0x0px-320x320size.jpg'),
(248, '1470134522_442.jpg', 'thumbs/1470134522_442-0x0px-640x640size.jpg', 'thumbs/1470134522_442-0x0px-320x320size.jpg'),
(249, '1470134712_727.jpg', 'thumbs/1470134712_727-0x0px-640x640size.jpg', 'thumbs/1470134712_727-0x0px-320x320size.jpg'),
(250, '1470135655_tulips.jpg', 'thumbs/1470135655_tulips-0x0px-640x640size.jpg', 'thumbs/1470135655_tulips-0x0px-320x320size.jpg'),
(251, '1470135692_hydrangeas.jpg', 'thumbs/1470135692_hydrangeas-0x0px-640x640size.jpg', 'thumbs/1470135692_hydrangeas-0x0px-320x320size.jpg'),
(252, '1470136475_tulips.jpg', 'thumbs/1470136475_tulips-0x0px-640x640size.jpg', 'thumbs/1470136475_tulips-0x0px-320x320size.jpg'),
(253, '1470136693_551.jpg', 'thumbs/1470136693_551-0x0px-640x640size.jpg', 'thumbs/1470136693_551-0x0px-320x320size.jpg'),
(254, '1470136703_41.jpg', 'thumbs/1470136703_41-0x0px-640x640size.jpg', 'thumbs/1470136703_41-0x0px-320x320size.jpg'),
(255, '1470137167_401.jpg', 'thumbs/1470137167_401-0x0px-640x640size.jpg', 'thumbs/1470137167_401-0x0px-320x320size.jpg'),
(256, '1470137494_227.jpg', 'thumbs/1470137494_227-0x0px-640x640size.jpg', 'thumbs/1470137494_227-0x0px-320x320size.jpg'),
(257, '1470138306_132.jpg', 'thumbs/1470138306_132-0x0px-640x640size.jpg', 'thumbs/1470138306_132-0x0px-320x320size.jpg'),
(269, '10205140637435160.jpg', 'thumbs/10205140637435160-0x0px-160x160size.jpg', 'thumbs/10205140637435160-0x0px-50x50size.jpg'),
(299, '1470649364_3552-bursaspor--12000.jpg', 'thumbs/1470649364_3552-bursaspor--12000-0x0px-640x640size.jpg', 'thumbs/1470649364_3552-bursaspor--12000-0x0px-320x320size.jpg'),
(261, '10205140637435160.jpg', 'thumbs/10205140637435160-0x0px-160x160size.jpg', 'thumbs/10205140637435160-0x0px-50x50size.jpg'),
(265, '1470145141_kosan.jpg', 'thumbs/1470145141_kosan-0x0px-640x640size.jpg', 'thumbs/1470145141_kosan-0x0px-320x320size.jpg'),
(271, '1470384077_adsiz.png', 'thumbs/1470384077_adsiz-0x0px-640x640size.png', 'thumbs/1470384077_adsiz-0x0px-320x320size.png'),
(301, '1470649365_burasi-turkiye-ofis-turkiye-1361370.jpg', 'thumbs/1470649365_burasi-turkiye-ofis-turkiye-1361370-0x0px-640x640size.jpg', 'thumbs/1470649365_burasi-turkiye-ofis-turkiye-1361370-0x0px-320x320size.jpg'),
(274, '1470385408_59_20131217_001324.jpg', 'thumbs/1470385408_59_20131217_001324-0x0px-160x160size.jpg', 'thumbs/1470385408_59_20131217_001324-0x0px-50x50size.jpg'),
(275, '1470385653_4_20131217_001324.jpg', 'thumbs/1470385653_4_20131217_001324-0x0px-160x160size.jpg', 'thumbs/1470385653_4_20131217_001324-0x0px-50x50size.jpg'),
(276, '1470385721_37_10704_410963789111074_4742037806884133949_n.jpg', 'thumbs/1470385721_37_10704_410963789111074_4742037806884133949_n-0x0px-160x160size.jpg', 'thumbs/1470385721_37_10704_410963789111074_4742037806884133949_n-0x0px-50x50size.jpg'),
(277, '1470392353_7_10923191_10203876347645390_5285820429700618087_n.jpg', 'thumbs/1470392353_7_10923191_10203876347645390_5285820429700618087_n-0x0px-160x160size.jpg', 'thumbs/1470392353_7_10923191_10203876347645390_5285820429700618087_n-0x0px-50x50size.jpg'),
(278, '1470392807_imagesr9sfsh83.jpg', 'thumbs/1470392807_imagesr9sfsh83-0x0px-640x640size.jpg', 'thumbs/1470392807_imagesr9sfsh83-0x0px-320x320size.jpg'),
(279, '1470392808_girl-1112647_960_720.jpg', 'thumbs/1470392808_girl-1112647_960_720-0x0px-640x640size.jpg', 'thumbs/1470392808_girl-1112647_960_720-0x0px-320x320size.jpg'),
(280, '1470392820_34482.jpg', 'thumbs/1470392820_34482-0x0px-640x640size.jpg', 'thumbs/1470392820_34482-0x0px-320x320size.jpg'),
(281, '1470392825_girl-546688_640.jpg', 'thumbs/1470392825_girl-546688_640-0x0px-640x640size.jpg', 'thumbs/1470392825_girl-546688_640-0x0px-320x320size.jpg'),
(302, '1470649365_ilham-veren-ofis.jpg', 'thumbs/1470649365_ilham-veren-ofis-0x0px-640x640size.jpg', 'thumbs/1470649365_ilham-veren-ofis-0x0px-320x320size.jpg'),
(292, '1470572351_tticaretnet3.jpg', 'thumbs/1470572351_tticaretnet3-0x0px-640x640size.jpg', 'thumbs/1470572351_tticaretnet3-0x0px-320x320size.jpg'),
(290, '1470572349_tticaretnet2.jpg', 'thumbs/1470572349_tticaretnet2-0x0px-640x640size.jpg', 'thumbs/1470572349_tticaretnet2-0x0px-320x320size.jpg'),
(297, '1470641939_afaaaa_1.jpg', 'thumbs/1470641939_afaaaa_1-0x0px-640x640size.jpg', 'thumbs/1470641939_afaaaa_1-0x0px-320x320size.jpg'),
(300, '1470649364_afaaaa_1.jpg', 'thumbs/1470649364_afaaaa_1-0x0px-640x640size.jpg', 'thumbs/1470649364_afaaaa_1-0x0px-320x320size.jpg'),
(303, '1470649366_sv1pukaesmw_1_1200x720.jpg', 'thumbs/1470649366_sv1pukaesmw_1_1200x720-0x0px-640x640size.jpg', 'thumbs/1470649366_sv1pukaesmw_1_1200x720-0x0px-320x320size.jpg'),
(304, '1470945253_img-20160811-wa0005.jpg', 'thumbs/1470945253_img-20160811-wa0005-0x0px-640x640size.jpg', 'thumbs/1470945253_img-20160811-wa0005-0x0px-320x320size.jpg'),
(305, '10209078284421729.jpg', 'thumbs/10209078284421729-0x0px-160x160size.jpg', 'thumbs/10209078284421729-0x0px-50x50size.jpg'),
(306, '10154374505891204.jpg', 'thumbs/10154374505891204-0x0px-160x160size.jpg', 'thumbs/10154374505891204-0x0px-50x50size.jpg'),
(307, '1474495559_umesaj28846_0ka5bdbnclca.jpg', 'thumbs/1474495559_umesaj28846_0ka5bdbnclca-0x0px-640x640size.jpg', 'thumbs/1474495559_umesaj28846_0ka5bdbnclca-0x0px-320x320size.jpg'),
(308, '1474495559_diagram_1.jpg', 'thumbs/1474495559_diagram_1-0x0px-640x640size.jpg', 'thumbs/1474495559_diagram_1-0x0px-320x320size.jpg'),
(309, '1474495560_umesaj28846_zcpq9fcvx6.png', 'thumbs/1474495560_umesaj28846_zcpq9fcvx6-0x0px-640x640size.png', 'thumbs/1474495560_umesaj28846_zcpq9fcvx6-0x0px-320x320size.png'),
(310, '1474496099_umesaj28846_0ka5bdbnclca.jpg', 'thumbs/1474496099_umesaj28846_0ka5bdbnclca-0x0px-640x640size.jpg', 'thumbs/1474496099_umesaj28846_0ka5bdbnclca-0x0px-320x320size.jpg');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kategoriler`
--

CREATE TABLE IF NOT EXISTS `kategoriler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adi` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `onay` tinyint(1) NOT NULL,
  `seo_link` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `kim` int(11) NOT NULL,
  `icon` varchar(233) COLLATE utf8_turkish_ci NOT NULL,
  `sirasi` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=13 ;

--
-- Tablo döküm verisi `kategoriler`
--

INSERT INTO `kategoriler` (`id`, `adi`, `onay`, `seo_link`, `kim`, `icon`, `sirasi`) VALUES
(1, 'Gezi', 1, 'gezi', 0, 'fa fa-car', 1),
(2, 'Spor', 1, 'spor', 0, 'fa fa-futbol-o', 2),
(3, 'Sohbet', 1, 'sohbet', 0, 'fa fa-comments-o', 3),
(4, 'Oyun', 1, 'oyun', 0, 'fa fa-gamepad', 4),
(5, 'Yüzme', 1, 'yuzme', 0, 'fa fa-life-ring', 5),
(6, 'Kitap Hobi', 1, 'kitap-hobi', 0, 'fa fa-book', 6),
(7, 'Konser', 1, 'konsert', 0, 'fa fa-music', 7),
(8, 'Genel', 1, 'genel', 0, 'fa fa-globe', 8),
(9, 'Moda', 0, 'moda', 0, 'fa fa-asterisk', 9),
(10, 'Alışveriş', 1, 'alisveris', 0, 'fa fa-shopping-bag', 10),
(11, 'Evcil Hayvanlar', 1, 'evcil-hayvanlar', 0, 'fa fa-paw', 11),
(12, 'İşler Güçler', 1, 'isler-gucler', 0, 'fa fa-at', 12);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanici`
--

CREATE TABLE IF NOT EXISTS `kullanici` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adi` varchar(20) COLLATE utf8_turkish_ci NOT NULL,
  `soyadi` varchar(15) COLLATE utf8_turkish_ci NOT NULL,
  `kadi` varchar(15) COLLATE utf8_turkish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `sifre` varchar(70) COLLATE utf8_turkish_ci NOT NULL,
  `onay` tinyint(1) NOT NULL,
  `yetki` tinyint(4) NOT NULL,
  `resim` int(11) NOT NULL,
  `hakkinda` text COLLATE utf8_turkish_ci NOT NULL,
  `tel` varchar(15) COLLATE utf8_turkish_ci NOT NULL,
  `cinsiyet` tinyint(1) NOT NULL,
  `dtarihi` date NOT NULL,
  `tip` tinyint(1) NOT NULL,
  `ktarihi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fbid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=191 ;

--
-- Tablo döküm verisi `kullanici`
--

INSERT INTO `kullanici` (`id`, `adi`, `soyadi`, `kadi`, `email`, `sifre`, `onay`, `yetki`, `resim`, `hakkinda`, `tel`, `cinsiyet`, `dtarihi`, `tip`, `ktarihi`, `fbid`) VALUES
(137, 'Metin ', 'Ağaoğlu', 'metinaga', 'iletisim@metinagaoglu.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 239, '', '', 0, '1970-01-01', 0, '2016-08-02 06:14:09', 10208585679393470),
(97, 'Hüseyin', 'Babuç', 'hbabuc', 'hbabuc@hotmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 1, 175, 'AktiviteTurkiye.com kurucularından\r\n\r\n', '053569369378', 1, '0000-00-00', 0, '2016-07-27 08:27:05', 0),
(136, 'muhammed', 'tahir', 'tahir', 'gouphath@gmail.com', '9e038864caae334fbd190b04a0fbfda4be801fbd', 1, 0, 0, '', '', 0, '0000-00-00', 0, '2016-08-01 20:13:41', 0),
(130, 'Derman', 'imrek', 'derman', 'imrek_yasemen@gmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 277, 'Haran Üniversitesi  Veteriner Fakültesi mezunu 2010', '0538561245', 0, '0000-00-00', 0, '2016-08-05 10:21:22', 0),
(148, 'Burçin', 'Baloglu', 'Burçin', 'baloglu@hotmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 0, '', '', 0, '0000-00-00', 0, '2016-08-05 10:29:42', 0),
(131, 'Ayda', 'kesmez', 'Ayda', 'aydakesmez20@hotmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 230, '', '', 0, '0000-00-00', 0, '2016-08-01 13:37:58', 0),
(132, 'Berfin', 'imrek', 'Berfin', 'imrek_berfin@hotmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 0, '', '', 0, '0000-00-00', 0, '2016-08-01 13:38:04', 0),
(133, 'Ömer', 'Yılmaz', 'omer', 'asd@asd.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 233, '', '', 0, '0000-00-00', 0, '2016-08-01 13:39:44', 0),
(135, 'Serdar ', 'Doğan', '', 'serdar.dogaan@gmail.com', '', 1, 0, 237, '', '', 0, '1970-01-01', 0, '2016-08-01 20:01:29', 10153979822827858),
(124, 'yasemin', 'imrek', 'yasmin', 'imrek_yasmin1@hotmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 1, 275, 'Doğu Akdeniz Üniversitesi Bilgisayar Mühendisliği 2016 mezunu\r\nKadir Has Üniversitesi Bilgisayar Programlama 2010 mezunu\r\n\r\n\r\n&amp;quot;Hayat&amp;quot;:çeşitliliği çok,müthiş bir sahne.Sahne de olmak ise harika.....', '0533 879 56 41', 0, '0000-00-00', 0, '2016-08-05 08:40:45', 0),
(127, 'sibel', 'şahin', 'sibel', 'sbl.sahn@hotmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 1, 276, '', '05342565521', 0, '0000-00-00', 0, '2016-08-09 13:52:13', 0),
(126, 'Metin', 'Ağaoğlu', 'agaoglu', 'metnagaoglu@gmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 127, 'Türkticaret.Net te Stajer', '', 1, '0000-00-00', 0, '2016-08-07 11:22:07', 0),
(128, 'fatih', 'Ağaoğlu', 'fatih', 'fatih@fatih.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 153, 'fatih işte', '', 1, '0000-00-00', 0, '2016-07-27 06:40:20', 0),
(138, 'Bawer ', 'Imrek', 'irmek', 'imrek_yasmin2@hotmail.com', '', 1, 0, 281, '', '', 0, '1970-01-01', 0, '2016-08-05 10:30:38', 1077236052330005),
(149, 'samet', 'Şirinel', 'unname1', 'bakbakalim123@gmail.com', 'cd70b4f6df31fbace7374051c2a34d92783dc548', 1, 0, 0, '', '', 0, '0000-00-00', 0, '2016-08-05 10:29:38', 0),
(150, 'Burcin', 'Bal', 'burcin', 'imrek_yasmin@hotmail.com', '63982e54a7aeb0d89910475ba6dbd3ca6dd4e5a1', 1, 0, 280, '', '', 0, '0000-00-00', 0, '2016-08-05 10:28:44', 0),
(146, 'Polat', 'YALDIZ', 'compistiyer', 'salamanyak21@gmail.com', '5ddbcbfd7b35da5fea7f44bbc8b6c38656aece5f', 1, 0, 0, '', '', 0, '0000-00-00', 0, '2016-08-02 12:21:37', 0),
(151, 'Hüseyin ', 'Babuc', 'babic', 'hbabuc@hotmail.com', '', 1, 0, 269, '', '', 0, '1970-01-01', 0, '2016-08-05 10:30:44', 10205140637435160),
(154, 'Bekir Samet ', 'Şirinel', 'unname', 'sametsirinel@gmail.com', 'cd70b4f6df31fbace7374051c2a34d92783dc548', 1, 0, 306, '', '', 0, '1992-11-12', 0, '2016-09-20 18:22:02', 10154374505891204),
(153, 'Emre ', 'Seymen', 'deathswords', '', 'ef4f06501025021ce41f9e3f5aedaa61bce46e34', 1, 0, 305, '', '', 0, '1970-01-01', 0, '2016-08-29 14:55:10', 10209078284421729),
(152, 'Gizem', 'Li', 'gizemli16', 'gizemli@gizem.li', '1f82ea75c5cc526729e2d581aeb3aeccfef4407e', 0, 0, 0, '', '', 0, '0000-00-00', 0, '2016-08-11 18:42:58', 0);

--
-- Tetikleyiciler `kullanici`
--
DROP TRIGGER IF EXISTS `kullanici_sil`;
DELIMITER //
CREATE TRIGGER `kullanici_sil` AFTER DELETE ON `kullanici`
 FOR EACH ROW BEGIN

    delete from kullanicitakip where kullanicitakip.userid = old.id or kullanicitakip.takipciid = old.id;
    delete from kullanici_sosyal where kullanici_sosyal.userid = old.id;
	delete from aktivite where aktivite.userid = old.id;
	delete from oylama where oylama.userid = old.id;
	delete from sikayet where sikayet.userid = old.id;
	delete from yorum_begeni where yorum_begeni.userid = old.id;
	delete from arkadaslar where arkadaslar.userid = old.id or arkadaslar.gonderenid =old.id;
	delete from bildirim where bildirim.userid = old.id;

END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanicitakip`
--

CREATE TABLE IF NOT EXISTS `kullanicitakip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `takipciid` int(11) NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=18 ;

--
-- Tablo döküm verisi `kullanicitakip`
--

INSERT INTO `kullanicitakip` (`id`, `userid`, `takipciid`, `tarih`) VALUES
(1, 126, 124, '2016-08-03 10:20:29'),
(2, 127, 124, '2016-08-04 11:10:59'),
(3, 97, 124, '2016-08-04 11:11:16'),
(7, 130, 127, '2016-08-05 08:37:33'),
(8, 133, 127, '2016-08-05 08:37:55'),
(9, 133, 124, '2016-08-05 08:41:53'),
(10, 137, 127, '2016-08-05 08:45:03'),
(12, 150, 127, '2016-08-05 10:29:05'),
(13, 127, 97, '2016-08-05 10:32:27'),
(14, 97, 127, '2016-08-05 10:34:24'),
(15, 127, 126, '2016-08-05 10:34:42'),
(17, 154, 127, '2016-09-21 22:58:47');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanici_sosyal`
--

CREATE TABLE IF NOT EXISTS `kullanici_sosyal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sosyalid` int(11) NOT NULL,
  `link` varchar(70) COLLATE utf8_turkish_ci NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=21 ;

--
-- Tablo döküm verisi `kullanici_sosyal`
--

INSERT INTO `kullanici_sosyal` (`id`, `sosyalid`, `link`, `userid`) VALUES
(20, 1, 'http://www.facebook.com/', 154),
(17, 2, 'http://twitter.com/jasmin63', 124),
(18, 1, 'http://www.facebook.com/', 130),
(19, 1, 'http://www.facebook.com/profile.php?id=1653766619', 126);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `oylama`
--

CREATE TABLE IF NOT EXISTS `oylama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aktiviteid` int(11) NOT NULL,
  `tip` tinyint(1) NOT NULL,
  `derece` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `yorum` text COLLATE utf8_turkish_ci NOT NULL,
  `onay` int(11) NOT NULL,
  `zaman` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=83 ;

--
-- Tablo döküm verisi `oylama`
--

INSERT INTO `oylama` (`id`, `aktiviteid`, `tip`, `derece`, `userid`, `yorum`, `onay`, `zaman`) VALUES
(56, 60, 0, 3, 126, 'Fena Değil', 1, '2016-07-25 06:54:28'),
(57, 64, 0, 0, 128, 'Bu ne biçim resim', 1, '2016-07-25 10:22:48'),
(63, 191, 0, 0, 130, 'Şanlıurfa dediğinizde her taşın altından bir efsane çıkar. Dünya üzerinde ortalıkta bu kadar çok efsane dolaşan ikinci bir yer var mıdır bilinmez. Efsanelerin gerçek olup olmadığını belki kanıtlayamayiz ama bu onların yüzyıllardır halk arasında Dilden dile aktarildigi gerçeğini değiştirmez.....', 1, '2016-07-28 08:41:00'),
(62, 183, 0, 0, 126, 'test', 1, '2016-07-28 06:15:01'),
(61, 79, 0, 9, 97, 'wtf', 1, '2016-07-27 07:04:20'),
(60, 58, 0, 0, 97, 'gergregerg', 1, '2016-07-27 06:41:54'),
(59, 58, 0, 0, 97, 'of ulan ne köfteydi be', 1, '2016-07-27 06:41:44'),
(58, 58, 0, 0, 126, 'Sahibi iznikli olmasa daha güzel olurdu', 1, '2016-07-25 12:12:24'),
(55, 58, 0, 10, 97, '10 numara köfteleri var yiyin gari', 1, '2016-07-22 13:48:31'),
(54, 11, 0, 9, 97, 'dgdds', 1, '2016-07-21 07:20:17'),
(64, 198, 0, 9, 97, 'wtf', 1, '2016-08-01 06:47:33'),
(81, 276, 0, 2, 124, 'Böyle güzel bir etkinlikte sizlerle birlikte olmak bizi çok mutlu etti.', 1, '2016-08-04 11:20:28'),
(76, 266, 0, 0, 127, 'Zaten katıldığım için yorum yapabiliyorum', 1, '2016-08-02 13:39:26'),
(74, 263, 0, 8, 97, 'Fkfkfkkfkf', 1, '2016-08-02 11:56:23'),
(78, 266, 0, 10, 97, 'Djjdjdjd', 1, '2016-08-02 13:48:55');

--
-- Tetikleyiciler `oylama`
--
DROP TRIGGER IF EXISTS `oylama_sil`;
DELIMITER //
CREATE TRIGGER `oylama_sil` AFTER DELETE ON `oylama`
 FOR EACH ROW delete from yorum_begeni where yorum_begeni.yorumid = old.id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `radiolar`
--

CREATE TABLE IF NOT EXISTS `radiolar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adi` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `type` int(11) NOT NULL,
  `degeri` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=11 ;

--
-- Tablo döküm verisi `radiolar`
--

INSERT INTO `radiolar` (`id`, `adi`, `type`, `degeri`) VALUES
(1, 'Takip İsteği', 2, 1),
(2, 'Arkadaşlık İsteği', 2, 2),
(3, 'Arkadaş Davetiyesi', 2, 3),
(4, 'Arkadaş olundu', 2, 4),
(5, 'Arkadaşlık Reddedildi', 2, 5),
(6, 'Aktiviteden Çıkarıldın', 2, 6),
(7, 'Davetiye İptali', 2, 7),
(8, 'Rezervasyon', 3, 1),
(9, 'Katılma', 3, 2),
(10, 'Davetiyesi Var', 3, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sikayet`
--

CREATE TABLE IF NOT EXISTS `sikayet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aktiviteid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `aciklama` text COLLATE utf8_turkish_ci NOT NULL,
  `tip` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=20 ;

--
-- Tablo döküm verisi `sikayet`
--

INSERT INTO `sikayet` (`id`, `aktiviteid`, `userid`, `aciklama`, `tip`) VALUES
(1, 11, 97, 'fuckthesystem', 0),
(2, 11, 97, 'deneme', 0),
(3, 11, 97, 'denemeergre', 0),
(4, 11, 97, 'eggeergerger', 3),
(5, 11, 97, 'fbhrr', 3),
(6, 11, 97, '&amp;lt;script&amp;gt;alert(&amp;quot;adasd&amp;quot;);&amp;lt;/script&amp;gt;', 2),
(7, 30, 124, 'Konum kısmınız çalışmadığı için mallesef  beni kaybetiniz :)', 3),
(17, 266, 97, 'fuck you', 2);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sosyalaglar`
--

CREATE TABLE IF NOT EXISTS `sosyalaglar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(20) COLLATE utf8_turkish_ci NOT NULL,
  `ismi` varchar(20) COLLATE utf8_turkish_ci NOT NULL,
  `link` varchar(70) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=3 ;

--
-- Tablo döküm verisi `sosyalaglar`
--

INSERT INTO `sosyalaglar` (`id`, `icon`, `ismi`, `link`) VALUES
(1, 'fa-facebook', 'Facebook', 'http://www.facebook.com/'),
(2, 'fa-twitter', 'Twitter', 'http://twitter.com/');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yaslar`
--

CREATE TABLE IF NOT EXISTS `yaslar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `baslangic` int(11) NOT NULL,
  `bitis` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=6 ;

--
-- Tablo döküm verisi `yaslar`
--

INSERT INTO `yaslar` (`id`, `baslangic`, `bitis`) VALUES
(1, 12, 18),
(2, 18, 25),
(3, 25, 45),
(4, 30, 50),
(5, 7, 70);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yetki`
--

CREATE TABLE IF NOT EXISTS `yetki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yetkiadi` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=12 ;

--
-- Tablo döküm verisi `yetki`
--

INSERT INTO `yetki` (`id`, `yetkiadi`) VALUES
(1, 'Yazılımcı'),
(11, 'Admin');

--
-- Tetikleyiciler `yetki`
--
DROP TRIGGER IF EXISTS `yetkilimi`;
DELIMITER //
CREATE TRIGGER `yetkilimi` AFTER DELETE ON `yetki`
 FOR EACH ROW delete from yetkilimi where yetkilimi.yetkilimi = old.id
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yetkilimi`
--

CREATE TABLE IF NOT EXISTS `yetkilimi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yetkiid` int(11) NOT NULL,
  `alanid` int(11) NOT NULL,
  `alanyetkiid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=2043 ;

--
-- Tablo döküm verisi `yetkilimi`
--

INSERT INTO `yetkilimi` (`id`, `yetkiid`, `alanid`, `alanyetkiid`) VALUES
(1566, 11, 13, 5),
(1565, 11, 13, 4),
(1564, 11, 13, 3),
(1563, 11, 13, 2),
(1562, 11, 13, 1),
(1561, 11, 12, 4),
(1560, 11, 12, 3),
(1559, 11, 12, 2),
(1558, 11, 12, 1),
(1557, 11, 11, 5),
(1556, 11, 11, 4),
(1555, 11, 11, 3),
(1554, 11, 11, 2),
(1553, 11, 11, 1),
(1552, 11, 8, 5),
(1551, 11, 8, 4),
(1550, 11, 8, 3),
(1549, 11, 8, 2),
(1548, 11, 8, 1),
(1547, 11, 7, 4),
(1546, 11, 7, 2),
(1545, 11, 7, 1),
(1544, 11, 2, 5),
(1543, 11, 2, 3),
(1542, 11, 2, 2),
(1541, 11, 2, 1),
(1540, 11, 1, 4),
(1539, 11, 1, 3),
(1538, 11, 1, 2),
(1537, 11, 1, 1),
(2041, 1, 17, 4),
(2040, 1, 17, 3),
(2039, 1, 17, 2),
(2038, 1, 17, 1),
(2037, 1, 16, 5),
(2036, 1, 16, 4),
(2035, 1, 16, 3),
(2034, 1, 16, 2),
(2033, 1, 16, 1),
(2032, 1, 15, 5),
(2031, 1, 15, 4),
(2030, 1, 15, 2),
(2029, 1, 15, 1),
(2028, 1, 14, 4),
(2027, 1, 14, 1),
(2026, 1, 11, 5),
(2025, 1, 11, 4),
(2024, 1, 11, 3),
(2023, 1, 11, 2),
(2022, 1, 11, 1),
(2021, 1, 8, 5),
(2020, 1, 8, 4),
(2019, 1, 8, 3),
(2018, 1, 8, 2),
(2017, 1, 8, 1),
(2016, 1, 7, 4),
(2015, 1, 7, 2),
(2014, 1, 7, 1),
(2013, 1, 6, 4),
(2012, 1, 6, 2),
(2011, 1, 6, 1),
(2010, 1, 3, 4),
(2009, 1, 3, 2),
(2008, 1, 3, 1),
(1567, 11, 14, 1),
(1568, 11, 14, 4),
(2007, 1, 2, 5),
(2006, 1, 2, 4),
(2005, 1, 2, 3),
(2004, 1, 2, 2),
(2003, 1, 2, 1),
(2002, 1, 1, 4),
(2001, 1, 1, 3),
(2000, 1, 1, 2),
(1999, 1, 1, 1),
(2042, 1, 17, 5);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yorum_begeni`
--

CREATE TABLE IF NOT EXISTS `yorum_begeni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tur` int(11) NOT NULL,
  `yorumid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `zaman` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=8 ;

--
-- Tablo döküm verisi `yorum_begeni`
--

INSERT INTO `yorum_begeni` (`id`, `tur`, `yorumid`, `userid`, `zaman`) VALUES
(5, 1, 78, 97, '2016-08-04 10:17:33'),
(2, 0, 76, 127, '2016-08-02 13:39:31'),
(3, 1, 76, 97, '2016-08-02 13:40:05'),
(4, 0, 76, 146, '2016-08-02 13:40:29'),
(7, 1, 81, 124, '2016-08-04 11:20:35');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
